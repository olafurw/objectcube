# - Find CppUnit
# http://root.cern.ch/viewvc/trunk/cint/reflex/cmake/modules/FindCppUnit.cmake
#
# This module finds an installed CppUnit package.
#
# It sets the following variables:
#  CppUnit_FOUND       - Set to false, or undefined, if CppUnit isn't found.
#  CppUnit_INCLUDE_DIR - The CppUnit include directory.
#  CppUnit_LIBRARY     - The CppUnit library to link against.

find_path(CppUnit_INCLUDE_DIR cppunit/Test.h)
find_library(CppUnit_LIBRARY NAMES cppunit)

if (CppUnit_INCLUDE_DIR AND CppUnit_LIBRARY)
   set(CppUnit_FOUND TRUE)
endif (CppUnit_INCLUDE_DIR AND CppUnit_LIBRARY)

if (CppUnit_FOUND)

   # show which CppUnit was found only if not quiet
   if (NOT CppUnit_FIND_QUIETLY)
	  message(STATUS "Found CppUnit: ${CppUnit_LIBRARY}")
   endif (NOT CppUnit_FIND_QUIETLY)

else (CppUnit_FOUND)

   # fatal error if CppUnit is required but not found
   if (CppUnit_FIND_REQUIRED)
	  message(FATAL_ERROR "Could not find CppUnit")
   endif (CppUnit_FIND_REQUIRED)

endif (CppUnit_FOUND)
