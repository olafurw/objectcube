// ObjectCube Includes
#include "../ObjectCube/LayerShared/Exception.h"
#include "../ObjectCube/LayerShared/Parameters.h"
#include "../ObjectCube/LayerShared/DebugInfo.h"

#include "../ObjectCube/Hub.h"
#include "../ObjectCube/ObjectTag.h"
#include "../ObjectCube/Tag/Tag.h"
#include "../ObjectCube/TagSet/TagSet.h"
#include "../ObjectCube/TagSet/AlphanumericalTagSet.h"
#include "../ObjectCube/Filters/TagFilter.h"
#include "../ObjectCube/Filters/NumericalRangeFilter.h"

#include "../ObjectCube/State/State.h"

// Standard lib includes
#include <iostream>
#include <memory>
#include <sstream>

using namespace std;
using namespace ObjectCube;

int main( int argc, char * const argv[] )
{
	// Hub* hub=Hub::getHub();
	
	AlphanumericalTagSet* tagset = AlphanumericalTagSet("RULabs").create();
	PersistentDimension* labsDimension = tagset->createPersistentDimension( tagset->fetchOrAddTag("RULabs") );
	PersistentHierarchyNode* labsRoot = labsDimension->getPersistentRoot();
	PersistentHierarchyNode* node1 = labsDimension->addNode( labsRoot->getId(), tagset->fetchOrAddTag("Datalab") );
	PersistentHierarchyNode* node2 = labsDimension->addNode( labsRoot->getId(), tagset->fetchOrAddTag("Cadia") );
	PersistentHierarchyNode* node3 = labsDimension->addNode( labsRoot->getId(), tagset->fetchOrAddTag("Syslab") );

    return 0;
}
