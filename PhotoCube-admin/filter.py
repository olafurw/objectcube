from wx import *
#from browser.common import * 
import wx.calendar as cal


class FilterListPopup( wx.Menu ):
    
    def __init__(self, parent):
        wx.Menu.__init__(self)
        self.parent = parent
        
        item = wx.MenuItem(self, wx.NewId(),"Remove selected filters")
        self.AppendItem(item)
        self.Bind(wx.EVT_MENU, self.on_delete, item)
        
    def on_delete(self, event):
        self.parent.delete_selected()


class FilterList(ListCtrl):

    """
    ListCtrl that shows what filters are currently set.
    """

    def __init__(self, parent, id=-1):
        self.columns = ['ID', 'Type', 'tagset', 'Info']
        self.init_base(parent, id)
        self.bind_events()

    def init_base(self, parent, id):
        ListCtrl.__init__(self, parent, id, style=LC_REPORT)
        
        for col, text in enumerate(self.columns):
            self.InsertColumn(col, text)


    def load_data(self):
        for filter in serviceLayer.get_filters():
            tagset = serviceLayer.get_tagset_by_id( filter.tagSetId )
            
            data = [filter.id, filter.typeAsString(), tagset.name]
            
            if filter.typeId == filter.TYPE.NUMERICAL_RANGE_FILTER:
                string = 'From: ' + str(filter.numberFrom) + ' To: ' + str(filter.numberTo)
                data.append(string)
            
            if filter.typeId == filter.TYPE.DATE_RANGE_FILTER:
                string = 'from:' + str(filter.yearFrom) + '/' + str(filter.monthFrom) + '/' + str(filter.dayOfMonthFrom)
                string += ' to:' + str(filter.yearTo) + '/' + str(filter.monthTo) + '/' + str(filter.dayOfMonthTo)
                data.append(string)


            if filter.typeId == filter.TYPE.TIME_RANGE_FILTER:
                if filter.hoursFrom is not -1: string += str(filter.hoursFrom) + ":" 
                else: string += '0:'
                
                if filter.minutesFrom is not -1: string += str(filter.minutesFrom) + ":" 
                else: string += '0:'
                
                if filter.secondsFrom is not -1: string += str(filter.secondsFrom) + "." 
                else: string += '0.'
                
                if filter.millisecondsFrom is not -1: string += str(filter.millisecondsFrom) + " - " 
                else: string += '0 - '
                
                if filter.hoursTo is not -1: string += str(filter.hoursTo) + ":" 
                else: string += '0:'
                
                if filter.minutesTo is not -1: string += str(filter.minutesTo) + ":" 
                else: string += '0:'
                
                if filter.secondsTo is not -1: string += str(filter.secondsTo) + "." 
                else: string += '0.'
                
                if filter.millisecondsTo is not -1: string += str(filter.millisecondsTo) 
                else: string += '0'
                data.append(string)
            
            self.Append(data) 
        
        # Fix the column width.
        for i in range(len(self.columns)):
            self.SetColumnWidth(i, wx.LIST_AUTOSIZE)


    def reload_data(self):
        self.DeleteAllItems()
        self.load_data()


    def bind_events(self):
        self.Bind(wx.EVT_RIGHT_DOWN, self.on_right_down)        

    
    def on_right_down(self, event):
        self.PopupMenu(FilterListPopup(self))

    
    def delete_selected(self):
        index_list = self.get_selected_items()
        serviceLayer.delete_filters(index_list)
        self.reload_data()

    

    def get_selected_items(self):
        """
        Gets the selected items for the list control.
        Selection is returned as a list of selected indices,
        low to high.
        """
        selection = []

        # start at -1 to get the first selected item
        current = -1
        while True:
            next = self.GetNextSelected(current)
            if next == -1:
                return selection

            selection.append(next)
            current = next

    def GetNextSelected(self, current):
        """Returns next selected item, or -1 when no more"""

        return self.GetNextItem(current,
                                LIST_NEXT_ALL,
                                LIST_STATE_SELECTED)




class AddNewNumericalRangeFilterDialog(Dialog):
    """
    Dialog for adding filters.
    """
    def __init__(self, parent):
        # Member variables. 
        self.id = -1
        self.parent = parent
        
        # Call the constructor of wx.Dialog.
        Dialog.__init__(self, self.parent, self.id, 'Add numerical range filter', size=(375, 185))

        # Center this dialog when it is opened.
        self.CenterOnScreen()

        self.__initalize_components()
        self.__bind_events()


    def __initalize_components(self):
        """
        Function for initalizing components for this dialog.
        """
        StaticText(self, -1, "Lower bound", pos=(20,20)) 
        StaticText(self, -1, "Upper bound", pos=(20,60)) 
        self.text_lower = TextCtrl ( self, -1, pos=(110, 20), size=Size(240, -1) )
        self.text_upper = TextCtrl ( self, -1, pos=(110, 60), size=Size(240, -1) )
        
        # Get list of all tagset names.
        StaticText(self, -1, "Tagset", pos=(20,105))
        
        # Only select numerical tagsets.
        drop_choices = [ tagset.name for tagset in serviceLayer.getAllTagsets() if tagset.typeId == NumericalTag.TYPE.NUMERICAL ]
        
        # Create drop down for tagsets.
        self.drop_tagsets = ComboBox(self, -1, pos=(110, 100), choices=drop_choices, size=(240,25))
        
        # Create buttons for the dialog. 
        self.btn_ok = Button(self, -1, 'Create', (170, 140))
        self.btn_cancel = Button(self, 1, 'Cancel', (265, 140))

    
    def __bind_events(self):
        self.Bind(EVT_BUTTON, self.on_button_ok, self.btn_ok)
        self.Bind(EVT_BUTTON, self.on_button_cancel, self.btn_cancel)

    
    def get_lower_bound(self):
        return self.text_lower.GetValue()
    
    
    def get_upper_bound(self):
        return self.text_upper.GetValue()
    
    
    def get_tagset_name(self):
        return self.drop_tagsets.GetValue()
    
    
    def on_button_ok(self, event):
        """
        On click event function for btn_ok.
        """
        if len(self.get_lower_bound()) is 0 or len(self.get_upper_bound()) is 0:
            MessageDialog(None, 'Lower and upper bound must be set', 'Exclamation', OK | ICON_EXCLAMATION).ShowModal()
        
        else:
            self.EndModal( ID_OK )
            self.Destroy()


    def on_button_cancel(self, event):
        """
        On click event function for btn_cancel.
        """
        self.EndModal ( ID_CANCEL )
        self.Destroy()




class AddNewDateRangeFilterDialog(Dialog):
    """
    Dialog for adding new Date range filter.
    """
    def __init__(self, parent):
        # Member variables. 
        self.parent = parent
        
        # Call the constructor of wx.Dialog.
        Dialog.__init__(self, self.parent, -1, 'Add date range filter', size=(430, 255))

        # Center this dialog when it is opened.
        self.CenterOnScreen()

        self.__initalize_components()
        self.__bind_events()


    def __initalize_components(self):
        """
        Function for initalizing components for this dialog.
        """
        
        # create calander for lower bound
        self.cal_lower = cal.CalendarCtrl(self, -1, wx.DateTime_Now(), 
                style = cal.CAL_SEQUENTIAL_MONTH_SELECTION, pos=(10, 30), size=(200, 120))

        self.cal_upper = cal.CalendarCtrl(self, -1, wx.DateTime_Now(), 
                style = cal.CAL_SEQUENTIAL_MONTH_SELECTION, pos=(220, 30), size=(200, 120))

        # create labels 
        StaticText(self, -1, "Lower bound", pos=(10, 10)) 
        StaticText(self, -1, "Upper bound", pos=(220,10)) 
        
        # Get list of all tagset names.
        StaticText(self, -1, "Tagset", pos=(10,165))
        
        # Only select numerical tagsets.
        drop_choices = [ tagset.name for tagset in serviceLayer.getAllTagsets() if tagset.typeId == NumericalTag.TYPE.DATE ]
        
        # Create drop down for tagsets.
        self.drop_tagsets = ComboBox(self, -1, pos=(10, 180), choices=drop_choices, size=(410,25), style=wx.CB_READONLY)
        
        # Create buttons for the dialog. 
        self.btn_ok = Button(self, -1, 'Create', (245, 220))
        self.btn_cancel = Button(self, 1, 'Cancel', (335, 220))

    
    def __bind_events(self):
        self.Bind(EVT_BUTTON, self.on_button_ok, self.btn_ok)
        self.Bind(EVT_BUTTON, self.on_button_cancel, self.btn_cancel)

    
    def get_tagset_name(self):
        return self.drop_tagsets.GetValue()
    
    
    def get_lower_date(self):
        return self.cal_lower.GetDate()

    
    def get_upper_date(self):
        return self.cal_upper.GetDate()


    def on_button_ok(self, event):
        if len(self.drop_tagsets.GetValue()) == 0:
            wx.MessageBox('Select tagset first', 'Info')
        else:
            self.EndModal( ID_OK )
            self.Destroy()


    def on_button_cancel(self, event):
        self.EndModal ( ID_CANCEL )
        self.Destroy()






class AddNewTimeRangeFilterDialog(Dialog):
    """
    Dialog for adding new Time range filter.
    """
    def __init__(self, parent):
        # Member variables. 
        self.parent = parent
        
        # Call the constructor of wx.Dialog.
        Dialog.__init__(self, self.parent, -1, 'Add time range filter', size=(320, 230))

        # Center this dialog when it is opened.
        self.CenterOnScreen()

        self.__initalize_components()
        self.__bind_events()


    def __initalize_components(self):
        """
        Function for initalizing components for this dialog.
        """
        StaticText(self, -1, "Lower bound (hour/min/sec/msec)", pos=(10, 10)) 
        self.lower_text_hour = wx.TextCtrl ( self, -1, pos=(10, 30),size=(25,25) )
        self.lower_text_min = wx.TextCtrl ( self, -1, pos=(40, 30),size=(25,25) )
        self.lower_text_sec = wx.TextCtrl ( self, -1, pos=(70, 30),size=(25,25) )
        self.lower_text_msec = wx.TextCtrl ( self, -1, pos=(100, 30),size=(40,25) )
        
        StaticText(self, -1, "Upper bound (hour/min/sec/msec)", pos=(10, 70)) 
        self.upper_text_hour = wx.TextCtrl ( self, -1, pos=(10, 90),size=(25,25) )
        self.upper_text_min = wx.TextCtrl ( self, -1, pos=(40, 90),size=(25,25) )
        self.upper_text_sec = wx.TextCtrl ( self, -1, pos=(70, 90),size=(25,25) )
        self.upper_text_msec = wx.TextCtrl ( self, -1, pos=(100, 90),size=(40,25) )

        # Get list of all tagset names.
        StaticText(self, -1, "Tagset", pos=(10, 130))
        
        # Only select numerical tagsets.
        drop_choices = [ tagset.name for tagset in serviceLayer.getAllTagsets() if tagset.typeId == NumericalTag.TYPE.TIME ]
        
        # Create drop down for tagsets.
        self.drop_tagsets = ComboBox(self, -1, pos=(10, 150), choices=drop_choices, size=(300,25), value=drop_choices[0])
        
        # Create buttons for the dialog. 
        self.btn_ok = Button(self, -1, 'Create', (130, 190))
        self.btn_cancel = Button(self, 1, 'Cancel', (225, 190))

    
    def __bind_events(self):
        self.Bind(EVT_BUTTON, self.on_button_ok, self.btn_ok)
        self.Bind(EVT_BUTTON, self.on_button_cancel, self.btn_cancel)

    def get_tagset_name(self):
        return self.drop_tagsets.GetValue()
    
    def get_lower_hour(self):
        return self.lower_text_hour.GetValue()

    def get_lower_min(self):
        return self.lower_text_min.GetValue()
    
    def get_lower_sec(self):
        return self.lower_text_sec.GetValue()
    
    def get_lower_msec(self):
        return self.lower_text_msec.GetValue()

    def get_upper_hour(self):
        return self.upper_text_hour.GetValue()
    
    def get_upper_min(self):
        return self.upper_text_min.GetValue()
    
    def get_upper_sec(self):
        return self.upper_text_sec.GetValue()
    
    def get_upper_msec(self):
        return self.upper_text_msec.GetValue()
 
    def on_button_ok(self, event):
        self.EndModal( ID_OK )
        self.Destroy()

    def on_button_cancel(self, event):
        self.EndModal ( ID_CANCEL )
        self.Destroy()
