import wx
from contribute.TextCtrlAutoComplete import *
from objectcube import objectCubeService

sys.path.append( '../ObjectCube' )
from ObjectCubePython import *



##########################################################
class DimensionPanel(wx.Panel):
    """
    Panel class for Dimension filters.
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.init_components()

    def reload(self):
        self.dimensionList.load_data()
    
    
    def init_components(self):
        # Create dimension list.
        self.dimensionList = DimensionsList(self)
        self.sizer=wx.BoxSizer(wx.HORIZONTAL)
        self.sizer.Add(self.dimensionList,1,wx.EXPAND)
        self.SetSizer(self.sizer)
###########################################################








##########################################################
class DimensionPopup( wx.Menu ):
    
    def __init__(self, parent):
        wx.Menu.__init__(self)
        self.parent = parent
        
        item1 = wx.MenuItem(self, wx.NewId(),"Add node")
        item2 = wx.MenuItem(self, wx.NewId(),"Delete node")
        self.AppendItem(item1)
        self.AppendItem(item2)
        
        # Bind new event.
        self.Bind(wx.EVT_MENU, self.on_new_node, item1)
        self.Bind(wx.EVT_MENU, self.onDeleteNode, item2)
        
    def on_new_node(self, event):
        self.parent.add_node_to_selected()
    
    def onDeleteNode(self, event):
        self.parent.deleteSelected()
##########################################################




##########################################################
class DimensionListPopup( wx.Menu ):
    
    def __init__(self, parent):
        wx.Menu.__init__(self)
        self.parent = parent
        
        menuItemEditHir = wx.MenuItem(self, wx.NewId(),"Edit Hierarchy")
        menuItemDeleteHir = wx.MenuItem(self, wx.NewId(),"Delete Hierarchy")
        
        self.AppendItem(menuItemEditHir)
        self.AppendItem(menuItemDeleteHir)

        # Bind new event.
        self.Bind(wx.EVT_MENU, self.onCliekmenuItemEditHir, menuItemEditHir)
        self.Bind(wx.EVT_MENU, self.onClickmenuItemDeleteHir, menuItemDeleteHir)

        
    def on_new_node(self, event):
        #self.parent.add_node_to_selected()
        pass
    
    
    def onClickmenuItemDeleteHir(self, event):
        hir = self.parent.getSelectedHir()
        print 'Remove hir with id', hir.id
    
    
    def onCliekmenuItemEditHir(self, event):
        self.parent.editSelected()
    
    
##########################################################





























###########################################################
class DimensionsList(wx.ListCtrl):
    
    """
    List Control for showing dimensions.
    """
    
    def __init__(self, parent, id=-1):
        """
        Constructor for the DimensionList class.
        Sets the column names and loads the data
        for the list when initalized.
        """
        self.hirs = list()
        self.columns = ['Name','Tagset', 'Dimension id']
        self.init_base(parent, id)
        self.load_data()
        
        self.Bind(wx.EVT_RIGHT_DOWN, self.onRightButtonDown)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.onListItemSelected, self)
        
        

    def init_base(self, parent, id):
        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
        
        for col, text in enumerate(self.columns):
            self.InsertColumn(col, text)

    def load_data(self):
        """
        Functions that loads the data for this list.
        Fetches all dimensions from framework.
        """
        self.DeleteAllItems()
        self.hirs = list()
        self.selectedItemIndex = None
        
        for hir in objectCubeService.get_all_dimensions():
            tagset = objectCubeService.get_tagset_by_id(hir.tagSetId)
            self.Append((tagset.name, hir.getRoot().name, hir.id))
            self.hirs.append( hir )
            
            # resize columns
            for i in range(len(self.columns)):
                self.SetColumnWidth(i, wx.LIST_AUTOSIZE)
    
    
    def onRightButtonDown(self, event):
        self.PopupMenu(DimensionListPopup(self))
    
    
    def onListItemSelected(self, event):
        self.selectedItemIndex = event.GetIndex()
    
    def getSelectedHir(self):
        return self.hirs[ self.selectedItemIndex ]
    
    
    def editSelected(self):
        dlg = EditHierarchyDialog(self, self.getSelectedHir())
        res = dlg.ShowModal()
        self.load_data()
    
###########################################################

class HierarchyTree(wx.TreeCtrl):
    def __init__(self, parent, pos, hir=None):
        wx.TreeCtrl.__init__(self,parent, 1, pos, (380,200), wx.TR_HAS_BUTTONS | wx.SIMPLE_BORDER)
        self.root = None  
        self.hir = hir      
        self.bind_events()
        

        if hir is not None:
            self.createTreeForHir(hir)
            self.current_selected_tagset = objectCubeService.get_tagset_by_id(hir.tagSetId)
            
        else:
            self.current_selected_tagset = -1
        
        
    def createTreeForHir(self, hir):
        hirRoot = hir.getRoot()
        
        # Add the root to the TreeCtrl
        root = self.AddRoot(hirRoot.name)
        self.GetItemData(root).SetData(hirRoot.name)


        for b in hirRoot.getBranches():
            parent = self.AppendItem(root, b.name) 
            self.GetItemData(parent).SetData(b.name)
            self.__processBranches(b, parent)

        
    
    def __processBranches(self, node, parent):
        for b in node.getBranches():
            parent2 = self.AppendItem(parent, b.name) 
            self.GetItemData(parent2).SetData(b.name)
            self.__processBranches(b, parent2)
            
    
    
    def bind_events(self):
        self.Bind(wx.EVT_RIGHT_DOWN, self.on_right_down)        
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnSelChanged, id=1) 
    
    def on_right_down(self, event):
        self.PopupMenu(DimensionPopup(self))

    def OnSelChanged(self, event):
        item =  event.GetItem()

    def deleteSelected(self):
        if self.hir is None:
            self.Delete( self.GetSelection()  )
        else:
            #print 'wow we are del !!! and we are editing.', self.GetSelection()
            nodeValue = self.GetItemData(self.GetSelection()).GetData()
            delNode = self.hir.getNode(nodeValue)
            print dir(delNode)
            
    
    
    def add_node_to_selected(self):
        # we must check what time of tagset we are working with now.        
        # If we are inserting a new root.        
        #print 'current tagset type', self.current_selected_tagset
        
        tag_name = None

        if self.current_selected_tagset.typeId == AlphanumericalTagSet.TYPE.ALPHANUMERICAL:
            dlg = AlphanumericalTagSetNode(self, self.current_selected_tagset)
            
            res = dlg.ShowModal()
            
            if res == wx.ID_OK:
                print 'dlg object:', dlg
                tag_name = str( dlg.txt_name.GetValue() )
                # Do we want to add it?
                ObjectTag( self.current_selected_tagset.fetchOrAddTag( tag_name ) )
                
                if self.GetCount() is 0:
                    self.AddRoot(tag_name)
                else:
                    self.AppendItem(self.GetSelection(), tag_name)
                    # if we are working with actual stuff
                    if self.hir is not None:
                        print 'we need to save this action.c'
                        # get the node that we were working with.
                        nodeValue = self.GetItemData(self.GetSelection()).GetData()
                        selNode = self.hir.getNode(nodeValue)
                        
                        tag = self.current_selected_tagset.fetchOrAddTag(tag_name)
                        
                        #print 'We are on node', selNode.name
                        self.hir.addNode(selNode.id, tag)
                        
                          
                

        elif self.current_selected_tagset.typeId == NumericalTagSet.TYPE.NUMERICAL:
            dlg = AlphanumericalTagSetNode(self, self.current_selected_tagset)
            
            res = dlg.ShowModal()
            
            if res == wx.ID_OK:
                print 'dlg object:', dlg
                tag_name = int( dlg.txt_name.GetValue() )
                # Do we want to add it?
                print dir(self.current_selected_tagset)
                ObjectTag( self.current_selected_tagset.getNumericalTag( tag_name ) )
                
                if self.GetCount() is 0:
                    self.AddRoot(str(tag_name))
                else:
                    self.AppendItem(self.GetSelection(), str(tag_name))
                    # if we are working with actual stuff
                    if self.hir is not None:
                        print 'we need to save this action.c'
                        # get the node that we were working with.
                        nodeValue = int(self.GetItemData(self.GetSelection()).GetData())
                        print 'nodeValue', nodeValue
                        selNode = self.hir.getNode(nodeValue)
                        #tag = self.current_selected_tagset.fetchOrAddTag(tag_name)
                        #self.hir.addNode(selNode.id, tag)
        
        
        elif self.current_selected_tagset.typeId == browser.objectcube.framework.DateTagSet.TYPE.DATE: 
            print 'we are adding DataTagset'
        
        elif self.current_selected_tagset.typeId == browser.objectcube.framework.TimeTagSet.TYPE.TIME:
            print 'we are adding time tagset'
        
        else:
            print 'Unknown tagset.'
################################################################################





###############################################################################
class AlphanumericalTagSetNode(wx.Dialog):
    def __init__(self, parent, tagset):
        # call the parent constructor
        wx.Dialog.__init__(self, parent, -1, "Add new Alphanumerical node", size=(270, 280))
        
        # center this dialog when it is open
        self.CenterOnScreen()
        self.tagset = tagset

        self.init_components()
        self.bind_events()
        self.may_reload = True
        
    def init_components(self):
        self.tags = []
        for tag in self.tagset.getTags():
            self.tags.append( tag.valueAsString() ) 
        
        self.tags.sort()
        
        self.txt_name = wx.TextCtrl(self, -1, "", size=(250,25), pos=(10,10))
        self.lc_suggestion = wx.ListBox(self, -1, pos=(10,40), size=(250,200), choices=self.tags)
        self.btn_ok = wx.Button(self, id=-1, label='Create', pos=(85, 250))
        self.btn_cancel = wx.Button(self, id=-1, label='Cancel', pos=(175, 250))
        

    def bind_events(self):
        self.Bind(wx.EVT_BUTTON, self.event_btnOk_click, self.btn_ok)
        self.Bind(wx.EVT_BUTTON, self.event_btnCancel_click, self.btn_cancel)
        self.Bind(wx.EVT_TEXT, self.event_txt_name_change, self.txt_name)
        self.Bind(wx.EVT_LISTBOX, self.event_lc_suggest_change, self.lc_suggestion )
        pass
    
    
    # TODO: This function is not working.
    def event_txt_name_change(self, event):
        """
        This event function is called when the text in self.txt_name textbox
        is changed.
        """
        if self.may_reload:
            if len( self.txt_name.GetValue() ) == 0:
                #print 'Reload'
                self.lc_suggestion.Clear()
                # Add the whole tag set again.
                for i,tag_name in enumerate(self.tags):
                    self.lc_suggestion.Insert(tag_name,i)
            else:
                self.lc_suggestion.Clear()
                for i,tag in enumerate(self.tags):
                    if tag.lower() in self.txt_name.GetValue().lower():
                        self.lc_suggestion.Insert(tag,i)


    def event_lc_suggest_change(self, event):
        self.may_reload = False
        self.txt_name.SetValue(event.GetString())
        self.may_reload = True
        
    
    def event_btnOk_click(self, event):
        self.EndModal ( wx.ID_OK )
        #self.Destroy()
        self.Close()
    def event_btnCancel_click(self, event):
        self.EndModal ( wx.ID_CANCEL )
        #self.Destroy()
        self.Close()
###############################################################################




class EditHierarchyDialog(wx.Dialog):
    """
    """
    def __init__(self, parent, hir):
        # call the parent constructor
        wx.Dialog.__init__(self, parent, -1, "Edit Hierarchy with id " + str(hir.id), size=(400, 340), style=wx.TAB_TRAVERSAL|wx.DEFAULT_FRAME_STYLE)
        self.hir = hir
        
        # Center this dialog when it opens.
        self.CenterOnScreen()
        
        # Create the components for this dialog.
        self.__setupComponents()
        
        #self.__init_components()
        #self.__bind_events()
    
    def __setupComponents(self):
        # Create HierarchyTree
        self.tree = HierarchyTree(self, (10,90), hir=self.hir)








###########################################################
class AddNewHierarchyDialog(wx.Dialog):
    """
    Dialog for adding a new Hierarchy
    """
    def __init__(self, parent):
        # call the parent constructor
        wx.Dialog.__init__(self, parent, -1, "Add new Hierarchy", size=(400, 340), style=wx.TAB_TRAVERSAL|wx.DEFAULT_FRAME_STYLE)
  
        # center this dialog when it is open
        self.CenterOnScreen()
        self.__init_components()
        self.__bind_events()
        
    def __init_components(self):
        # Horizontal box.
        #hbox = wx.BoxSizer(wx.VERTICAL)
        
        # Create drop down for the tagset.
        tagsets = [tagset.name for tagset in objectCubeService.getAllTagsets()]
        wx.StaticText(self, -1, 'Select tag set:' , wx.Point(10, 10))
        self.cmb_tagsets = wx.ComboBox(self, -1, pos=(10, 30), size=(380, 25), choices=tagsets, style=wx.CB_READONLY)
        wx.StaticText(self, -1, 'Create Hierarchy:' , wx.Point(10, 70))
        

        # create label for the file url.
        self.tree = HierarchyTree(self, (10,90))
        
        # create submit buttons.
        self.btn_ok = wx.Button(self, id=-1, label='Add', pos=(215, 300))
        self.btn_cancel = wx.Button(self, id=-1, label='Cancel', pos=(305, 300))

    
    def __bind_events(self):
	    self.Bind(wx.EVT_COMBOBOX, self.on_cmb_select_change, self.cmb_tagsets)
	    self.Bind(wx.EVT_BUTTON, self.on_button_ok, self.btn_ok)
	    self.Bind(wx.EVT_BUTTON, self.on_button_cancel, self.btn_cancel)

    # event function.
    def on_button_ok(self, event):
        
        # Get all the tags in the tag set. Note they might have been
        # updated during the process of creating the hierarchy.
        # Thats why we are getting it again here.
        self.tags = {}
        for tag in self.tree.current_selected_tagset.getTags():
            self.tags[tag.valueAsString()] = tag  
        
        # read the tree and create a dim.
        root = self.tree.GetRootItem()
        
        # Get the tag object for the root node.
        if self.tree.current_selected_tagset.typeId == NumericalTagSet.TYPE.NUMERICAL:
            root_node = self.tree.current_selected_tagset.getNumericalTag(int(self.tree.GetItemText(root)))
        else:
            root_node = self.tree.current_selected_tagset.fetchOrAddTag(str(self.tree.GetItemText(root)))
            

        self.hierarchy = self.tree.current_selected_tagset.createPersistentDimension( root_node )
        

        #print 'Created dim with root node:', root_node.id, '(' + root_node.name + ')'
        
        # We must keep the hierarchy nodes in dictionary.
        self.tree_nodes = dict()
        self.tree_nodes[root_node.valueAsString()] = self.hierarchy.getRoot()
        
        # Go through the subtree
        subItem = self.tree.GetFirstChild(root)[0]
        
        while subItem.IsOk():
            self.process_tree(subItem)
            subItem = self.tree.GetNextSibling(subItem)
        
        self.EndModal ( wx.ID_OK )
        self.Destroy()
    

    def process_tree(self, treeItem):
        parent = self.tree.GetItemText(self.tree.GetItemParent(treeItem)) 
        node = self.tree.GetItemText(treeItem)
        parent_tag = self.tags[parent]
        node_tag = self.tags[node]
        
        # Get the node from the stored node dictionary.
        parent_node = self.tree_nodes[parent_tag.valueAsString()]
        

        #print 'Adding node with id', node_tag.id, '(' + node_tag.name + ')', 'with parent', parent_tag.id, '(' + parent_tag.name + ')', 'to dim.'
        new_node = self.hierarchy.addNode(parent_node.id, node_tag)
        self.tree_nodes[node_tag.valueAsString()] = new_node
        
        subItem = self.tree.GetFirstChild(treeItem)[0]
        while subItem.IsOk():
            self.process_tree(subItem)
            subItem = self.tree.GetNextSibling(subItem)
    
    
    # event function.
    def on_button_cancel(self, event):
        self.EndModal ( wx.ID_CANCEL )
        self.Destroy()

    def on_cmb_select_change(self, event):
        """
        This function is called when the user selects a new tagset from the
        tagset combobox. The hirerchy that that user has been created is cleared
        where it is not allowed to create hirarchies between tagsets.        
        """
        # Remove everything from the tree.
        # TODO: Check if that is what the user wants?        
        self.tree.DeleteAllItems()
        
        tagset_name = event.GetString()
        if len(tagset_name) is 0:
            self.tree.current_selected_tagset = None
        else:
            self.tree.current_selected_tagset = objectCubeService.getTagsetByName(tagset_name)
###############################################################################
