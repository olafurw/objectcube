import wx
import sys
sys.path.append( '../ObjectCube' )
from ObjectCubePython import *
from objectcube import objectCubeService


class ObjectTagsList(wx.ListCtrl):
    """
    List for viewing tags that a given object has
    """

    def __init__(self, parent, object, id=-1):
        self.object = object
        # List of the columns in this list.
        self.columns = ['Tagset','Tag','Tagset type']
        self.tags = []
        self.selected_tag_hash = {}
        self.init_base(parent, id)
        self.load_data()
        self.bind_events()        
        self.currently_selected_tag = -1


    def init_base(self, parent, id):
        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT)
        
        for col, text in enumerate(self.columns):
            self.InsertColumn(col, text)

        for i in range(len(self.columns)):
            self.SetColumnWidth(i, 30)


    def bind_events(self):
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.event_selected, self)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.event_deselect, self)


    def load_data(self):
        """
        Function for loading data from the framework
        and add them to the this listctrl.
        """
        self.tags = []
        self.selected_tag_hash = {}

        for n in objectCubeService.get_object_tags(self.object):
            tag = n[1]
            tagset_name = n[0]
            self.add_line([tagset_name, tag.valueAsString(), tag.typeAsString()])
            self.tags.append(tag)
        

    def reload_data(self):
        # Delete everything from the list.
        self.DeleteAllItems()
        
        # reload the object that we have in memory.
        self.object = objectCubeService.get_object_by_id(self.object.id)    
        
        # Load the data with the new object.
        self.load_data()

    
    def add_line(self, line):
        # add the line to the list
        index = self.Append(line)

        # resize columns
        for i in range(len(self.columns)):
            self.SetColumnWidth(i, wx.LIST_AUTOSIZE)
        
        return index

    
    def event_selected(self, event):
        
        index = event.GetIndex()
        self.selected_tag_hash[index] = self.tags[index]


        # get the index of selected
        self.currently_selected_tag = index

    
    def event_deselect(self, event):
        del self.selected_tag_hash[event.GetIndex()]
    
    
    def has_selected(self):
        if len(self.selected_tag_hash) == 0: return False
        return True

    
    def remove_selected(self):
        for key in self.selected_tag_hash.keys():
            self.object.removeTag(self.tags[key])
    
        self.reload_data()




