import wx
from tagset import TagsetList


class TagsetPanel(wx.Panel):
    def __init__(self, parent):
        """
        Constructor for the PageTagset panel.
        """
        # Initalize the panel.
        wx.Panel.__init__(self, parent)

        # Dictionary for tagsets. Keys are index
        # and id is value.
        self.tagsets = {}
        
        # create list ctrl
        self.tagsetList = TagsetList(self)
        
        # Create sizer for expanding the listbox.
        self.sizer=wx.BoxSizer(wx.HORIZONTAL)
        self.sizer.Add(self.tagsetList,1,wx.EXPAND)
        self.SetSizer(self.sizer)
        

    def reload_tagset_list(self):
        self.tagsetList.load_data()


