import wx
from objectcube import objectCubeService


############################################################################
class AddNewTagsetDialog(wx.Dialog):
    """
    Dialog window for adding a new tagset.
    """
    def __init__(self, parent, id, title):
        wx.Dialog.__init__(self, parent, id, title, size=(295,220))
        # Center this dialog when it is opened.
        self.CenterOnScreen()
        # Create the components. 
        self.__initalize_components()
        # Bind events for components on this frame. 
        self.__bind_events()
        
    
    
    def __initalize_components(self):
        # Get the available tagset types from the framework.
        types = []
        for x in objectCubeService.get_all_tagset_types():
            types.append(x[1])

        # Create controls for the name.
        self.text_name = wx.TextCtrl ( self, -1, pos=(20,45), size=(250,20))
        wx.StaticText(self, -1, 'Tagset name:', pos=(20, 20)) 
        
        # Create controls for tagset types.
        self.drop_down_type = wx.ComboBox(self, -1, choices=types, style=wx.CB_READONLY, pos=(20,105), size=(250,25)) 
        self.drop_down_type.Select(0)
        wx.StaticText(self, -1, 'Tagset type:', style=wx.ALIGN_CENTRE, pos=(20,80))
        
        # Create buttons
        self.btn_ok = wx.Button(self, id=-1, label='Create', pos=(115, 150))
        self.btn_cancel = wx.Button(self, id=-1, label='Cancel', pos=(200, 150))

    
    def __bind_events(self):
        self.Bind(wx.EVT_BUTTON, self.on_button_ok, self.btn_ok) 
        self.Bind(wx.EVT_BUTTON, self.on_button_cancel, self.btn_cancel)
    

    def on_button_ok(self, event):
        self.EndModal ( wx.ID_OK )
        self.Close()


    def on_button_cancel(self, event):
        self.EndModal ( wx.ID_CANCEL )
        self.Close()
############################################################################


