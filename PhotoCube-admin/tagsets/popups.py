import wx
import sys
sys.path.append( '../ObjectCube' )
from ObjectCubePython import *
from tagsets.dialogs import AddNewTagsetDialog
from objectcube import objectCubeService

##########################################################
class TagsetListPopupMenu( wx.Menu ):
    def __init__(self, parent):
        wx.Menu.__init__(self)
        self.parent = parent
        
        item1 = wx.MenuItem(self, wx.NewId(),"Add new tagset")
        item2 = wx.MenuItem(self, wx.NewId(),"Delete selected tagsets")
        
        self.AppendItem(item1)
        self.AppendItem(item2)
        
        # Bind new event.
        self.Bind(wx.EVT_MENU, self.on_new_tagset, item1)
        self.Bind(wx.EVT_MENU, self.on_delete_selected_tagsets, item2)
        
    def on_new_tagset(self, event):   
        dialog = AddNewTagsetDialog(self.parent, -1, 'Add new tagset')
        
        if dialog.ShowModal() == wx.ID_OK:
            tagset_name = dialog.text_name.GetValue()
            tagset_type = dialog.drop_down_type.GetValue()

            # Get the id of the tagset that will be created. Might be a better
            # way of finding this id from the framework.
            tagset_type_id = objectCubeService.get_tagset_type_id_by_name(tagset_type)
            
            tagset = None
            
            if tagset_type_id == TagSet.TYPE.ALPHANUMERICAL:
                tagset = AlphanumericalTagSet(tagset_name.encode('utf-8'))
            
            if tagset_type_id == TagSet.TYPE.NUMERICAL:
                tagset = NumericalTagSet( str(tagset_name) )
                
            if tagset_type_id == TagSet.TYPE.DATE:
                tagset = DateTagSet( str(tagset_name) )

            if tagset_type_id == TagSet.TYPE.DATE:
                tagset = DateTagSet( str(tagset_name) )
            
            if tagset_type_id == TagSet.TYPE.TIME:
                tagset = TimeTagSet( str(tagset_name) )

            try:
                # Create the tagset.
                tagset.create()
                
                # Ask the parent (in this case the Tagset list) to reload its data.
                self.parent.load_data()
            except:
                wx.MessageBox('Unable to create tagset.', 'Error' , wx.ICON_ERROR)

    
    def on_delete_selected_tagsets(self, event):
        print 'Not supported.'
##########################################################
