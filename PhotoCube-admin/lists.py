import wx
from objects.dialogs import ViewObjectDialog
#from browser.objectcube import objectCubeService
from objectcube import objectCubeService



class ObjectList(wx.ListCtrl):

    """
    ListCtrl for Objects.
    """

    def __init__(self, parent, id=-1):
        self.columns = ['Object id','Object location']
        self.init_base(parent, id)
        self.load_data()
        self.bind_events()

    def init_base(self, parent, id):
        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
        
        for col, text in enumerate(self.columns):
            self.InsertColumn(col, text)

    # Should the list be implementing event's or the parent of the list?
    def bind_events(self):
        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.event_mouse_right_click, self)
        self.Bind( wx.EVT_LIST_ITEM_ACTIVATED, self.event_double_click, self)

    def event_mouse_right_click(self, event):
        print 'right mouse click event.'
        pass

    def event_double_click(self, event):
        # Get the id of the object that the user double clicked.
        object_id = int(event.Item.GetText())
        
        # create a view object dialog.
        dialog = ViewObjectDialog(self, -1, object_id)
        dialog.Show()

    def load_data(self):
        """
        Function for loading data from the framework
        and add them to the this listctrl.
        """
        for n in objectCubeService.get_all_objects():
            self.add_line([n.id, n.name])

    
    def reload_data(self):
        self.DeleteAllItems()
        self.load_data()


    def add_line(self, line):
        # add the line to the list
        self.Append(line)

        # resize columns
        for i in range(len(self.columns)):
            self.SetColumnWidth(i, wx.LIST_AUTOSIZE)

