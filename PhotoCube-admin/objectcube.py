import sys
sys.path.append( '../ObjectCube' )
from ObjectCubePython import *
import ObjectCubePython
import random
from common import dbFile

####################################################################
class ObjectCube:
    """
    This class serves as a API layer on top of the
    object cube framework.
    """
    def __init__(self, language=None):
        """
        Constructor for the ServiceLayer class.
        """
        # Create hub for the object cube framework.
        #Parameters.getParameters().add( "dataAccessType", "3" )
        Parameters.getParameters().add( "SQLite", dbFile ) 
        
        if language is not None:
            Hub.setLanguageId(language)
        self.hub = Hub.getHub()
        
        # Store the tagset types in memory as a list of tuples
        # where each tuple contains an id and string.
        self.tagset_types = []

        for x in TagSet.TYPE.values:
            self.tagset_types.append((x,TagSet.typeAsStringStatic(x)))

        # Collections of filters that have been created.
        self.filters = []
        
        # TODO: Make this configurable.
        #Parameters.getParameters().add( "DimensionFilterV2", "0")
        #Parameters.getParameters().add( "outputDebugInfo", "0" )
        #Parameters.getParameters().add( "outputStateHierarchyObjects", "0" )
        #Parameters.getParameters().add( "SQLite", "/home/hs01/temp/objectcube/trunk/PhotoCube/Database/x" )         
        self.state = State.getState()
        

    def get_object_by_name(self, name):
        return Object.fetch(name)
    
    
    def get_state(self):
        return self.state
    
    def update_state(self):
        self.state = self.state.getState()
    
    
    def add_filter(self, filter):
        # We store the filters in memory so they can be saved.
        #self.filters.append( filter )
        State.addFilter( filter )

    
    def get_filters(self):
        print 'getting filters'
        #return self.filters
        l = []
        for filter in State.getFilters():
            l.append(filter)
        return l

    
    def get_filter_by_id(self, id):
        """
        Function for fetching filter by id
        """
        for filter in self.get_filters():
            if filter.id == id:
                return filter
        return None

    
    def remove_filter(self, filter):
        print 'removeing filter', filter
        State.removeFilter(filter)

    
    
    def getAllTagsets(self, onlyUserDefined=False):
        if onlyUserDefined:
            returnList = []
            for tagset in self.hub.getTagSets():
                if tagset.accessId  == TagSet.ACCESS_TYPE.USER:
                    returnList.append(tagset)
            return returnList 
        else: 
            return self.hub.getTagSets()


    
    def get_tagset_by_id(self, id):
        for tagset in self.hub.getTagSets():
            if tagset.id == id: return tagset
        return None
    
    
    
    def getTagsetByName(self, tagset_name):
        """
        Get tag by name (case ins). If there is not tagset
        with a given search name, then the function returns
        none value.
        """
        for tagset in self.getAllTagsets():
            if tagset.name.lower() == tagset_name.lower():
                return tagset
        return None

    
    
    def get_all_tagset_types(self):
        return self.tagset_types

    
    
    def get_tagset_type_id_by_name(self, name):
        for n in self.tagset_types: 
            if n[1] == name: return n[0]
        return -1

    
    
    def get_object_by_id(self, object_id):
        o = Object.fetch(object_id)
        return o
    
    
    
    def get_all_objects(self):
        return_list = []
        State.removeAllFilters() 
        
        state = State.getState()
        objects = state.getObjects()
        
        for n in range(len(objects)):
            return_list.append(objects[n])

        return return_list

    

    def get_object_tags(self, object):
        """
        Function that returns list of quadtripplet on the form:
        (tagset_id, tagset_name, tag, tagset_id) that have been tagged
        to object with object_id.
        """
        #o = Object.fetch(object_id)
        returnList = []
        
        for object_tag in object.getTags():
            tag = object_tag.getTag()
            tagset_id = tag.tagSetId
            tagset = self.get_tagset_by_id(tagset_id) 
            returnList.append([tagset.name, tag])
        return returnList

    
    
    def remove_tag_from_object(self, object_id, tag_id, tagset_id):
        """
        Removes tag from object.
        """
        tagset = self.hub.getTagSet(tagset_id)
        tag = tagset.getTag(tag_id)
        o = Object.fetch(object_id)
        o.removeTag(tag)


    
    def add_tag_to_object(self, tagset_id, tag_id, object_id):
        tagset = self.hub.getTagSet(tagset_id)
        tag = tagset.getTag(tag_id)
        o = Object.fetch(object_id)
        o_tag = ObjectTag(tag)
        o.addTag(o_tag)
   

    
    def get_object_location(self, object_id):
        o = Object.fetch(object_id)
        return o.name

    
    
    def get_all_dimensions(self):
        """
        Retuns all dimensions that have been stored to the db.
        """
        return_list = []
        for dim in self.hub.getPersistentDimensions():
            return_list.append(dim)
        return return_list
        
    
    def get_tag(self, tagset_id, tag_id):
        tagset = self.hub.getTagSet(tagset_id)
        return tagset.getTag(tag_id)
                    
####################################################################
objectCubeService = ObjectCube()
