#objectcube

This page details the steps required to build ObjectCube and PhotoCube, and to install a small test database.  

Note: This is an installation for a Linux based operating system. The examples used will reflect an rpm based system (fedora, centos, redhat)

## Step 0: Get the Code

Clone the source, for example by pressing "Clone" above and copy-pasting the link, or using the commands below:

	git clone https://<yourusernamehere>@bitbucket.org/olafurw/objectcube.git

The latest version is in the branch plugin-threads, so switch to that.

    git checkout plugin-threads

## Step 1: Install the required libraries

CMake is required to generate the make files

    yum install cmake

GCC is required to build the software, you'll need 4.8.1+ for C++11 support.

    yum install gcc-c++

The database library MonetDB is required, so head over to [http://dev.monetdb.org/downloads/](http://dev.monetdb.org/downloads/) and download that. For RPM systems select Fedora and install the release package and then install the library through yum.

    yum install MonetDB-client-devel MonetDB5-server MonetDB-SQL-server5 MonetDB

CXXTest is required for unit testing.

    yum install cxxtest

For the EXIF Plugin, the EXIV2 library is required

    yum install exiv2-devel exiv2

Photocube runs on QT4 (4.8.5), so that library is required.

    yum install qt qt-devel qt-x11 qt-creator

For Photocube, the following libraries are required.

    yum install freeglut-devel freeglut opencv-devel opencv ImageMagick-c++-devel ftgl-devel

This should be enough to start developing for the ObjectCube or PhotoCube system.

## Step 2: Setting up the database

This step assumes that MonetDB is installed. No prior configuration is needed.

This next step is a one time step only, when you have perfomed it, you do not have to do it again for the same system.

Go to your home directory and create a file called .monetdb

    cd;
    vim .monetdb;

Add this text to the file

    user=monetdb
    password=monetdb
    language=sql

And save it. :x

Now go back to the ObjectCube repository directory and run these commands

    monetdbd create objectcube;
    monetdbd start objectcube;

These two commands create the database farm in the repository directory. This is you only have to do once.

Assuming you are still in the ObjectCube repostitory directory, run this command to initialize the database with data and register the plugins.

    ./dbstatus -a reset -p register

This is your go to command to reset the database as well if needed. Check the help command for this tool for more information

    ./dbstatus --help

## Step 2: Build ObjectCube and Plugins

This step expects you to be in the root directory of the repository.

    cd ObjectCube/;
    mkdir build; # Create a directory for all the build files
    cd build;
    cmake ..; # Generate the make files
    make; # Compile
    sudo make install; # Install the library to the /usr/local/lib(64) directory

This will build the plugins as well.

If you are working with PhotoCube and do some changes with ObjectCube, you will need to do the last step, it is not enough to make, but you must make install as well.

## Step 3: Build PhotoCube

This step expects you to be in the root directory of the repository.

    cd PhotoCubeQTGUi/;
    mkdir build; # Create a directory for all the build files
    cd build;
    cmake ..; # Generate the make files
    make; # Compile

This will generate a binary called Photocube in the build directory, you can run this directly.
