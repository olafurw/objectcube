--Drop

DROP TABLE plugin_tag_set;
DROP TABLE plugin;
DROP TABLE dimension;
DROP TABLE object_tag;
DROP TABLE object;
DROP TABLE time_tag;
DROP TABLE date_tag;
DROP TABLE numerical_tag;
DROP TABLE alphanumerical_tag;
DROP TABLE tag;
DROP TABLE tag_type;
DROP TABLE tag_set;
DROP TABLE tag_set_type;
DROP TABLE tag_set_access;
DROP TABLE filter_type;
DROP TABLE translation;
DROP TABLE language;
DROP TABLE dimension_tag_object;
