INSERT INTO tag_set_access values(1, 'System');
INSERT INTO tag_set_access values(2, 'User');

INSERT INTO tag_set_type values(1, 'Alphanumerical tag-set');
INSERT INTO tag_set_type values(2, 'Numerical tag-set');
INSERT INTO tag_set_type values(3, 'Date tag-set');
INSERT INTO tag_set_type values(4, 'Time tag-set');

INSERT INTO tag_type values(1, 'Alphanumerical tag');
INSERT INTO tag_type values(2, 'Numerical tag');
INSERT INTO tag_type values(3, 'Date tag');
INSERT INTO tag_type values(4, 'Time tag');

INSERT INTO filter_type VALUES( 1, 'Tag filter');
INSERT INTO filter_type VALUES( 2, 'Dimension filter');
INSERT INTO filter_type VALUES( 3, 'Numerical range filter');
INSERT INTO filter_type VALUES( 4, 'Time range filter');
INSERT INTO filter_type VALUES( 5, 'Date range filter');

INSERT INTO language VALUES( 1, 'Islenska');
INSERT INTO language VALUES( 2, 'English');

-- Uncategorized support
INSERT INTO tag_set ( name, description, type_id, access_id ) values( 'UNCATEGORIZED', 'A system tag-set for the Uncategorized tag', 1, 1);
INSERT INTO tag (tag_set_id, type_id) values( ( select max(id) from tag_set ), 1);
INSERT INTO alphanumerical_tag values( (select max(id) from tag), 'Uncategorized', (select max(id) from tag_set));

INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 1, 1, 'Texta tag' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 2, 1, 'Numeriskt tag' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 3, 1, 'Dagsetningar tag' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 4, 1, 'Tima tag' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 1, 2, 'Alphanumerical tag' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 2, 2, 'Numerical tag' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 3, 2, 'Date tag' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagType', 4, 2, 'Time tag' );

INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 1, 1, 'Texta tag-sett' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 2, 1, 'Numeriskt tag-sett' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 3, 1, 'Dagsetningar tag-sett' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 4, 1, 'Tima tag-sett' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 1, 2, 'Alphanumerical tag-set' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 2, 2, 'Numerical tag-set' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 3, 2, 'Date tag-set' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetType', 4, 2, 'Time tag-set' );

INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetAccessType', 1, 1, 'Kerfis adgangur' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetAccessType', 2, 1, 'Notenda adgangur' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetAccessType', 1, 2, 'System access' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'TagSetAccessType', 2, 2, 'User access' );

INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 1, 1, 'Tag filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 2, 1, 'Viddar filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 3, 1, 'Numerabils filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 4, 1, 'Timabils filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 5, 1, 'Dagsetningarbils filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 1, 2, 'Tag filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 2, 2, 'Dimensional filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 3, 2, 'Numerical range filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 4, 2, 'Time range filter' );
INSERT INTO translation ( "entity_name", "entity_id", "language_id", "translated_text") VALUES( 'FilterType', 5, 2, 'Date range filter' );

