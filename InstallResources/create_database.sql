CREATE TABLE "tag_set_access" (
    "id" int NOT NULL
    ,"description" varchar(30) NOT NULL
    ,CONSTRAINT "tag_set_access_pk" PRIMARY KEY ("id")
    ,CONSTRAINT "tag_set_access_desc_uq" UNIQUE ("description")
);

CREATE TABLE "tag_set_type" (
    "id" int NOT NULL
    ,"description" varchar(30) NOT NULL
    ,CONSTRAINT "tag_set_type_pk" PRIMARY KEY ("id")
    ,CONSTRAINT "tag_set_type_desc_uq" UNIQUE ("description")
);

CREATE TABLE "tag_set" (
	"id" int AUTO_INCREMENT NOT NULL
	,"name" varchar(200) NOT NULL
	,"description" varchar(500)
	,"type_id" int NOT NULL
	,"access_id" int NOT NULL
	,CONSTRAINT "tag_set_pk" PRIMARY KEY ("id")
	,CONSTRAINT "tag_set_name_uq" UNIQUE ("name")
	,CONSTRAINT "tag_set_type_fk" FOREIGN KEY ("type_id") REFERENCES "tag_set_type" ("id")
	,CONSTRAINT "tag_set_access_id_uq" FOREIGN KEY ("access_id") REFERENCES "tag_set_access" ("id")
);

CREATE TABLE "tag_type" (
	"id" int NOT NULL
	,"description" varchar(50) NOT NULL
	,CONSTRAINT "tag_type_pk" PRIMARY KEY ("id")
	,CONSTRAINT "tag_type_desc_uq" UNIQUE ("description")
);

CREATE TABLE "tag" (
	"id" int AUTO_INCREMENT NOT NULL
	,"tag_set_id" int NOT NULL
	,"type_id" int NOT NULL
	,CONSTRAINT "tag_pk" PRIMARY KEY ("id")
	,CONSTRAINT "tag_tag_set_fk" FOREIGN KEY ("tag_set_id") REFERENCES "tag_set" ("id") ON DELETE CASCADE
	,CONSTRAINT "tag_tag_type_fk" FOREIGN KEY ("type_id") REFERENCES "tag_type" ("id")
);

CREATE INDEX "tag_tag_set_id_i" on "tag" ("tag_set_id");

CREATE TABLE "alphanumerical_tag" (
	"id" int NOT NULL
	,"tag_string" varchar(250) NOT NULL
	,"tag_set_id" int NOT NULL
	,CONSTRAINT "alphanumerical_tag_pk" PRIMARY KEY ("id")
	,CONSTRAINT "alphanumerical_tag_tag_fk" FOREIGN KEY ("id") REFERENCES "tag" ("id")
	,CONSTRAINT "alphanumerical_tag_tag_set_fk" FOREIGN KEY ("tag_set_id") REFERENCES "tag_set" ("id") ON DELETE CASCADE--Cannot reference tag(tag_set_id), unlike SQLite
	,CONSTRAINT "alphanumerical_tag_tag_set_string_uq" UNIQUE ("tag_set_id", "tag_string")
);

CREATE TABLE "numerical_tag" (
	"id" int NOT NULL
	,"number" bigint NOT NULL
	,"tag_set_id" int NOT NULL
	,CONSTRAINT "numerical_tag_pk" PRIMARY KEY ("id")
	,CONSTRAINT "numerical_tag_tag_fk" FOREIGN KEY ("id") REFERENCES "tag" ("id")
	,CONSTRAINT "numerical_tag_tag_set_fk" FOREIGN KEY ("tag_set_id") REFERENCES "tag_set" ("id") ON DELETE CASCADE--Cannot reference tag(tag_set_id), unlike SQLite
	,CONSTRAINT "numerical_tag_tag_set_number_uq" UNIQUE ("tag_set_id", "number")
);

CREATE INDEX "numerical_tag_number_id_i" on "numerical_tag" ("number", "id");

CREATE TABLE "date_tag" (
	"id" int NOT NULL
	,"tag_date" date NOT NULL
	,"tag_set_id" int NOT NULL
	,CONSTRAINT "date_tag_pk" PRIMARY KEY ("id")
	,CONSTRAINT "date_tag_tag_fk" FOREIGN KEY ("id") REFERENCES "tag" ("id")
	,CONSTRAINT "date_tag_tag_set_fk" FOREIGN KEY ("tag_set_id") REFERENCES "tag_set" ("id") ON DELETE CASCADE--Cannot reference tag(tag_set_id), unlike SQLite
	,CONSTRAINT "date_tag_tag_set_date_uq" UNIQUE ("tag_set_id", "tag_date")
);

CREATE INDEX "date_tag_date_id_i" on "date_tag" ("tag_date", "id");

CREATE TABLE "time_tag" (
	"id" int NOT NULL
	,"tag_time" time NOT NULL
	,"tag_set_id" int NOT NULL
	,CONSTRAINT "time_tag_pk" PRIMARY KEY ("id")
	,CONSTRAINT "time_tag_tag_fk" FOREIGN KEY ("id") REFERENCES "tag" ("id")
	,CONSTRAINT "time_tag_tag_set_fk" FOREIGN KEY ("tag_set_id") REFERENCES "tag_set" ("id") ON DELETE CASCADE--Cannot reference tag(tag_set_id), unlike SQLite
	,CONSTRAINT "time_tag_tag_set_time_uq" UNIQUE ("tag_set_id", "tag_time")
);

CREATE INDEX "time_tag_time_id_i" on "time_tag" ("tag_time", "id");

CREATE TABLE "object" (
    "id" int AUTO_INCREMENT NOT NULL
	,"qualified_name" varchar(150) NOT NULL
	,CONSTRAINT "object_pk" PRIMARY KEY ("id")
	,CONSTRAINT "object_name_uq" UNIQUE ("qualified_name")
);

CREATE TABLE "object_tag" (
    "object_id" int NOT NULL
    ,"tag_id" int NOT NULL
     ,"upper_left_x" numeric(5,2) NOT NULL
     ,"upper_left_y" numeric(5,2) NOT NULL
     ,"lower_right_x" numeric(5,2) NOT NULL
     ,"lower_right_y" numeric(5,2) NOT NULL
     ,"confirmed" boolean NOT NULL
     ,CONSTRAINT "object_tag_pk" PRIMARY KEY ("object_id", "tag_id", "upper_left_x", "upper_left_y", "lower_right_x", "lower_right_y")
     ,CONSTRAINT "object_tag_object_fk" FOREIGN KEY ("object_id") REFERENCES "object" ("id") ON DELETE CASCADE
     ,CONSTRAINT "object_tag_tag_fk" FOREIGN KEY ("tag_id") REFERENCES "tag" ("id")
);

CREATE TABLE "dimension" (
	"id" int NOT NULL
	,"node_id" int NOT NULL
	,"tag_set_id" int NOT NULL
	,"tag_id" int NOT NULL
	,"child_category_title" varchar(50)
	,"left_border" int NOT NULL
	,"right_border" int NOT NULL
	,CONSTRAINT "dimension_pk" PRIMARY KEY("id", "node_id")
	,CONSTRAINT "dimension_tag_set_fk" FOREIGN KEY ("tag_set_id") REFERENCES "tag_set" ("id")
	,CONSTRAINT "dimension_tag_fk" FOREIGN KEY ("tag_id") REFERENCES "tag" ("id")
	,CONSTRAINT "dimension_id_tag_uq" UNIQUE ("id", "tag_id")
);

CREATE TABLE "plugin" (
	"id" int AUTO_INCREMENT NOT NULL
	,"qualified_library_name" varchar(300) NOT NULL
	,"description" varchar(500)
	,CONSTRAINT "plugin_id" PRIMARY KEY ("id")
	,CONSTRAINT "plugin_qual_lib_name_uq" UNIQUE ("qualified_library_name")
);

CREATE TABLE "plugin_tag_set"(
	"id" int AUTO_INCREMENT NOT NULL
	,"plugin_id" int NOT NULL
	,"tag_set_type_id" int NOT NULL
	,"tag_type_id" int NOT NULL
	,"name" varchar(100) NOT NULL
	,"tag_set_name" varchar(100) NOT NULL
	,"tag_set_id" int
	,CONSTRAINT "plugin_tag_set_pk" PRIMARY KEY ("id")
	,CONSTRAINT "plugin_tag_set_plugin_fk" FOREIGN KEY ("plugin_id") REFERENCES "plugin" ("id")
	,CONSTRAINT "plugin_tag_set_tag_set_type_fk" FOREIGN KEY ("tag_set_type_id") REFERENCES "tag_set_type" ("id")
	,CONSTRAINT "plugin_tag_set_tag_type_fk" FOREIGN KEY ("tag_type_id") REFERENCES "tag_type" ("id")
	,CONSTRAINT "plugin_tag_set_tag_set_fk" FOREIGN KEY ("tag_set_id") REFERENCES "tag_set" ("id")
	,CONSTRAINT "plugin_tag_set_plugin_name_uq" UNIQUE ("plugin_id", "name")
	,CONSTRAINT "plugin_tag_set_plugin_tag_set_name_uq" UNIQUE ("plugin_id", "tag_set_name")
);

CREATE TABLE "filter_type" (
	"id" int NOT NULL
	,"description" varchar(30) NOT NULL
	,CONSTRAINT "filter_type_pk" PRIMARY KEY ("id")
	,CONSTRAINT "filter_type_desc_uq" UNIQUE ("description")
);

CREATE TABLE "language" (
	"id" int NOT NULL
	,"description" varchar(100) NOT NULL
	,CONSTRAINT "language_pk" PRIMARY KEY ("id")
	,CONSTRAINT "language_desc_uq" UNIQUE ("description")
);

CREATE TABLE "translation" (
    "id" int AUTO_INCREMENT NOT NULL
    ,"entity_name" varchar(50) NOT NULL
    ,"entity_id" int NOT NULL
    ,"language_id" int NOT NULL
    ,"translated_text" varchar(100) NOT NULL
    ,CONSTRAINT "translation_pk" PRIMARY KEY ("id")
    ,CONSTRAINT "translation_language_fk" FOREIGN KEY ("language_id") REFERENCES "language" ("id")
	,CONSTRAINT "translation_name_ent_lang_uq" UNIQUE ("entity_name", "entity_id", "language_id") 
);

CREATE TABLE "dimension_tag_object" AS
    select d.id as "dimension_id"
    , d.node_id, ot.tag_id, ot.object_id, ot.upper_left_x, ot.upper_left_y
    , ot.lower_right_x, ot.lower_right_y, ot.confirmed, d.left_border, d.right_border
    from dimension d, object_tag ot
    where d.tag_id = ot.tag_id
    order by d.node_id with data;

CREATE INDEX "dimension_t_o_borders_idx" on "dimension_tag_object" ("dimension_id", "left_border", "right_border");
