#include "toolbar.h"

ToolBar::ToolBar(QWidget *parent): QToolBar(parent)
{
    this->setContextMenuPolicy(Qt::PreventContextMenu);
    t_import        = new QAction(("&Import"), this);
    t_importFolder  = new QAction(("&Import Folder"), this);
    t_cardMode      = new QAction(("&Card Mode"), this);
    t_clear         = new QAction(("&Clear"), this);
    t_hideFilters   = new QAction(("&Hide/Show Filters"), this);
    t_hideTreeView  = new QAction(("&Hide/Show TreeView"), this);

    this->addAction(t_import);
    this->addAction(t_importFolder);
    this->addAction(t_clear);
    this->addAction(t_cardMode);
    this->addAction(t_hideFilters);
    this->addAction(t_hideTreeView);
    this->setAllowedAreas(Qt::BottomToolBarArea);
}

