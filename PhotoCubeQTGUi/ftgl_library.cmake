# FTGL Library and Include directories
FIND_LIBRARY(FTGL_LIBRARY NAMES ftgl PATHS /usr/lib /usr/lib64 /usr/local/lib /usr/local/lib64)
FIND_PATH(FTGL_INCLUDE_DIR ftgl.h PATHS /usr/local/include/FTGL /usr/local/include/freetype2 /usr/include/FTGL)

IF(FTGL_INCLUDE_DIR AND FTGL_LIBRARY)
    SET(FTGL_FOUND TRUE)
ENDIF(FTGL_INCLUDE_DIR AND FTGL_LIBRARY)
 
IF(FTGL_FOUND)
    MESSAGE(STATUS "Found FTGL\n\tinclude dir.:${FTGL_INCLUDE_DIR}\n\tlibrary path: ${FTGL_LIBRARY} ")
ELSE(FTGL_FOUND)
    MESSAGE(FATAL_ERROR "Could not find FTGL\n\tinclude dir.:${FTGL_INCLUDE_DIR}\n\tlibrary path: ${FTGL_LIBRARY} ")
ENDIF(FTGL_FOUND)
