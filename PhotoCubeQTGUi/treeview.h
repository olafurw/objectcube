#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <vector>
#include <algorithm>
#include <QWidget>
#include <QTreeWidget>

#include "maincontroller.h"
#include "treeitem.h"
#include "common.h"

using namespace ObjectCube;

class MainController;

class TreeView: public QTreeWidget
{
    Q_OBJECT
public:
    TreeView(QWidget *parent);    


    //Actions available when tree items are right clicked
    QAction *tv_filter;
    QAction *tv_addTag;
    QAction *tv_addTagSet;
    QAction *tv_addHierarchy;
    QAction *tv_setAsFront;
    QAction *tv_setAsBack;
    QAction *tv_setAsUp;
    QAction *tv_removeFilter;
    QAction *tv_removeDimension;
    QAction *tv_delete;
    QAction *tv_viewSlides;

};

#endif // TREEVIEW_H
