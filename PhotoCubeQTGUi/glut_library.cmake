# glut libraries and include directories
FIND_LIBRARY(GLUT_LIBRARY NAMES glut PATHS /usr/lib /usr/lib64 /usr/local/lib /usr/local/lib64 /opt/X11/lib)
FIND_PATH(GLUT_INCLUDE_DIR glut.h PATHS /usr/local/include/GL /usr/include/GL /opt/X11/include/GL)

IF(GLUT_INCLUDE_DIR AND GLUT_LIBRARY)
	SET(GLUT_FOUND TRUE)
ENDIF(GLUT_INCLUDE_DIR AND GLUT_LIBRARY)
 
IF(GLUT_FOUND)
	MESSAGE(STATUS "Found GLUT\n\tinclude dir.:${GLUT_INCLUDE_DIR}\n\tlibrary path: ${GLUT_LIBRARY} ")
ELSE(GLUT_FOUND)
	MESSAGE(FATAL_ERROR "Could not find GLUT\n\tinclude dir.:${GLUT_INCLUDE_DIR}\n\tlibrary path: ${GLUT_LIBRARY} ")
ENDIF(GLUT_FOUND)

