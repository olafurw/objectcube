#ifndef PHOTOCUBE_H
#define PHOTOCUBE_H


#include <QMainWindow>
#include <QMessageBox>
#include <QInputDialog>

#include "maincontroller.h"
#include "toolbar.h"
#include "glwidget.h"
#include "treeview.h"
#include "listwidget.h"
#include "tablewidgetitem.h"
#include "common.h"
#include "ui_deleteitem.h"

//Debug includes
#include <iostream>
#include <sstream>

const int NUMBER_OF_AXES = 3;

namespace Ui {
    class PhotoCube;
}

class PhotoCube : public QMainWindow
{
    Q_OBJECT
public:
    MainController *mainController;
    TreeView *leftTree;
    TreeView *viewTags;
    GLWidget *glWidget;
    //ListWidget *listWidget;
    TreeView *filterList;
    QTableWidget *axisTable;

    QDockWidget *leftDock;
    QDockWidget *leftDockTags;
    QDockWidget *rightDock;
    QDockWidget *rightDockTable; 

    TableWidgetItem *frontTableItem;
    TableWidgetItem *upTableItem;
    TableWidgetItem *backTableItem;

    explicit PhotoCube(QWidget *parent = 0);
    void connectMenu();
    void connectRigthClickOptions();
    void keyPressEvent( QKeyEvent *e );
    void keyReleaseEvent( QKeyEvent *e );
    void setMenuItems();
    void setUpRightClickMenuForListItems();
    void setUpRightClickMenuForTreeItems();


    ~PhotoCube();

public slots:
	void addTagFilterToList();
	void clearView();
	void createHierarchy();
    void createTag();
    void createTagInTree();
    void createTagSet();
    void deleteTreeItem();
    void dropTaggingSlot(QDropEvent *event, int imageId);
    void dropTagCellSlot(QDropEvent*, vector<ObjectCube::Object> cellObjects);
    void exitProgram();
    void getHelp();
    void switchToCubeMode();
    void importFolder();
    void importImage();
    void infoAboutPhotoCube();
    void removeFilter();
    void setAsFront();
    void setAsUp();
    void setAsBack();
    void toggleAllWidgets();
    void toggleFilterList();
    void toggleTreeView();
    void treeItemDoubleClicked();
    void viewTagImagesAsSlides();
    void axisTableContextMenu( const QPoint& );
    void leftTreeContextMenu( const QPoint& );

private:
    Ui::PhotoCube *ui;
    void setupDockWidgets();
    void updateAxisTable();
    QMenu *menu;

    QAction *a_aboutPhotoCube;
    QAction *a_cardMode;
    QAction *a_clear;
    QAction *a_createHierarchy;
    QAction *a_createTag;
    QAction *a_createTagSet;
    QAction *a_cubeMode;
    QAction *a_exitProgram;
    QAction *a_help;
    QAction *a_import;
    QAction *a_importFolder;
    QAction *a_hideFilters;
    QAction *a_hideTreeView;
    QAction *a_toggleAllWidgets;
};

#endif // PHOTOCUBE_H
