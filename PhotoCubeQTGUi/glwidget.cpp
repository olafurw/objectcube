#include <QtGui>
#include <QtOpenGL>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include "dialogs/chooseplugin.h"
#include "glwidget.h"
#include "ui/ImageCacher.h"
#include "ui/TextureCacher.h"
#include "ImageData.h"
#include <ObjectCube/State/Cell.h>
#include "State/StateObject.h"
#include "ui/CubeImage.h"
#include "common.h"
#include "modes/modeManager.h"
#include "modes/modeData.h"


using namespace std;
using namespace cube;

// Constructor for the GLWidget. Configures OpenGL and QT
// for the widget.
GLWidget::GLWidget(QWidget *parent): QGLWidget(parent) {
    
    // Let the plug-in manager have instance of the widget
    // if modes need to UI informations from it, such as mouse
    // position within that widget.
    modes::modeManager::instance()->glWidget = this;

    glEnable(GL_DEPTH_TEST);
    
    this->m_need_repaint = false;
    this->setAcceptDrops(true);
    this->setMouseTracking(true);
    
    this->startTimer(0);
}

// Overridden initializeGL for GLWidget.
void GLWidget::initializeGL() {
    glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glRenderMode(GL_RENDER);
    this->mode = GL_RENDER;
}

void GLWidget::resizeGL(int width, int height) {
    
    this->w = width;
    this->h = height;

    if(modes::modeManager::instance()->hasConstructedMode()){
        modes::modeManager::instance()->getMode()->setWH(width, height);
    }

    // If we resize the window, we set the repaint bit to true
    // where we need the window to be repainted.
    //this->m_need_repaint = true;

    glViewport(0,0, width, height);
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    gluPerspective(45.,((GLfloat)width)/((GLfloat)height), 0.1f, 1000.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void GLWidget::paintGL() {
    // TODO: This cannot be here, but needed now to force buffer clean. Need to only check
    // if need to repaint pix.
    
    // Check if our paint bit is set, if so we paint
    glRenderMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    if(modes::modeManager::instance()->hasConstructedMode()){
        modes::modeManager::instance()->getMode()->draw();
    }
}

// Event handler for QTTimerEvent.
void GLWidget::timerEvent(QTimerEvent* event) {
    if(modes::modeManager::instance()->hasConstructedMode()){
        //modes::modeManager::instance()->getMode()->handleTimerTicks(event);
    }
    updateGL();
}

void GLWidget::dragMoveEvent(QDragMoveEvent *event) {
    event->accept();
}

void GLWidget::dragEnterEvent(QDragEnterEvent *event) {
    event->accept();
}

void GLWidget::draw(const ObjectCube::MultiDimensionalView* mdv, Coordinates* coordinates) {    
    this->clear();

    // Create plugin data for the selected plugin.
    modes::modeData* data = new modes::modeData(mdv, coordinates);

    // Construct the viewmode from the plugin.
    modes::modeManager::instance()->constructMode(modes::CUBEMODE, data);

    modes::modeManager::instance()->getMode()->setWH(this->width(), this->height());
    
    // Get the viewmode.
    //AbstractMode* viewmode = modes::modeManager::instance()->getMode();

    updateGL();
}

void GLWidget::clear(){
    if(modes::modeManager::instance()->hasConstructedMode()){
        modes::modeManager::instance()->clear();
    }
}

//Will probably be here. Will be implemented in each plugin seperatly
int GLWidget::getCurrentlyHoveredImage() {
    return 0;
}

//Will probably be here. Will be implemented in each plugin seperatly
int GLWidget::getClickedImage() {
    return 0;
}

//Will probably be here. Will be implemented in each plugin seperatly
int GLWidget::getClickedCell() {
    return 0;
}

void GLWidget::reset() {        
    //this->currentCube->camera.SetPosition(F3dVector(0.0f,0.0f,0.0f));
}

void GLWidget::mouseMoveEvent(QMouseEvent * e)
{
    if(modes::modeManager::instance()->hasConstructedMode())
    {
        modes::modeManager::instance()->getMode()->handleMouseMovedEvent(e);
    }  
}

void GLWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if(modes::modeManager::instance()->hasConstructedMode())
    {
        modes::modeManager::instance()->getMode()->handleMouseReleasedEvent(e);
    }   
}

void GLWidget::handleKeyEvent_up(QKeyEvent* e)
{
    if(modes::modeManager::instance()->hasConstructedMode())
    {
        modes::modeManager::instance()->getMode()->handleKeyBoardReleasedEvent(e);
    }
}

void GLWidget::handleKeyEvent(QKeyEvent* e)
{
    if(modes::modeManager::instance()->hasConstructedMode())
    {
        modes::modeManager::instance()->getMode()->handleKeyBoardPressedEvent(e);
    }
}

void GLWidget::wheelEvent(QWheelEvent *e)
{
    if(modes::modeManager::instance()->hasConstructedMode())
    {
        modes::modeManager::instance()->getMode()->handleMouseScroll(e);
    }
}

/**
 * The widget should display a dialog box that enables you to switch between modes,
 * regardless of what mode you are currently in.
 */
void GLWidget::mousePressEvent(QMouseEvent* e)
{
	auto currentMode = modes::modeManager::instance()->getCurrentMode();

	if(currentMode == modes::NOMODE)
	{
		return;
	}

	int image_id = -1;

	// if current mode is cube mode, we need to get clicked coordinates.
	if(currentMode == modes::CUBEMODE)
	{
		image_id = modes::modeManager::instance()->getMode()->getMouseHoverImageId( (GLdouble)e->x(), (GLdouble)e->y() );
	}

	if(image_id == -1)
	{
		return;
	}

	//Setup dialog box
	QStringList listOfModes;
	vector<std::string> availPlugins = modes::modeManager::instance()->getAvailableModes();

	for(unsigned int i = 0; i < availPlugins.size(); i++)
	{
		listOfModes.push_back(QString::fromStdString(availPlugins.at(i)));
	}

	QDialog *dialog = new QDialog;
	Ui::choosePlugin dialogUi;
	dialogUi.setupUi(dialog);
	dialogUi.comboBox->addItems(listOfModes);

	if(dialog->exec() == QDialog::Accepted) //will not execute if user hits cancel
	{
		string selectedMode = dialogUi.comboBox->currentText().toUtf8().constData();

		/*
		modes::MODES enumMode = modes::modeManager::instance()->convertStringToEnumType(selectedMode);
		modes::modeManager::instance()->setMode(enumMode);
		modes::modeManager::instance()->constructMode();
*/

		if(selectedMode == "Card")
		{
			modes::modeManager::instance()->getMode()->switchToAnotherMode( "Card", image_id);
		}
		else if(selectedMode == "Cube")
		{
			modes::modeManager::instance()->getMode()->switchToAnotherMode("Cube", image_id);
		}
		else if(selectedMode == "Slideshow")
		{
			modes::modeManager::instance()->getMode()->switchToAnotherMode("Slideshow",image_id);
		}
		else if(selectedMode == "Face recognition")
		{

		}
		else
		{
			//Do nothing???
		}

	}

    //Enable cube exploration
    if(e->button() == Qt::LeftButton)
    {
    	this->setFocus();
	}
}


