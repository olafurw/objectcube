/*
Project: hsimage
File: boundingbox.h
Developer: Hlynur Sigurþórsson (hlynursigurthorsson@gmail.com)
Timestamp: Fall, 2011.
*/

#include "boundingbox.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "../../ObjectCube/Hub.h"
#include "../../ObjectCube/TagSet/TagSet.h"
#include "../../ObjectCube/Tag/Tag.h"
#include "../../ObjectCube/Tag/AlphanumericalTag.h"
#include "../../ObjectCube/Object.h"
#include <math.h>

//#include "PluginWrapper.h"
#include "image.h"

using namespace std;
namespace hsimage
{

// Constructor for BoundingBox.
// Parameters: None.
// Functionality: Markes the bounding initially as "unconfirmed".
// Disables glowing initially (which might be set on mouseover).
// Marks the object as not deleted.
BoundingBox::BoundingBox(Image *image, double x, double y, double x_width, double y_width)
{
    this->glow = false;
    this->deleted = false;
    this->confirmed = false;
    this->isFromDB = false;
	this->lned_name = NULL;

    this->image = image;
	
	this->x = x;
	this->x_width = x_width;
	this->y = y;
	this->y_width = y_width;

	this->cmb_candidates = NULL;

	this->initOriginalBB();
}

void BoundingBox::initOriginalBB()
{
	double imageWidth, imageHeight;
	imageWidth = this->image->get_image_width();
	imageHeight = this->image->get_image_height();

	double uleftx = floor( (this->x/100.0) * imageWidth );
	double ulefty = floor( (this->y/100.0) * imageHeight ) ;
	double lrightx = floor( ((this->x_width)/100.0) * imageWidth );
	double lrighty = floor( ((this->y_width)/100.0) * imageHeight );

	ObjectCube::Coordinate2D upperLeft(uleftx, ulefty);
	ObjectCube::Coordinate2D lowerRight(lrightx, lrighty);

	this->originalBB = ObjectCube::BoundingBox();
	this->originalBB.setUpperLeftCorner(upperLeft);
	this->originalBB.setLowerRightCorner(lowerRight);
}

BoundingBox::BoundingBox(Image *image, ObjectCube::BoundingBox bb, ObjectCube::ObjectTag Tag)
{
    this->glow = false;
    this->deleted = false;
    this->confirmed = Tag.getConfirmed();
    this->isFromDB = true;
	this->lned_name = NULL;

	this->image = image;

	double imageWidth, imageHeight;
	imageWidth = this->image->get_image_width();
	imageHeight = this->image->get_image_height();

	this->x = bb.getUpperLeftCorner().getX();
	this->y = bb.getLowerRightCorner().getY();

	this->x_width = bb.getLowerRightCorner().getX();
	this->y_width = bb.getUpperLeftCorner().getY();

	this->cmb_candidates = NULL;

	this->originalBB = bb;
}

void BoundingBox::confirm()
{
    this->confirmed = true;
}

void BoundingBox::unconfirm()
{
    this->confirmed = false;
}

bool BoundingBox::isConfirmed()
{
    return this->confirmed;
}

void BoundingBox::setPersonName(string name)
{
    this->person_name = name;
}

void BoundingBox::setDbPersonName(string name)
{
    this->db_person_name = name;
}

void BoundingBox::addCandidate(string name)
{
	this->candidate_names.push_back(name);
}

void BoundingBox::show_dialog()
{
    setWindowTitle(tr("Bounding box edit"));
    
    // create label for drop down box.
    QLabel* l_box = new QLabel("Select person", this);
    
    // create drop down list with all the candidates.
    this->cmb_candidates = new QComboBox(this);
    QObject::connect(cmb_candidates, SIGNAL(currentIndexChanged(int)), this, SLOT(cmbCandidates(int)));
    
    // place the candidates into the combobox.
    for (unsigned int i = 0; i < this->candidate_names.size(); i += 1)
    {
        cmb_candidates->addItem( this->candidate_names.at(i).c_str() );
    }
    
    // If the bounding box has not been confirmed.
    if(!this->isConfirmed())
    {
        cmb_candidates->setCurrentIndex(-1);
    }
    
	lned_name = new QLineEdit("Name", this);
	lned_name->setText(QString(this->person_name.c_str()));

    // Create buttons.
    QPushButton *btn_save = new QPushButton("Save", this);
    QObject::connect(btn_save, SIGNAL(clicked()), this, SLOT(btnSavePress()));
    
    QPushButton *btn_cancel = new QPushButton("Cancel", this);
    QObject::connect(btn_cancel, SIGNAL(clicked()),this, SLOT(btnCancel()));
    
    QPushButton *btn_delete = new QPushButton("Delete", this);
    QObject::connect(btn_delete, SIGNAL(clicked()),this, SLOT(btnDelete()));

    // resize the dialog.
    this->resize(310, 150);
    
    // position the widgets on the screen.
    cmb_candidates->setGeometry(140, 20, 158, 30);
    l_box->setGeometry(20, 20, 270, 30);
	lned_name->setGeometry(20, 60, 150, 30);
    btn_save->setGeometry(20, 110, 80, 30);
    btn_cancel->setGeometry(105, 110, 80, 30);
    btn_delete->setGeometry(190, 110, 80, 30);
    this->exec();
}

void BoundingBox::cmbCandidates(int index)
{
	if(this->lned_name == NULL)
	{
		return;
	}

	QString text = cmb_candidates->itemText(index);

	if(index != -1)
	{
		lned_name->setText(text);
	}
}

void BoundingBox::btnSavePress()
{
	ObjectCube::BoundingBox();

	double imageWidth, imageHeight;
	imageWidth = this->image->get_image_width();
	imageHeight = this->image->get_image_height();

	this->person_name = lned_name->text().toStdString();
	this->addCandidate(this->person_name);

	string image_location = this->image->get_image_location();
	int image_size = this->image->get_image_size();

	//-------------------------------------
	fstream raw_image(image_location.c_str());

	raw_image.seekg(0,ios::end);
	image_size = raw_image.tellg();
	raw_image.seekg(ios::beg);

	char* image_data = new char[image_size];
	raw_image.read(image_data, image_size);
	//-------------------------------------

	ObjectCube::Hub* hub = ObjectCube::Hub::getHub();
	ObjectCube::TagSet* tagSet = hub->getTagSet("Person");
	const ObjectCube::Tag* tag = tagSet->fetchOrAddTag( person_name );

	// Remove the old tag
	image->objectCubeImage.removeTags( originalBB );

	// Create a new tag
	ObjectCube::ObjectTag newTag(tag, originalBB);

	// Add it to the database
	image->objectCubeImage.updateTagging( newTag, tag );
	confirm();

	raw_image.close();

	close();
}
    
void BoundingBox::btnCancel()
{
	close();
}

void BoundingBox::btnDelete()
{

}

bool BoundingBox::is_deleted()
{
	return this->deleted;
}

}
