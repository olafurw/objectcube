/*
Project: hsimage
File: image.cpp
Developer: Hlynur Sigurþórsson (hlynursigurthorsson@gmail.com)
Timestamp: Fall, 2011.
*/

#include "image.h"
#include "imagewidget.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#include "PluginWrapper.h"
#include "FaceRecognition/FaceRecognitionPlugin.h"
#include <Object.h>
#include <Plugin/PluginObject.h>
#include <Plugin/PluginTagging.h>
#include <Plugin/PluginReply.h>
#include <Tag/Tag.h>
#include <opencv2/opencv.hpp>
#include <fstream>

using namespace std;

Image::Image(string image_location)
{
    this->image_location = image_location;

    this->objectCubeImage = ObjectCube::Object::fetch(this->image_location);

    fstream raw_image(image_location.c_str());

    raw_image.seekg(0,ios::end);
    image_size = raw_image.tellg();
    raw_image.seekg(ios::beg);

    image_data = new char[image_size];
	raw_image.read(image_data, image_size);

	Mat img = imread(image_location,CV_LOAD_IMAGE_COLOR);

	image_height = img.rows;
    image_width = img.cols;
    search_for_faces();
}

Image::~Image()
{
	if(image_data != NULL)
	{
		delete image_data;
	}
	
}

string Image::get_name()
{
    return this->image_location;
}

int Image::search_for_faces()
{
	int num_bounding_boxes = this->objectCubeImage.getTags().size();
	const vector<ObjectCube::ObjectTag> imageTags = this->objectCubeImage.getTags();
	
    for(int i = 0; i < imageTags.size(); ++i)
    {
		const ObjectCube::ObjectTag tag = imageTags[i];
		
		// We ignore tags that have no "interesting" bounding boxes
		if( tag.getBoundingBox().getUpperLeftCorner().getX() == 0
		 && tag.getBoundingBox().getUpperLeftCorner().getY() == 100
		 && tag.getBoundingBox().getLowerRightCorner().getX() == 100
		 && tag.getBoundingBox().getLowerRightCorner().getY() == 0 )
		{
			continue;
		}

        hsimage::BoundingBox* b = new hsimage::BoundingBox( this, tag.getBoundingBox(), tag);
		b->addCandidate(imageTags[i].getTag()->valueAsString());

		if(tag.getConfirmed())
		{
			b->confirm();
		}

        this->add_bounding_box(b);
    }
    
    // Return the number of bounding box found.
    return num_bounding_boxes;
}

vector<hsimage::BoundingBox*>& Image::get_bounding_boxes()
{
    return this->boundingBoxes;
}

void Image::add_bounding_box(hsimage::BoundingBox* bb)
{
    this->boundingBoxes.push_back(bb);  
}

int Image::show_in_edit_dialog()
{
    setWindowTitle(tr(this->image_location.c_str()));
    QVBoxLayout *vbox = new QVBoxLayout(this);
    ImageWidget* iw = new ImageWidget(this);
    iw->show_image(this);
    vbox->addWidget(iw);
    setLayout(vbox);
    this->resize(800, 600);
    
    this->exec();
    delete iw;
    delete vbox;
    return 0;
}
