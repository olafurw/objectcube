#ifndef CREATETAGUI_H
#define CREATETAGUI_H

#include <QDialog>

namespace Ui {
    class createTagUi;
}

class createTagUi : public QDialog
{
    Q_OBJECT

public:
    explicit createTagUi(QWidget *parent = 0);
    void initializeComboBox();
    ~createTagUi();

private:
    Ui::createTagUi *ui;
};

#endif // CREATETAGUI_H
