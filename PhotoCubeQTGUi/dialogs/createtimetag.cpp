#include "createtimetag.h"
#include "ui_createtimetag.h"

CreateTimeTag::CreateTimeTag(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateTimeTag)
{
    ui->setupUi(this);
    this->setWindowTitle("Create Tag");
}

CreateTimeTag::~CreateTimeTag()
{
    delete ui;
}
