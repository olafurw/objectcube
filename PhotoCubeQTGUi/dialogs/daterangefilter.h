#ifndef DATERANGEFILTER_H
#define DATERANGEFILTER_H

#include <QDialog>

namespace Ui {
    class DateRangeFilter;
}

class DateRangeFilter : public QDialog
{
    Q_OBJECT

public:
    explicit DateRangeFilter(QWidget *parent = 0);
    ~DateRangeFilter();

private:
    Ui::DateRangeFilter *ui;
};

#endif // DATERANGEFILTER_H
