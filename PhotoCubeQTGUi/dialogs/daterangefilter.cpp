#include "daterangefilter.h"
#include "ui_daterangefilter.h"

DateRangeFilter::DateRangeFilter(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DateRangeFilter)
{
    ui->setupUi(this);
    this->setWindowTitle("Set range");
}

DateRangeFilter::~DateRangeFilter()
{
    delete ui;
}
