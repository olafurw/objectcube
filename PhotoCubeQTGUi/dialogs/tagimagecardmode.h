#ifndef TagImageCardMode_H
#define TagImageCardMode_H

#include <QDialog>
#include <QString>
#include <QListWidget>
#include <QInputDialog>
#include <QButtonGroup>

#include <Hub.h>
#include <TagSet/TagSet.h>
#include <Tag/Tag.h>
#include <Tag/AlphanumericalTag.h>

#include "DataAccess/Factories/ObjectDataAccessFactory.h"
#include "DataAccess/Root/ObjectDataAccess.h"
#include "Converters/ObjectConverter.h"
#include "Converters/ObjectTagConverter.h"

#include "ui_createtaguicardmode.h"

class TagImageCardMode : public QObject
{
    Q_OBJECT
public:
    TagImageCardMode(const std::string& nameOfImage);

private slots:
    void changeTagBox(int index);
    void tagImage();
    void closeWindow();

private:
    QButtonGroup* group;
    QDialog* dialog;
    Ui::createTagUiCardMode tagPictureDialog;
    ObjectCube::Hub* hub;
    std::string name;
};

#endif
