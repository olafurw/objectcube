#include "numericalrangefilter.h"
#include "ui_numericalrangefilter.h"

NumericalRangeFilter::NumericalRangeFilter(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NumericalRangeFilter)
{
    ui->setupUi(this);
    this->setWindowTitle("Set range");
}

NumericalRangeFilter::~NumericalRangeFilter()
{
    delete ui;
}
