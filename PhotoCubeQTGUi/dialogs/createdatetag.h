#ifndef CREATEDATETAG_H
#define CREATEDATETAG_H

#include <QDialog>

namespace Ui {
    class CreateDateTag;
}

class CreateDateTag : public QDialog
{
    Q_OBJECT

public:
    explicit CreateDateTag(QWidget *parent = 0);
    ~CreateDateTag();

private:
    Ui::CreateDateTag *ui;
};

#endif // CREATEDATETAG_H
