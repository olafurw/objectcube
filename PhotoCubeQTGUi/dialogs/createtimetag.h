#ifndef CREATETIMETAG_H
#define CREATETIMETAG_H

#include <QDialog>

namespace Ui {
    class CreateTimeTag;
}

class CreateTimeTag : public QDialog
{
    Q_OBJECT

public:
    explicit CreateTimeTag(QWidget *parent = 0);
    ~CreateTimeTag();

private:
    Ui::CreateTimeTag *ui;
};

#endif // CREATETIMETAG_H
