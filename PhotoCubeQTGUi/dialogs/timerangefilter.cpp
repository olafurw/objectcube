#include "timerangefilter.h"
#include "ui_timerangefilter.h"

TimeRangeFilter::TimeRangeFilter(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimeRangeFilter)
{
    ui->setupUi(this);
    this->setWindowTitle("Set range");
}

TimeRangeFilter::~TimeRangeFilter()
{
    delete ui;
}
