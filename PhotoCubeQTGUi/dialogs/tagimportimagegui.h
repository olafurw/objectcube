#ifndef TagImportImageGui_H
#define TagImportImageGui_H

#include <QDialog>
#include <QString>
#include <QListWidget>
#include <iostream>
#include <vector>
#include <algorithm>
#include <Hub.h>
#include <TagSet/TagSet.h>
#include <Tag/Tag.h>
#include <LayerShared/SharedDefinitions.h>

#include "ui_tagimportimage.h"

class TagImportImageGui : public QObject
{
    Q_OBJECT
public:
    TagImportImageGui();

    void addImage(const QString& image);

    void show();

    int count();

signals:
    void tagged();

private slots:
	void changeTagBox(int index);
    void addTag();
    void tagImages();
    void removeTag();
    void closeWindow();

private:
    QDialog* m_dialog = new QDialog;
    Ui::TagImportImage m_tagImages;
    std::vector<int> m_tagIdsToApply;
    std::vector<const ObjectCube::Tag*> m_tagsToApply;
    std::vector<ObjectCube::Object*> m_objects;
    ObjectCube::Hub* m_hub;
};

#endif
