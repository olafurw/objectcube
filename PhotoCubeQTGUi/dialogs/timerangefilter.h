#ifndef TIMERANGEFILTER_H
#define TIMERANGEFILTER_H

#include <QDialog>

namespace Ui {
    class TimeRangeFilter;
}

class TimeRangeFilter : public QDialog
{
    Q_OBJECT

public:
    explicit TimeRangeFilter(QWidget *parent = 0);
    ~TimeRangeFilter();

private:
    Ui::TimeRangeFilter *ui;
};

#endif // TIMERANGEFILTER_H
