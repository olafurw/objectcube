#ifndef CHOOSEPLUGIN_H
#define CHOOSEPLUGIN_H

#include <QDialog>

#include "ui_chooseplugin.h"

class choosePlugin : public QDialog
{
    Q_OBJECT
    
public:
    explicit choosePlugin(QWidget* parent = 0);
    ~choosePlugin();
    
private:
    Ui_choosePlugin *ui;
};

#endif // CHOOSEPLUGIN_H
