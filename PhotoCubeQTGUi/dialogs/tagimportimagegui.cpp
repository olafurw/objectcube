#include "tagimportimagegui.h"

TagImportImageGui::TagImportImageGui()
{
    m_hub = ObjectCube::Hub::getHub();

    m_dialog = new QDialog;
    m_tagImages.setupUi(m_dialog);

    // Populate the tagset combo box, this does not change in this lifetime
    std::vector<ObjectCube::TagSet*> allTagSets = m_hub->getTagSets();
    for(auto tagset : allTagSets)
    {
        m_tagImages.conceptComboBox->addItem(QString(tagset->getName().c_str()), QVariant(tagset->getId()));
    }

    // Set the tag box to the first concept value (usually uncategorized)
    changeTagBox(0);

    connect(m_tagImages.conceptComboBox, SIGNAL(activated(int)), this, SLOT(changeTagBox(int)));
    connect(m_tagImages.addTagButton, SIGNAL(released()), this, SLOT(addTag()));
    connect(m_tagImages.buttonBox, SIGNAL(accepted()), this, SLOT(tagImages()));
    connect(m_tagImages.buttonBox, SIGNAL(rejected()), this, SLOT(closeWindow()));
    connect(m_tagImages.removeTag, SIGNAL(clicked()), this, SLOT(removeTag()));
}

void TagImportImageGui::addImage(const QString& image)
{
    m_tagImages.imageList->addItem(image);

    ObjectCube::Object* obj = new ObjectCube::Object(image.toUtf8().constData());
    m_objects.push_back(obj);
}

void TagImportImageGui::changeTagBox(int index)
{
	// Someone changed the tagset combo box, so we need to change the data in the tag combo box
	int value = m_tagImages.conceptComboBox->itemData(index).toInt();

	// Clear it first, before we add the new data
	m_tagImages.tagComboBox->clear();

	// Get the tags that should be added, and then place them into the combo box
    std::vector<ObjectCube::Tag*> tags = m_hub->getTagsByTagSet(value);
    for(auto tag : tags)
    {
        m_tagImages.tagComboBox->addItem(QString(tag->valueAsString().c_str()), QVariant(tag->getId()));
    }
}

void TagImportImageGui::addTag()
{
    int current = m_tagImages.tagComboBox->currentIndex();

    // Combo box is empty
    if(current == -1)
    {
    	return;
    }

    int value = m_tagImages.tagComboBox->itemData(current).toInt();
    QString text = m_tagImages.tagComboBox->itemText(current);

    // Only allow adding a tag once to the list
    if(std::find(m_tagIdsToApply.begin(), m_tagIdsToApply.end(), value) == m_tagIdsToApply.end())
    {
        m_tagIdsToApply.push_back(value);
        m_tagsToApply.push_back(m_hub->getTag(value));

        // To get the concept text to put on the tagList
        int conceptId = m_tagImages.conceptComboBox->currentIndex();
        QString conceptText = m_tagImages.conceptComboBox->itemText(conceptId);
        QListWidgetItem* item = new QListWidgetItem(conceptText + ": " + text);
        item->setData(Qt::UserRole, QVariant(value));

        m_tagImages.tagList->addItem(item);
    }
}

void TagImportImageGui::removeTag()
{
    QListWidgetItem* toRemove = m_tagImages.tagList->currentItem();
    int value = toRemove->data(Qt::UserRole).toInt();

    auto itr = std::find(m_tagIdsToApply.begin(), m_tagIdsToApply.end(), value);

    // Remove only if we find it
    if(itr != m_tagIdsToApply.end())
    {
        int index = itr - m_tagIdsToApply.begin();

        // Remove from the taglist
        m_tagImages.tagList->takeItem(m_tagImages.tagList->currentIndex().row());
        delete toRemove;

        // Remove from the helper lists
        m_tagIdsToApply.erase(m_tagIdsToApply.begin() + index);
        m_tagsToApply.erase(m_tagsToApply.begin() + index);
    }
}

void TagImportImageGui::tagImages()
{
	// For each object, go through the tags list and add those tags to the object
    for(auto obj : m_objects)
    {
    	if(obj->getId() == ObjectCube::INVALID_VALUE)
    	{
    		obj->create();
    	}

        for(auto tag : m_tagsToApply)
        {
            ObjectCube::ObjectTag ot( tag );
            obj->addTag(ot);
        }
    }

    // Add the objects to be tagged by the plugins
    for(auto obj : m_objects)
    {
    	m_hub->addObject(*obj);
    }

    // Done, we let the parent know
    emit tagged();

    m_dialog->close();
}

void TagImportImageGui::show()
{
    m_dialog->show();
}

void TagImportImageGui::closeWindow()
{
    m_dialog->close();
}

int TagImportImageGui::count()
{
    return m_objects.size();
}
