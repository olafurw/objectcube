#include "chooseplugin.h"

choosePlugin::choosePlugin(QWidget* parent) :
    QDialog(parent),
    ui(new Ui_choosePlugin)
{
    ui->setupUi(this);
    this->setWindowTitle("Mode selector");
}

choosePlugin::~choosePlugin()
{
    delete ui;
}
