#include "createdatetag.h"
#include "ui_createdatetag.h"

CreateDateTag::CreateDateTag(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateDateTag)
{
    ui->setupUi(this);
    this->setWindowTitle("Create Tag");
}

CreateDateTag::~CreateDateTag()
{
    delete ui;
}
