#include "tagimagecardmode.h"

TagImageCardMode::TagImageCardMode(const std::string& nameOfImage)
{
    name = nameOfImage;

    hub = ObjectCube::Hub::getHub();

    //Create new dialogbox
    group = new QButtonGroup();
    dialog = new QDialog;
    tagPictureDialog.setupUi(dialog);

    // Make the two radio buttons group together
    group->addButton(tagPictureDialog.choiceButton1);
    group->addButton(tagPictureDialog.choiceButton2);
    tagPictureDialog.choiceButton1->setChecked(true);

    // When selecting the bottom combo box, the bottom tag box is populated
    connect(tagPictureDialog.conceptComboBox2, SIGNAL(activated(int)), this, SLOT(changeTagBox(int)));
    connect(tagPictureDialog.buttonBox, SIGNAL(accepted()), this, SLOT(tagImage()));
    connect(tagPictureDialog.buttonBox, SIGNAL(rejected()), this, SLOT(closeWindow()));

    for(auto tagset : hub->getTagSets())
    {
        if(tagset->getTypeId() == 1)
        {
            tagPictureDialog.conceptComboBox1->addItem(QString(tagset->getName().c_str()), QVariant(tagset->getId()));

        }
        tagPictureDialog.conceptComboBox2->addItem(QString(tagset->getName().c_str()), QVariant(tagset->getId()));
    }

    for(auto tag : hub->getAllTags())
    {
        // 1 is the value for alphanumerical tags
        // TODO: For the default built in tag types, create an enum for christs sake!
        if( tag->getTypeId() == 1 )
        {
            ObjectCube::AlphanumericalTag* atag = static_cast<ObjectCube::AlphanumericalTag*>(tag);

            tagPictureDialog.tagComboBox->addItem(QString::fromStdString(atag->getName()), QVariant(atag->getId()));
        }
    }

    dialog->show();
}

void TagImageCardMode::changeTagBox(int index)
{
    // Someone changed the tagset combo box, so we need to change the data in the tag combo box
    int value = tagPictureDialog.conceptComboBox2->itemData(index).toInt();

    // Clear it first, before we add the new data
    tagPictureDialog.tagComboBox->clear();

    // Get the tags that should be added, and then place them into the combo box
    std::vector<ObjectCube::Tag*> tags = hub->getTagsByTagSet(value);
    for(auto tag : tags)
    {
        tagPictureDialog.tagComboBox->addItem(QString(tag->valueAsString().c_str()), QVariant(tag->getId()));
    }
}

void TagImageCardMode::tagImage()
{
    ObjectCube::Object objToTag;
    bool objectFound = false;

    // Locate image object
    // TODO Refactor this to use filters to fetch a single object. Might need to add to filters to do this.
    auto_ptr<ObjectCube::ObjectDataAccess> dataAccess( ObjectCube::ObjectDataAccessFactory::create() );
    vector<ObjectCube::ObjectDataAccess*> objectsDA = dataAccess->fetchAllObjects();
    vector<ObjectCube::Object> objects = ObjectCube::ObjectConverter::dataAccessToLogic( objectsDA );

    for( auto object : objects)
    {
        if (object.getName() == name)
        {
            objToTag = object;
            objectFound = true;
            break;
        }
    }

    if(!objectFound)
    {
        dialog->close();
        return;
    }

    // Create and tag
    if(tagPictureDialog.choiceButton1->isChecked())
    {
        string boxText = tagPictureDialog.tagText->text().toStdString();
        int conceptIndex = tagPictureDialog.conceptComboBox1->currentIndex();
        if(conceptIndex == -1)
        {
            return;
        }

        int conceptId = tagPictureDialog.conceptComboBox1->itemData(conceptIndex).toInt();

        ObjectCube::TagSet* ts = hub->getTagSet(conceptId);

        // Add the tag to the tagset and create it as well.
        ObjectCube::AlphanumericalTag* ant = new ObjectCube::AlphanumericalTag(boxText);
        const ObjectCube::Tag* correctTag = ts->addTag(ant);

        // Tag the object
        ObjectCube::ObjectTag ot(correctTag);
        objToTag.addTag(ot);
    }
    // Only tag, it is already created
    else if(tagPictureDialog.choiceButton2->isChecked())
    {
        int tagIndex = tagPictureDialog.tagComboBox->currentIndex();
        if(tagIndex == -1)
        {
            return;
        }

        int tagId = tagPictureDialog.tagComboBox->itemData(tagIndex).toInt();

        // Tag already exists
        auto tag = hub->getTag(tagId);

        // Tag the object
        ObjectCube::ObjectTag ot(tag);
        objToTag.addTag(ot);
    }

    dialog->close();
}

void TagImageCardMode::closeWindow()
{
    dialog->close();
}
