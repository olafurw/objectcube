#include "createtagui.h"
#include "ui_createtagui.h"

createTagUi::createTagUi(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::createTagUi)
{
    ui->setupUi(this);
    this->setWindowTitle("Create Tag");
}

createTagUi::~createTagUi()
{
    delete ui;
}
