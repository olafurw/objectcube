#ifndef DELETEITEM_H
#define DELETEITEM_H

#include <QDialog>

namespace Ui {
    class deleteItem;
}

class deleteItem : public QDialog
{
    Q_OBJECT

public:
    explicit deleteItem(QWidget *parent = 0);
    ~deleteItem();

private:
    Ui::deleteItem *ui;
};

#endif // DELETEITEM_H
