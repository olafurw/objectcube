#ifndef CREATEHIERARCHY_H
#define CREATEHIERARCHY_H

#include <QDialog>

namespace Ui {
    class CreateHierarchy;
}

class CreateHierarchy : public QDialog
{
    Q_OBJECT

public:
    explicit CreateHierarchy(QWidget *parent = 0);
    ~CreateHierarchy();

private:
    Ui::CreateHierarchy *ui;
};

#endif // CREATEHIERARCHY_H
