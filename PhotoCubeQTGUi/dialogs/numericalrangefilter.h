#ifndef NUMERICALRANGEFILTER_H
#define NUMERICALRANGEFILTER_H

#include <QDialog>

namespace Ui {
    class NumericalRangeFilter;
}

class NumericalRangeFilter : public QDialog
{
    Q_OBJECT

public:
    explicit NumericalRangeFilter(QWidget *parent = 0);
    ~NumericalRangeFilter();

private:
    Ui::NumericalRangeFilter *ui;
};

#endif // NUMERICALRANGEFILTER_H
