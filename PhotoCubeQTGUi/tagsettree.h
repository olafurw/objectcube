#ifndef TAGSETTREE_H
#define TAGSETTREE_H

#include <QTreeWidgetItem>
#include <TagSet/TagSet.h>

using namespace ObjectCube;

class TagSetTree: public QTreeWidgetItem
{
public:
    TagSetTree();
    TagSet *tagSet;
};

#endif // TAGSETTREE_H
