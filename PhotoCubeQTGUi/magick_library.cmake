find_package(ImageMagick COMPONENTS Magick++ REQUIRED)
message(STATUS "Found Image Magick C++\n\tinclude dir.:${ImageMagick_INCLUDE_DIRS}\n\tlibrary path: ${ImageMagick_LIBRARIES} ")
# mandatory options for newer ImageMagick versions, these probably match the old defaults
add_definitions(-DMAGICKCORE_QUANTUM_DEPTH=16 -DMAGICKCORE_HDRI_ENABLE=0)
