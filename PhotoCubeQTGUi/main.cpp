#include <QtGui/QApplication>
#include <QGridLayout>
#include "photocube.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
    glutInit(&argc, argv);

    QApplication a(argc, argv);
    PhotoCube w;
    try
    {
        w.resize(1240,800);
        w.show();

        return a.exec();
    }
    catch(exception &e)
    {
        cout << e.what() << endl;
    }
}
