#ifndef TABLEWIDGETITEM_H
#define TABLEWIDGETITEM_H
#include <QTableWidgetItem>
#include "common.h"

class TableWidgetItem : public QTableWidgetItem
{
public:
    TableWidgetItem();
    void *item;
    TREE_ITEM_TYPES type;

    TREE_ITEM_TYPES getType();
};

#endif // TABLEWIDGETITEM_H
