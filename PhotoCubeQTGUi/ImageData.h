#ifndef IMAGEDATA_H
#define IMAGEDATA_H

namespace common
{

class ImageData
{
public:
	unsigned int width;
	unsigned int height;
	unsigned char* data;
	const char* image_name;

	ImageData()
	{
		// Initalize all the member variables.
		this->width = 0;
		this->height = 0;
		this->data = nullptr;
		this->image_name = nullptr;
	}
};

}
#endif
