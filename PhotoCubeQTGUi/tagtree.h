#ifndef TAGTREE_H
#define TAGTREE_H

#include <QTreeWidgetItem>
#include <Tag/Tag.h>

using namespace ObjectCube;

class TagTree: public QTreeWidgetItem
{
public:
    TagTree();
    Tag* tag;

};

#endif // TAGTREE_H
