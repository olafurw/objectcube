# opencv2 libraries and include directories
FIND_LIBRARY(OCV_LIBRARY NAMES opencv_core opencv_highgui PATHS /usr/lib /usr/lib64 /usr/local/lib /usr/local/lib64 /usr/local/Cellar/opencv/2.4.0/lib)
FIND_PATH(OCV_INCLUDE_DIR opencv2/opencv.hpp PATHS /usr/local/include/GL /usr/include/GL /usr/local/Cellar/opencv/2.4.0/include)

IF(OCV_INCLUDE_DIR AND OCV_LIBRARY)
	SET(OCV_FOUND TRUE)
ENDIF(OCV_INCLUDE_DIR AND OCV_LIBRARY)
 
IF(OCV_FOUND)
	MESSAGE(STATUS "Found opencv2\n\tinclude dir.:${OCV_INCLUDE_DIR}\n\tlibrary path: ${OCV_LIBRARY} ")
ELSE(OCV_FOUND)
	MESSAGE(FATAL_ERROR "Could not find opencv2\n\tinclude dir.:${OCV_INCLUDE_DIR}\n\tlibrary path: ${OCV_LIBRARY} ")
ENDIF(OCV_FOUND)
