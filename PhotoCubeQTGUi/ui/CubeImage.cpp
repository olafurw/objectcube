#include "ui/CubeImage.h"
#include "ui/TextureCacher.h"
#include "common.h"
#include <string>
#include <iostream>

using namespace std;

ui::CubeImage::CubeImage(std::string imageNameString, int i, int objectCubeId)
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
    this->width = 1.5;
    this->height = 1.5;
    this->id = i;
    this->cellIndex = 0;
    this->textureId = -1;
    this->imageName = imageNameString.c_str();
    this->objectCubeId = objectCubeId;
    this->hasSearchedForTexture = false;
}

void ui::CubeImage::draw(GLenum mode)
{
	// if we have not loaded the texture
	if (textureId == -1){
		//load the image

		textureId = ui::TextureCacher::Instance()->getTexture(imageName);
	}

	//render image on screen
    glBindTexture(GL_TEXTURE_2D, textureId);
    
    float x_1 = (0.2 + this->width) * this->x; // 0.2 is image space.
    float x_2 = x_1 + this->width;
    float y_1 = (0.2 + this->height) * this->y; // 0.2 is image space.
    float y_2 = y_1 + this->height;
    
    float z = ( (this->z * 1.5) * -1) - this->cellIndex * (0.03) ;

    if(mode == GL_SELECT)
    {
    	glPushName(this->id);
    }
        
    glBegin(GL_QUADS);
       //Draw our four points, clockwise.
       glTexCoord2f(0, 0); glVertex3f(x_1, y_2, z);
       glTexCoord2f(1, 0); glVertex3f(x_2, y_2, z);
       glTexCoord2f(1, 1); glVertex3f(x_2, y_1, z);
       glTexCoord2f(0, 1); glVertex3f(x_1, y_1, z);
    glEnd();
}

void ui::CubeImage::setPos(GLuint x, GLuint y, GLuint z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

void ui::CubeImage::setSize(GLfloat h, GLfloat w) {
    this->height = h;
    this->width = w;
}

void ui::CubeImage::setCellIndex(int cellIndex) {
    this->cellIndex = cellIndex;
}

int ui::CubeImage::getCellindex() {
    return this->cellIndex;
}

std::string ui::CubeImage::getCellKey(){
    std::string cellkey = std::to_string(this->x) + ":" + std::to_string(this->y) + ":" + std::to_string(this->z);
    return cellkey;
}

