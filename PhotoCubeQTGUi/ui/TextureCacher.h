#ifndef TEXTURECACHER_H
#define TEXTURECACHER_H

#include <QtOpenGL>
#include <map>

namespace ui {

class TextureCacher {


public:
	static TextureCacher* Instance();

	// Clears all textures that have been allocated in OpenGL
	// and clears the cache of the textures.
	void clearAllTextures();

	// Returns the texture id for a given image. If the texture
	// has not been loaded in OpenGL, it is loaded and cached.
	const GLint& getTexture(std::string imageName);

private:
   
	std::map<std::string, GLint> m_textureContainer;

	// Private so that it can  not be called
	TextureCacher(){};
   
	// copy constructor is private
	TextureCacher(TextureCacher const&){};
	void loadTexture(std::string imageName/*, GLint& textureId*/);
   
	static TextureCacher* m_pInstance;

};

}
#endif
