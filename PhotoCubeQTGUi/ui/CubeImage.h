#ifndef IMAGE_H
#define IMAGE_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <glut.h>
#endif

#include <string>

namespace ui {

class CubeImage {
	private: 
    // Location of the image.
    GLuint x, y, z;
    GLfloat height, width;
    GLint textureId;
    const char* imageName;

    bool hasSearchedForTexture;

    int id;
    int cellIndex;
    int objectCubeId;

	public:
    // Constructor for the image class. This constructors
    // accepts string of image name.
    CubeImage(std::string imageName, int i, int objectCubeId);
    
    // Getter function for the image name.
    std::string getName()
    {
        return this->imageName;
    }

    // Draw creates a GL_QUADS placed at the location that is
    // set in the x,y,z member variables. The Quad that is 
    // drawn will contain the texture of the image.
    void draw(GLenum mode);

    // Sets the position of the image. This must be set
    // Before the image is drawn.
    void setPos(GLuint x, GLuint y, GLuint z);
    
    // Returns the cell key that this image is part of.
    std::string getCellKey();

    void setSize(GLfloat h, GLfloat w);
    void setCellIndex(int);
    int getCellindex();

    // Getter for the texture id that belongs to the image instance.
    GLuint getTextureId(){
        return this->textureId;
    }
};
}
#endif
