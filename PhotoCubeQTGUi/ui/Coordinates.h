#ifndef COORDINATES_H
#define COORDINATES_H

//Struct to glWidget so it knows which axis is set
struct Coordinates
{
    bool x; //front
    bool y; //up
    bool z; //back

    Coordinates()
    {
    	x = false;
    	y = false;
    	z = false;
    }

    Coordinates(bool ax, bool ay, bool az)
    {
    	x = ax;
    	y = ay;
    	z = az;
    }
};

#endif // COORDINATES_H
