#include "ImageCacher.h"
#include "../ImageData.h"

#include "common.h"

#include <iostream>
#include <Magick++.h>
#include <cstdio>
#include <string.h>
#include <stdlib.h>

namespace ui {

// Global static pointer used to ensure a single instance of the class.
ImageCacher* ImageCacher::m_pInstance = 0;  

ImageCacher* ImageCacher::Instance() {
   // Only allow one instance of class to be generated.
   if (!m_pInstance)   
      m_pInstance = new ImageCacher();

   return m_pInstance;
}

common::ImageData* ImageCacher::getImage(string file_name) {

    if(this->imageCache.find(file_name) != this->imageCache.end())
    {

        return this->imageCache[file_name];
    }

    common::ImageData* fileData = this->load_image(file_name.c_str());

    if(fileData != NULL)
    {
    	this->imageCache[file_name] = fileData;
    	return this->imageCache[file_name];
    }

    return nullptr;
}

common::ImageData* ImageCacher::load_image(const char *filename)
{
	common::ImageData* returnData = new common::ImageData();

	Magick::Image image(filename);
	image.resize("800x>");
	image.magick("RGBA");

	Magick::Blob* blob = new Magick::Blob();
	image.write(blob);

	returnData->image_name = filename;
	returnData->width = image.columns();
	returnData->height = image.rows();
	returnData->data = (unsigned char*)blob->data();

	return returnData;
}

}
