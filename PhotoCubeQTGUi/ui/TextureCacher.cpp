#include "TextureCacher.h"
#include <iostream>
#include <string>
#include "common.h"
#include "ui/ImageCacher.h"
#include "ImageData.h"

#include <QtOpenGL>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <glut.h>
#endif


namespace ui {

// Global static pointer used to ensure a single instance of the class.
TextureCacher* TextureCacher::m_pInstance = 0;  


TextureCacher* TextureCacher::Instance() {
   // Only allow one instance of class to be generated.
   if (!m_pInstance)   
      m_pInstance = new TextureCacher();
   return m_pInstance;
}

void TextureCacher::clearAllTextures() {
	this->Instance()->m_textureContainer.clear();
}

const GLint& TextureCacher::getTexture(string imageName)
{
	// find the image in container
	auto it = this->m_textureContainer.find(imageName);
	// const GLint textureId = it->second;

	// if the texture is not in Container, add it
	if (it == this->m_textureContainer.end() )
	{
		loadTexture(imageName);
	}

	return m_textureContainer[imageName];

}

void TextureCacher::loadTexture(string imageName /*, GLint& textureId*/)
{
	// Load the image to raw buffer.
	common::ImageData* d = ui::ImageCacher::Instance()->getImage(imageName);

	if(d == NULL)
	{
		// TODO: ÁE throw exception
	}

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, d->width, d->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, d->data);

	// Add texture to the map
	m_textureContainer[imageName] = texture;

	return;
}

} // namespace end.
