#ifndef IMAGECACHER_H
#define IMAGECACHER_H

#include <map>
#include <string>

#include "../ImageData.h"

using namespace std;

namespace ui {

// ImageCacher is a singleton class that caches raw image data
// in memory. This class has nothing to do with the Image objects
// that are placed on the screen, it only contains raw image data.
class ImageCacher {

private:
	std::map<std::string, common::ImageData*> imageCache;

   // The constructor is private.
   ImageCacher(){};  
   
	// copy constructor is private
   ImageCacher(ImageCacher const&){};
   
   // assignment operator is private
   ImageCacher& operator=(ImageCacher const&) = delete;  // Assignment is not allowed, since we use singleton pattern
   static ImageCacher* m_pInstance;

   common::ImageData* load_image(const char *filename);


public:
	static ImageCacher* Instance();
	common::ImageData* getImage(std::string file_name);

};

}
#endif
