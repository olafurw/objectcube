#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QInputDialog>
#include <QStringList>
#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QStringBuilder>
#include <QProgressDialog>

//ObjectCube includes
#include <Hub.h>
#include <TagSet/TagSet.h>
#include <TagSet/AlphanumericalTagSet.h>
#include <TagSet/NumericalTagSet.h>
#include <TagSet/DateTagSet.h>
#include <TagSet/TimeTagSet.h>
#include <Tag/Tag.h>
#include <Tag/AlphanumericalTag.h>
#include <Tag/NumericalTag.h>
#include <Tag/DateTag.h>
#include <Tag/TimeTag.h>
#include <Hierarchy/DefaultDimension.h>
#include <State/MultiDimensionalView.h>
#include <Hierarchy/HierarchyNode.h>
#include <State/StateDimension.h>
#include <Hierarchy/Dimension.h>

#include <Filters/TagFilter.h>
#include <Filters/DimensionFilter.h>
#include <Filters/DateRangeFilter.h>
#include <Filters/NumericalRangeFilter.h>
#include <Filters/TimeRangeFilter.h>

#include <LayerShared/Parameters.h>
#include <LayerShared/TagCommon.h>
#include <LayerShared/TagSetCommon.h>

#include <ui/Coordinates.h>
#include "treeitem.h"
#include "treeview.h"
#include "listwidget.h"
#include "listwidgetitem.h"
#include "glwidget.h"
#include "tablewidgetitem.h"
#include "ui_createtagui.h"
#include "ui_numericalrangefilter.h"
#include "ui_daterangefilter.h"
#include "ui_timerangefilter.h"
#include "ui_createtimetag.h"
#include "ui_createdatetag.h"
#include "ui_createhierarchy.h"
#include "ui_createdimensionui.h"
#include "ui_createtagintagsetui.h"
#include "dialogs/tagimportimagegui.h"

using namespace ObjectCube;

class TreeView;
class PhotoCube;

class MainController: public QWidget
{
	Q_OBJECT
private:
	int stateId_;
	QProgressDialog* progressDialog;
	QTimer* progressTimer;

    TreeItem *frontAxis;
    TreeItem *backAxis;
    TreeItem *upAxis;

public:
    MainController();
    ~MainController();
    Hub *hub;
    ObjectCube::MultiDimensionalView *mdv;
    TagImportImageGui* tiig;

    Coordinates *coordinates;

    TreeItem* getFrontAxis();
    TreeItem* getBackAxis();
    TreeItem* getUpAxis();

    void addFilter(TreeItem *selectedItem);
    void addFilter(TreeItem *selectedItem, TreeView *filterList, bool addItem);//Add filter on the dataset
    void addTagSet(TreeView *treeView);
    void buildTree(TreeView *treeView); // Gets all tagsets and puts the in the tree
    void buildEachTagSetTree(TreeItem *parent, TreeView *treeView); // Builds tree when a certain tagset has been selected
    void buildHierarchy(HierarchyNode *hierarchyNode, TreeItem *treeItem); //Builds hierarchy tree for each tag-set
    void clearView();
    void createHierarchy(TreeItem *selectedItem, TreeView *treeView);
    void createTag(TreeView *treeView);//create tag when right clicking a tagset
    void createTag(TreeItem *item, TreeView *viewTags);//create tag when right clicking when viewing tags
    void deleteTag(TreeItem *item);// ObjectCube has not yet implemented this
    void deleteTagSet(TreeItem *item);
    void getStateAndDraw(GLWidget *glWidget);
    void importFolderOfImages();
    void importImage();
    void removeFilter(TreeItem *selectedItem, TreeView *filterList, GLWidget *glWidget);
    void tagObject(int ObjectID);
    void treeItemDoubleClicked(TreeItem *item, TreeView *filterList, GLWidget *glWidget);
    void viewAsSlides(TreeItem *selectedItem, TreeView *filterList, GLWidget *glWidget);

    void addTagToObject(TreeItem* selectedItem, int objectId);
    void addTagToCell(TreeItem* selectedItem, vector<Object> cellObjects);

    void refresh();

    // set either Tag-Set or hierarchy on an axis in the hybercube
    void setAsFront(TreeItem *item, TreeView *filterList, GLWidget *glWidget, bool addItem = true);
    void setAsBack(TreeItem *item, TreeView *filterList, GLWidget *glWidget, bool addItem = true);
    void setAsUp(TreeItem *item, TreeView *filterList, GLWidget *glWidget, bool addItem = true);

    void drillDownHierarchy(TreeItem *selectedItem, TreeView *filterList, GLWidget *glWidget);

    //Create range filter on different tag types
    void createNumericalRangeFilter(TagSet *tagSet, TreeView *filterList);
    void createDateRangeFilter(TagSet *tagSet, TreeView *filterList);
    void createTimeRangeFilter(TagSet *tagSet, TreeView *filterList);

    //String conversion
    QString convertStringsFromObjectCube(TreeItem *item);
    QString convertStringsFromObjectCube(string objectCubeString);

private slots:
    void startImportProcess();
	void updateImportProgress();
};

#endif // MAINCONTROLLER_H
