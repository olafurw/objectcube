#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include <QListWidget>
#include <QWidget>
#include <QtGui>
#include <Hierarchy/HierarchyNode.h>
#include <Tag/Tag.h>
#include <TagSet/TagSet.h>
#include "listwidgetitem.h"


class ListWidget: public QListWidget
{
    Q_OBJECT
public:
    ListWidget(QWidget *parent);
    QAction *lw_remove;
    QAction *lw_setAsFront;
    QAction *lw_setAsUp;
    QAction *lw_setAsBack;
    void addFilterToList(ListWidgetItem *listItem);
    //QString convertStringsFromObjectCube(string objectCubeString);
    //QString convertStringsFromObjectCube(ListWidgetItem *listItem);

private:
    void buildHierarchy(ListWidgetItem *listItem);
};

#endif // LISTWIDGET_H
