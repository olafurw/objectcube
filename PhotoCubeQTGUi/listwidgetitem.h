#ifndef LISTWIDGETITEM_H
#define LISTWIDGETITEM_H

#include <QListWidgetItem>
#include "common.h"

class ListWidgetItem : public QListWidgetItem
{
public:
    ListWidgetItem();
    void *item;
    void *filter;
    TREE_ITEM_TYPES type;
    TREE_ITEM_TYPES getType();
};

#endif // LISTWIDGETITEM_H
