#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QtOpenGL/QGLWidget>
#include <QDropEvent>
#include <iostream>
#include <vector>
#include <map>
#include <ui/Coordinates.h>
#include <common.h>
#include "ImageData.h"
#include "modes/cube/Cube.h"
#include "modes/abstractmode.h"
#include "modes/modeManager.h"


class GLWidget: public QGLWidget {
    Q_OBJECT

public:
    GLWidget(QWidget *parent);
    ~GLWidget(){}
    void draw(const ObjectCube::MultiDimensionalView* mdv, Coordinates *coordinates);
	void clear();
	int getCurrentlyHoveredImage();
	int getClickedImage();
	int getClickedCell();
	void resetCamera();
    
    void handleKeyEvent(QKeyEvent* event);
    void handleKeyEvent_up(QKeyEvent* event);
    void reset();
    void timerEvent(QTimerEvent *);

private:
    // Member variable for clear color of the glwidget.
    QColor clearColor;
    
    // Is this good 
    bool m_need_repaint;

    GLfloat w,h;
    GLenum mode;
    //RENDERMODE renderMode;

    AbstractMode* currentMode;
    

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void dragMoveEvent(QDragMoveEvent *);
    void dragEnterEvent(QDragEnterEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void wheelEvent(QWheelEvent *event);

signals:
    void dropTaggingCell(QDropEvent*, vector<ObjectCube::Object>);
    void dropTagging(QDropEvent*, int);
};

#endif // GLWIDGET_H
