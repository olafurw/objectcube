#include "maincontroller.h"
#include "photocube.h"
#include "modes/modeManager.h"

#include <iostream>
#include <memory>
#include <functional>
#include <Hierarchy/PersistentDimension.h>
#include <Hierarchy/PersistentHierarchyNode.h>

MainController::MainController(): QWidget()
{
    try
    {
        //Initialize hub
        Hub::setDataAccessType(Hub::MONETDB);
        //Parameters::getParameters()->add( "outputDebugInfo", "1" );
        hub = Hub::getHub();
    }
    catch(exception &e)
    {
        QMessageBox::information(this, "Error Starting P3", e.what());
        exit(0);
    }

    frontAxis = 0;
    backAxis = 0;
    upAxis = 0;

    stateId_ = -1;

    coordinates = new Coordinates();
    coordinates->x = false;
    coordinates->y = false;
    coordinates->z = false;

    mdv = nullptr;
    progressDialog = nullptr;
    progressTimer = nullptr;
    tiig = nullptr;
}

MainController::~MainController()
{
    delete mdv;
    delete progressDialog;
}


QString MainController::convertStringsFromObjectCube(string objectCubeString)
{
    QString name;
    const char *stringValue = objectCubeString.c_str();
    QByteArray encodedString = stringValue;
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    name = codec->toUnicode(encodedString);
    return name;
}

QString MainController::convertStringsFromObjectCube(TreeItem *item)
{
    QString name;
    if(item->getType() == TAG)
    {
        Tag *tag = reinterpret_cast<Tag *>(item->item);
        int typeId = tag->getTypeId();
        if(typeId == 1)
        {
            name = convertStringsFromObjectCube(tag->valueAsString());
        }
        else
        {
            name = QString::fromStdString(tag->valueAsString());
        }
    }
    else if(item->getType() == TAGSET)
    {
        TagSet *tagset = reinterpret_cast<TagSet *>(item->item);
        name = convertStringsFromObjectCube(tagset->getName());
    }
    else if(item->getType() == HIERARCHY)
    {
        HierarchyNode *hierarchyNode = reinterpret_cast<HierarchyNode *>(item->item);
        name = convertStringsFromObjectCube(hierarchyNode->getName());
    }
    return name;
}


void MainController::buildTree(TreeView *treeView)
{
    treeView->clear();

    vector<TagSet*> all_tagsets = hub->getTagSets();
    vector<PersistentDimension*> tagSetDimensions;
    PersistentDimension *currentDim;
    TreeItem *itm;
    QIcon tagSetIcon(":/Icons/folder.png");

    //Create roots in the tagset tree
    for(unsigned int i = 0 ; i < all_tagsets.size(); i++)
    {
        TagSet* p_tagset = all_tagsets[i];
        itm = new TreeItem();
        itm->item = p_tagset;
        itm->type = TAGSET;
        itm->setIcon(0,tagSetIcon);

        QString name = convertStringsFromObjectCube(itm);

        itm->setText(0,name);
        treeView->addTopLevelItem(itm);

        tagSetDimensions = p_tagset->getPersistentDimensions();

        for(unsigned int j = 0; j < tagSetDimensions.size(); j++)
        {
            currentDim = tagSetDimensions.at(j);
            buildHierarchy(currentDim->getRoot(), itm);
        }
        itm->setExpanded(true);
    }
}


void MainController::buildEachTagSetTree(TreeItem *parent, TreeView *treeView)
{
    if(treeView->isHidden())
        treeView->show();

   //Get all tags in tagSet
   TagSet *p_tagSet = reinterpret_cast<TagSet *>(parent->item) ;
   vector<Tag*> tagList = p_tagSet->getTags();
   QIcon icon(":/Icons/rss_tag.png");
   //Add tagsets under allTagsets node
   for(unsigned int i = 0; i < tagList.size(); i++)
   {
       TreeItem *itm = new TreeItem();
       Tag* p_tag = tagList[i];
       itm->item = p_tag;
       itm->type = TAG;    
       itm->setIcon(0,icon);
       QString name = convertStringsFromObjectCube(itm);
       itm->setText(0,name);
       treeView->addTopLevelItem(itm);
   }
}


void MainController::buildHierarchy(HierarchyNode *hierarchyNode, TreeItem *treeItem)
{
    TreeItem *newItem = new TreeItem();
    QIcon icon(":/Icons/Hierarchy-32.png");
    newItem->item = hierarchyNode;
    newItem->type = HIERARCHY;

    QString name = convertStringsFromObjectCube(newItem);

    newItem->setText(0, name);
    newItem->setIcon(0,icon);
    treeItem->addChild(newItem);

    vector<HierarchyNode*> branches = hierarchyNode->getBranches();

    for(unsigned int i = 0; i < branches.size(); i++)
    {
        HierarchyNode *childNode = branches.at(i);
        buildHierarchy(childNode, newItem);
    }
}

void MainController::startImportProcess()
{
    if(tiig == nullptr)
    {
        return;
    }

    // Create the progress dialog and set the timer
    progressDialog = new QProgressDialog("Operation in progress.", "", 0, tiig->count());
    progressDialog->setCancelButton(nullptr);
    progressDialog->show();

    // Set the timer that checks for the progress
    progressTimer = new QTimer(this);
    connect(progressTimer, SIGNAL(timeout()), this, SLOT(updateImportProgress()));
    progressTimer->start(1000);
}

void MainController::importImage()
{
    QFileDialog *importFileDialog = new QFileDialog(this, tr("Import file"));
    if(importFileDialog->exec() == QDialog::Accepted)
    {
        if(tiig != nullptr)
        {
            disconnect(tiig, SIGNAL(tagged()), this, SLOT(startImportProcess()));
            delete tiig;
        }
        tiig = new TagImportImageGui();
        connect(tiig, SIGNAL(tagged()), this, SLOT(startImportProcess()));

        QStringList filenames = importFileDialog->selectedFiles();
        for(auto file : filenames)
        {
            tiig->addImage(file);
        }

        tiig->show();
    }

    delete importFileDialog;
}

void MainController::importFolderOfImages()
{
    QString folderName = QFileDialog::getExistingDirectory(this,tr("Import folder"));
    QDir *imageDir = new QDir(folderName);
    try
    {
        QStringList filters;
        filters << "*.jpg" << "*.JPG" << "*.jpeg" << "*.JPEG" << "*.png" << "*.PNG";
        imageDir->setNameFilters(filters);
        QStringList files = imageDir->entryList();

        if(files.size() > 0)
        {
            if(tiig != nullptr)
            {
                disconnect(tiig, SIGNAL(tagged()), this, SLOT(startImportProcess()));
                delete tiig;
            }
            tiig = new TagImportImageGui();
            connect(tiig, SIGNAL(tagged()), this, SLOT(startImportProcess()));

            for(auto file : files)
            {
                tiig->addImage(imageDir->absoluteFilePath(file));
            }

            tiig->show();
        }
    }
    catch(exception &e)
    {
        QMessageBox::information(this,"Could not add images.", e.what());
    }

    delete imageDir;
}

void MainController::updateImportProgress()
{
	unsigned int remaining = hub->itemsRemaining();
	unsigned int processProgress = hub->itemsTotal() - remaining;

	progressDialog->setValue(processProgress);

	// Progress done, cleanup.
	if(remaining == 0)
	{
		refresh();

		progressDialog->close();
		progressTimer->stop();
		disconnect(progressTimer, SIGNAL(timeout()), this, SLOT(updateImportProgress()));
		delete progressTimer;
		delete progressDialog;

		return;
	}
}

void MainController::refresh()
{
	// Filters will be set by this function
	// If any axis are set
	hub->removeAllFilters();

	if(stateId_ == -1)
	{
		return;
	}

	// Create a new state
	// Since we possibly have new data
	hub->removeState(stateId_);
	stateId_ = hub->storeState();
	shared_ptr<ObjectCube::State> s = hub->getState(stateId_);

	// Since we have a new state, we need a new mdv with those new objects
	if(mdv != nullptr)
	{
		delete mdv;
	}
	mdv = const_cast<ObjectCube::MultiDimensionalView*>(s->getNewMultiDimensionalView());

	if(frontAxis != nullptr)
	{
		addFilter(frontAxis);

	    if(frontAxis->getType() == TAGSET)
	    {
	        mdv->setAxis(MultiDimensionalView::AXIS_1,reinterpret_cast<TagSet*>(frontAxis->item));
	    }
	    else if(frontAxis->getType() == HIERARCHY)
	    {
	        try
	        {
	            HierarchyNode *node = reinterpret_cast<HierarchyNode*>(frontAxis->item);
	            State state;
	            StateDimension dim = state.getDimension(node->getDimensionId());
	            mdv->setAxis(MultiDimensionalView::AXIS_1, dim.getNode(node->getId()));

	        }
	        catch(exception &e)
	        {
	            cout << endl << "ERROR: " << e.what() << endl;
	            return;
	        }
	    }
	}

	if(upAxis != nullptr)
	{
		addFilter(upAxis);

		if(upAxis->getType() == TAGSET)
		{
			mdv->setAxis(MultiDimensionalView::AXIS_2,reinterpret_cast<TagSet *>(upAxis->item));
		}
		else if(upAxis->getType() == HIERARCHY)
		{
			try
			{
				HierarchyNode *node = reinterpret_cast<HierarchyNode*>(upAxis->item);
				State state;
				StateDimension dim = state.getDimension(node->getDimensionId());
				mdv->setAxis(MultiDimensionalView::AXIS_2, dim.getNode(node->getId()));

			}
			catch(exception &e)
			{
				cout << endl << "ERROR: " << e.what() << endl;
				return;
			}
		}
	}

	if(backAxis != nullptr)
	{
		addFilter(backAxis);

		if(backAxis->getType() == TAGSET)
		{
			mdv->setAxis(MultiDimensionalView::AXIS_3,reinterpret_cast<TagSet*>(backAxis->item));
		}
		else if(backAxis->getType() == HIERARCHY)
		{
			try
			{
				HierarchyNode *node = reinterpret_cast<HierarchyNode*>(backAxis->item);
				State state;
				StateDimension dim = state.getDimension(node->getDimensionId());
				mdv->setAxis(MultiDimensionalView::AXIS_3, dim.getNode(node->getId()));

			}
			catch(exception &e)
			{
				cout << endl << "ERROR: " << e.what() << endl;
				return;
			}
		}
	}

	// Set the mdv based on the axis set above and send that
	// mdv into the mode manager to refresh the mode.
	// Since the mode was constructed with the old data.
	s->initializeMdv(mdv);
	modes::modeManager::instance()->refreshCurrentMode(mdv);
}

void MainController::setAsFront(TreeItem *item, TreeView *filterList, GLWidget* /*glWidget*/, bool addItem)
{
    addFilter(item, filterList, addItem);
    frontAxis = item;

    stateId_ = hub->storeState();
	shared_ptr<ObjectCube::State> s = hub->getState(stateId_);
    if(mdv == NULL)
    {
        mdv = const_cast<ObjectCube::MultiDimensionalView*>(s->getNewMultiDimensionalView());
    }

    if(frontAxis->getType() == TAGSET)
    {
        mdv->setAxis(MultiDimensionalView::AXIS_1,reinterpret_cast<TagSet*>(frontAxis->item));
    }
    else if(frontAxis->getType() == HIERARCHY)
    {
        try
        {
            HierarchyNode *node = reinterpret_cast<HierarchyNode*>(frontAxis->item);
            State state;
            StateDimension dim = state.getDimension(node->getDimensionId());
            mdv->setAxis(MultiDimensionalView::AXIS_1, dim.getNode(node->getId()));
        }
        catch(exception &e)
        {
            cout << endl << "ERROR: " << e.what() << endl;
            return;
        }
    }
    coordinates->x = true;
    s->initializeMdv(mdv);
}


void MainController::setAsBack(TreeItem *item, TreeView *filterList, GLWidget* /*glWidget*/, bool addItem)
{
	addFilter(item, filterList, addItem);
	backAxis = item;

	stateId_ = hub->storeState();
	shared_ptr<ObjectCube::State> s = hub->getState(stateId_);
	if(mdv == NULL)
	{
		mdv = const_cast<ObjectCube::MultiDimensionalView*>(s->getNewMultiDimensionalView());
	}

	if(backAxis->getType() == TAGSET)
	{
		mdv->setAxis(MultiDimensionalView::AXIS_3,reinterpret_cast<TagSet*>(backAxis->item));
	}
	else if(backAxis->getType() == HIERARCHY)
	{
		try
		{
			HierarchyNode *node = reinterpret_cast<HierarchyNode*>(backAxis->item);
			State state;
			StateDimension dim = state.getDimension(node->getDimensionId());
			mdv->setAxis(MultiDimensionalView::AXIS_3, dim.getNode(node->getId()));
		}
		catch(exception &e)
		{
			cout << endl << "ERROR: " << e.what() << endl;
			return;
		}
	}
	coordinates->z = true;
	s->initializeMdv(mdv);
}


void MainController::setAsUp(TreeItem *item, TreeView *filterList, GLWidget* /*glWidget*/, bool addItem)
{
	addFilter(item, filterList, addItem);
	upAxis = item;

	stateId_ = hub->storeState();
	shared_ptr<ObjectCube::State> s = hub->getState(stateId_);
	if(mdv == NULL)
	{
		mdv = const_cast<ObjectCube::MultiDimensionalView*>(s->getNewMultiDimensionalView());
	}

	if(upAxis->getType() == TAGSET)
	{
		mdv->setAxis(MultiDimensionalView::AXIS_2,reinterpret_cast<TagSet *>(upAxis->item));
	}
	else if(upAxis->getType() == HIERARCHY)
	{
		try
		{
			HierarchyNode *node = reinterpret_cast<HierarchyNode*>(upAxis->item);
			State state;
			StateDimension dim = state.getDimension(node->getDimensionId());
			mdv->setAxis(MultiDimensionalView::AXIS_2, dim.getNode(node->getId()));
		}
		catch(exception &e)
		{
			cout << endl << "ERROR: " << e.what() << endl;
			return;
		}
	}
	coordinates->y = true;
	s->initializeMdv(mdv);
}


void MainController::drillDownHierarchy(TreeItem *selectedItem, TreeView *filterList, GLWidget *glWidget)
{
    HierarchyNode *front;
    HierarchyNode *up;
    HierarchyNode *back;
    if(frontAxis != 0)
        front = reinterpret_cast<HierarchyNode*>(frontAxis->item);
    if(upAxis != 0)
        up = reinterpret_cast<HierarchyNode*>(upAxis->item);
    if(backAxis != 0)
        back = reinterpret_cast<HierarchyNode*>(backAxis->item);

    HierarchyNode *current = reinterpret_cast<HierarchyNode*>(selectedItem->item);

    if(mdv == NULL)
    {
        stateId_ = hub->storeState();
        shared_ptr<ObjectCube::State> s = hub->getState(stateId_);
        mdv = const_cast<ObjectCube::MultiDimensionalView*>(s->getNewMultiDimensionalView());
    }

    if(frontAxis != 0 && current->getDimensionId() == front->getDimensionId())
    {
        mdv->clearAxis(MultiDimensionalView::AXIS_1);
        this->setAsFront(selectedItem,filterList,glWidget, false);
        //Update other axis's if needed
        if(upAxis != 0)
        {
            this->setAsUp(upAxis, filterList, glWidget,false);
        }
        if(backAxis != 0)
        {
            this->setAsBack(backAxis, filterList, glWidget, false);
        }
    }
    if(upAxis != 0 && current->getDimensionId() == up->getDimensionId())
    {
        mdv->clearAxis(MultiDimensionalView::AXIS_2);
        this->setAsUp(selectedItem,filterList,glWidget, false);
        //Update other axis's if needed
        if(frontAxis != 0)
        {
            this->setAsFront(frontAxis, filterList, glWidget,false);
        }
        if(backAxis != 0)
        {
            this->setAsBack(backAxis, filterList, glWidget, false);
        }
    }
    if(backAxis != 0 && current->getDimensionId() == back->getDimensionId())
    {
        mdv->clearAxis(MultiDimensionalView::AXIS_3);
        this->setAsBack(selectedItem, filterList, glWidget, false);
        //Update other axis's if needed
        if(upAxis != 0)
        {
            this->setAsUp(upAxis, filterList, glWidget,false);
        }
        if(frontAxis != 0)
        {
            this->setAsFront(frontAxis, filterList, glWidget, false);
        }
    }

    this->getStateAndDraw(glWidget);
}


void MainController::clearView()
{
    //clear axis info
    frontAxis = 0;
    backAxis = 0;
    upAxis = 0;
    coordinates->x = false;
    coordinates->y = false;
    coordinates->z = false;
    //Remove filters

    if(mdv == NULL)
    {
        stateId_ = hub->storeState();
        shared_ptr<ObjectCube::State> s = hub->getState(stateId_);
        mdv = const_cast<ObjectCube::MultiDimensionalView*>(s->getNewMultiDimensionalView());
    }

    mdv->clearAxes();
	hub->removeAllFilters();
}

TreeItem* MainController::getFrontAxis()
{
	return frontAxis;
}

TreeItem* MainController::getBackAxis()
{
	return backAxis;
}

TreeItem* MainController::getUpAxis()
{
	return upAxis;
}

void MainController::addFilter(TreeItem *selectedItem)
{
    Filter *filter;

    if(selectedItem->getType() == TAG)
    {
        Tag *tag = reinterpret_cast<Tag *>(selectedItem->item);

        filter = new TagFilter(tag, tag->getTagSetId());

    	Hub* hub = Hub::getHub();
    	hub->addFilter( filter );
    }
    else if(selectedItem->getType() == TAGSET)
    {
        TagSet *tagSet = reinterpret_cast<TagSet *>(selectedItem->item);

        const DefaultDimension *dim = tagSet->getDefaultDimension();

        filter = new DimensionFilter(*(dim->getRoot()), tagSet->getId());

        Hub* hub = Hub::getHub();
        hub->addFilter(filter);
    }
    else if(selectedItem->getType() == HIERARCHY)
    {
        HierarchyNode *hierarchyNode = 0;
        hierarchyNode = reinterpret_cast<HierarchyNode*>(selectedItem->item);

        Dimension *dim = hub->getTagSet(hierarchyNode->getTagSetId())->getDimension(hierarchyNode->getDimensionId());

        HierarchyNode *root = dim->getRoot();
        filter = new DimensionFilter(*root, root->getTagSetId());

        Hub* hub = Hub::getHub();
        hub->addFilter(filter);
    }
}

void MainController::addFilter(TreeItem *selectedItem, TreeView *filterList, bool addItem)
{
    Filter *filter;

    if(selectedItem->getType() == TAG)
    {
        Tag *tag = reinterpret_cast<Tag *>(selectedItem->item);

        filter = new TagFilter(tag, tag->getTagSetId());

    	Hub* hub = Hub::getHub();
    	hub->addFilter( filter );

        if(addItem)
        {
            TreeItem *itm = new TreeItem();
            itm->setText(0,selectedItem->text(0));
            itm->item = tag;
            itm->type = TAG;
            itm->filter = filter;
            filterList->addTopLevelItem(itm);
        }
    }
    else if(selectedItem->getType() == TAGSET)
    {
        TagSet *tagSet = reinterpret_cast<TagSet *>(selectedItem->item);

        const DefaultDimension *dim = tagSet->getDefaultDimension();

        filter = new DimensionFilter(*(dim->getRoot()), tagSet->getId());

        Hub* hub = Hub::getHub();
        hub->addFilter(filter);

        if(addItem)
        {
            TreeItem *itm = new TreeItem();
            itm->setText(0,selectedItem->text(0));
            itm->item = tagSet;
            itm->type = TAGSET;
            itm->filter = filter;
            filterList->addTopLevelItem(itm);
        }
    }
    else if(selectedItem->getType() == HIERARCHY)
    {
        HierarchyNode *hierarchyNode = 0;
        hierarchyNode = reinterpret_cast<HierarchyNode*>(selectedItem->item);

        Dimension *dim = hub->getTagSet(hierarchyNode->getTagSetId())->getDimension(hierarchyNode->getDimensionId());

        cout << "Dimension:" << dim->getId() << endl;
        HierarchyNode *root = dim->getRoot();
        cout << "Root:" << root->getId() << endl;
        filter = new DimensionFilter(*root, root->getTagSetId());

        Hub* hub = Hub::getHub();
        hub->addFilter(filter);

        if(addItem)
        {
            TreeItem *itm = new TreeItem();
            itm->setText(0,selectedItem->text(0));
            itm->item = hierarchyNode;
            itm->type = HIERARCHY;
            itm->filter = filter;
            filterList->addTopLevelItem(itm);
            vector<HierarchyNode*> brances = hierarchyNode->getBranches();
            for(unsigned int i = 0; i < brances.size(); i++)
            {
                buildHierarchy(brances.at(i), itm);
            }
        }
    }
}

// TODO: Rename TagSet to Concepts. Needs to be thoroughly done throughout the whole codebase.
void MainController::addTagSet(TreeView *treeView)
{
    QStringList listOfTagSetTypes;

    listOfTagSetTypes.push_back("ALPHANUMERICAL");
    listOfTagSetTypes.push_back("NUMERICAL");
    listOfTagSetTypes.push_back("DATE");
    listOfTagSetTypes.push_back("TIME");

    //Create a dialog for new tag creation
    QDialog *dialog = new QDialog;
    Ui::createDimensionUi dialogUi;
    dialogUi.setupUi(dialog);
    dialog->setWindowTitle("Create New Concept / Tag set");
    dialogUi.comboBox->addItems(listOfTagSetTypes);
    dialogUi.label1->setText("Name of new Concept / Tag set: ");
    dialogUi.label2->setText("Choose type of new Concept / Tag set: ");

    TagSet *newTagSet;
    if(dialog->exec() == QDialog::Accepted)
    {
        int tagType = dialogUi.comboBox->currentIndex();
        QString tagName = dialogUi.lineEdit->text();

        switch(tagType)
        {
            case 0:{
                newTagSet = new AlphanumericalTagSet(tagName.toUtf8().constData());
                break;
            }
            case 1:{
                newTagSet = new NumericalTagSet(tagName.toUtf8().constData());
                break;
            }
            case 2:{
                newTagSet = new DateTagSet(tagName.toUtf8().constData());
                break;
            }
            case 3:{
                newTagSet = new TimeTagSet(tagName.toUtf8().constData());
                break;
            }
        }

        QIcon tagSetIcon(":/Icons/folder.png");
        TagSet *createdTagSet = newTagSet->create();
        TreeItem *newItem = new TreeItem();
        newItem->item = createdTagSet;
        newItem->type = TAGSET;
        newItem->setText(0,tagName);
        newItem->setIcon(0,tagSetIcon);
        treeView->addTopLevelItem(newItem);
        delete newTagSet;
    }
}


void MainController::treeItemDoubleClicked(TreeItem *item, TreeView *filterList, GLWidget *glWidget)
{
    addFilter(item, filterList, glWidget);
}


void MainController::createTag(TreeView *treeView)
{
    TreeItem* selectedItem = reinterpret_cast<TreeItem*>(treeView->currentItem());
    if(selectedItem->getType() != TAGSET)
    {
        return;
    }

    TagSet* selectedTagSet = static_cast<TagSet*>(selectedItem->item);
    //Determine type
    int typeId = selectedTagSet->getTypeId();

    //Create a dialog for new tag creation
    QDialog *dialog = new QDialog;
    Ui::createTagInTagSetUi dialogUi;
    dialogUi.setupUi(dialog);
    dialogUi.label1->setText(QString("Create a tag for: ") + QString(selectedTagSet->getName().c_str()));

    //If user hit OK
    if(dialog->exec() == QDialog::Accepted)
    {
        //Get info from dialog and use it.
        QString tagName = dialogUi.lineEdit->text();

        Tag *newTag;
        try
        {
            switch(typeId)
            {
                case 1:
                {
                    newTag = new AlphanumericalTag(tagName.toUtf8().constData());
                    break;
                }
                case 2:
                {
                    newTag = new NumericalTag(tagName.toInt());
                    break;
                }
                case 3:
                {
                    QDialog *dialog = new QDialog;
                    Ui::CreateDateTag dialogUi;
                    dialogUi.setupUi(dialog);
                    if(dialog->exec() == QDialog::Accepted)
                    {
                        newTag = new DateTag(dialogUi.dateEdit->date().year(), dialogUi.dateEdit->date().month(), dialogUi.dateEdit->date().day());
                        break;
                    }
                    return;
                }
                case 4:
                {
                    QDialog *dialog = new QDialog;
                    Ui::CreateTimeTag dialogUi;
                    dialogUi.setupUi(dialog);
                    if(dialog->exec() == QDialog::Accepted)
                    {
                        newTag = new TimeTag(dialogUi.timeEdit->time().hour(), dialogUi.timeEdit->time().minute(), dialogUi.timeEdit->time().second(),0);
                        break;
                    }
                    return;
                }
            }

            selectedTagSet->addTag(newTag);

            //Add new tag to the tree in the right place.
            /*Tag *createdTag = const_cast<Tag *>(selectedTagSet->addTag(newTag));
            TreeItem *itm = new TreeItem();
            itm->item = createdTag;
            itm->setText(0, tagName);
            QList<QTreeWidgetItem*> items = treeView->findItems(QString::fromStdString(selectedTagSet->getName()),Qt::MatchExactly,0);
            items.at(0)->addChild(itm);*/
            delete newTag;
        }
        catch (exception& e)
        {
            QMessageBox::information(this,"Error message", e.what());
        }
    }

    delete dialog;
}


void MainController::createTag(TreeItem *item, TreeView *viewTags)
{
    if(item->getType() == TAGSET)
    {
        TagSet *selectedTagSet = reinterpret_cast<TagSet *>(item->item);
        int typeId = selectedTagSet->getTypeId();

        QIcon tagIcon(":/Icons/rss_tag.png");
        Tag *newTag = 0;
        QDialog *dialog = new QDialog;
        try
        {
            if(typeId == 1 || typeId == 2)
            {
                Ui::createTagUi dialogUi;
                dialogUi.setupUi(dialog);
                dialogUi.comboBox->addItem(item->text(0));
                //If user hit OK
                if(dialog->exec() == QDialog::Accepted)
                {
                     QString tagName = dialogUi.lineEdit->text();
                     if(typeId == 1)
                     {
                         newTag = new AlphanumericalTag(tagName.toUtf8().constData());
                     }
                     if(typeId == 2)
                     {
                         newTag = new NumericalTag(tagName.toInt());
                     }

                     //Add new tag to the tree in the right place.
                     Tag *createdTag = const_cast<Tag *>(selectedTagSet->addTag(newTag));
                     TreeItem *itm = new TreeItem();
                     itm->item = createdTag;
                     itm->setText(0, tagName);
                     itm->setIcon(0, tagIcon);

                     if(!viewTags->isHidden())
                     {
                        viewTags->addTopLevelItem(itm);
                     }
                     delete newTag;
                }
            }
            if(typeId == 3)
            {
                Ui::CreateDateTag dialogUi;
                dialogUi.setupUi(dialog);
                if(dialog->exec() == QDialog::Accepted)
                {
                    QDate dateTag = dialogUi.dateEdit->date();
                    newTag = new DateTag(dateTag.year(), dateTag.month(), dateTag.day());

                    //Add new tag to the tree in the right place.
                    Tag *createdTag = const_cast<Tag *>(selectedTagSet->addTag(newTag));
                    TreeItem *itm = new TreeItem();
                    itm->item = createdTag;
                    itm->setText(0, dateTag.toString());
                    itm->setIcon(0,tagIcon);
                    viewTags->addTopLevelItem(itm);
                    delete newTag;
                }
            }

            if(typeId == 4)
            {
                Ui::CreateTimeTag dialogUi;
                dialogUi.setupUi(dialog);
                if(dialog->exec() == QDialog::Accepted)
                {
                    QTime timeTag = dialogUi.timeEdit->time();
                    newTag = new TimeTag(timeTag.hour(), timeTag.minute(),timeTag.second(), 0);

                    //Add new tag to the tree in the right place.
                    Tag *createdTag = const_cast<Tag *>(selectedTagSet->addTag(newTag));
                    TreeItem *itm = new TreeItem();
                    itm->item = createdTag;
                    itm->setText(0, timeTag.toString());
                    itm->setIcon(0,tagIcon);
                    viewTags->addTopLevelItem(itm);
                    delete newTag;
                }
            }

            }
            catch (exception& e)
            {
                //TODO: create a better error message.
                //If this error occurs it is most likely a database corruption. Check values in Table: tag_type
                QMessageBox::information(this,"Error creating tag", e.what());
            }

        delete dialog;
    }
    else if (item->getType() == HIERARCHY)
    {
        QStringList listOfTags;
        PersistentHierarchyNode *node = reinterpret_cast<PersistentHierarchyNode*>(item->item);

        QIcon hierarchyIcon(":/Icons/Hierarchy-32.png");
        TagSet *tagSet = hub->getTagSet(node->getTagSetId());
        PersistentDimension *hierarchy = tagSet->getPersistentDimension(node->getDimensionId());

        vector<Tag*> allTags = tagSet->getTags();


        for(unsigned int i = 0; i < allTags.size(); i++)
        {
            int typeId = allTags.at(i)->getTypeId();
            if(typeId == 1)
            {
                listOfTags.push_back(convertStringsFromObjectCube(allTags.at(i)->valueAsString()));
            }
            else
            {
                listOfTags.push_back(QString::fromStdString(allTags.at(i)->valueAsString()));
            }

        }

        QDialog *dialog = new QDialog;
        Ui::CreateHierarchy dialogUi;
        dialogUi.setupUi(dialog);
        dialogUi.comboBox->addItems(listOfTags);
        dialog->setWindowTitle("Create new dimension (hierarchy)");
        if(dialog->exec() == QDialog::Accepted)
        {
            try
            {
                Tag *selectedTag = allTags.at(dialogUi.comboBox->currentIndex());
                std::cout << "createTag: about to add newNode " << std::endl;
                PersistentHierarchyNode *newNode = hierarchy->addNode(node->getId(), selectedTag);
                std::cout << "createTag: newNode added" << std::endl;

                //Add to the tree
                TreeItem *itm = new TreeItem();
                itm->setText(0, convertStringsFromObjectCube(selectedTag->valueAsString()));
                itm->item = newNode;
                itm->type = HIERARCHY;
                itm->setIcon(0,hierarchyIcon);
                item->addChild(itm);
            }
            catch(exception &e)
            {
                QMessageBox::information(this,"Error", "Could not add tag to hierarchy. Please make sure that it has not already been added.");
            }
        }
    }
}


void MainController::createHierarchy(TreeItem *selectedItem, TreeView* /*treeView*/)
{
    QStringList listOfTags;

    if (selectedItem->type != 0){ // If selected item is not a tag set
    	QMessageBox::information(this, "Error", "A hierarchy must be created from a tag set.");
    	    	return;
    }
    TagSet *tagSet = reinterpret_cast<TagSet *>(selectedItem->item);
    QIcon hierarchyIcon(":/Icons/Hierarchy-32.png");
    vector<Tag*> allTags = tagSet->getTags();

    int numberOfTags = allTags.size();
    if (numberOfTags == 0){
    	QMessageBox::information(this, "Error", "There are no tags to create hierarchy from. Please add tag(s) and try again.");
    	return;
    }
    for(unsigned int i = 0; i < numberOfTags; i++)
    {
        int typeId = allTags.at(i)->getTypeId();
        if(typeId == 1)
        {
            listOfTags.push_back(convertStringsFromObjectCube(allTags.at(i)->valueAsString()));
        }
        else
        {
            listOfTags.push_back(QString::fromStdString(allTags.at(i)->valueAsString()));
        }
    }

    QDialog *dialog = new QDialog;
    Ui::CreateHierarchy dialogUi;
    dialogUi.setupUi(dialog);
    dialog->setWindowTitle("Create new dimension (hierarchy)");
    dialogUi.comboBox->addItems(listOfTags);
    dialogUi.label->setText("Choose a root for the hierarchy");
    if(dialog->exec() == QDialog::Accepted)
    {
        try
        {
            Tag *rootTag = allTags.at(dialogUi.comboBox->currentIndex());
            PersistentDimension* hierarchy = tagSet->createPersistentDimension(rootTag);
            PersistentHierarchyNode *newNode = hierarchy->getPersistentRoot();
            TreeItem *itm = new TreeItem();
            itm->setText(0,convertStringsFromObjectCube(rootTag->valueAsString()));
            itm->item = newNode;
            itm->type = HIERARCHY;
            itm->setIcon(0,hierarchyIcon);
            selectedItem->addChild(itm);
        }
        catch(exception &e)
        {
            QMessageBox::information(this, "Error", "Could not set this tag as root element.");
        }
     }
}


void MainController::removeFilter(TreeItem *selectedItem, TreeView* /*filterList*/, GLWidget *glWidget)
{
    Filter *filterToRemove = reinterpret_cast<Filter*>(selectedItem->filter);
	Hub::getHub()->removeFilter( filterToRemove );
    delete selectedItem;
    getStateAndDraw(glWidget);
    refresh();
}


void MainController::deleteTag(TreeItem* /*item*/)
{
    QMessageBox::information(this,"Unimplemented feature", "TODO: Add this functionality to ObjectCube");
}


void MainController::deleteTagSet(TreeItem *item)
{
    try
    {
        TagSet *tagSetToDelete = reinterpret_cast<TagSet *>(item->item);
        tagSetToDelete->erase();
    }
    catch (exception& e)
    {
        QMessageBox::information(this,"Error message", "Could not delete tag-set. Please note that it is not possible to delete all tag-sets.");
    }
}


void MainController::viewAsSlides(TreeItem *selectedItem, TreeView *filterList, GLWidget *glWidget) {
    addFilter(selectedItem,filterList,true);
    TagSet *tagSet = hub->getTagSet(reinterpret_cast<Tag*>(selectedItem->item)->getTagSetId());
    //glWidget->setRenderMode(SLIDESHOW);
    //Must set axis or else cell count is 0.

    if(mdv == NULL)
    {
        stateId_ = hub->storeState();
        shared_ptr<ObjectCube::State> s = hub->getState(stateId_);
        mdv = const_cast<ObjectCube::MultiDimensionalView*>(s->getNewMultiDimensionalView());
    }

    mdv->setAxis(MultiDimensionalView::AXIS_1, tagSet);
    this->getStateAndDraw(glWidget);
}


void MainController::createNumericalRangeFilter(TagSet *tagSet, TreeView *filterList)
{
    try
    {
        QDialog *dialog = new QDialog;
        Ui::NumericalRangeFilter dialogUi;
        dialogUi.setupUi(dialog);
        int from = 0;
        int to = 0;
        //If user hit OK
        if(dialog->exec() == QDialog::Accepted)
        {            
            from = dialogUi.lineEdit->text().toInt();
            to = dialogUi.lineEdit_2->text().toInt();
            Filter *filter = new NumericalRangeFilter(from,to,tagSet->getId());
            hub->addFilter(filter);
            TreeItem *itm = new TreeItem();
            itm->item = tagSet;
            QString filterText = convertStringsFromObjectCube(tagSet->getName()) + " (" + QString::number(from) + "," + QString::number(to) + ")";
            itm->setText(0,filterText);
            filterList->addTopLevelItem(itm);
        }
        delete dialog;
    }
    catch(exception &e)
    {
        QMessageBox::information(this, "Error", "Could not add numerical range filter. Please make sure that you put the lower number first.");
    }
}


void MainController::createDateRangeFilter(TagSet *tagSet, TreeView *filterList)
{
    try
    {
        QDialog *dialog = new QDialog;
        Ui::DateRangeFilter dialogUi;
        dialogUi.setupUi(dialog);
        QDate fromDate;
        QDate toDate;
        Filter *filter;
        if(dialog->exec() == QDialog::Accepted)
        {
            fromDate = dialogUi.dateEdit->date();
            toDate = dialogUi.dateEdit_2->date();

            if(fromDate > toDate)
            {
                filter = new DateRangeFilter(fromDate.year(), tagSet->getId());
            }
            else
            {
                filter = new DateRangeFilter(fromDate.year(), fromDate.month(), fromDate.day(), toDate.year(), toDate.month(), toDate.day(), tagSet->getId());
            }
            hub->addFilter(filter);
            TreeItem *itm = new TreeItem();
            itm->item = tagSet;
            QString filterText;
            filterText.append(convertStringsFromObjectCube(tagSet->getName()));
            filterText.append(" (" + fromDate.toString() + "," + toDate.toString() + ")");
            itm->setText(0,filterText);
            filterList->addTopLevelItem(itm);
        }
        delete dialog;
    }
    catch(exception &e)
    {
        QMessageBox::information(this, "Error", "Could not set Date range filter. Make sure you are not trying to go back in time.");
    }
}


void MainController::createTimeRangeFilter(TagSet *tagSet, TreeView *filterList)
{
    try
    {
        QDialog *dialog = new QDialog;
        Ui::TimeRangeFilter dialogUi;
        dialogUi.setupUi(dialog);
        QTime fromTime;
        QTime toTime;
        Filter *filter;
        if(dialog->exec() == QDialog::Accepted)
        {
            fromTime = dialogUi.timeEdit->time();
            toTime = dialogUi.timeEdit_2->time();

            if(fromTime > toTime)
            {
                QMessageBox::information(this,"Input error", "We cannot go back in time");
            }
            else
            {
                filter = new TimeRangeFilter(fromTime.hour(), toTime.hour(), tagSet->getId());
                hub->addFilter(filter);
                TreeItem *itm = new TreeItem();
                itm->item = tagSet;
                QString filterText = convertStringsFromObjectCube(tagSet->getName()) + " (" + fromTime.toString() + "," + toTime.toString() + ")";
                itm->setText(0,filterText);
                filterList->addTopLevelItem(itm);
            }
        }
        delete dialog;
      }
    catch(exception &e)
    {
        QMessageBox::information(this, "Error", "Could not set Time range filter. Make sure you are not trying to go back in time.");
    }
}


void MainController::getStateAndDraw(GLWidget *glWidget)
{
    try
    {
        glWidget->draw(mdv, coordinates);
    }
    catch(exception &e)
    {
        QMessageBox::information(this,"Error", e.what());
    }
}


void MainController::addTagToObject(TreeItem* selectedItem, int objectId)
{
    Tag *tagToAdd = reinterpret_cast<Tag*>(selectedItem->item);
    ObjectTag *oTag = new ObjectTag(tagToAdd);
    Object objectToTag = Object::fetch(objectId);
    objectToTag.addTag(*oTag);
}


void MainController::addTagToCell(TreeItem* selectedItem, vector<ObjectCube::Object> cellObjects) {
    Tag *tagToAdd = reinterpret_cast<Tag*>(selectedItem->item);
    ObjectTag *oTag = new ObjectTag(tagToAdd);
    cout << "cellObjects size controller: " << cellObjects.size() << endl;
    for(unsigned int i = 0; i < cellObjects.size(); i++) {
        cellObjects.at(i).addTag(*oTag);
    }
}
