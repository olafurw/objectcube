//
//  Position.h
//  sdf
//
//  Created by Hlynur Sigurþórson on 5/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef sdf_Position_h
#define sdf_Position_h

namespace common {
    class Position{
    public:
        // Coordinate member variables.
        GLuint x,y,z;
        
        // Constructor for the Position class. This
        // constructo initializes all the coordinates with the
        // values passed into the constructor.
        Position(GLuint x, GLuint y, GLuint z){
            this->x = x;
            this->y = y;
            this->z = z;
        }
        
        // Default constructor for the Position class. This
        // constructo initializes all the coordinates to 0.
        Position(){
            this->x = 0;
            this->y = 0;
            this->z = 0;
        }
    };
}


#endif
