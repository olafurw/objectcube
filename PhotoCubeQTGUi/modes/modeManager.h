#ifndef H_MODEMANAGER
#define H_MODEMANAGER

#include <thread>
#include <map>
#include <stack>
#include <vector>
#include "abstractmode.h"
#include "modeData.h"
#include "common.h"

class GLWidget;

namespace modes {

enum MODES { NOMODE, CUBEMODE, SLIDESHOWMODE, CARDMODE, FACEMODE };

// Singleton class for managing viewmode modes.
// When initalized, all available modes are loaded.
// modes then can be requested by ids or names.
class modeManager {
  public:
    GLWidget* glWidget;
    static modeManager* instance();

    bool hasConstructedMode();
    void constructMode(MODES m, modeData* md);

    MODES getCurrentMode();
    void refreshCurrentMode(ObjectCube::MultiDimensionalView* mdv);
    void setLastMode(); // Set the last mode that is on the mode stack.
    AbstractMode* getMode(); // Returns the mode that was created last with constructMode.
    vector<std::string> getAvailableModes();
    MODES convertStringToEnumType(string s);
    void clear();
    void faceMode(); //temporary function until face-recognition mode is a proper mode

  private:
    void constructMode(modeData* md);

    stack<MODES> modeStack;
    AbstractMode* lastConstructedMode;
    MODES currentMode; // holds current wiewmode
    modeManager();
    static modeManager* m_pInstance;
    mutex m_guard;
};
}

#endif
