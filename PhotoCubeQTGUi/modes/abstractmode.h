#ifndef ABSTRACTMODE_H
#define ABSTRACTMODE_H

#include <QKeyEvent>
#include <State/MultiDimensionalView.h>
#include <State/Cell.h>
#include <Object.h>
#include "ui/camera.h"
#include "modeData.h"


// Abstract class for viewmode plugins.
class AbstractMode {    
    public:
    AbstractMode(){}
    virtual ~AbstractMode(){}
    virtual void draw(/*GLenum mode,ObjectCube::MultiDimensionalView *mdv, list<ObjectCube::Cell*> cells, ObjectCube::Object* image*/ ) = 0;
    virtual void handleKeyBoardPressedEvent(QKeyEvent* event)= 0;
    virtual void handleKeyBoardReleasedEvent(QKeyEvent* event) = 0;
    virtual void handleMousePressedEvent(QMouseEvent* event) = 0;
    virtual void handleMouseReleasedEvent(QMouseEvent* event) = 0;
    virtual void handleMouseMovedEvent(QMouseEvent* event) = 0;
    virtual void switchToAnotherMode(string mode, int imageId) = 0;
    virtual void load(modes::modeData* md) = 0;
    virtual int getMouseHoverImageId(GLdouble x, GLdouble y) = 0;
    virtual void handleMouseScroll(QWheelEvent* /*event*/){}

    void setWH(GLfloat w, GLfloat h) {this->w = w; this->h = h;}

    virtual int getClickedImage(GLdouble /*x*/, GLdouble /*y*/){return -1;}
    virtual std::vector<ObjectCube::Object> getClickedCell(GLdouble x, GLdouble y) = 0;
    int viewCubeMode;
    int viewImageMode;
    CCamera camera;

    // Map for keys-states
    std::map<int, bool> keyState;
    protected:
    modes::modeData* mData;
    GLfloat w,h;
};

#endif // ABSTRACTMODE_H
