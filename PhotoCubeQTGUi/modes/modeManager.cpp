#include "modes/modeManager.h"

#include "cube/Cube.h"
#include "slideshow/slideshow.h"
#include "card/card.h"
#include "common.h"
#include "face_rec/maindialog.h"
#include "glwidget.h"

using namespace modes;

// Global static pointer used to ensure a single instance of the class.
modeManager* modeManager::m_pInstance = 0;

// Function for fetching instance of the modeManager. Note that
// this function is NOT thread safe.
modeManager* modeManager::instance()
{
	if (!m_pInstance)
	{
		m_pInstance = new modeManager();
	}
	return m_pInstance;
}

// Default constructor for the modeManager.
// This constructor is private.
modeManager::modeManager()
{
	// Initialize the mode data pointer.
	this->glWidget = nullptr;
	this->currentMode = NOMODE; // Start the plugin manager in cubemode.
	this->modeStack.push(NOMODE); // Push the cubemode to the stack where we begin in that mode.
}

void modeManager::constructMode(MODES m, modeData* md)
{
	this->currentMode = m;
	constructMode(md);
}

// Constructs a mode from the currently selected mode and uses the mode
// data that was sent via setModeData.
void modeManager::constructMode(modeData* md)
{
	if(this->currentMode == NOMODE)
	{
		return;
	}

	// Create mutex guard. This function must be thread safe. We might
	// Get context switch during this function and the GlWidget might try to
	// repaint.
	{
		lock_guard<mutex> guard(m_guard);

		if(this->currentMode == CUBEMODE)
		{
			lastConstructedMode = new cube::Cube();
		}
		if(this->currentMode == SLIDESHOWMODE)
		{
			lastConstructedMode = new slideshow::SlideShow();
		}
		if(this->currentMode == CARDMODE)
		{
			lastConstructedMode = new card::Card();
		}

		lastConstructedMode->load(md);
	}
}

void modeManager::refreshCurrentMode(ObjectCube::MultiDimensionalView* mdv)
{
	if(this->currentMode == NOMODE)
	{
		return;
	}

	if(this->currentMode == CUBEMODE)
	{
		cube::Cube* cube = static_cast<cube::Cube*>(lastConstructedMode);
		Coordinates oldCoord = cube->getCoordinates();

	    // Create plugin data for the selected plugin.
	    modes::modeData* data = new modes::modeData(mdv, new Coordinates(oldCoord.x, oldCoord.y, oldCoord.z));
	    constructMode(data);
	}
}

// Returns the mode that was created last with constructMode.
AbstractMode* modeManager::getMode()
{
	if(this->currentMode == NOMODE)
	{
		return nullptr;
	}

	return this->lastConstructedMode;
}

void modeManager::setLastMode()
{
	if(this->modeStack.size() > 0)
	{
		this->currentMode = this->modeStack.top();
		this->modeStack.pop();
	}
	else
	{
		// Throw exception
		throw 0;
	}
}

bool modeManager::hasConstructedMode()
{
	if(this->currentMode == NOMODE)
	{
		return false;
	}

	return !(this->lastConstructedMode == 0);
}

vector<std::string> modeManager::getAvailableModes()
{
    vector<std::string> availablePlugins;
    availablePlugins.push_back("Card");
    availablePlugins.push_back("Cube");
    availablePlugins.push_back("Slideshow");
    availablePlugins.push_back("Face recognition");
    return availablePlugins;
}

// Default return value is Cubemode - that is most likely to be correct if sth fails.
MODES modeManager::convertStringToEnumType(string s){
	if (s == "Cube"){
		return CUBEMODE;
	}
	else if (s == "Slideshow"){
		return SLIDESHOWMODE;
	}
	else if (s == "Card"){
		return CARDMODE;
	}
	else if (s == "Face recognition"){
		return FACEMODE;
	}
	else{
		return NOMODE;
	}
}

void modeManager::clear() {
  if(this->lastConstructedMode != 0) {
    delete this->lastConstructedMode;
    this->lastConstructedMode = 0;
  }
  this->currentMode = NOMODE;
}

void modeManager::faceMode() {
    /*facerec::MainDialog *fr = new facerec::MainDialog;
    fr->setPluginData(this->mData);
    fr->load();
    fr->show();*/
}

MODES modeManager::getCurrentMode() {
    return this->currentMode;
}



