#include "modes/slideshow/slideshow.h"
#include <iostream>
using namespace slideshow;
SlideShow::SlideShow() {

}

void SlideShow::draw() {
    this->camera.Render();
    glPushMatrix();
    glTranslatef(-4.0f, 0.0f, -10.0f);

    ui::CubeImage* img = this->allImages.at(currentImage); 
    img->setSize(1.5, 2.0);
    img->draw(GL_RENDER);

    glPopMatrix();
}


void SlideShow::handleKeyBoardPressedEvent(QKeyEvent *event) {

    if(event->key() == Qt::Key_Left)
    {
        this->incrementCounter(-1);
    }
    if(event->key() == Qt::Key_Right)
    {
        this->incrementCounter(1);
    }
    if(event->key() == Qt::Key_W) {
        if(!keyState[Qt::Key_W])
            keyState[Qt::Key_W] = true;
    }

    if(event->key() == Qt::Key_S) {
        if(!keyState[Qt::Key_S])
            keyState[Qt::Key_S] = true;
    }

    if(event->key() == Qt::Key_A) {
        if(!keyState[Qt::Key_A])
            keyState[Qt::Key_A] = true;
    }

    if(event->key() == Qt::Key_D) {
        if(!keyState[Qt::Key_D])
            keyState[Qt::Key_D] = true;
    }

}

void SlideShow::handleKeyBoardReleasedEvent(QKeyEvent *e) {

    if(e->key() == Qt::Key_W)
        keyState[Qt::Key_W] = false;

    if(e->key() == Qt::Key_S)
        keyState[Qt::Key_S] = false;

    if(e->key() == Qt::Key_A)
        keyState[Qt::Key_A] = false;

    if(e->key() == Qt::Key_D)
        keyState[Qt::Key_D] = false;

}

void SlideShow::handleMousePressedEvent(QMouseEvent* /*event*/)
{
    return;
}

void SlideShow::switchToAnotherMode(string /*mode*/, int /*imageId*/)
{
	return;
}

void SlideShow::initSlideShow() {
    /*
    std::cout << "INIT" << endl;
    SF3dVector pos;
    pos.x = 1.0f;
    pos.y = 0.75f;
    pos.z = 2.0f;
    this->camera.SetPosition(pos);
    cout << this->mdv->getCells().size() << endl;
    map<string, ObjectCube::Cell> c = this->mdv->getCells();
    std::map<std::string, ObjectCube::Cell>* cells = const_cast< map<string, ObjectCube::Cell>* >( &c );

    for(map<string, ObjectCube::Cell>::iterator it = cells->begin(); it != cells->end(); ++it) {
        ObjectCube::Cell currentCell = it->second;

        vector<ObjectCube::StateObject> cellObjects = currentCell.getObjects();

        for(unsigned int i = 0; i < cellObjects.size(); i++) {
            this->allStateObjects.push_back(cellObjects.at(i));
        }
    }
    for(unsigned int j = 0 ; j < this->allStateObjects.size(); j++) {
        string _imageName = allStateObjects.at(j).getName();
        ui::CubeImage *image = new ui::CubeImage(const_cast<char*>(_imageName.c_str()), j);
        this->allImages.push_back(image);
    }
    cout << "Size of image collection:" << allImages.size() << endl;
    */
}


int SlideShow::getClickedImage(GLdouble /*x*/, GLdouble /*y*/) {
    cout << "A" << endl;
    ui::CubeImage *image = allImages.at(currentImage);
    cout << "B" << endl;
    string imageName = image->getName();
    cout << "C" << endl;
    ObjectCube::Object obj = ObjectCube::Object::fetch(imageName);
    cout << "Object id: " << obj.getId() << endl;                
    return obj.getId();
}

int SlideShow::getMouseHoverImageId(GLdouble /*x*/, GLdouble /*y*/) {
    ui::CubeImage *image = allImages.at(currentImage);
    string imageName = image->getName();
    ObjectCube::Object obj = ObjectCube::Object::fetch(imageName);
    return obj.getId();
}

void SlideShow::load(modes::modeData* md){
	this->mData = md;
    SF3dVector pos;
    pos.x = -3.0f;
    pos.y = 0.75f;
    pos.z = -8.0f;
    this->camera.SetPosition(pos);
    std::vector<std::string> images = mData->getImages();
    for(unsigned int j = 0 ; j < images.size(); j++) {
        ui::CubeImage *image = new ui::CubeImage(images.at(j), j, 0);
        this->allImages.push_back(image);
    }
    this->currentImage = 0;
}


void SlideShow::incrementCounter(int incrementValue) {
    currentImage = (currentImage + incrementValue < 0) ? (allImages.size() -1) : ((currentImage + incrementValue) % allImages.size());
}
