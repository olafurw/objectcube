#ifndef SLIDESHOW_H
#define SLIDESHOW_H
#include "modes/abstractmode.h"

#include "ui/CubeImage.h"
namespace slideshow{

class SlideShow: public AbstractMode
{
private:
    ObjectCube::MultiDimensionalView* mdv;
    void initSlideShow();
    vector<ObjectCube::StateObject> allStateObjects;
    vector<ui::CubeImage*> allImages;
    int currentImage;

public:
    SlideShow();

    // Implement draw according to AbstractMode interface.
    void draw();
    
    void handleKeyBoardPressedEvent(QKeyEvent* /*event*/);
    void handleKeyBoardReleasedEvent(QKeyEvent* /*event*/);
    void handleMousePressedEvent(QMouseEvent* /*event*/);
    void handleMouseReleasedEvent(QMouseEvent* /*event*/){}
    void handleMouseMovedEvent(QMouseEvent* /*event*/){}
    void switchToAnotherMode(string mode, int imageId);

    //to get the id of the image that is being showed
    //used for drag n drop tagging
    int getClickedImage(GLdouble x, GLdouble y);
    std::vector<ObjectCube::Object> getClickedCell(GLdouble /*x*/, GLdouble /*y*/){}
    int getMouseHoverImageId(GLdouble x, GLdouble y);

    //To increment currentImage value so that it is possible to loop through the images in allImages
    void incrementCounter(int incrementValue);
    void load(modes::modeData* md);
};
}//end namespace
#endif // SLIDESHOW_H
