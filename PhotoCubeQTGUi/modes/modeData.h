
#ifndef H_PLUGINDATA
#define H_PLUGINDATA

#include <string>
#include <vector>
#include <State/MultiDimensionalView.h>

#include "ui/Coordinates.h"

namespace modes {
  class modeData {
    private:
      ObjectCube::MultiDimensionalView* mdv;
      Coordinates* coordinates;

    public:

      std::vector<std::string> images;

      modeData() {
        this->coordinates = 0;
        this->mdv = 0;
      }

      modeData(const ObjectCube::MultiDimensionalView* mdv, Coordinates* coordinates) {
        this->mdv = const_cast<ObjectCube::MultiDimensionalView*>( mdv );
        this->coordinates = coordinates;
      }

      ObjectCube::MultiDimensionalView* getMDV() {
        return this->mdv;
      }
      Coordinates* getCoordinate() {
        return this->coordinates;
      }

      std::vector<std::string> getImages() {
          return this->images;
      }
  };
}
#endif
