//
//  Cube.cpp
//  Impementation for Cube.h
//
//
//
#include "modes/cube/Cube.h"

// QT includes.
#include <QtGui>
#include <QtOpenGL>
#include <QDialog>
#include <QInputDialog>

// STD include.
#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

// Project includes.
#include "../../../ObjectCube/Hub.h"
#include "ui/ImageCacher.h"
#include "ui/TextureCacher.h"
#include "ui/camera.h"
#include "../../dialogs/chooseplugin.h"
#include "ui_chooseplugin.h"
#include "modes/modeManager.h"
#include "glwidget.h"
#include "ImageData.h"
#include "State/Cell.h"
#include "State/StateObject.h"
#include "common.h"

#include <QDebug>

namespace cube {

Cube::~Cube() {
    // Clean up the cell pointers.
    typedef std::map<std::string, cube::Cell*>::iterator it_type;
    for(it_type iterator = this->cubecells.begin(); iterator != this->cubecells.end(); iterator++)
        delete this->cubecells[iterator->first];
    
    delete font;
}

// Default constructor for cube. Set the mode value to
// GL_RENDER for the fort drawing of the cube.
Cube::Cube()
{
    this->mode = GL_RENDER;
    this->scale = 1.5;
    this->mouseSensitivity = 0.05;
    this->rotate = 1.0;
    this->imageScaleFactor = 1;

    this->mdv = nullptr;
    this->coordinates = nullptr;

    // Need to add a config file to load this from a certain location
    font = new FTTextureFont("./PhotoCubeQTGUi/Fonts/Ubuntu-L.ttf");
	//font->FaceSize(96);
}

Cube::Cube(ObjectCube::MultiDimensionalView* mdv, Coordinates* coordinates)
{
    this->mode = GL_RENDER;
    this->scale = 1.5;
    this->mouseSensitivity = 0.05;
    this->rotate = 1.0;
    this->imageScaleFactor = 1;

    this->mdv = mdv;
    this->coordinates = coordinates;
}

void Cube::load(modes::modeData* md)
{
	this->mData = md;
    // Initalize the keystate. To begin with, all the keys
    // are not currently pressed.
    keyState[Qt::Key_W] = false;
    keyState[Qt::Key_S] = false;
    keyState[Qt::Key_A] = false;
    keyState[Qt::Key_D] = false;
    keyState[Qt::Key_Z] = false;
    keyState[Qt::Key_X] = false;
    keyState[Qt::Key_Up] = false;
    keyState[Qt::Key_Down] = false;
    keyState[Qt::Key_Left] = false;
    keyState[Qt::Key_Right] = false;
    keyState[Qt::Key_Shift] = false;
    keyState[Qt::Key_Control] = false;
    keyState[Qt::Key_PageUp] = false;

    // Store pointer to the mdv.
	this->mdv = this->mData->getMDV();
    // Store pointer to the coordinates set in the client.
    this->coordinates = this->mData->getCoordinate();
    // Store the image scale factor.
    this->imageScaleFactor = 10;
    
    this->rotate = 0;
    
    // Initalize the cube.
    this->initCube();
}

void Cube::handleMouseScroll(QWheelEvent *event){
    
    // If the shift button is down and we are scrolling then we scale the cube.
    if(keyState[Qt::Key_Shift])
    {
        // Detect the scroll direction.
        if((event->delta()/120.0) > 0)
        {
            this->scale = this->scale - 0.1;
            if(this->scale <= 0)
                this->scale = 0;
        }
        else 
        {
            this->scale = this->scale + 0.2;
        }

        for(std::map<int, ui::CubeImage*>::iterator it = this->cubeImages.begin(); it != this->cubeImages.end(); ++it)
        {
            it->second->setSize(this->scale, this->scale);
        }
    }
    else
    {
        // Get the screen position with respect to the glWidget.
        QPoint p = modes::modeManager::instance()->glWidget->mapFromGlobal(QCursor::pos());

        // Check if we have the mouse over some image.
        int image_id = getMouseHoverImageId((GLdouble)p.x(), (GLdouble)p.y());

        if(image_id != -1 && cubeImages[image_id])
        {
            // Get the pointer to the image that we are scrolling over.
            ui::CubeImage* hover_image = this->cubeImages[image_id];
            
            // Get pointer to the cell that the image belongs to.
            Cell* cell = this->cubecells[hover_image->getCellKey()];

            // Detect the scroll direction.
            if((event->delta()/120.0) > 0)
            {
                cell->scrollUp();
            }
            else
            {
                cell->scrollDown();
            }
        }
    }
}

void Cube::drawCubeLines()
{
    glDisable(GL_TEXTURE_2D);
    // TODO: Move to draw lines for coordinate functions.
    glColor3f(1.0 ,1.0, 1.0); 
        
    // Draw line for x-axis.
    if(this->coordinates->x)
    {
        // Calculate the length of the axis cell count.
        int xAxisCellCount = this->mdv->getAxisCellCount(0);
        if(xAxisCellCount > 0)
        {
			glBegin(GL_LINES);
				glVertex3f(-0.1, 0, 0.0); // origin of the line
				glVertex3f(-0.1 + ( (xAxisCellCount * this->scale + xAxisCellCount*0.2  ) + 0.2 ), 0, 0.0); // ending point of the line
			glEnd();
        }
    }
        
    // Draw line for y-axis.
    if(this->coordinates->y)
    {
        // Calculate the length of the axis cell count.
        int yAxisCellCount = this->mdv->getAxisCellCount(1);
        if(yAxisCellCount > 0)
        {
			glBegin(GL_LINES);
				glVertex3f(-0.1, 0, 0.0); // origin of the line
				glVertex3f(-0.1 , ( (yAxisCellCount * this->scale ) + 0.1 ), 0.0); // ending point of the line
			glEnd();
        }
    }
        
    // Draw line for z-axis.
    if(this->coordinates->z)
    {
        // Calculate the length of the axis cell count.
        int zAxisCellCount = this->mdv->getAxisCellCount(2);
        if(zAxisCellCount > 0)
        {
        	glBegin(GL_LINES);
        		glVertex3f(-0.1, 0, 0.0); // origin of the line
        		glVertex3f(-0.1, 0.0, -((zAxisCellCount * this->scale ) + 0.1 ));
        	glEnd();
        }
    }
    glEnable(GL_TEXTURE_2D);
}
 
void Cube::drawImages()
{
    for( auto& cell : cubecells )
    {
        cell.second->drawCell(this->mode);
    }
}

void Cube::draw()
{
	// Zoom in
    if(keyState[Qt::Key_W])
    {
        this->camera.MoveForward( -0.1 ) ;
    }
    if(keyState[Qt::Key_Z])
    {
        this->camera.MoveForward( -0.1 ) ;
    }

    // Zoom out
    if(keyState[Qt::Key_S])
    {
        this->camera.MoveForward( 0.1 ) ;
    }
    if(keyState[Qt::Key_X])
    {
    	this->camera.MoveForward( 0.1 ) ;
    }

    // Right movement
    if(keyState[Qt::Key_D])
    {
        this->camera.StrafeRight( 0.1 ) ;
    }
	if(keyState[Qt::Key_Right])
	{
		this->camera.StrafeRight( 0.1 ) ;
	}

    // Left movement
    if(keyState[Qt::Key_A])
    {
        this->camera.StrafeRight( -0.1 ) ;
    }
	if(keyState[Qt::Key_Left])
	{
		this->camera.StrafeRight( -0.1 ) ;
	}

	// Upwards movement
	if(keyState[Qt::Key_Up])
	{
		this->camera.MoveUpward( 0.1 ) ;
	}

	// Downwards movement
	if(keyState[Qt::Key_Down])
	{
		this->camera.MoveUpward( -0.1 ) ;
	}

    if(this->mode != GL_SELECT)
    {
        this->camera.Render();
    }
    
    // Translate the model view matrix -5.0 on the z-axis.
    glPushMatrix();
    glTranslatef(-4.0f, 0.0f, -10.0f);
    
    // rotate stuff (need to bind to mouse.) 
    glRotatef(this->rotate, 0.0f, 1.0f, 0.0f);
            
    // Draw the cube lines.
    this->drawCubeLines();
    
    // Draw the images into the cube.
    this->drawImages();

    // Draw axis labels.
    this->drawAxisLabels();

    glPopMatrix();
}

void Cube::initCube()
{
    // ID counter for images. Images in the cube are assigned unique number. This number is then used
    // when the image is clicked to identify the image.
    int id_counter = 1;
    
    vector<ObjectCube::Cell> cells = this->mdv->getCells();

    // Loop over the cells.
    for( auto& mdvCell : cells )
    {
        Cell* cell = new Cell();
        
        cell->setPos(mdvCell.getPosition(0), mdvCell.getPosition(1), mdvCell.getPosition(2));
        try
        {
        	cell->setXLabel(mdvCell.getAxisLabel(0));
        }catch (...){}
        try
        {
        	cell->setYLabel(mdvCell.getAxisLabel(1));
        }catch (...){}
        try
        {
        	cell->setZLabel(mdvCell.getAxisLabel(2));
        }catch (...){}
        const vector<ObjectCube::StateObject> cellObjects = mdvCell.getObjects();

        // Go through all the images for that given cell.
        for(unsigned int j = 0; j < cellObjects.size(); j++)
        {
            // Get the image name.
            string _imageName = std::string(cellObjects[j].getName());
            int objectCubeId = cellObjects[j].getId();

            // Create image object.
            ui::CubeImage *image = new ui::CubeImage(_imageName, id_counter, objectCubeId);
            
            // Add the image to the cell.
            cell->addImage(image); 

            // also keep pointer to the image in the cube
            this->cubeImages[id_counter] = image;
            id_counter++;
        }

        string cellkey = std::to_string(mdvCell.getPosition(0)) + ":" + std::to_string(mdvCell.getPosition(1)) + ":" + std::to_string(mdvCell.getPosition(2));

        this->cubecells[cellkey] = cell;
    }
}

void Cube::drawAxisLabels()
{
    map<std::string, cube::Cell*>::const_iterator itr;
    
    // Find all cells top row on the x-axis.
    for(itr = this->cubecells.begin(); itr != this->cubecells.end(); ++itr)
    {
        std::string cellKey = (*itr).first;
        Cell* cell = this->cubecells[cellKey];

        // Draw the x-axis labels.
        if(cellKey.at(2) == '0' && cellKey.at(4) == '0') {
            glDisable(GL_TEXTURE_2D);
            glPushMatrix();
				glTranslatef(cell->getX() * (0.2 + this->scale) + this->scale / 2, -0.3, 0);
				float sc = 0.0025;
				glScalef(sc, sc, 1);
				font->Render(cell->getXLabel().c_str());
            glPopMatrix();
            glEnable(GL_TEXTURE_2D);
        } // end of x-axis drawing.


        // draw y-labels
        if(cellKey.at(0) == '0' && cellKey.at(4) == '0')
        {
            glDisable(GL_TEXTURE_2D);
            glPushMatrix();
				glTranslatef(-0.5, cell->getY() * (0.2+this->scale) + this->scale / 2, 0);
				float sc = 0.0025;
				glScalef(sc, sc, 1);
				font->Render(cell->getYLabel().c_str());
			glPopMatrix();
			glEnable(GL_TEXTURE_2D);
        } // end of y-axis drawing.


        // draw z-labels
        if(cellKey.at(0) == '0' && cellKey.at(2) == '0') {
            glDisable(GL_TEXTURE_2D);
            glPushMatrix();
            
            glTranslatef(-0.5, -0.3, -cell->getZ() * (0.2+this->scale));
				float sc = 0.0025;
				glScalef(sc, sc, 1);
				font->Render(cell->getZLabel().c_str());
			glPopMatrix();
			glEnable(GL_TEXTURE_2D);
        } // end of z-axis drawing.
        

    }
}

void Cube::handleKeyBoardPressedEvent(QKeyEvent *e) {
    if(e->key() == Qt::Key_W) {
        if(!keyState[Qt::Key_W])
            keyState[Qt::Key_W] = true;
    }

    if(e->key() == Qt::Key_S) {
        if(!keyState[Qt::Key_S])
            keyState[Qt::Key_S] = true;
    }

    if(e->key() == Qt::Key_A) {
        if(!keyState[Qt::Key_A])
            keyState[Qt::Key_A] = true;
    }

    if(e->key() == Qt::Key_D) {
        if(!keyState[Qt::Key_D])
            keyState[Qt::Key_D] = true;
    }

    if(e->key() == Qt::Key_Z) {
        if(!keyState[Qt::Key_Z])
            keyState[Qt::Key_Z] = true;
    }

    if(e->key() == Qt::Key_X) {
        if(!keyState[Qt::Key_X])
            keyState[Qt::Key_X] = true;
    }

    if(e->key() == Qt::Key_Up) {
        if(!keyState[Qt::Key_Up])
            keyState[Qt::Key_Up] = true;
    }

    if(e->key() == Qt::Key_Down) {
        if(!keyState[Qt::Key_Down])
            keyState[Qt::Key_Down] = true;
    }

    if(e->key() == Qt::Key_Left) {
        if(!keyState[Qt::Key_Left])
            keyState[Qt::Key_Left] = true;
    }

    if(e->key() == Qt::Key_Right) {
        if(!keyState[Qt::Key_Right])
            keyState[Qt::Key_Right] = true;
    }

    if(e->key() == Qt::Key_Shift){
        if(!keyState[Qt::Key_Shift])
            keyState[Qt::Key_Shift] = true;
    }
    if(e->key() == Qt::Key_PageUp)
    {
        if(!keyState[Qt::Key_PageUp])
        {
            keyState[Qt::Key_PageUp] = true;
        }
    }
    // 16777250 is ctrl key number on mac.
    if(e->key() == 16777250)
    {
        if(keyState[Qt::Key_Control])
        {
            keyState[Qt::Key_Control] = false;
            //QWidget::setCursor(Qt::ArrowCursor);
        }

        else
        {
            //QWidget::setCursor(Qt::CrossCursor);
            keyState[Qt::Key_Control] = true;
        }
    }
}

void Cube::handleKeyBoardReleasedEvent(QKeyEvent *e) {
    if(e->key() == Qt::Key_W)
    {
        keyState[Qt::Key_W] = false;
    }
    
    if(e->key() == Qt::Key_S)
    {
        keyState[Qt::Key_S] = false;
    }

    if(e->key() == Qt::Key_A)
    {
        keyState[Qt::Key_A] = false;
    }

    if(e->key() == Qt::Key_D)
    {
        keyState[Qt::Key_D] = false;
    }

    if(e->key() == Qt::Key_Z)
	{
		keyState[Qt::Key_Z] = false;
	}

    if(e->key() == Qt::Key_X)
    {
        keyState[Qt::Key_X] = false;
    }

	if(e->key() == Qt::Key_Up )
	{
		keyState[Qt::Key_Up] = false;
    }

	if(e->key() == Qt::Key_Down)
	{
		keyState[Qt::Key_Down] = false;
    }

	if(e->key() == Qt::Key_Left)
	{
		keyState[Qt::Key_Left] = false;
    }

	if(e->key() == Qt::Key_Right)
	{
		keyState[Qt::Key_Right] = false;
    }

    if(e->key() == Qt::Key_Shift)
    {
        keyState[Qt::Key_Shift] = false;
    }
    if(e->key() == Qt::Key_PageUp)
    {
        keyState[Qt::Key_PageUp] = false;
    }
}

int Cube::getMouseHoverImageId(GLdouble x, GLdouble y) {
    GLuint selectBuf[512] = {0};
    GLint hits, viewport[4];

    glGetIntegerv(GL_VIEWPORT, viewport);
    glSelectBuffer (512, selectBuf);
    glRenderMode (GL_SELECT);
    this->mode = GL_SELECT;

    glInitNames();
    glPushName(1000);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    gluPickMatrix(x, (GLdouble)(viewport[3] - y), 1.0, 1.0, viewport);
    gluPerspective(45.,(this->w)/(this->h), 0.1f, 1000.0f);

    glMatrixMode(GL_MODELVIEW);
    this->draw();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    hits = glRenderMode (GL_RENDER);

    int image_id = -1;
    if(hits != 0)
    {
        image_id = this->processHits (hits, selectBuf);
    }

    glMatrixMode(GL_MODELVIEW);
    this->mode = GL_RENDER;

    return image_id;
}

void Cube::handleMousePressedEvent(QMouseEvent* /*e*/) {
	/*
    // Get the image id that the mouse is currently hovering.
    int image_id = this->getMouseHoverImageId( (GLdouble)e->x(), (GLdouble)e->y() );

    if(image_id != -1 && cubeImages[image_id] != NULL) {
        cout << "We clicked image with id " << image_id << endl;
        cout << this->cubeImages[image_id]->getName() << endl;
        QStringList listOfModes;
        vector<std::string> availPlugins = modes::modeManager::instance()->getAvailableModes();
        for(unsigned int i = 0; i < availPlugins.size(); i++){
        	listOfModes.push_back(QString::fromStdString(availPlugins.at(i)));
        }

        QDialog *dialog = new QDialog;
        Ui::choosePlugin dialogUi;
        dialogUi.setupUi(dialog);
        dialogUi.comboBox->addItems(listOfModes);
        
        if(dialog->exec() == QDialog::Accepted) {
            string selectedMode = dialogUi.comboBox->currentText().toUtf8().constData();
            if(selectedMode == "Card"){
                cube::Cell* cell = this->cubecells[this->cubeImages[image_id]->getCellKey()];

                modes::modeData* modeData = new modes::modeData();
                modeData->images = cell->getImageNames();

                modes::modeManager::instance()->setPluginData(modeData);
                modes::modeManager::instance()->setMode(modes::CARDMODE);
                modes::modeManager::instance()->constructMode();
            }
            else if(selectedMode == "Slideshow"){
                //cout << "Slides" << endl;
                
                cube::Cell* cell = this->cubecells[this->cubeImages[image_id]->getCellKey()];

                modes::modeData* modeData = new modes::modeData();
                modeData->images = cell->getImageNames();

                modes::modeManager::instance()->setPluginData(modeData);
                modes::modeManager::instance()->setMode(modes::SLIDESHOWMODE);
                modes::modeManager::instance()->constructMode();
            }
            else if(selectedMode == "Face recognition") {
                cube::Cell* cell = this->cubecells[this->cubeImages[image_id]->getCellKey()];

                modes::modeData* modeData = new modes::modeData();
                modeData->images = cell->getImageNames();

                modes::modeManager::instance()->setPluginData(modeData);
                modes::modeManager::instance()->faceMode();
            }
            else{
                //Do nothing???
            }
        }

        //string imageName = this->cubeImages[image_id]->getName();


    }
    */
}

void Cube::handleMouseReleasedEvent(QMouseEvent* /*event*/) {
}

void Cube::handleMouseMovedEvent(QMouseEvent* /*event*/)
{
    if(keyState[Qt::Key_PageUp])
    {
        int height = modes::modeManager::instance()->glWidget->parentWidget()->geometry().height();
        int width = modes::modeManager::instance()->glWidget->parentWidget()->geometry().width();

        QPoint p = QCursor::pos();

        float x_delta = abs( (width/2.0) - (p.x() + 0.0) );
        float y_delta = abs( (height/2.0) - (p.y() + 0.0) );


        if(p.x()+0.0 < (width/2.0)) {
            GLfloat old_rot = this->camera.getRTX();
            this->camera.RotateX(-old_rot);
            this->camera.RotateY( this->mouseSensitivity * x_delta ) ;
            this->setMouseToCenter();
            this->camera.Render();
            this->camera.RotateX(old_rot);
        }


        if(p.x() + 0.0 > (width/2.0)) {
            GLfloat old_rot = this->camera.getRTX();
            this->camera.RotateX(-old_rot);
            this->camera.RotateY( -this->mouseSensitivity * x_delta ) ;
            this->setMouseToCenter();
            this->camera.Render();
            this->camera.RotateX(old_rot);
        }

        if(p.y()+0.0 > (height/2.0)) {
            this->camera.RotateX( -this->mouseSensitivity * y_delta ) ;
            this->setMouseToCenter();

        }

        if(p.y() +0.0 < (height/2.0)) {
            this->camera.RotateX( this->mouseSensitivity * y_delta ) ;
            this->setMouseToCenter();
        }
    }
}

/**
 * Takes a mode as parameter and switches to that mode
 */
void Cube::switchToAnotherMode(std::string mode, int imageId)
{
	if(imageId == -1)
	{
		return;
	}

	if (mode == "Cube")
	{
		return; // We are already in this mode.
	}
	else if (mode == "Card")
	{
		Cell* cell = this->cubecells[this->cubeImages[imageId]->getCellKey()];

		modes::modeData* modeData = new modes::modeData();
		modeData->images = cell->getImageNames();

		modes::modeManager::instance()->constructMode(modes::CARDMODE, modeData);
	}
	else if (mode == "Slideshow")
	{
		cube::Cell* cell = this->cubecells[this->cubeImages[imageId]->getCellKey()];

		modes::modeData* modeData = new modes::modeData();
		modeData->images = cell->getImageNames();

		modes::modeManager::instance()->constructMode(modes::SLIDESHOWMODE, modeData);
	}
	else if (mode == "Face Recognition")
	{
		/*cube::Cell* cell = this->cubecells[this->cubeImages[imageId]->getCellKey()];

		modes::modeData* modeData = new modes::modeData();
		modeData->images = cell->getImageNames();

		modes::modeManager::instance()->setModeData(modeData);
		modes::modeManager::instance()->faceMode();*/
	}
	return;
}

std::vector<ObjectCube::Object> Cube::getClickedCell(GLdouble x, GLdouble y) {
    int imageID = getMouseHoverImageId(x,y);
    std::vector<ObjectCube::Object> objects;
    ui::CubeImage *img = this->cubeImages[imageID];
    Cell *cell = this->cubecells[img->getCellKey()];
    std::vector<ui::CubeImage*> images = cell->getImages();
    for(unsigned int i = 0; i < images.size(); i++)
    {
        ObjectCube::Object obj = ObjectCube::Object::fetch(this->cubeImages[imageID]->getName());
        objects.push_back(obj);
    }
    return objects;
}

int Cube::processHits (GLint hits, GLuint buffer[]) {
  int closest_hit = -1;
  unsigned int i, j;
   GLuint names, *ptr, minZ,*ptrNames, numberOfNames;
   ptr = (GLuint *) buffer;
   minZ = 0xffffffff;
   for (i = 0; i < hits; i++) {
      names = *ptr;
      ptr++;
      if (*ptr < minZ) {
          numberOfNames = names;
          minZ = *ptr;

          ptrNames = ptr+2;
      }
      
      ptr += names+2;
    }

  ptr = ptrNames;
  for (j = 0; j < numberOfNames; j++,ptr++) {
     closest_hit = *ptr;
  }
  return closest_hit;
}

void Cube::setMouseToCenter(){
    int height = modes::modeManager::instance()->glWidget->parentWidget()->geometry().height();
    int width = modes::modeManager::instance()->glWidget->parentWidget()->geometry().width();
    QCursor::setPos ( width/2.0, height/2.0 );
}

Coordinates Cube::getCoordinates()
{
	return *coordinates;
}

} // end of namespace.





