//
//  Cell.cpp
//  sdf
//
//  Created by Hlynur Sigurþórson on 5/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#include "modes/cube/Cell.h"
#include "common.h"
#include <iostream>

using namespace std;

cube::Cell::Cell() {
    this->x = 0;
    this->y = 0;
    this->z = 0;
    this->maxNumberImageDrawn = 5;
    
    // Initalize the cell counter. This counter is used
    // to give image cell index within the cell. This number
    // decides where in the cell the images will appear.
    this->cellIndexCounter = 0;
}

cube::Cell::Cell(int x, int y, int z){
    this->x = x;
    this->y = y;
    this->z = z;

    this->maxNumberImageDrawn = 5;
    
    // Initalize the cell counter. This counter is used
    // to give image cell index within the cell. This number
    // decides where in the cell the images will appear.
    this->cellIndexCounter = 0;
}

void cube::Cell::setPos(int x, int y, int z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

cube::Cell::~Cell() {
    // Loop over all the images and delete them.
    unsigned int i;
    for(i = 0; i<this->cellImages.size(); i++)
    {
        delete this->cellImages[i];
    }

}

// Adds image to the cell.
void cube::Cell::addImage(ui::CubeImage* image) {
    image->setPos(this->x, this->y, this->z);
    image->setCellIndex(this->cellIndexCounter);
    this->cellIndexCounter++;
    this->cellImages.push_back(image);
}

// Returns all the images that are in the cell.
std::vector<ui::CubeImage*> cube::Cell::getImages() {
    return this->cellImages;
}

// Get the number of images that are inn the cell.
int cube::Cell::getImageCount() {
    return this->cellImages.size();
}

// Draws the cell into the cube. This functions calls the
// draw routine for all the images in the cell. Note that
// the counter maxNumbeImageDrawn is used to fix the number
// of images within a given cell.
void cube::Cell::drawCell(GLenum mode) {
    int num = 0;

    for(unsigned int i = 0; i < this->cellImages.size(); i++) {
        this->cellImages[i]->draw(mode);
        if(i > this->maxNumberImageDrawn)
            break;
    }
    
    //this->drawBitmapText(  const_cast<char*>(this->xLabel.c_str()), this->x * (0.2 + 1.5), this->y-0.6, 0);
}

void cube::Cell::scrollUp(){
    // Check if there are any images in the cell, if not we return.
    if(this->cube::Cell::getImageCount() <= 1)
        return;
    
    for(unsigned i=0; i<maxNumberImageDrawn; i++) {
        if(this->cellImages[i]->getCellindex() == 0) {
            if(this->cellImages.size() > this->maxNumberImageDrawn)
                this->cellImages[i]->setCellIndex( this->maxNumberImageDrawn-1 );
            else
                this->cellImages[i]->setCellIndex( this->cellImages.size()-1 );

            for(unsigned int j=0; j<this->maxNumberImageDrawn; j++) {
                if(j > this->cellImages.size()-1 )
                    break;
                if(j!=i)
                    this->cellImages[j]->setCellIndex( this->cellImages[j]->getCellindex() - 1 );
            }

            break;
        } 
    }
}

void cube::Cell::scrollDown(){
    // Check if there are any images in the cell, if not we return.
    if(this->cube::Cell::getImageCount() <= 1)
        return;
    // Find the biggest.
    unsigned p = 0;
    unsigned biggest = this->cellImages[0]->getCellindex();
    
    for(unsigned int i=1; i < this->maxNumberImageDrawn; i++) {
        if(i > this->cellImages.size()-1 )
            break;
        if(biggest < this->cellImages[i]->getCellindex()){
            p = i;
            biggest = this->cellImages[i]->getCellindex();
        }
    }

    this->cellImages[p]->setCellIndex( 0 );
    
    for(unsigned i = 0; i < this->maxNumberImageDrawn; i++) {
        if(i > this->cellImages.size()-1 )
            break;
        if(i != p)
            this->cellImages[i]->setCellIndex( this->cellImages[i]->getCellindex() + 1 );

    } 
}

std::string cube::Cell::getCellKey(){
    std::string cellkey = std::to_string(this->x) + ":" + std::to_string(this->y) + ":" + std::to_string(this->z);
    return cellkey;
}

void cube::Cell::drawBitmapText(char* string, float x, float y, float z){
    
    int w;

    glDisable(GL_TEXTURE_2D);

    glPushMatrix();
    
    glTranslatef(x, y, z);
    glColor3f(1.0, 1.0, 1.0);
    float sc = 800.0;
    glScalef(1/sc, 1/sc, 1/sc);
    
    for( char* p = string; *p; p++) {
        glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, *p);
        w += glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, *p);
        glTranslatef(-30, 0, 0);
    }
    glPopMatrix();

    glEnable(GL_TEXTURE_2D);
}

std::vector<std::string> cube::Cell::getImageNames() {
    std::vector<std::string> v;
    for(unsigned int i=0; i< this->getImages().size(); i++)
        v.push_back( this->cellImages[i]->getName() );

    return v;
}






