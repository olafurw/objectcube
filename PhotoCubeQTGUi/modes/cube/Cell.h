//
//  Cell.h
//  sdf
//
//  Created by Hlynur Sigurþórson on 5/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Cell_h
#define Cell_h

// STD includes.
#include <vector>
#include <string>
#include <ui/CubeImage.h>


namespace cube {

    class Cell {
    private:
        std::string xLabel;
        std::string yLabel;
        std::string zLabel;

        // Cell has a vector of images.
        std::vector<ui::CubeImage*> cellImages;
        int x,y,z;
        unsigned int maxNumberImageDrawn;
        int cellIndexCounter;

        void drawBitmapText(char* text, float x, float y, float z);

        
    public:
        // Adds image to the cell.
        void addImage(ui::CubeImage* image);

        // Returns list of all the images
        // tha have been added to the cell.
        std::vector<ui::CubeImage*> getImages();
        std::vector<std::string> getImageNames();
        
        // Get the number of images that are inn the cell.
        int getImageCount();
        

        void drawCell(GLenum mode);

        void scrollUp();
        void scrollDown();
        
        // The Cell constructor accepts three variables that
        // represents the location of the cell in the cube.
        Cell(int x, int y, int z);

        // Empty constructor 
        Cell();

        // Sets the position of the cell within the cube.
        void setPos(int x, int y, int z);

        int getX() {
            return this->x;
        }

        int getY() {
            return this->y;
        }

        int getZ() {
            return this->z;
        }

        //Cell();
        
        // Deconstructor for the Cell class. Deletes all the images
        // that the cell contains.
        ~Cell();

        // Returns the cell key value as string. If the cell has the position
        // (x,y,z) in the cube the function returns the string xyz.
        std::string getCellKey();


        void setXLabel(std::string label){
            this->xLabel = label;
        }
        
        void setYLabel(std::string label){
            this->yLabel = label;
        }
        
        void setZLabel(std::string label){
            this->zLabel = label;
        }

        std::string getXLabel(){
            return this->xLabel;
        }
        std::string getYLabel(){
            return this->yLabel;
        }
        std::string getZLabel(){
            return this->zLabel;
        }
    };
}
#endif
