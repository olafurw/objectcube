#ifndef CUBE_H
#define CUBE_H

// Include from PhotoCube
#include "ui/Coordinates.h"
#include "modes/abstractmode.h"

// Include from ObjectCube.
#include "State/StateObject.h"
#include "ui/CubeImage.h"

#include "modes/cube/Cell.h"

// Include from std.
#include <map>
#include <vector>
#include <string>

#include <FTGL/ftgl.h>

namespace cube
{

class Cube : public AbstractMode {

private:
    
    float scale;

    // Member variables.
    ObjectCube::MultiDimensionalView* mdv;
    
    // Member variable that stores all the cells within the cube.
    std::map<std::string, cube::Cell*> cubecells;

    std::map<int, ui::CubeImage*> cubeImages;

    Coordinates* coordinates;

    float mouseSensitivity;

    // Current GlRendering mode.
    GLenum mode;
    
    // TODO: Store this in some global configuration.
    int imageScaleFactor;
    
    // Initializes the cube with the set member variables.
    // There must be a mdv set so that the cube can be constructed.
    void initCube();
    
    // Draw the lines in the current cube.
    void drawCubeLines();
    
    // Draw the cube images on the screen.
    void drawImages();
    
    // Holds the rotation of the cube.
    float rotate;

    int processHits (GLint hits, GLuint buffer[]);
    int getMouseHoverImageId(GLdouble x, GLdouble y);

    // Force the mouse to the center of the screen.
    void setMouseToCenter();

    FTFont* font;

public:
    // Default constructor for 
    Cube();
    Cube(ObjectCube::MultiDimensionalView* mdv, Coordinates* coordinates);
    ~Cube();
    void draw();
    void handleKeyBoardPressedEvent(QKeyEvent *event);
    void handleKeyBoardReleasedEvent(QKeyEvent *event);
    void handleMousePressedEvent(QMouseEvent *event);
    void handleMouseReleasedEvent(QMouseEvent *event);
    void handleMouseMovedEvent(QMouseEvent *event);
    void switchToAnotherMode(string mode, int imageId);
    void load(modes::modeData* md);
    std::vector<ObjectCube::Object> getClickedCell(GLdouble x, GLdouble y);
    Coordinates getCoordinates();
    void handleMouseScroll(QWheelEvent *event);

    void drawAxisLabels();
};

}
#endif
