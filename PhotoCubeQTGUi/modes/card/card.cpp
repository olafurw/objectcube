#include "modes/card/card.h"
#include <QtGui>
#include <QtOpenGL>
#include <QDialog>
#include <QInputDialog>
#include <QButtonGroup>

using namespace card;

//STD include
#include <iostream>
#include <vector>


void Card::draw(){

    this->camera.Render();
    glPushMatrix();
    glTranslatef(-4.0f, 0.0f, -10.0f);

    ui::CubeImage* img = this->allImages.at(currentImage);
    img->setSize(2.0, 2.0);
    img->draw(GL_RENDER);

    glPopMatrix();
}
void Card::handleKeyBoardPressedEvent(QKeyEvent* event){
    if(event->key() == Qt::Key_Left)
    {
        this->incrementCounter(-1);
    }
    if(event->key() == Qt::Key_Right)
    {
        this->incrementCounter(1);
    }
}
void Card::handleKeyBoardReleasedEvent(QKeyEvent* /*event*/){
	return;
}

//On mouseclick, tag can be added to the current image.
void Card::handleMousePressedEvent(QMouseEvent* /*event*/){
	return;
}

void Card::handleMouseScroll(QWheelEvent* /*event*/){
	this->tagImage();
	return;
}

void Card::tagImage()
{
    string nameOfImage = this->allImages.at(currentImage)->getName(); //name of image to tag

    if(ticm != nullptr)
    {
        delete ticm;
    }
    ticm = new TagImageCardMode(nameOfImage);
}

void Card::handleMouseReleasedEvent(QMouseEvent* /*event*/){
	return;
}

void Card::handleMouseMovedEvent(QMouseEvent* /*event*/){
	return;
}

/**
 * Takes a mode as parameter and switches to that mode
 */
void Card::switchToAnotherMode(std::string mode, int /*imageId*/){
	if (mode == "Card")
		{
			return; // We are already in this mode.
		}
		else if (mode == "Cube")
		{
			// Cell* cell = this->cubecells[this->cubeImages[imageId]->getCellKey()];

			modes::modeData* modeData = new modes::modeData();
			// modeData->images = cell->getImageNames();

			modes::modeManager::instance()->constructMode(modes::CUBEMODE, modeData);
		}
		else if (mode == "Slideshow")
		{
			//cube::Cell* cell = this->cubecells[this->cubeImages[imageId]->getCellKey()];

			modes::modeData* modeData = new modes::modeData();
			//modeData->images = cell->getImageNames();

			modes::modeManager::instance()->constructMode(modes::SLIDESHOWMODE, modeData);
		}
		else if (mode == "Face Recognition")
		{
			/*
			//cube::Cell* cell = this->cubecells[this->cubeImages[imageId]->getCellKey()];

			modes::modeData* modeData = new modes::modeData();
			//modeData->images = cell->getImageNames();

			modes::modeManager::instance()->setModeData(modeData);
			modes::modeManager::instance()->faceMode();*/
		}
		return;
}

void Card::incrementCounter(int incrementValue) {
    currentImage = (currentImage + incrementValue < 0) ? (allImages.size() -1) : ((currentImage + incrementValue) % allImages.size());
}

void Card::load(modes::modeData* md) {
	this->mData = md;
    SF3dVector pos;
    pos.x = -3.0f;
    pos.y = 0.75f;
    pos.z = -8.0f;
    this->camera.SetPosition(pos);
    std::vector<std::string> images = mData->getImages();
    for(unsigned int j = 0 ; j < images.size(); j++) {
        ui::CubeImage *image = new ui::CubeImage(images.at(j), j, 0);
        this->allImages.push_back(image);
    }
    this->currentImage = 0;
}


void Card::initCardMode()
{
    ObjectCube::MultiDimensionalView *mdv = this->mData->getMDV();
    vector<ObjectCube::Cell> cells = mdv->getCells();

    for( auto& cell : cells )
    {
        vector<ObjectCube::StateObject> cellObjects = cell.getObjects();

        for(unsigned int i = 0; i < cellObjects.size(); i++)
        {
            this->allStateObjects.push_back(cellObjects.at(i));
        }
    }

    for(unsigned int j = 0 ; j < this->allStateObjects.size(); j++)
    {
        string _imageName = allStateObjects.at(j).getName();
        ui::CubeImage *image = new ui::CubeImage(_imageName, j, allStateObjects.at(j).getId());
        this->allImages.push_back(image);
    }
}
