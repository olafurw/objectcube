#ifndef CARD_H
#define CARD_H

#include "ui/CubeImage.h"
#include "modes/abstractmode.h"

#include "modes/modeManager.h"
// #include "modes/abstractmode.h"
#include "../../../ObjectCube/Tag/Tag.h"
#include "../../../ObjectCube/TagSet/TagSet.h"
#include "../../../ObjectCube/Tag/AlphanumericalTag.h"
#include "../../../ObjectCube/DataAccess/Root/ObjectDataAccess.h"
#include "../../../ObjectCube/DataAccess/Factories/ObjectDataAccessFactory.h"
#include "../../../ObjectCube/Converters/ObjectConverter.h"
#include "../../../ObjectCube/State/StateObject.h"
#include "../../../ObjectCube/Hub.h"

#include "dialogs/tagimagecardmode.h"

namespace card
{

class Card : public AbstractMode
{
    ObjectCube::MultiDimensionalView *mdv;
    vector<ObjectCube::StateObject> allStateObjects;
    vector<ui::CubeImage*> allImages;
    int currentImage;
    void draw();
    void handleKeyBoardPressedEvent(QKeyEvent* event);
    void handleKeyBoardReleasedEvent(QKeyEvent* event);
    void handleMousePressedEvent(QMouseEvent* event);
    void handleMouseReleasedEvent(QMouseEvent* event);
    void handleMouseMovedEvent(QMouseEvent* event);
    void handleMouseScroll(QWheelEvent *event);
    void switchToAnotherMode(string mode, int imageId);
    int getMouseHoverImageId(GLdouble /*x*/, GLdouble /*y*/){return -1;};
    std::vector<ObjectCube::Object> getClickedCell(GLdouble /*x*/, GLdouble /*y*/){}
    void load(modes::modeData* md);
    void tagImage();
    void incrementCounter(int);
    void initCardMode();

private:
    TagImageCardMode* ticm;
};

}

#endif

