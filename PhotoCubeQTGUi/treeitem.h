#ifndef TREEITEM_H
#define TREEITEM_H

#include <QTreeWidgetItem>
#include <Filters/Filter.h>
#include "common.h"

class TreeItem: public QTreeWidgetItem
{
public:
    TreeItem();
    void *item;
    ObjectCube::Filter *filter;
    TREE_ITEM_TYPES type;
    TREE_ITEM_TYPES getType();
};

#endif // TREEITEM_H
