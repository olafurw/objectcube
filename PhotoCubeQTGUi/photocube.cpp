#include "photocube.h"
#include "ui_photocube.h"
#include "modes/modeManager.h"

PhotoCube::PhotoCube(QWidget *parent) : QMainWindow(parent), ui(new Ui::PhotoCube)
{
    ui->setupUi(this);
    this->setAcceptDrops(true);

    this->mainController = new MainController();
    //This treeview is for tag-sets and hierarchies
    this->leftTree = new TreeView(this);
    //This treeview is for tags
    this->viewTags = new TreeView(this);
    this->viewTags->setDragEnabled(true);
    //For rendering
    this->glWidget = new GLWidget(this);
    //To show list of filters applied to the dataset
    this->filterList = new TreeView(this);

    this->frontTableItem  = nullptr;
    this->upTableItem     = nullptr;
    this->backTableItem   = nullptr;

    this->setupDockWidgets();
    this->leftTree->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(leftTree, SIGNAL( customContextMenuRequested( const QPoint& ) ), this, SLOT( leftTreeContextMenu( const QPoint& ) ) );
    this->mainController->buildTree(leftTree);
    this->setUpRightClickMenuForTreeItems();

    this->setMenuItems();

    //Callback handler for drag 'n drop tagging
    connect(this->glWidget, SIGNAL(dropTagging(QDropEvent*, int)), this, SLOT(dropTaggingSlot(QDropEvent*, int)));
    connect(this->glWidget, SIGNAL(dropTaggingCell(QDropEvent*, vector<ObjectCube::Object>)), this, SLOT(dropTagCellSlot(QDropEvent*, vector<ObjectCube::Object>)));

}

PhotoCube::~PhotoCube()
{
    delete ui;
}

void PhotoCube::setupDockWidgets()
{
    leftDock  = new QDockWidget();
    leftDock->setWindowTitle("Tag-Sets and Hierarchies");
    leftDock->setWidget(this->leftTree);
    leftDockTags = new QDockWidget();
    leftDockTags->setWindowTitle("Tags");
    leftDockTags->setWidget(viewTags);
    leftDockTags->hide();
    rightDock = new QDockWidget();
    rightDock->setWindowTitle("Filters");
    rightDock->setWidget(filterList);

    axisTable = new QTableWidget(3,1);
    QStringList labels;
    labels << "Tag-Set/Hierarchy";
    axisTable->setHorizontalHeaderLabels(labels);
    labels.clear();
    labels << "Front" << "Up" << "Back";
    axisTable->setVerticalHeaderLabels(labels);
    //Later, find a better way to set the size of this table....
    axisTable->setColumnWidth(0, 220);
    axisTable->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(axisTable, SIGNAL( customContextMenuRequested( const QPoint& ) ), this, SLOT( axisTableContextMenu( const QPoint& ) ) );

    rightDockTable = new QDockWidget();
    rightDockTable->setWindowTitle("Axis labels");
    rightDockTable->setWidget(axisTable);
    //I found 142 to be a good number
    //Problem for future Gísli
    rightDockTable->setMaximumHeight(142);
    rightDockTable->setMinimumHeight(142);

    this->setCentralWidget(glWidget);
    this->addDockWidget(Qt::LeftDockWidgetArea, leftDock, Qt::Vertical);
    this->addDockWidget(Qt::LeftDockWidgetArea, leftDockTags, Qt::Vertical);
    this->addDockWidget(Qt::RightDockWidgetArea, rightDockTable, Qt::Vertical);
    this->addDockWidget(Qt::RightDockWidgetArea, rightDock, Qt::Vertical);
}

void PhotoCube::axisTableContextMenu( const QPoint& /*pos*/ )
{
	QPoint currentPos = QCursor::pos();

	QMenu menu(this);
	QAction* remove = new QAction(("&Remove dimension"), this);
	menu.addAction(remove);
	QAction* action = menu.exec(currentPos);

	if(action == remove)
	{
		QModelIndexList list = axisTable->selectionModel()->selectedIndexes();

		// Get the old dimensions
		TreeItem* oldFront = mainController->getFrontAxis();
		TreeItem* oldUp = mainController->getUpAxis();
		TreeItem* oldBack = mainController->getBackAxis();

		// Figure out what dimension we are removing
		for(auto r : list)
		{
			if(r.row() == 0)
			{
				oldFront = nullptr;
			}
			if(r.row() == 1)
			{
				oldUp = nullptr;
			}
			if(r.row() == 2)
			{
				oldBack = nullptr;
			}
		}

		// We are removing at least some dimension
		if(oldFront == nullptr || oldUp == nullptr || oldBack == nullptr)
		{
			// Clear the views and then set the dimensions again
			// But skipping the ones we are going to remove
			glWidget->clear();
			filterList->clear();
			mainController->clearView();

			if(oldFront != nullptr)
			{
				mainController->setAsFront(oldFront, filterList, glWidget);
			}
			if(oldUp != nullptr)
			{
				mainController->setAsUp(oldUp, filterList, glWidget);
			}
			if(oldBack != nullptr)
			{
				mainController->setAsBack(oldBack, filterList, glWidget);
			}

			updateAxisTable();

			// We have at least one dimension to draw
			if(oldFront != nullptr || oldUp != nullptr || oldBack != nullptr)
			{
				mainController->getStateAndDraw(glWidget);
			}
		}
	}
}

void PhotoCube::connectMenu()
{
    //Signals and slots for the menu, Signals good, slots baaaad
    connect(a_import,           SIGNAL(triggered()),    this, SLOT(importImage()));
    connect(a_exitProgram,      SIGNAL(triggered()),    this, SLOT(exitProgram()));
    connect(a_createHierarchy,  SIGNAL(triggered()),    this, SLOT(createHierarchy()));
    connect(a_createTag,        SIGNAL(triggered()),    this, SLOT(createTag()));
    connect(a_createTagSet,     SIGNAL(triggered()),    this, SLOT(createTagSet()));
    connect(a_clear,            SIGNAL(triggered()),    this, SLOT(clearView()));
    connect(a_help,             SIGNAL(triggered()),    this, SLOT(getHelp()));
    connect(a_cubeMode,         SIGNAL(triggered()),    this, SLOT(switchToCubeMode()));
    connect(a_aboutPhotoCube,   SIGNAL(triggered()),    this, SLOT(infoAboutPhotoCube()));
    connect(a_importFolder,     SIGNAL(triggered()),    this, SLOT(importFolder()));
    connect(a_toggleAllWidgets, SIGNAL(triggered()),    this, SLOT(toggleAllWidgets()));
    connect(a_hideFilters,    	SIGNAL(triggered()), 	this, SLOT(toggleFilterList()));
    connect(a_hideTreeView,   	SIGNAL(triggered()), 	this, SLOT(toggleTreeView()));

    connect(this->leftTree, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(treeItemDoubleClicked()));
    connect(this->viewTags, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(treeItemDoubleClicked()));
    connect(this->filterList, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(treeItemDoubleClicked()));
}

void PhotoCube::connectRigthClickOptions()
{
    //Connect right click options for Tag-Sets and hierarchies
	connect(this->leftTree->tv_setAsFront,  SIGNAL(triggered()),    this,   SLOT(setAsFront()));
	connect(this->leftTree->tv_setAsUp,     SIGNAL(triggered()),    this,   SLOT(setAsUp()));
	connect(this->leftTree->tv_addTag,      SIGNAL(triggered()),    this,   SLOT(createTagInTree()));
    connect(this->leftTree->tv_addTagSet,   SIGNAL(triggered()),    this,   SLOT(createTagSet()));
    connect(this->leftTree->tv_addHierarchy,SIGNAL(triggered()),    this,   SLOT(createHierarchy()));
    connect(this->leftTree->tv_setAsBack,   SIGNAL(triggered()),    this,   SLOT(setAsBack()));
    connect(this->leftTree->tv_delete,      SIGNAL(triggered()),    this,   SLOT(deleteTreeItem()));

    //Connect right click options for Tags
    connect(this->viewTags->tv_filter,         		SIGNAL(triggered()), this, SLOT(addTagFilterToList()));
    connect(this->viewTags->tv_addTag,         		SIGNAL(triggered()), this, SLOT(createTagInTree()));
    connect(this->viewTags->tv_viewSlides,   		SIGNAL(triggered()), this, SLOT(viewTagImagesAsSlides()));
    connect(this->filterList->tv_removeFilter, 		SIGNAL(triggered()), this, SLOT(removeFilter()));
}


void PhotoCube::keyPressEvent( QKeyEvent *e )
{
    //If the glWidget has focus, then it handles the event
    //This is done to make it easier to control the events in the glWidget
    if(glWidget->hasFocus()) {
        this->glWidget->handleKeyEvent(e);
    }

    if(e->key() == Qt::Key_Control)
    {
        if(!this->glWidget->hasFocus())
        {
            this->glWidget->setFocus();
        }
    }

    if(e->key() == Qt::Key_Backspace)
    {
        if(!this->leftDockTags->isHidden())
        {
            this->leftDockTags->hide();
            this->leftDock->show();
            this->viewTags->clear();
        }
    }

    if(e->key() == Qt::Key_F1)
    {
        //Put selected item as front
        this->setAsFront();
    }

    if(e->key() == Qt::Key_F2)
    {
        //Put selected item as up
        this->setAsUp();
    }

    if(e->key() == Qt::Key_F3)
    {
        //Put selected item as in
        this->setAsBack();
    }

    if(e->key() == Qt::Key_F4)
    {
        //Set range filter on selected item if that item supports range filters
        TreeItem *selectedItem = reinterpret_cast<TreeItem *>(leftTree->currentItem());
        TagSet* tagSet = reinterpret_cast<TagSet *>(selectedItem->item);

        int typeId = tagSet->getTypeId();
        //If typeId == 1, then we have an alphanumerical tagset which we can not have a range filter on.

        if(typeId == 2)
        {
            mainController->createNumericalRangeFilter(tagSet, filterList);
        }
        if(typeId == 3)
        {
            mainController->createDateRangeFilter(tagSet, filterList);
        }
        if(typeId == 4)
        {
            mainController->createTimeRangeFilter(tagSet, filterList);
        }
        mainController->getStateAndDraw(glWidget);
    }

    if(e->key() == Qt::Key_F5)
    {
        this->clearView();
    }

    if(e->key() == Qt::Key_F8)
    {
        this->toggleTreeView();
    }

    if(e->key() == Qt::Key_F9)
    {
        this->toggleFilterList();
    }

    if(e->key() == Qt::Key_F12)
    {
        this->toggleAllWidgets();
    }
}
// keyReleaseEvent is called when a given key gets the up-state.
void PhotoCube::keyReleaseEvent ( QKeyEvent * event ) {

    if(glWidget->hasFocus()) {
        this->glWidget->handleKeyEvent_up(event);
    }
}

void PhotoCube::setMenuItems()
{
    //Setting up actions
    a_createTag         = new QAction(("&New Tag"), this);
    a_createTag->setShortcut(tr("CTRL+T"));

    a_createTagSet      = new QAction(("&Create new concept (TagSet)"), this);
    a_createTagSet->setShortcut(tr("CTRL+SHIFT+T"));

    a_import            = new QAction(("&Import"), this);
    a_import->setShortcut(tr("CTRL+I"));

    a_importFolder      = new QAction(("&Import Folder"), this);
    a_importFolder->setShortcut(tr("CTRL+SHIFT+I"));

    a_exitProgram       = new QAction(("&Exit"), this);
    a_exitProgram->setShortcut(tr("CTRL+Q"));

    a_createHierarchy      = new QAction(("&New Hierarchy"), this);
    a_createHierarchy->setShortcut(tr("CTRL+H"));

    //a_cardMode          = new QAction(("&Card Mode"), this);
    a_cubeMode			= new QAction(("&Cube Mode"), this);
    a_cubeMode->setShortcut(tr("CTRL+M"));

    a_clear             = new QAction(("&Clear"), this);
    a_clear->setShortcut(tr("CTRL+C"));

    a_help              = new QAction(("&Help"), this);
    a_help->setShortcut(tr("CTRL+F1"));

    a_aboutPhotoCube    = new QAction(("&About"), this);
    a_hideFilters		= new QAction(("&Hide/Show Filter Bar"), this);
    a_hideFilters->setShortcut(tr("CTRL+R"));

    a_hideTreeView		= new QAction(("&Hide/Show Tree Bar"), this);
    a_hideTreeView->setShortcut(tr("CTRL+E"));

    a_toggleAllWidgets		= new QAction(("&Hide/Show All Widgets"), this);
    a_toggleAllWidgets->setShortcut(tr("CTRL+W"));


    connectMenu();
    connectRigthClickOptions();

    //Adding to the menu
    //File
    menu = menuBar()->addMenu("&File");
    menu->addAction(a_import);
    menu->addAction(a_importFolder);
    menu->addSeparator();
    menu->addAction(a_exitProgram);
    //Create
    menu = menuBar()->addMenu("&Create");
    menu->addAction(a_createHierarchy);
    menu->addAction(a_createTagSet);
    menu->addAction(a_createTag);
    //View
    menu = menuBar()->addMenu("&View");
    menu->addAction(a_clear);
    //menu->addAction(a_cardMode);
    menu->addAction(a_cubeMode);
    menu->addSeparator();
    menu->addAction(a_hideFilters);
    menu->addAction(a_hideTreeView);
    menu->addAction(a_toggleAllWidgets);
    //Help
    menu = menuBar()->addMenu("&Help");
    menu->addAction(a_help);
    menu->addAction(a_aboutPhotoCube);
}


void PhotoCube::setUpRightClickMenuForTreeItems()
{
    //Actions for Tags
    viewTags->addAction(viewTags->tv_filter);
    viewTags->addAction(viewTags->tv_addTag);
    viewTags->addAction(viewTags->tv_viewSlides);

    filterList->addAction(filterList->tv_removeFilter);
}

void PhotoCube::leftTreeContextMenu( const QPoint& /*pos*/ )
{
    QPoint currentPos = QCursor::pos();

    TreeItem* curr = reinterpret_cast<TreeItem*>(leftTree->currentItem());

    if(curr)
    {
        QMenu menu(this);
        QAction* a_setAsFront       = new QAction(("&Set as Front"), this);
        QAction* a_setAsUp          = new QAction(("&Set as Up"), this);
        QAction* a_setAsBack        = new QAction(("&Set as Back"), this);

        QAction* a_createTag        = new QAction(("&Create Tag"), this);
        QAction* a_addTag           = new QAction(("&Add Tag"), this);
        QAction* a_addTagSet        = new QAction(("&Add Tag Set"), this);
        QAction* a_addHierarchy     = new QAction(("&Add Hierarchy"), this);

        if(curr->getType() == HIERARCHY)
        {
            menu.addAction(a_setAsFront);
            menu.addAction(a_setAsUp);
            menu.addAction(a_setAsBack);
            menu.addAction(a_addTag);
        }
        else if(curr->getType() == TAGSET)
        {
            menu.addAction(a_setAsFront);
            menu.addAction(a_setAsUp);
            menu.addAction(a_setAsBack);
            menu.addAction(a_createTag);
            menu.addAction(a_addHierarchy);
        }

        QAction* action = menu.exec(currentPos);
        if(action == a_setAsFront)
        {
            setAsFront();
        }
        else if(action == a_setAsUp)
        {
            setAsUp();
        }
        else if(action == a_setAsBack)
        {
            setAsBack();
        }
        else if(action == a_createTag)
        {
            createTag();
        }
        else if(action == a_addTag)
        {
            createTagInTree();
        }
        else if(action == a_addTagSet)
        {
            createTagSet();
        }
        else if(action == a_addHierarchy)
        {
            createHierarchy();
        }
    }
}

//   ----  SLOTS. Call functions in controller  ------

void PhotoCube::addTagFilterToList()
{
    QList <QTreeWidgetItem *> list = filterList->findItems(viewTags->currentItem()->text(0), Qt::MatchExactly);
    if(list.isEmpty())
    {
        TreeItem *selectedItem = reinterpret_cast<TreeItem *>(viewTags->currentItem());
        mainController->addFilter(selectedItem, filterList, true);
        mainController->getStateAndDraw(glWidget);
    }
}


void PhotoCube::clearView()
{
    this->filterList->clear();
    //this->glWidget->reset();
    this->glWidget->clear();
    this->mainController->clearView();

    //Clears the widget deletes all items in the widget.
    this->axisTable->clearContents();
    this->frontTableItem = nullptr;
    this->upTableItem = nullptr;
    this->backTableItem = nullptr;
}

void PhotoCube::createHierarchy()
{
    TreeItem *selectedItem = reinterpret_cast<TreeItem *>(leftTree->currentItem());
    mainController->createHierarchy(selectedItem, leftTree);
}

void PhotoCube::createTag()
{
    mainController->createTag(this->leftTree);
}

void PhotoCube::createTagInTree()
{
    TreeItem *selectedItem = reinterpret_cast<TreeItem *>(leftTree->currentItem());
    mainController->createTag(selectedItem, viewTags);
}

void PhotoCube::createTagSet()
{
    mainController->addTagSet(this->leftTree);
}

void PhotoCube::deleteTreeItem()
{
    TreeItem *item;

    //ObjectCube only supports deleting tag-sets at this point.
    //While it doesn't create problems, this will be allowed to stay because hopefully we'll be able to delete tags at some point.
    if(leftDockTags->isHidden())
    {
         item = reinterpret_cast<TreeItem *>(leftTree->currentItem());
    }
    else
    {
        item = reinterpret_cast<TreeItem *>(viewTags->currentItem());
    }


    if(item->getType() == 2)
    {
    	cout << "deleteTreeItem says: item is " << item->type << endl;
        QDialog *dialog = new QDialog;

        cout << "deleteTreeItem says: created dialog" << endl;
        Ui::deleteItem dialogUi;

        cout << "deleteTreeItem says: created dialog pt2" << endl;
        dialogUi.setupUi(dialog);

        cout << "deleteTreeItem says: dialog set up" << endl;
        // dialogUi.textBrowser->setText("Are you sure you want to delete tag: " + viewTags->currentItem()->text(0));

        cout << "deleteTreeItem says: Waiting for user to accept or cancel" << endl;
        if(dialog->exec() == QDialog::Accepted)
        {
            mainController->deleteTag(item);
            delete item;
        }

    }
    else
    {
        /*
        QDialog *dialog = new QDialog;
        Ui::deleteItem dialogUi;
        dialogUi.setupUi(dialog);
        dialogUi.textBrowser->setText("Are you sure you want to delete tag set: " + treeView->currentItem()->text(0));
        if(dialog->exec() == QDialog::Accepted)
        {
            mainController->deleteTagSet(item);
            delete item;
        }*/
        QMessageBox::information(this, "Unable to delete", "You can not delete a Tag-Set");
    }
}

void PhotoCube::dropTagCellSlot(QDropEvent* /*event*/, vector<ObjectCube::Object> cellObjects) {
    TreeItem *selectedItem = reinterpret_cast<TreeItem*>(viewTags->currentItem());
    cout << "In dropTagCellSlot" << endl;
    this->mainController->addTagToCell(selectedItem, cellObjects);
}

void PhotoCube::dropTaggingSlot(QDropEvent* /*event*/, int imageId)
{
    TreeItem *selectedItem = reinterpret_cast<TreeItem*>(viewTags->currentItem());
    this->mainController->addTagToObject(selectedItem, imageId);
}

void PhotoCube::exitProgram()
{
    exit(0);
}

//Where can I go for help??
void PhotoCube::getHelp()
{
    QMessageBox *helpMessage = new QMessageBox(this);
    helpMessage->setText("To get help, please contact gislio08@ru.is");
    helpMessage->show();
}

void PhotoCube::switchToCubeMode()
{
    modes::modeData* modeData = new modes::modeData(mainController->mdv, mainController->coordinates);
    modes::modeManager::instance()->constructMode(modes::CUBEMODE, modeData);
    mainController->refresh();
    mainController->getStateAndDraw(glWidget);
}

void PhotoCube::importFolder()
{
    mainController->importFolderOfImages();
}

void PhotoCube::importImage()
{
    mainController->importImage();
}

//Hopefully this will reference a website with info about the project as a whole...
void PhotoCube::infoAboutPhotoCube()
{
    QMessageBox *infoMessage = new QMessageBox(this);
    infoMessage->setText("PhotoCube, version 2.0 \nWritten by the Datalab team of Reykjavik University, Iceland \n2011-2014 \n Gisli Kristjan Olafsson \n Hlynur Sigurthorsson \n Grimur Tomasson \n Aslaug Eiriksdottir \n Olafur Waage \n Mirco Pazzaglia \n Giacomo Cesca");
    infoMessage->show();
}


void PhotoCube::removeFilter()
{
    TreeItem *selectedItem = reinterpret_cast<TreeItem*>(filterList->currentItem());
    mainController->removeFilter(selectedItem, filterList, glWidget);
}

void PhotoCube::updateAxisTable()
{
    TreeItem* frontAxis = mainController->getFrontAxis();
    if(frontAxis != nullptr)
    {
    	if(frontTableItem == nullptr)
    	{
    		frontTableItem = new TableWidgetItem();
    		axisTable->setItem(0, 0, frontTableItem);
    	}

    	frontTableItem->item = frontAxis->item;
    	frontTableItem->setText(mainController->convertStringsFromObjectCube(frontAxis));
    	frontTableItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    }
    else
    {
    	if(frontTableItem != nullptr)
    	{
    		frontTableItem->item = nullptr;
    		frontTableItem->setText("");
    	}
    }

    TreeItem* upAxis = mainController->getUpAxis();
    if(upAxis != nullptr)
	{
    	if(upTableItem == nullptr)
		{
    		upTableItem = new TableWidgetItem();
    		axisTable->setItem(0, 1, upTableItem);
		}

    	upTableItem->item = upAxis->item;
    	upTableItem->setText(mainController->convertStringsFromObjectCube(upAxis));
    	upTableItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
	}
    else
    {
    	if(upTableItem != nullptr)
		{
    		upTableItem->item = nullptr;
    		upTableItem->setText("");
		}
    }

    TreeItem* backAxis = mainController->getBackAxis();
    if(backAxis != nullptr)
	{
    	if(backTableItem == nullptr)
		{
    		backTableItem = new TableWidgetItem();
    		axisTable->setItem(0, 2, backTableItem);
		}

    	backTableItem->item = backAxis->item;
    	backTableItem->setText(mainController->convertStringsFromObjectCube(backAxis));
    	backTableItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
	}
    else
    {
    	if(backTableItem != nullptr)
		{
    		backTableItem->item = nullptr;
    		backTableItem->setText("");
		}
    }
}

void PhotoCube::setAsFront()
{
    try
    {
        TreeItem *selectedItem = reinterpret_cast<TreeItem *>(leftTree->currentItem());
        mainController->setAsFront(selectedItem, filterList, glWidget);
        mainController->getStateAndDraw(glWidget);
        updateAxisTable();
    }
    catch (...)
    {
        cout << "Unexpected error" << endl;
    }
}

void PhotoCube::setAsUp()
{
	try
	{
		TreeItem *selectedItem = reinterpret_cast<TreeItem *>(leftTree->currentItem());
		mainController->setAsUp(selectedItem, filterList, glWidget);
		mainController->getStateAndDraw(glWidget);
		updateAxisTable();
	}
    catch (...)
    {
        cout << "Unexpected error" << endl;
    }
}

void PhotoCube::setAsBack()
{
    try
    {
        TreeItem *selectedItem = reinterpret_cast<TreeItem *>(leftTree->currentItem());
        mainController->setAsBack(selectedItem, filterList, glWidget);
        mainController->getStateAndDraw(glWidget);
        updateAxisTable();
    }
    catch (...)
    {
    	cout << "Unexpected error" << endl;
    }
}
void PhotoCube::toggleAllWidgets()
{
    if(this->leftDock->isHidden())
    {
        this->leftDock->show();
        this->rightDock->show();
        this->rightDockTable->show();
    }
    else
    {

        this->leftDock->hide();
        this->leftDockTags->hide();
        this->rightDock->hide();
        this->rightDockTable->hide();
        this->glWidget->setFocus();
    }
}

//Show or hide the filter list on the right side. Triggered by toolbar or F9 key
void PhotoCube::toggleFilterList()
{
    if(rightDock->isHidden())
    {
        rightDock->show();
        rightDockTable->show();
    }
    else
    {
        rightDock->hide();
        rightDockTable->hide();
    }
}

//Show or hide the tree view on the left side of the screen. Triggered by toolbar or F8 key
void PhotoCube::toggleTreeView()
{
    if(!leftDockTags->isHidden())
    {
        leftDockTags->hide();
        viewTags->clear();
    }
    else if(leftDock->isHidden())
    {
        leftDock->show();
    }
    else
    {
        leftDock->hide();
    }
}

void PhotoCube::treeItemDoubleClicked()
{
    TreeItem *selectedItem;

    if(this->leftTree->hasFocus())
    {
        selectedItem = reinterpret_cast<TreeItem *>(this->leftTree->currentItem());
    }
    else if(this->filterList->hasFocus())
    {
        selectedItem = reinterpret_cast<TreeItem *>(this->filterList->currentItem());
    }
    else if(this->viewTags->hasFocus())
    {
        selectedItem = reinterpret_cast<TreeItem *>(this->viewTags->currentItem());
    }

    if(selectedItem->type == TAGSET)
    {
        viewTags->clear();
        mainController->buildEachTagSetTree(selectedItem, viewTags);
        leftDock->hide();
        leftDockTags->show();
    }
    else if(selectedItem->type == HIERARCHY)
    {
        if(filterList->hasFocus())
        {
            TreeItem *selectedItem = reinterpret_cast<TreeItem*>(filterList->currentItem());
            mainController->drillDownHierarchy(selectedItem, filterList, glWidget);
            updateAxisTable();
        }
    }
    else
    {
        //This will add a filter on the Tag that was double clicked
        mainController->treeItemDoubleClicked(selectedItem, filterList, glWidget);
        mainController->getStateAndDraw(glWidget);
    }
}

void PhotoCube::viewTagImagesAsSlides()
{
    /*
    this->clearView();
    if(!rightDock->isHidden())
    {
        this->toggleAllWidgets();
    }
    TreeItem *selectedItem = reinterpret_cast<TreeItem*>(viewTags->currentItem());
    mainController->viewAsSlides(selectedItem, filterList, glWidget);
    */
}


