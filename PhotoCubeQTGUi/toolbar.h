#ifndef TOOLBAR_H
#define TOOLBAR_H

#include <QToolBar>
#include <QAction>

class ToolBar: public QToolBar
{
    Q_OBJECT
public:
    ToolBar(QWidget *parent);
    QAction *t_import;
    QAction *t_importFolder;
    QAction *t_cardMode;
    QAction *t_clear;
    QAction *t_hideFilters;
    QAction *t_hideTreeView;
};

#endif // TOOLBAR_H
