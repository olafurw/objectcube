#include "listwidget.h"

ListWidget::ListWidget(QWidget *parent): QListWidget(parent)
{
    lw_remove       = new QAction(("&Remove"), this);
    lw_setAsFront   = new QAction(("&Set as Front"), this);
    lw_setAsUp      = new QAction(("&Set as Up"), this);
    lw_setAsBack    = new QAction(("&Set as Back"), this);

    this->setContextMenuPolicy(Qt::ActionsContextMenu);
    this->addAction(this->lw_remove);
    this->addAction(this->lw_setAsBack);
    this->addAction(this->lw_setAsFront);
    this->addAction(this->lw_setAsUp);
}
/*
QString ListWidget::convertStringsFromObjectCube(string objectCubeString)
{
    QString name;
    const char *stringValue = objectCubeString.c_str();
    QByteArray encodedString = stringValue;
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    name = codec->toUnicode(encodedString);
    return name;
}

QString ListWidget::convertStringsFromObjectCube(ListWidgetItem *listItem)
{
    QString name;
    if(listItem->getType() == TAG)
    {
        ObjectCube::Tag *tag = reinterpret_cast<ObjectCube::Tag *>(listItem->item);
        int typeId = tag->getTypeId();
        if(typeId == 1)
        {
            name = convertStringsFromObjectCube(tag->valueAsString());
        }
        else
        {
            name = QString::fromStdString(tag->valueAsString());
        }
    }
    else if(listItem->getType() == TAGSET)
    {
        ObjectCube::TagSet *tagset = reinterpret_cast<ObjectCube::TagSet *>(listItem->item);
        name = convertStringsFromObjectCube(tagset->getName());
    }
    else if(listItem->getType() == HIERARCHY)
    {
        ObjectCube::HierarchyNode *hierarchyNode = reinterpret_cast<ObjectCube::HierarchyNode *>(listItem->item);
        name = convertStringsFromObjectCube(hierarchyNode->getName());
    }
    return name;
}*/

void ListWidget::addFilterToList(ListWidgetItem *listItem)
{
    this->addItem(listItem);
    if(listItem->getType() == HIERARCHY)
    {
        ObjectCube::HierarchyNode *rootNode = reinterpret_cast<ObjectCube::HierarchyNode*>(listItem->item);
        rootNode->getBranches();
        this->buildHierarchy(listItem);
    }
}

void buildHierarchyRecursive( ObjectCube::HierarchyNode *hierarchyNode, ListWidgetItem *listItem)
{
    ListWidgetItem *newItem = new ListWidgetItem();
    newItem->item = hierarchyNode;
    newItem->type = HIERARCHY;

    //QString name = convertStringsFromObjectCube(newItem);
    QString name = "Testing";


    newItem->setText(name);


    vector<ObjectCube::HierarchyNode*> branches = hierarchyNode->getBranches();

    for(unsigned int i = 0; i < branches.size(); i++)
    {
        ObjectCube::HierarchyNode *childNode = branches.at(i);
        buildHierarchyRecursive(childNode, newItem);
    }
}

void ListWidget::buildHierarchy(ListWidgetItem *listItem)
{
    ObjectCube::HierarchyNode *rootNode = reinterpret_cast<ObjectCube::HierarchyNode*>(listItem->item);

    vector<ObjectCube::HierarchyNode*> branches = rootNode->getBranches();

    for(unsigned int i = 0; i < branches.size(); i++)
    {
        ObjectCube::HierarchyNode *childNode = branches.at(i);
        buildHierarchyRecursive(childNode, listItem);
    }
}


