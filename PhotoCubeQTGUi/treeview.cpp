#include "treeview.h"

using namespace ObjectCube;


TreeView::TreeView(QWidget *parent): QTreeWidget(parent)
{
    //this->setHeaderLabel("Images");
    this->setHeaderHidden(true);
    this->setContextMenuPolicy(Qt::ActionsContextMenu);


    //Actions available when tree item is right clicked
    tv_filter       	= new QAction(("&Filter"),this);
    tv_addTag       	= new QAction(("&Add Tag"), this);
    tv_addTagSet    	= new QAction(("&Add Tag Set"), this);
    tv_addHierarchy 	= new QAction(("&Add Hierarchy"), this);
    tv_setAsFront   	= new QAction(("&Set as Front"), this);
    tv_setAsBack    	= new QAction(("&Set as Back"), this);
    tv_setAsUp      	= new QAction(("&Set as Up"), this);
    tv_removeFilter 	= new QAction(("&Remove filter"), this);
    tv_removeDimension	= new QAction(("&Remove dimension"), this);
    tv_delete       	= new QAction(("&Delete"), this);
    tv_viewSlides   	= new QAction(("&View as slides"), this);
}





