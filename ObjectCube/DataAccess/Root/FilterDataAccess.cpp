/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#include "FilterDataAccess.h"

#include "../../LayerShared/SharedDefinitions.h"
#include "../../LayerShared/Exception.h"

using namespace ObjectCube;



FilterDataAccess::FilterDataAccess()
{
	setId( INVALID_VALUE );
	setTypeId( INVALID_VALUE );
	setTagSetId( INVALID_VALUE );
	isNull_ = true;	
}


FilterDataAccess::~FilterDataAccess()
{
}


string FilterDataAccess::getSelectionPredicate()
{
	if( isNull() )
	{
		throw Exception( "Filter::getSelectionPredicate", "Attempt to get predicate from an uninitialized filter" );
	}
	return getSelectionPredicate_();
}


string FilterDataAccess::getFiltrationPredicate()
{
	if( isNull() )
	{
		throw Exception( "Filter::getFiltrationPredicate", "Attempt to get predicate from an uninitialized filter" );
	}
	return getFiltrationPredicate_();	
}


