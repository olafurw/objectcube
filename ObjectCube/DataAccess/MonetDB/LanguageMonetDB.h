/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_LANGUAGE_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_LANGUAGE_MONETDB_
#define ObjectCube_LANGUAGE_MONETDB_

#include "../Root/LanguageDataAccess.h"

namespace ObjectCube
{
	
	class LanguageMonetDB : public LanguageDataAccess
	{
	public:
		const string getTranslatedText( const string& entityName, int entityType, int languageId );
		
	};
	
}

#endif