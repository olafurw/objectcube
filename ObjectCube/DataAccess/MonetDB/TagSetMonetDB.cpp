/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "TagSetMonetDB.h"

#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "../Factories/TagDataAccessFactory.h"
#include "../Root/TagDataAccess.h"
#include "../Root/AlphanumericalTagDataAccess.h"
#include "../Root/NumericalTagDataAccess.h"
#include "../Root/TimeTagDataAccess.h"
#include "TimeTagMonetDB.h"
#include "../Root/DateTagDataAccess.h"
#include "DateTagMonetDB.h"
#include "../../LayerShared/Exception.h"
#include "../../LayerShared/TagCommon.h"

using namespace ObjectCube;
using namespace std;



TagSetDataAccess* TagSetMonetDB::create()
{
	CommonMonetDB db;
	stringstream stringStream;
	stringStream << "insert into tag_set ( name, description, type_id, access_id ) values( " << "'" << getName() << "', " << "'" << getDescription() << "', " << getTypeId() << ", " << getAccessId() << ")";
	string sql = stringStream.str();
	
	MapiHdl resultsHandle = db.executeSQL( sql );
	setId( db.getLastAutoincrementedInsertId( resultsHandle ) );
	
	return this;
}


TagSetDataAccess* TagSetMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select d.name, d.description, d.type_id, d.access_id from tag_set d where d.id = ?", id );
	
	char* name = 0;
	char* description = 0;
	
	mapi_bind( resultsHandle, 0, &name );
	mapi_bind( resultsHandle, 1, &description );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &typeId_);
	mapi_bind_var( resultsHandle, 3, MAPI_INT, &accessId_ );

	int gotARow = mapi_fetch_row( resultsHandle );
	if( gotARow )
	{
		if( name != 0 )
		{
			setName( string(name) );
		}
		if( description != 0 )
		{
			setDescription( string(description) );
		}
	}
	
	db.cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("TagSetMonetDB::fetch", "No tag-set retrieved" );
	}
	
	setId( id );

	return this;
}


vector<TagDataAccess*> TagSetMonetDB::getTags()
{	
	CommonMonetDB db;
	
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &id_, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &id_, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &id_, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &id_, CommonMonetDB::BoundParameter::INT ) );
	
	/*
	 select t.id, t.tag_set_id, t.type_id, at.tag_string, -1 as number, current_date() as tag_date, current_time() as tag_time
	 from tag t, alphanumerical_tag at where t.id = at.id and t.tag_set_id = 11
	 UNION ALL
	 select t.id, t.tag_set_id, t.type_id, null as tag_string, nt.number, current_date() as tag_date, current_time() as tag_time
	 from tag t, numerical_tag nt where t.id = nt.id and t.tag_set_id = 11
	 UNION ALL
	 select t.id, t.tag_set_id, t.type_id, null as tag_string, -1 as number, dt.tag_date, current_time() as tag_time
	 from tag t, date_tag dt where t.id = dt.id and t.tag_set_id = 11
	 UNION ALL
	 select t.id, t.tag_set_id, t.type_id, null as tag_string, -1 as number, current_date() as tag_date, tt.tag_time
	 from tag t, time_tag tt where t.id = tt.id and t.tag_set_id = 11
	 */	
	
	MapiHdl resultsHandle = db.executeBoundSQL( " select t.id, t.tag_set_id, t.type_id, at.tag_string, -1 as number, current_date() as tag_date, current_time() as tag_time from tag t, alphanumerical_tag at where t.id = at.id and t.tag_set_id = ? \
												UNION ALL \
												select t.id, t.tag_set_id, t.type_id, null as tag_string, nt.number, current_date() as tag_date, current_time() as tag_time from tag t, numerical_tag nt where t.id = nt.id and t.tag_set_id = ? \
												UNION ALL \
												select t.id, t.tag_set_id, t.type_id, null as tag_string, -1 as number, dt.tag_date, current_time() as tag_time from tag t, date_tag dt where t.id = dt.id and t.tag_set_id = ? \
												UNION ALL \
												select t.id, t.tag_set_id, t.type_id, null as tag_string, -1 as number, current_date() as tag_date, tt.tag_time from tag t, time_tag tt where t.id = tt.id and t.tag_set_id = ? \
												", parameters );
	
	vector<TagDataAccess*> tags;
	
	char* tagString = 0;
	char* tagDate = 0;
	char* tagTime = 0;
	
	int id, tagSetId, typeId, number;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &tagSetId );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &typeId );
	mapi_bind( resultsHandle, 3, &tagString );
	mapi_bind_var( resultsHandle, 4, MAPI_INT, &number );
	mapi_bind( resultsHandle, 5, &tagDate );
	mapi_bind( resultsHandle, 6, &tagTime );
	
	while( mapi_fetch_row( resultsHandle ) )
	{
		TagDataAccess* tag = TagDataAccessFactory::create( typeId );
		tag->setId( id );
		tag->setTagSetId( tagSetId );
		
		switch ( tag->getTypeId() ) 
		{
			case TagCommon::ALPHANUMERICAL :
				dynamic_cast<AlphanumericalTagDataAccess*>( tag )->setName( tagString );
				break;
			case TagCommon::NUMERICAL :
				dynamic_cast<NumericalTagDataAccess*>( tag )->setNumber( number );
				break;
			case TagCommon::DATE :
				DateTagMonetDB::sqlDateToTag( dynamic_cast<DateTagDataAccess*>( tag ), tagDate ); 
				break;
			case TagCommon::TIME :
				TimeTagMonetDB::sqlTimeToTag( dynamic_cast<TimeTagDataAccess*>( tag ), tagTime );
				break;
			default:
				throw new Exception( "TagSetMonetDB::fetchTags_", "Unknown tag type", tag->getTypeId() );
		}
		tags.push_back( tag );		
	}
	
	db.cleanupBound( resultsHandle );
	
	return tags;
}


vector<int> TagSetMonetDB::fetchDimensionIds()
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL("select distinct(d.id) from dimension d where d.tag_set_id = ?", getId() );
	
	int id;
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id );
	
	vector<int> dimensionIds;
	while( mapi_fetch_row( resultsHandle ) )
	{
		dimensionIds.push_back( id );
	}
	db.cleanupBound( resultsHandle );

	return dimensionIds;
}


void TagSetMonetDB::erase()
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		db.executeBoundSQLNoResults( "delete from dimension_tag_object where tag_id in (select t.id from tag t where t.tag_set_id = ? )", getId() );
		db.executeBoundSQLNoResults( "delete from object_tag where tag_id in (select t.id from tag t where t.tag_set_id = ? )", getId() );
		db.executeBoundSQLNoResults( "delete from dimension where tag_set_id = ?", getId() );
		db.executeBoundSQLNoResults( "delete from alphanumerical_tag where tag_set_id = ?", getId() );
		db.executeBoundSQLNoResults( "delete from numerical_tag where tag_set_id = ?", getId() );
		db.executeBoundSQLNoResults( "delete from date_tag where tag_set_id = ?", getId() );
		db.executeBoundSQLNoResults( "delete from time_tag where tag_set_id = ?", getId() );
		db.executeBoundSQLNoResults( "delete from tag where tag_set_id = ?", getId() );
		db.executeBoundSQLNoResults( "delete from tag_set where id = ?", getId() );
	
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}



#endif
