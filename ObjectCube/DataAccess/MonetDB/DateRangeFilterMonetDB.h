/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_DATE_RANGE_FILTER_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_DATE_RANGE_FILTER_MONETDB_
#define ObjectCube_DATE_RANGE_FILTER_MONETDB_

#include "../Root/DateRangeFilterDataAccess.h"

namespace ObjectCube 
{	
	class DateRangeFilterMonetDB : public DateRangeFilterDataAccess
	{
	public:
		DateRangeFilterMonetDB() {;} 
		virtual ~DateRangeFilterMonetDB() {;}
		
	protected:
		virtual string getSelectionPredicate_();
		virtual string getFiltrationPredicate_();
		
	private:
		string getSelectionPostfix_();
		
	};
	
}

#endif