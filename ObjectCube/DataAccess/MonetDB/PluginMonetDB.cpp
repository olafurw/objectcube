/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "PluginMonetDB.h"

#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "PluginTagSetMonetDB.h"
#include "../Root/PluginTagSetDataAccess.h"
#include "../../LayerShared/Exception.h"

using namespace ObjectCube;
using namespace std;



PluginDataAccess* PluginMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select p.id, p.qualified_library_name, p.description from plugin p where p.id = ?", id );
	
	char* name = 0;
	char* description = 0;

	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id_ );
	mapi_bind( resultsHandle, 1, &name );
	mapi_bind( resultsHandle, 2, &description );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	if( gotARow )
	{
		if( name != 0 )
		{
			setName( name );
		}
		if( description != 0 )
		{
			setDescription( description );
		}
	}
	
	db.cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("PluginMonetDB::fetch", "No plugin retrieved" );
	}
	
	setPluginTagSets( fetchPluginTagSets_( id_ ) );
	
	return this;
}


vector<PluginDataAccess*> PluginMonetDB::fetchAll()
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeSQL( "select p.id, p.qualified_library_name, p.description from plugin p" );
	
	int id = 0; 
	char* name = 0;
	char* description = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id );
	mapi_bind( resultsHandle, 1, &name );
	mapi_bind( resultsHandle, 2, &description );
	
	vector<PluginDataAccess*> plugins;
	while( mapi_fetch_row( resultsHandle ) )
	{
		PluginDataAccess* plugin = new PluginMonetDB();
		plugin->setId( id );
		if( name != 0 )
		{
			plugin->setName( name );
		}
		if( description != 0 )
		{
			plugin->setDescription( description );
		}
		
		plugins.push_back( plugin );
	}
	db.cleanup( resultsHandle );

	return plugins;
}


vector<PluginTagSetDataAccess*> PluginMonetDB::fetchPluginTagSets_( int pluginId )
{
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select pt.id, pt.tag_set_type_id, pt.tag_type_id, pt.name, pt.tag_set_name, pt.tag_set_id from plugin_tag_set pt where pt.plugin_id = ?", pluginId );
	
	int id, tagSetTypeId, tagTypeId, tagSetId;
	char* name = 0;
	char* tagSetName = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &tagSetTypeId );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &tagTypeId );
	mapi_bind( resultsHandle, 3, &name );
	mapi_bind( resultsHandle, 4, &tagSetName );
	mapi_bind_var( resultsHandle, 5, MAPI_INT, &tagSetId );
	
	vector<PluginTagSetDataAccess*> pluginTagSets;
	while( mapi_fetch_row( resultsHandle ) )
	{
		PluginTagSetDataAccess* pluginTagSet = new PluginTagSetMonetDB();
		pluginTagSet->setId( id );
		pluginTagSet->setPluginId( pluginId );
		pluginTagSet->setTagSetTypeId( tagSetTypeId );
		pluginTagSet->setTagTypeId( tagTypeId );
		if( name != 0 )
		{
			pluginTagSet->setName( name );
		}
		if( tagSetName != 0 )
		{
			pluginTagSet->setTagSetName( tagSetName );
		}
		
		if( mapi_fetch_field_len( resultsHandle, 5 ) > 0 )
		{
			pluginTagSet->setTagSetId( tagSetId );
		}
		
		pluginTagSets.push_back( pluginTagSet );
	}
	
	db.cleanupBound( resultsHandle );
	
	return pluginTagSets;
}



#endif