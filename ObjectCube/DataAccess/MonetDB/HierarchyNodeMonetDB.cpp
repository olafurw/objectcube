/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "HierarchyNodeMonetDB.h"

#include <sstream>

#include "CommonMonetDB.h"

using namespace ObjectCube;
using namespace std;



HierarchyNodeDataAccess* HierarchyNodeMonetDB::create()
{
	CommonMonetDB db;
	setId( db.getMaxValue( "dimension", "node_id") + 1 );
	stringstream stringStream;
	stringStream << "insert into dimension values( " << getDimensionId() << ", " << getId() << ", " << getTagSetId() << ", " << 
	getTagId() << ", '" << getChildCategoryTitle() << "', " << getLeftBorder() << ", " << getRightBorder() << ")";
	string sql = stringStream.str();
	
	db.beginTransaction();
	try 
	{
		db.executeSQLNoResults( sql );
		//This should be in a trigger, MonetDB has limited support
		db.executeBoundSQLNoResults( "insert into dimension_tag_object \
		select d.id as dimension_id, d.node_id, ot.tag_id, ot.object_id, \
		ot.upper_left_x, ot.upper_left_y, ot.lower_right_x, ot.lower_right_y, ot.confirmed, \
		d.left_border, d.right_border \
		from dimension d, object_tag ot \
		where d.tag_id = ot.tag_id and node_id = ?", getId() );
		
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
	return this;
}


void HierarchyNodeMonetDB::update()
{
	stringstream stringStream;
	stringStream << "update dimension set left_border = " << getLeftBorder() 
	<<  ", right_border = " << getRightBorder()
	<< " where id = " << getDimensionId()
	<< " and node_id = " << getId();
	
	string sql =  stringStream.str();
	
	CommonMonetDB db;
	db.beginTransaction();
	try 
	{
		db.executeSQLNoResults( sql );	
		
		//This should be a trigger, MonetDB has limited support
		stringstream stringStreamMV;
		stringStreamMV << "update dimension_tag_object set left_border = " << getLeftBorder() 
		<<  ", right_border = " << getRightBorder() << "where dimension_id = " << getDimensionId()
		<< " and node_id = " << getId();
		db.executeSQLNoResults( stringStreamMV.str() );
		
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}


void HierarchyNodeMonetDB::remove()
{
	stringstream stringStream;
	stringStream << "delete from dimension where id = " << getDimensionId() << " and node_id = " << getId();
	string sql =  stringStream.str();
	
	
	CommonMonetDB db;
	db.beginTransaction();
	try 
	{
		db.executeSQLNoResults( sql );
		//This should be in a trigger, MonetDB has limited support
		vector<CommonMonetDB::BoundParameter> parameters;
				parameters.push_back( CommonMonetDB::BoundParameter( (void*) &hierarchyId_, CommonMonetDB::BoundParameter::INT ) );
		parameters.push_back( CommonMonetDB::BoundParameter( (void*) &id_, CommonMonetDB::BoundParameter::INT ) );
		db.executeBoundSQLNoResults( "delete from dimension_tag_object where dimension_id = ? and node_id = ?", parameters );
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
	setId( INVALID );
}



#endif
