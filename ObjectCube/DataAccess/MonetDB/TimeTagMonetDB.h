/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_TIME_TAG_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_TIME_TAG_MONETDB_
#define ObjectCube_TIME_TAG_MONETDB_

#include "../Root/TimeTagDataAccess.h"

//An unsavoury forward declaration
struct MapiStatement;
typedef struct MapiStatement *MapiHdl;

using namespace std;

namespace ObjectCube
{
	class CommonMonetDB;
	
	class TimeTagMonetDB : public TimeTagDataAccess
	{
	public:
		
		virtual TagDataAccess* fetch( int id );
		virtual TagDataAccess* create( );
		virtual int inUse( int id );
		virtual void remove( int id );
		
		virtual TimeTagDataAccess* fetch( int tagSetId, int hours, int minutes, int seconds, int milliseconds );  //ATH
		static void sqlTimeToTag( TimeTagDataAccess* tag, const string& timeAsString );
		
	private:
		void processFetch_( MapiHdl resultsHandle, CommonMonetDB* db );
	};
	
}

#endif