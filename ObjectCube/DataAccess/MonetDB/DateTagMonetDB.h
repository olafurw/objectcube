/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_DATE_TAG_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_DATE_TAG_MONETDB_
#define ObjectCube_DATE_TAG_MONETDB_

#include "../Root/DateTagDataAccess.h"

//An unsavoury forward declaration
struct MapiStatement;
typedef struct MapiStatement *MapiHdl;

namespace ObjectCube
{
	class CommonMonetDB;
	
	class DateTagMonetDB : public DateTagDataAccess
	{
	public:
		virtual TagDataAccess* fetch( int id );
		virtual TagDataAccess* create( );
		virtual int inUse( int id );
		virtual void remove( int id );
		
		virtual DateTagDataAccess* fetch( int tagSetId, int year, int month, int dayOfMonth );  
		
		static void sqlDateToTag( DateTagDataAccess* tag, const string& dateAsString ); 
		
	private:
		void processFetch_( MapiHdl resultsHandle, CommonMonetDB* db );
		
	};
	
}

#endif