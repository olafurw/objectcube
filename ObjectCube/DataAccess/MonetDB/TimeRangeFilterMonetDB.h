/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_TIME_RANGE_FILTER_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_TIME_RANGE_FILTER_MONETDB_
#define ObjectCube_TIME_RANGE_FILTER_MONETDB_

#include "../Root/TimeRangeFilterDataAccess.h"

namespace ObjectCube 
{	
	
	class TimeRangeFilterMonetDB : public TimeRangeFilterDataAccess
	{
	public:
		//Constructors 
		TimeRangeFilterMonetDB() {;}
		virtual ~TimeRangeFilterMonetDB() {;}
		
	protected:
		virtual string getSelectionPredicate_();
		virtual string getFiltrationPredicate_();
		
	private:
		string getSelectionPostfix_();
		
	};
	
}

#endif