/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "CommonMonetDB.h"

#include <sstream>
#include <memory>
#include <iomanip>
#include <stdio.h>
#include <string.h>
#include <iostream> //for debug

#include "../../LayerShared/Exception.h"
#include "../../LayerShared/Parameters.h"
#include "../../LayerShared/DebugInfo.h"

using namespace ObjectCube;
using namespace std;



Mapi CommonMonetDB::monetInterfaceDescriptor_;
bool CommonMonetDB::connected_ = false;

//Parameters
/*
string CommonMonetDB::hostParameterName_ = "MonetDB_host";
string CommonMonetDB::portParameterName_ = "MonetDB_port";
string CommonMonetDB::databaseParameterName_ = "MonetDB_database";
string CommonMonetDB::userParameterName_ = "MonetDB_user";
string CommonMonetDB::passwordParameterName_ = "MonetDB_password";
 */

#include <mutex>
#include <iostream>
std::recursive_mutex _hack;


CommonMonetDB::CommonMonetDB() :
	hostParameterName_( "MonetDB_host" ),
	portParameterName_( "MonetDB_port" ),
	databaseParameterName_( "MonetDB_database" ),
	userParameterName_( "MonetDB_user" ),
	passwordParameterName_( "MonetDB_password" )
{
//	std::cerr << "HACK: CommonMonetDB locking!" << std::endl;
	if (!_hack.try_lock()) {
		std::cerr << "HACK: CommonMonetDB access race! Serializing access! (May deadlock)" << std::endl;
		_hack.lock();
	}
	host_ = "localhost";
	port_ = 50000;
	database_ = "objectcube";
	user_ = "monetdb";
	password_ = "monetdb";		
}


CommonMonetDB::~CommonMonetDB()
{
	_hack.unlock();
//	std::cerr << "HACK: CommonMonetDB unlocked!" << std::endl;
}


void CommonMonetDB::connectToDb_()
{
//	DebugInfo::getDebugInfo()->output("CommonMonetDB", "connectToDb_", "Start" );
	if( connected_ )
	{
//		DebugInfo::getDebugInfo()->output("CommonMonetDB", "connectToDb_", "Done - was connected" );
		return;
	}
	
	if( Parameters::getParameters()->contains( hostParameterName_ ) )
	{
		host_ = Parameters::getParameters()->getValue( hostParameterName_ );
	}
	if( Parameters::getParameters()->contains( portParameterName_ ) )
	{
		port_ = Parameters::getParameters()->getValueAsInt( portParameterName_ );
	}
	if( Parameters::getParameters()->contains( databaseParameterName_ ) )
	{
		database_ = Parameters::getParameters()->getValue( databaseParameterName_ );
	}
	if( Parameters::getParameters()->contains( userParameterName_ ) )
	{
		user_ = Parameters::getParameters()->getValue( userParameterName_ );
	}
	if( Parameters::getParameters()->contains( passwordParameterName_ ) )
	{
		password_ = Parameters::getParameters()->getValue( passwordParameterName_ );
	}	
	
	//DebugInfo::getDebugInfo()->output("CommonMonetDB", "connectToDb_", "Preparing to connect" );
	
	monetInterfaceDescriptor_ = mapi_connect( host_.c_str(), port_, user_.c_str(), password_.c_str(), "sql", database_.c_str() );
	checkConnectionStatus_();
	connected_ = true;
	DebugInfo::getDebugInfo()->output("CommonMonetDB", "connectToDb_" , "\tDone - new connection established");
}


void CommonMonetDB::checkConnectionStatus_()
{
	if( mapi_error( monetInterfaceDescriptor_ ) != MOK )  //Connection error
	{
		stringstream sstream;
		sstream << "Error in monetdb connection, error number: " << mapi_error( monetInterfaceDescriptor_ ) << ".  Error string: "  << mapi_error_str( monetInterfaceDescriptor_ );
		DebugInfo::getDebugInfo()->output( "CommonMonetDB", "checkConnectionStatus_", sstream.str() );
		disconnect();
		throw Exception("CommonMonetDB::checkConnectionStatus_", sstream.str() );
	}
}


void CommonMonetDB::processError_( MapiHdl hdl )
{
    stringstream sstream;
    
    if( hdl != MOK ) 
    {
        mapi_explain_query( hdl, stderr );
        sstream << "QUERY =\n" << mapi_get_query( hdl ) << endl;
        do 
        {
            if (mapi_result_error(hdl) != 0)
            {
                mapi_explain_result(hdl, stderr);
                sstream << "ERROR:\n" << mapi_result_error( hdl ) << endl;
            }
        } 
        while( mapi_next_result( hdl ) == 1 );
        cleanup( hdl );
        disconnect();
    }    
    else if (monetInterfaceDescriptor_ != 0) 
    {
        mapi_explain(monetInterfaceDescriptor_, stderr);
        disconnect();
    } 
    else 
    {
         sstream << "Unknown error in MonetDB";   
    }
    throw Exception( "CommonMonetDB::checkExecutionStatus_", sstream.str() );
}


void CommonMonetDB::checkExecutionStatus_( MapiHdl hdl )
{
	//DebugInfo::getDebugInfo()->output("CommonMonetDB", "checkExecutionStatus_ 1", "" );
	
    if( hdl == 0 || mapi_error( monetInterfaceDescriptor_ ) != MOK )
    {
        processError_( hdl );
    }
    
    /*
    stringstream sstream;
=======
	DebugInfo::getDebugInfo()->output("CommonMonetDB", "checkExecutionStatus_ BEGIN", "" );
	stringstream sstream;
	
    if( hdl != 0 && mapi_error( monetInterfaceDescriptor_ ) == MOK ) 
    {
        DebugInfo::getDebugInfo()->output("CommonMonetDB", "checkExecutionStatus_", "OK" );
        return;
    }
>>>>>>> 460ad8f65919850e0c3a28fc311b58a924d05040
	
	if( hdl == 0 || mapi_error( monetInterfaceDescriptor_ ) != MOK ) //Connection error
	{
		mapi_explain( monetInterfaceDescriptor_, stderr );
		sstream << "ERROR:\n" << mapi_error_str( monetInterfaceDescriptor_ ) << endl;
		disconnect();
		throw Exception( "CommonMonetDB::checkExecutionStatus_ - connection", sstream.str() );
	} 
	
	if( mapi_result_error( hdl ) != MOK ) //sql execution error
	{
		mapi_explain_query( hdl, stderr );
		sstream << "QUERY =\n" << mapi_get_query( hdl ) << endl;
		
		do 
		{
			if( mapi_result_error( hdl ) != 0 )
			{
				mapi_explain_result( hdl, stderr );
				sstream << "ERROR:\n" << mapi_result_error( hdl ) << endl;
			}
		} 
		while ( mapi_next_result( hdl ) == 1 );
	 
		cleanup( hdl );
		disconnect();
		throw Exception( "CommonMonetDB::checkExecutionStatus_ - query", sstream.str() );
	 } 
<<<<<<< HEAD
	*/
	//OK
	
	/*
     //Query test:
     if ((ret = mapi_query(dbh, q)) == NULL || mapi_error(dbh) != MOK)
     
     //Update test:
     if (mapi_close_handle(ret) != MOK)
     
     //Connect test:
     if (mapi_error(dbh))
     
          */
	
		//DebugInfo::getDebugInfo()->output("CommonMonetDB", "checkExecutionStatus_ 3", "" );
	
	//OK
    DebugInfo::getDebugInfo()->output("CommonMonetDB", "checkExecutionStatus_ END", "" );
}


int CommonMonetDB::getLastAutoincrementedInsertId( MapiHdl hdl )
{
	return mapi_get_last_id( hdl );
}


void CommonMonetDB::disconnect()
{
	if( connected_ )
	{
		connected_ = false;
		mapi_destroy( monetInterfaceDescriptor_ );
	}
}


void CommonMonetDB::cleanup( MapiHdl hdl )
{
	mapi_clear_bindings( hdl );
	mapi_close_handle( hdl ); /* CLOSE QUERY HANDLE */	
	//delete hdl;
	//hdl = 0;
}


void CommonMonetDB::cleanupBound( MapiHdl hdl )
{
	mapi_clear_params( hdl );
	cleanup( hdl );
}


void CommonMonetDB::beginTransaction()
{
	connectToDb_();
	
	executeSQL( "start transaction;" );
}


void CommonMonetDB::commitTransaction()
{
	connectToDb_();
	
	executeSQL( "commit;" );
}


void CommonMonetDB::rollbackTransaction()
{
	connectToDb_();
	
	executeSQL( "rollback;" );
}


int CommonMonetDB::getMaxValue( const string& table, const string& column )
{
	connectToDb_();
	
	stringstream stringStream;
	stringStream << "SELECT MAX(" << column << ") FROM " << table;
	MapiHdl hdl = executeSQL( stringStream.str() );
	
	int id;
	mapi_bind_var( hdl, 0, MAPI_INT, &id );
	
	int gotARow = mapi_fetch_row( hdl );
	cleanupBound( hdl );
	if( !gotARow )
	{
		throw Exception("CommonMonetDB::getMaxValue", "No id retrieved" );
	}
	return id;
}


MapiHdl CommonMonetDB::executeSQL( const string& sql )
{
	//DebugInfo::getDebugInfo()->output("CommonMonetDB", "executeSQL", sql );
	connectToDb_();
	
	//unique_ptr<MapiHdl> hdl( new MapiHdl() );
	const char* sqlPointer = sql.c_str();
	
//	cout << "sqlPointer: " << *sqlPointer << endl;
	
	MapiHdl hdl = mapi_query( monetInterfaceDescriptor_, sqlPointer );
	
	checkExecutionStatus_( hdl );

	//DebugInfo::getDebugInfo()->output("CommonMonetDB", "executeSQL", "Done" );
	
	return hdl;
}


void CommonMonetDB::executeSQLNoResults( const string& sql )
{	
	MapiHdl hdl = 0;
	try 
	{
		hdl = executeSQL( sql );
	}
	catch ( ... ) 
	{
		if( hdl )
		{
			cleanup( hdl );
		}
		throw;
	}
}


void CommonMonetDB::executeBoundSQLNoResults( const string& sql, int value )
{	
	MapiHdl hdl = 0;
	try 
	{
		hdl = executeBoundSQL( sql, value );
	}
	catch ( ... ) 
	{
		if( hdl )
		{
			cleanupBound( hdl );
		}
		throw;
	}	
}


MapiHdl CommonMonetDB::executeBoundSQL( const string& sql, int value )
{
	//DebugInfo::getDebugInfo()->output("CommonMonetDB", "executeBoundSQL(int)", sql );
	
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &value, CommonMonetDB::BoundParameter::INT ) );

	return executeBoundSQL( sql, parameters );	
}


MapiHdl CommonMonetDB::executeBoundSQL( const string& sql, const string& value )
{
	//DebugInfo::getDebugInfo()->output("CommonMonetDB", "executeBoundSQL(string)", sql );
	
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &value, CommonMonetDB::BoundParameter::STRING ) );
	
	return executeBoundSQL( sql, parameters );
}


MapiHdl CommonMonetDB::executeBoundSQL( const string& sql, vector<CommonMonetDB::BoundParameter>& parameters )
{
	//DebugInfo::getDebugInfo()->output("CommonMonetDB", "executeBoundSQL(vector)", sql );
	connectToDb_();
	
	MapiHdl hdl = 0;
	int noOfParameters = parameters.size() + 1;
	
	//mapi_query_array(dbh, "bat.insert(emp,\"?\",?);", parm)
	char** paramArray = new char*[ noOfParameters ];
	int index = 0;
	for( vector<CommonMonetDB::BoundParameter>::iterator itr = parameters.begin(); itr != parameters.end(); ++itr )
	{
		paramArray[ index++ ] = bindToString_( (*itr).getData(), static_cast<CommonMonetDB::BoundParameter::TYPE>( (*itr).getType() ) );
	}
	paramArray[ noOfParameters - 1 ] = 0; //End with a zero, as per CWI demo code
	
	hdl = mapi_query_array( monetInterfaceDescriptor_, sql.c_str(), paramArray  );
	
	boundParameterCleanup_( paramArray, noOfParameters );
	checkExecutionStatus_( hdl );
	
	return hdl;
}


void CommonMonetDB::executeBoundSQLNoResults( const string& sql, vector<CommonMonetDB::BoundParameter>& parameters )
{	
	MapiHdl hdl = 0;
	try 
	{
		hdl = executeBoundSQL( sql, parameters );
	}
	catch ( ... ) 
	{
		if( hdl )
		{
			cleanupBound( hdl );
		}
		throw;
	}	
	
}


void CommonMonetDB::boundParameterCleanup_( char** parameters, int size )
{
	for( int i = 0; i < size; ++i )
	{
		if( parameters[ i ] != 0 )
		{
			delete[] parameters[ i ];
			parameters[ i ] = 0;
		}
	}
	delete [] parameters;
	parameters = 0;
}


char* CommonMonetDB::bindToString_( void* value, CommonMonetDB::BoundParameter::TYPE type)
{
	stringstream sstream;

	switch ( type ) 
	{
		case CommonMonetDB::BoundParameter::BOOLEAN : 
			sstream << *(static_cast<bool*>( value ));
			break;
		case CommonMonetDB::BoundParameter::INT :
			sstream << *(static_cast<int*>( value ) );
			break;
		case CommonMonetDB::BoundParameter::DOUBLE : 
			sstream << *(static_cast<double*>( value ) );
			break;
		case CommonMonetDB::BoundParameter::STRING :
			sstream << *(static_cast<string*>( value ) );
			break;
			
		default:
			throw Exception( "CommonMonetDB::bindToString_", "Unknown data type in CommonMonetDB::BoundParameter", type );
	}	

	string stringparam = sstream.str();
	return bindString_( &stringparam );	
}


char* CommonMonetDB::bindString_( const string* value )
{
	const char* stringBuffer = value->c_str();
	int len = strlen(stringBuffer ) + 1;
	//unique_ptr<char> param( new char[ len ] );
	char* param = new char[ len ];
	memcpy( param, stringBuffer, len );
	return param;
}


CommonMonetDB::BoundParameter::BoundParameter( void* data, int type )
{
	data_ = data;
	type_ = type;
}


string CommonMonetDB::dateToSQLString( int year, int month, int dayOfMonth )
{
	stringstream stringStream, ssYear, ssMonth, ssDayOfMonth;
	
	//Creating correct sized and zero filled versions
	ssYear << setw(4) << setfill('0') << right << year;
	ssMonth << setw(2) << setfill('0') << right << month;
	ssDayOfMonth << setw(2) << setfill('0') << right << dayOfMonth;
	
	stringStream << "'" << ssYear.str() << "-" << ssMonth.str() << "-" << ssDayOfMonth.str() << "'";
	return stringStream.str();
}


string CommonMonetDB::timeToSQLString( int hours, int minutes, int seconds, int milliseconds )
{
	stringstream stringStream, ssHours, ssMinutes, ssSeconds, ssMilliseconds;
	
	ssHours << setw(2) << setfill('0') << right << hours;
	ssMinutes << setw(2) << setfill('0') << right << minutes;
	ssSeconds << setw(2) << setfill('0') << right << seconds;
	ssMilliseconds << setw(3) << setfill('0') << right << milliseconds;	
	
	stringStream << "'" << ssHours.str() << ":" << ssMinutes.str() << ":" << ssSeconds.str() << "." << ssMilliseconds.str() << "'"; 
	
	return stringStream.str();	
}



#endif
