/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "ObjectMonetDB.h"

#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "../Factories/TagDataAccessFactory.h"
#include "../Factories/ObjectTagDataAccessFactory.h"
#include "../Root/TagDataAccess.h"
#include "../Root/BoundingBoxDataAccess.h"
#include "../../LayerShared/Exception.h"
#include "../../LayerShared/DebugInfo.h"

using namespace ObjectCube;
using namespace std;



ObjectDataAccess* ObjectMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select p.id, p.qualified_name from object p where p.id = ?", id );
	
	processFetch_( resultsHandle, &db );
	setTagInfo( fetchTagInfo_( getId() ) );
	
	return this;
}


ObjectDataAccess* ObjectMonetDB::fetch( const string& qualifiedName )
{
	DebugInfo::getDebugInfo()->output( "ObjectMonetDB", "fetch(name)", "qualifiedName:\n" + qualifiedName );
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select p.id, p.qualified_name from object p where p.qualified_name = '?'", qualifiedName );
	
	processFetch_( resultsHandle, &db );
	setTagInfo( fetchTagInfo_( getId() ) );
	DebugInfo::getDebugInfo()->output( "ObjectMonetDB", "fetch(name)", "Done" );
	return this;
	
}


void ObjectMonetDB::processFetch_( MapiHdl resultsHandle, CommonMonetDB* db )
{
	DebugInfo::getDebugInfo()->output( "ObjectMonetDB", "processFetch_", "Start" );
	char* name = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id_);
	mapi_bind( resultsHandle, 1, &name );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	if( gotARow && name != 0 )
	{
		setName( name );	
	}
	db->cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("ObjectMonetDB::processFetch_", "No object retrieved" );
	}
	DebugInfo::getDebugInfo()->output( "ObjectMonetDB", "processFetch_", "Done" );
}


ObjectDataAccess* ObjectMonetDB::create( )
{
	CommonMonetDB db;
	// cout << " About to insert " << getName() << " into the database";
	MapiHdl resultsHandle = db.executeBoundSQL( "insert into object ( qualified_name ) values ( '?' )", getName() );
	setId( db.getLastAutoincrementedInsertId( resultsHandle ) );
	db.cleanupBound( resultsHandle );
	
	return this;
}


void ObjectMonetDB::erase()
{
	for( vector<ObjectTagDataAccess*>::iterator itr = objectTags_.begin(); itr != objectTags_.end(); ++itr ) //Remove all tags (notice that this is less fault tolerant than deleting all tags, using only object id )
	{
		removeTag( (*itr) );
	}
	
	CommonMonetDB db;
	db.beginTransaction();
	 try 
	 {
		 db.executeBoundSQLNoResults( "delete from object where id = ?", getId() );
		 //This should be in a trigger, MonetDB has limited support
		 db.executeBoundSQLNoResults( "delete from dimension_tag_object where object_id = ?", getId() );
		 db.commitTransaction();
	 }
	 catch( ... ) 
	 {
		 db.rollbackTransaction();
		 throw;
	 }
}


void ObjectMonetDB::addTag( ObjectTagDataAccess* objectTag )
{
	objectTag->create();
}


void ObjectMonetDB::removeTag( ObjectTagDataAccess* objectTag ) 
{
	objectTag->erase();
}


vector<ObjectTagDataAccess*> ObjectMonetDB::fetchTagInfo_( int objectId )
{
	DebugInfo::getDebugInfo()->output( "ObjectMonetDB", "fetchTagInfo_", "Start" );
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select tag_id, upper_left_x, upper_left_y, lower_right_x, lower_right_y, confirmed from object_tag where object_id = ? order by tag_id", objectId );

	char* confirmedText = 0;
	int tagId;
	double upperLeftX, upperLeftY, lowerRightX, lowerRightY;
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &tagId );
	mapi_bind_var( resultsHandle, 1, MAPI_DOUBLE, &upperLeftX );
	mapi_bind_var( resultsHandle, 2, MAPI_DOUBLE, &upperLeftY );
	mapi_bind_var( resultsHandle, 3, MAPI_DOUBLE, &lowerRightX );
	mapi_bind_var( resultsHandle, 4, MAPI_DOUBLE, &lowerRightY );
	mapi_bind( resultsHandle, 5, &confirmedText );
	
	vector<ObjectTagDataAccess*> tagInfo;
	while( mapi_fetch_row( resultsHandle ) )
	{
		std::string confirmed{ confirmedText };

		ObjectTagDataAccess* objectTag = ObjectTagDataAccessFactory::create(); //new ObjectTagDataAccess();
		objectTag->setObjectId( objectId );
		objectTag->setTagId( tagId );
		objectTag->setConfirmed( (confirmed ==  "true" ? true : false) );
		
		Coordinate2DDataAccess upperLeft( upperLeftX, upperLeftY );
		Coordinate2DDataAccess lowerRight( lowerRightX, lowerRightY );
		BoundingBoxDataAccess* bBox = new BoundingBoxDataAccess( upperLeft, lowerRight);
		objectTag->setBoundingBox( bBox );
		
		tagInfo.push_back( objectTag );
	}

	db.cleanupBound( resultsHandle );
	DebugInfo::getDebugInfo()->output( "ObjectMonetDB", "fetchTagInfo_", "Done" );
	return tagInfo;
}
/*
vector<int> ObjectMonetDB::fetchTagIds_( int objectId )
{
	CommonMonetDB db;
	MapiHdl* resultsHandle = db.executeBoundSQL( "select t.id from tag t, object_tag pt where t.id = pt.tag_id and pt.object_id = ? order by t.id", objectId );
	
	int tagId;
	mapi_bind_var( *resultsHandle, 0, MAPI_INT, &tagId );

	vector<int> ids;
	while( mapi_fetch_row( *resultsHandle ) )
	{
		ids.push_back( tagId );
	}
	db.cleanupBound( resultsHandle );

	return ids;
}
 */


vector<ObjectDataAccess*> ObjectMonetDB::fetchAllObjects()
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeSQL( "select p.id, p.qualified_name from object p" );
	
	int id;
	char* name = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id );
	mapi_bind( resultsHandle, 1, &name );
	
	vector<ObjectDataAccess*> objectsDA;
	while( mapi_fetch_row( resultsHandle ) )
	{
		ObjectMonetDB* objectDA = new ObjectMonetDB();
		objectDA->setId( id );
		if( name != 0 )
		{
			objectDA->setName( name );
		}
		//This is highly inefficient, fix if this gets used
		objectDA->setTagInfo( fetchTagInfo_( id ) );
		
		objectsDA.push_back( objectDA );
	}
	db.cleanup( resultsHandle );
	
	return objectsDA;
}



#endif
