/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_ALPHANUMERICAL_TAG_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_ALPHANUMERICAL_TAG_MONETDB_
#define ObjectCube_ALPHANUMERICAL_TAG_MONETDB_

#include "../Root/AlphanumericalTagDataAccess.h"


//An unsavoury forward declaration
struct MapiStatement;
typedef struct MapiStatement *MapiHdl;

namespace ObjectCube
{
	
	class CommonMonetDB;
		
	class AlphanumericalTagMonetDB : public AlphanumericalTagDataAccess
	{
	public:
		virtual AlphanumericalTagDataAccess* fetch( int id );
		virtual AlphanumericalTagDataAccess* fetch( int tagSetId, const string& name );
		virtual AlphanumericalTagDataAccess* create();
		virtual void remove( int id );
		virtual int inUse( int id );
		
	private:
		void processFetch_( MapiHdl resultsHandle, CommonMonetDB* db );
	};
	
}

#endif