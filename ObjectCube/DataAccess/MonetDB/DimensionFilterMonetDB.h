/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_DIMENSION_FILTER_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_DIMENSION_FILTER_MONETDB_
#define ObjectCube_DIMENSION_FILTER_MONETDB_

#include "../Root/DimensionFilterDataAccess.h"

#include <map>

namespace ObjectCube 
{
	
	class DimensionFilterMonetDB : public DimensionFilterDataAccess
	{
	public:
		//Constructors
		DimensionFilterMonetDB() {;}
		virtual ~DimensionFilterMonetDB() {;}
		
	protected:
		virtual string getSelectionPredicate_();
		virtual string getFiltrationPredicate_();
		
	private:
		string getSelectionPredicateUnions_();
		string getSelectionPredicateMV_();
		
		string getFiltrationPredicateIn_();
		string getFiltrationPredicateMV_();
		
		static string dimensionFilterVersion2Parameter_;
	};
	
}

#endif