/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "DateTagMonetDB.h"

#include <sstream>
#include <memory>
#include <stdlib.h>

#include "CommonMonetDB.h"
#include "TagMonetDB.h"
#include "../../LayerShared/Exception.h"
#include "../../LayerShared/DateSupport.h"

using namespace ObjectCube;
using namespace std;



TagDataAccess* DateTagMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, dt.tag_date from tag t, date_tag dt where t.id = dt.id and t.id =   ?", id );
	
	processFetch_( resultsHandle, &db );
	
	return this;
}


DateTagDataAccess* DateTagMonetDB::fetch( int tagSetId, int year, int month, int dayOfMonth )
{
	string formattedDate = CommonMonetDB::dateToSQLString( year, month, dayOfMonth );
	
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagSetId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &formattedDate, CommonMonetDB::BoundParameter::STRING ) );
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, dt.tag_date from tag t, date_tag dt where t.id = dt.id and t.tag_set_id = ?  and tag_date = ?", parameters );
	
	processFetch_( resultsHandle, &db );	
	
	return this;			
}


void DateTagMonetDB::processFetch_( MapiHdl resultsHandle, CommonMonetDB* db )
{
	char* date = 0;

	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id_ );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &tagSetId_ );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &typeId_ );
	mapi_bind( resultsHandle, 3, &date );
	
	
	int gotARow = mapi_fetch_row( resultsHandle );
	if( gotARow )
	{
		DateTagMonetDB::sqlDateToTag( this, date );
	}
	db->cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("NumericalTagMonetDB::processFetch_", "No tag retrieved" );
	}
	
}


void DateTagMonetDB::sqlDateToTag( DateTagDataAccess* tag, const string& dateAsString )
{
	try 
	{
		int startPos = 0, delimiterPos = 0;
		delimiterPos = dateAsString.find( '-', startPos );
		tag->setYear( atoi( dateAsString.substr( startPos, delimiterPos - startPos ).data() ) );
		startPos = delimiterPos + 1;
		delimiterPos = dateAsString.find( '-', startPos );
		tag->setMonth( atoi( dateAsString.substr( startPos, delimiterPos - startPos ).data() ) );
		tag->setDayOfMonth( atoi( dateAsString.substr( delimiterPos + 1 ).data() ) );
	}
	catch (exception& e) 
	{
		throw Exception( "DateTagMonetDB::sqlDateToTag", e.what() );
	}
	catch( ... )
	{
		throw Exception( "DateTagMonetDB::sqlDateToTag", "Unknown error in converting string to DateTag!" );
	}
}


TagDataAccess* DateTagMonetDB::create( )
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		setId( TagMonetDB::createTag( getTagSetId(), getTypeId() ) );
		
		stringstream stringStream;
		stringStream << "insert into date_tag (id, tag_set_id, tag_date) values( " << getId() << ", " << getTagSetId() 
														   << ", " << CommonMonetDB::dateToSQLString( getYear(), getMonth(), getDayOfMonth() ) << " )";
		string sql = stringStream.str();
		
		db.executeSQLNoResults( sql );
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}

	return this;	
}


int DateTagMonetDB::inUse( int id )
{
	return TagMonetDB::tagInUse( id );
}


void DateTagMonetDB::remove( int id )
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		db.executeBoundSQLNoResults( "delete from date_tag where id = ?", id );
		TagMonetDB::removeTag( id );
		
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}



#endif
