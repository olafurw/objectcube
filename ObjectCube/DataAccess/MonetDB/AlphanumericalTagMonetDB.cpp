/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "AlphanumericalTagMonetDB.h"

#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "TagMonetDB.h"
#include "../../LayerShared/Exception.h"
#include "../../LayerShared/StringSupport.h"

using namespace ObjectCube;
using namespace std;



AlphanumericalTagDataAccess* AlphanumericalTagMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, at.tag_string from tag t, alphanumerical_tag at where t.id = at.id and t.id = ?", id );
	
	processFetch_( resultsHandle, &db );
	
	return this;
}


AlphanumericalTagDataAccess* AlphanumericalTagMonetDB::fetch( int tagSetId, const string& name )
{
	string escapedName = StringSupport::neutralizeControlCharacters( name );
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagSetId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &escapedName, CommonMonetDB::BoundParameter::STRING ) );
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, at.tag_string from tag t, alphanumerical_tag at where t.id = at.id and t.tag_set_id = ? and at.tag_string = '?'", parameters );
	
	processFetch_( resultsHandle, &db );	
	
	return this;
}


void AlphanumericalTagMonetDB::processFetch_( MapiHdl resultsHandle, CommonMonetDB* db )
{
	char* name = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id_ );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &tagSetId_ );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &typeId_ );
	mapi_bind( resultsHandle, 3, &name );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	if( gotARow )
	{
		setName( name );	
	}
	db->cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("AlphanumericalTagMonetDB::processFetch_", "No tag retrieved" );
	}

}


AlphanumericalTagDataAccess* AlphanumericalTagMonetDB::create()
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		setId( TagMonetDB::createTag( getTagSetId(), getTypeId() ) );
		
		//backslash all ' and " in original string
		string escapedName = StringSupport::neutralizeControlCharacters( getName() );
		
		stringstream stringStream;
		stringStream << "insert into alphanumerical_tag (id, tag_string, tag_set_id) values( " << getId() << ", '" << escapedName << "', " << getTagSetId() << ")";
		string sql = stringStream.str();
		
		db.executeSQLNoResults( sql );
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
	return this;
}


void AlphanumericalTagMonetDB::remove( int id )
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		db.executeBoundSQLNoResults( "delete from alphanumerical_tag where id = ?", id );
		TagMonetDB::removeTag( id );
		
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}


int AlphanumericalTagMonetDB::inUse( int id )
{
	return TagMonetDB::tagInUse( id );
}



#endif