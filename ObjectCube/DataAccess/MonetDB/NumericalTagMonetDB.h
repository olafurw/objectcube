/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_NUMERICAL_TAG_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_NUMERICAL_TAG_MONETDB_
#define ObjectCube_NUMERICAL_TAG_MONETDB_

#include "../Root/NumericalTagDataAccess.h"

//An unsavoury forward declaration
struct MapiStatement;
typedef struct MapiStatement *MapiHdl;

namespace ObjectCube
{
	class CommonMonetDB;
	
	class NumericalTagMonetDB : public NumericalTagDataAccess
	{
	public:
		virtual NumericalTagDataAccess* fetch( int id );
		virtual NumericalTagDataAccess* fetch( int tagSetId, long number );
		virtual NumericalTagDataAccess* create();
		virtual void remove( int id );
		virtual int inUse( int id );
		
	private:
		void processFetch_( MapiHdl resultsHandle, CommonMonetDB* db );
		
	};
	
}

#endif