/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "TimeRangeFilterMonetDB.h"

#include <sstream>

#include "../../LayerShared/SharedDefinitions.h"
#include "../../LayerShared/Exception.h"
#include "CommonMonetDB.h"

using namespace ObjectCube;



string TimeRangeFilterMonetDB::getSelectionPredicate_()
{
	stringstream stringStream;	
	stringStream << "select o.id, o.qualified_name, ot.tag_id, " << getId() << " as filter_id, " << INVALID_VALUE << " as dimension_node_id, ot.upper_left_x, ot.upper_left_y, ot.lower_right_x, ot.lower_right_y " 
	<< "from object o, object_tag ot, time_tag tt "
	<< "where o.id = ot.object_id and tt.id = ot.tag_id and "
	<< getSelectionPostfix_();
	return stringStream.str();
}


string TimeRangeFilterMonetDB::getFiltrationPredicate_()
{
	stringstream stringStream;
	stringStream << "select distinct( ot.object_id ) as id " 
	<< "from object_tag ot, time_tag tt "
	<< "where tt.id = ot.tag_id and "
	<< getSelectionPostfix_();
	return stringStream.str();
}


string TimeRangeFilterMonetDB::getSelectionPostfix_()
{
	stringstream stringStream;
	stringStream << "tt.tag_time between " 
	<< CommonMonetDB::timeToSQLString( getHoursFrom(), getMinutesFrom(), getSecondsFrom(), getMillisecondsFrom() )
	<< " and " 
	<< CommonMonetDB::timeToSQLString( getHoursTo(), getMinutesTo(), getSecondsTo(), getMillisecondsTo() )
	<< " and tt.tag_set_id = " << getTagSetId();
	
	return stringStream.str();
}



#endif