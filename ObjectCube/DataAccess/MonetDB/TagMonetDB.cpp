/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "TagMonetDB.h"

#include <sstream>
#include "CommonMonetDB.h"
#include "../../LayerShared/Exception.h"

using namespace ObjectCube;
using namespace std;



TagDataAccess* TagMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id from tag t where t.id = ?", id );
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id_ );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &tagSetId_ );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &typeId_ );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	db.cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("TagMonetDB::fetch", "No tag retrieved for id: ", id );
	}

	return this;
}


TagDataAccess* TagMonetDB::create()
{
	setId( TagMonetDB::createTag( getTagSetId(), getTypeId() ) );
	return this;
}

int TagMonetDB::inUse( int id )
{
	return TagMonetDB::tagInUse( id );
}


void TagMonetDB::remove( int id )
{
	TagMonetDB::removeTag( id );
}


int TagMonetDB::createTag( int tagSetId, int typeId )
{
	stringstream stringStream;
	stringStream << "insert into tag (tag_set_id, type_id) values ( " << tagSetId << ", " << typeId << ")";
	string sql = stringStream.str();
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeSQL( sql );
	int id = db.getLastAutoincrementedInsertId( resultsHandle );
	db.cleanup( resultsHandle );
	return id;
}


void TagMonetDB::removeTag( int id )
{
	ostringstream stringStream;
	stringStream << "delete from tag where id = " << id;
	string sql = stringStream.str();

	CommonMonetDB db;
	db.executeSQLNoResults( sql );
	db.executeBoundSQLNoResults( "delete from dimension_tag_object where tag_id = ?", id );
}


int TagMonetDB::tagInUse( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select count(tag_id) from object_tag where tag_id = ?", id );

	int count;
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &count );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	db.cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("TagMonetDB::tagInUse", "No in use count for id: ", id );
	}
	return count;
}




#endif