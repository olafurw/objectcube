/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_PLUGIN_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_PLUGIN_MONETDB_
#define ObjectCube_PLUGIN_MONETDB_

#include "../Root/PluginDataAccess.h"

using namespace std;

namespace ObjectCube
{
	
	class PluginMonetDB : public PluginDataAccess
	{
	public:
		virtual PluginDataAccess* fetch( int id );
		virtual vector<PluginDataAccess*> fetchAll();
		
	private:
		virtual vector<PluginTagSetDataAccess*> fetchPluginTagSets_( int pluginId );
		
	};
	
}

#endif