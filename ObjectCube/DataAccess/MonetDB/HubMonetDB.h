/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_HUBMONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_HUBMONETDB_
#define ObjectCube_HUBMONETDB_

#include "../Root/HubDataAccess.h"

#include <iostream>

namespace ObjectCube
{
	class StateObjectDataAccess;
	class TagSetDataAccess;
	
	class HubMonetDB : public HubDataAccess
	{
	public:
		virtual vector<StateObjectDataAccess*> getObjects( const string& select );
		virtual vector<StateObjectDataAccess*> getObjects( const vector<FilterDataAccess*>& filters );
		
		virtual StateDataAccess getState( const string& select );
		virtual StateDataAccess getState( const vector<FilterDataAccess*>& filters  );

		virtual vector<TagSetDataAccess*> getTagSets();
		
		
		virtual void disconnect();
		virtual void refreshMaterializedViews();
		
	private:
		string filtersToSelect_( const vector<FilterDataAccess*>& filters );

	};
	
}

#endif
