/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_OBJECT_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_OBJECT_MONETDB_
#define ObjectCube_OBJECT_MONETDB_

#include "../Root/ObjectDataAccess.h"
#include <iostream>

//An unsavoury forward declaration
struct MapiStatement;
typedef struct MapiStatement *MapiHdl;

namespace ObjectCube
{
	class TagDataAccess;
	class CommonMonetDB;
	
	class ObjectMonetDB : public ObjectDataAccess
	{
	public:
		virtual ObjectDataAccess* fetch( int id );
		virtual ObjectDataAccess* fetch( const string& qualifiedName );
		virtual ObjectDataAccess* create( );
		virtual void erase();
		
		virtual void addTag( ObjectTagDataAccess* objectTag );
		//virtual void addTag( int objectId, int tagId );
		//virtual void addTag( int objectId, int tagId, BoundingBoxDataAccess boundingBox );
		
		virtual void removeTag( ObjectTagDataAccess* objectTag );
		virtual vector<ObjectDataAccess*> fetchAllObjects();
	
	protected:
		virtual vector<ObjectTagDataAccess*> fetchTagInfo_( int objectId );
		
	private:
		void processFetch_( MapiHdl resultsHandle, CommonMonetDB* db );
		
	};
	
}

#endif
