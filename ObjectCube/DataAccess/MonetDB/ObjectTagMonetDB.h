/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2011. All rights reserved.
 **********************************************************************/

#if !defined(ObjectCube_OBJECT_TAG_MONETDB_) && defined(OC_MONETDB)
#define ObjectCube_OBJECT_TAG_MONETDB_

#include "../Root/ObjectTagDataAccess.h"

namespace ObjectCube
{
	class CommonMonetDB;
	
	class ObjectTagMonetDB : public ObjectTagDataAccess
	{
		virtual void create();
		virtual void erase();
		virtual void modify();
		
	private:
		void addToMaterializedView_( CommonMonetDB* db );
	
	};
}

#endif