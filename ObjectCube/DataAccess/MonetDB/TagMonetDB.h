/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_TAG_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_TAG_MONETDB_
#define ObjectCube_TAG_MONETDB_

#include "../Root/TagDataAccess.h"

namespace ObjectCube
{
	
	class TagMonetDB : public TagDataAccess
	{
	public:
		virtual TagDataAccess* fetch( int id );
		virtual TagDataAccess* create();
		virtual int inUse( int id );
		virtual void remove( int id );
		
		//Support for tag types
		static int createTag( int tagSetId, int typeId );
		static void removeTag( int id );
		static int tagInUse( int id );
		
	};
	
}

#endif