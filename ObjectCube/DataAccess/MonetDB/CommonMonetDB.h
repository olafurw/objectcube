/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_COMMON_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_COMMON_MONETDB_
#define ObjectCube_COMMON_MONETDB_

#include <string>
#include <vector>

#include <monetdb/mapi.h>

using namespace std;

namespace ObjectCube 
{
	
	class CommonMonetDB
	{
	public:
		CommonMonetDB();
		~CommonMonetDB();
		
		//Transaction control
		void beginTransaction();
		void commitTransaction();
		void rollbackTransaction();
		
		MapiHdl executeSQL( const string& sql );
		void executeSQLNoResults( const string& sql );
		
		MapiHdl executeBoundSQL( const string& sql, int value );
		MapiHdl executeBoundSQL( const string& sql, const string& value );
		void executeBoundSQLNoResults( const string& sql, int value );
		
		int getLastAutoincrementedInsertId( MapiHdl hdl );
		
		int getMaxValue( const string& table, const string& column );
		
		void disconnect();
		
		void cleanup( MapiHdl hdl );
		void cleanupBound( MapiHdl hdl );
		
		static string dateToSQLString( int year, int month, int dayOfMonth ); 
		static string timeToSQLString( int hours, int minutes, int seconds, int milliseconds );
		
	protected:
		void connectToDb_();
		void checkConnectionStatus_();
		void checkExecutionStatus_( MapiHdl hdl );
        void processError_( MapiHdl hdl );
		
	private:
		static Mapi monetInterfaceDescriptor_;
		static bool connected_;
		
		string host_;
		int port_;
		string database_;
		string user_;
		string password_;
		
		const string hostParameterName_;
		const string portParameterName_;
		const string databaseParameterName_;
		const string userParameterName_;
		const string passwordParameterName_;
		
	public:
		class BoundParameter
		{
		public:
			BoundParameter( void* data, int type );
			
			void* getData() { return data_; }
			int getType() const { return type_; }
			
			enum TYPE
			{
				BOOLEAN = 1,
				INT = 2,
				DOUBLE = 3,
				STRING = 4

			};
		private:
			void* data_;
			int type_;
		};
		
		MapiHdl executeBoundSQL( const string& sql, vector<CommonMonetDB::BoundParameter>& parameters );
		void executeBoundSQLNoResults( const string& sql, vector<CommonMonetDB::BoundParameter>& parameters );
		
	private:
		//Helpers for binding
		char* bindToString_( void* value, CommonMonetDB::BoundParameter::TYPE type );
		char* bindString_( const string* value );
		
		void boundParameterCleanup_( char** parameters, int size );
		
	};
}

#endif
