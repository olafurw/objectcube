/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2011. All rights reserved.
 **********************************************************************/

#include "ObjectTagMonetDB.h"

#include "CommonMonetDB.h"
#include "../Root/BoundingBoxDataAccess.h"

using namespace ObjectCube;
using namespace std;



void ObjectTagMonetDB::create()
{	
	int tagId = getTagId();
	int objectId = getObjectId();
	bool confirmed = getConfirmed();
	
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &objectId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &confirmed, CommonMonetDB::BoundParameter::BOOLEAN ) );
	
	double upperLeftX = getBoundingBox()->getUpperLeftCorner().getX();
	double upperLeftY = getBoundingBox()->getUpperLeftCorner().getY();
	double lowerRightX = getBoundingBox()->getLowerRightCorner().getX();
	double lowerRightY = getBoundingBox()->getLowerRightCorner().getY();
	
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftX, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftY, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightX, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightY, CommonMonetDB::BoundParameter::DOUBLE ) );

	CommonMonetDB db;
	db.beginTransaction();
	try 
	{
		db.executeBoundSQLNoResults( "insert into object_tag ( object_id, tag_id, confirmed, upper_left_x, upper_left_y, lower_right_x, lower_right_y ) values ( ?, ?, ?, ?, ?, ?, ? )", parameters );
		addToMaterializedView_( &db );
		
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}


void ObjectTagMonetDB::erase()
{
	int tagId = getTagId();
	int objectId = getObjectId();

	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &objectId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagId, CommonMonetDB::BoundParameter::INT ) );
	
	double upperLeftX = getBoundingBox()->getUpperLeftCorner().getX();
	double upperLeftY = getBoundingBox()->getUpperLeftCorner().getY();
	double lowerRightX = getBoundingBox()->getLowerRightCorner().getX();
	double lowerRightY = getBoundingBox()->getLowerRightCorner().getY();
	
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftX, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftY, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightX, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightY, CommonMonetDB::BoundParameter::DOUBLE ) );
	
	CommonMonetDB db;
	db.beginTransaction();
	try 
	{
		db.executeBoundSQLNoResults( "delete from object_tag where object_id = ? and tag_id = ? and upper_left_x = ? and \
									upper_left_y = ? and lower_right_x = ? and lower_right_y = ?", parameters );
		//This should be in a trigger, MonetDB has limited support
        
		db.executeBoundSQLNoResults( "delete from dimension_tag_object where object_id = ? and tag_id = ? and upper_left_x = ? and \
									upper_left_y = ? and lower_right_x = ? and lower_right_y = ?", parameters );
         
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}


void ObjectTagMonetDB::modify() //Confirmed is the only column not in the primary key
{
	int tagId = getTagId();
	int objectId = getObjectId();
	bool confirmed = getConfirmed();
	
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &confirmed, CommonMonetDB::BoundParameter::BOOLEAN ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &objectId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagId, CommonMonetDB::BoundParameter::INT ) );
	
	double upperLeftX = getBoundingBox()->getUpperLeftCorner().getX();
	double upperLeftY = getBoundingBox()->getUpperLeftCorner().getY();
	double lowerRightX = getBoundingBox()->getLowerRightCorner().getX();
	double lowerRightY = getBoundingBox()->getLowerRightCorner().getY();
	
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftX, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftY, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightX, CommonMonetDB::BoundParameter::DOUBLE ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightY, CommonMonetDB::BoundParameter::DOUBLE ) );

	CommonMonetDB db;
	db.beginTransaction();
	try 
	{
		db.executeBoundSQLNoResults( "update object_tag set confirmed = ? where object_id = ? and tag_id = ? and upper_left_x = ? and \
									upper_left_y = ? and lower_right_x = ? and lower_right_y = ?", parameters );
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
}


void ObjectTagMonetDB::addToMaterializedView_( CommonMonetDB* db )
{
	//This should be in a trigger, MonetDB has limited support
	vector<CommonMonetDB::BoundParameter> mvParameters;
	int tagId = getTagId();
	int objectId = getObjectId();
	
	mvParameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagId, CommonMonetDB::BoundParameter::INT ) );
	mvParameters.push_back( CommonMonetDB::BoundParameter( (void*) &objectId, CommonMonetDB::BoundParameter::INT ) );
	
	double upperLeftX = getBoundingBox()->getUpperLeftCorner().getX();
	double upperLeftY = getBoundingBox()->getUpperLeftCorner().getY();
	double lowerRightX = getBoundingBox()->getLowerRightCorner().getX();
	double lowerRightY = getBoundingBox()->getLowerRightCorner().getY();
	
	mvParameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftX, CommonMonetDB::BoundParameter::DOUBLE ) );
	mvParameters.push_back( CommonMonetDB::BoundParameter( (void*) &upperLeftY, CommonMonetDB::BoundParameter::DOUBLE ) );
	mvParameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightX, CommonMonetDB::BoundParameter::DOUBLE ) );
	mvParameters.push_back( CommonMonetDB::BoundParameter( (void*) &lowerRightY, CommonMonetDB::BoundParameter::DOUBLE ) );
	
	db->executeBoundSQLNoResults( "insert into dimension_tag_object \
								 select	d.id as dimension_id, d.node_id, ot.tag_id, ot.object_id, ot.upper_left_x, \
								 ot.upper_left_y, ot.lower_right_x, ot.lower_right_y, ot.confirmed, d.left_border, d.right_border \
								 from	dimension d, object_tag ot \
								 where	ot.tag_id = ? and d.tag_id = ot.tag_id and ot.object_id = ? and ot.upper_left_x = ? and \
								 ot.upper_left_y = ? and ot.lower_right_x = ? and ot.lower_right_y = ?", mvParameters );  
	
}

