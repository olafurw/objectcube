/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "TimeTagMonetDB.h"

#include <sstream>
#include <memory>
#include <stdlib.h>

#include "CommonMonetDB.h"
#include "TagMonetDB.h"
#include "../../LayerShared/Exception.h"
#include "../../LayerShared/TimeSupport.h"

using namespace ObjectCube;
using namespace std;



TagDataAccess* TimeTagMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, tt.tag_time from tag t, time_tag tt where t.id = tt.id and t.id = ?", id );
	
	processFetch_( resultsHandle, &db );
	
	return this;	
}


TimeTagDataAccess* TimeTagMonetDB::fetch( int tagSetId, int hours, int minutes, int seconds, int milliseconds )
{
	string formattedTime = CommonMonetDB::timeToSQLString( hours, minutes, seconds, milliseconds );
	
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagSetId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &formattedTime, CommonMonetDB::BoundParameter::STRING ) );
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, tt.tag_time from tag t, time_tag tt where t.id = tt.id and tt.tag_set_id = ? and tt.tag_time = ?", parameters );
	
	processFetch_( resultsHandle, &db );	
	
	return this;	
}


void TimeTagMonetDB::processFetch_( MapiHdl resultsHandle, CommonMonetDB* db )
{
	char* time = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id_ );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &tagSetId_ );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &typeId_ );
	mapi_bind( resultsHandle, 3, &time );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	if( gotARow )
	{
		TimeTagMonetDB::sqlTimeToTag( this, time );
	}
	db->cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("TimeTagMonetDB::processFetch_", "No tag retrieved" );
	}
}


void TimeTagMonetDB::sqlTimeToTag( TimeTagDataAccess* tag, const string& timeAsString )
{
	int startPos = 0, delimiterPos = 0;
	try 
	{
		delimiterPos = timeAsString.find( ':', startPos );
		tag->setHours( atoi( timeAsString.substr( startPos, delimiterPos - startPos ).data() ) );
		startPos = delimiterPos + 1;
		delimiterPos = timeAsString.find( ':', startPos );
		tag->setMinutes( atoi( timeAsString.substr( startPos, delimiterPos - startPos ).data() ) );
		startPos = delimiterPos + 1;
		delimiterPos = timeAsString.find( '.', startPos );
		tag->setSeconds( atoi( timeAsString.substr( startPos, delimiterPos - startPos ).data() ) );
		tag->setMilliseconds( atoi( timeAsString.substr( delimiterPos + 1 ).data() ) );
	}
	catch (exception& e) 
	{
		throw Exception( "TimeTagMonetDB::sqlTimeToTag", e.what() );
	}
	catch( ... )
	{
		throw Exception( "TimeTagMonetDB::sqlTimeToTag", "Unknown error in converting string to TimeTag!" );
	}
}


TagDataAccess* TimeTagMonetDB::create( )
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		setId( TagMonetDB::createTag( getTagSetId(), getTypeId() ) );
		
		stringstream stringStream;
		stringStream << "insert into time_tag (id, tag_set_id, tag_time) values( " << getId() << ", " << getTagSetId() 
		<< ", " << CommonMonetDB::timeToSQLString( getHours(), getMinutes(), getSeconds(), getMilliseconds() ) << " )";
		string sql = stringStream.str();
		
		db.executeSQLNoResults( sql );
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
	return this;
}


int TimeTagMonetDB::inUse( int id )
{
	return TagMonetDB::tagInUse( id );
}


void TimeTagMonetDB::remove( int id )
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		db.executeBoundSQLNoResults( "delete from time_tag where id = ?", id );
		TagMonetDB::removeTag( id );
		
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}



#endif