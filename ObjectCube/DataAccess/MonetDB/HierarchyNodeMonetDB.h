/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_HIERARCHY_NODE_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_HIERARCHY_NODE_MONETDB_
#define ObjectCube_HIERARCHY_NODE_MONETDB_

#include "../Root/HierarchyNodeDataAccess.h"

namespace ObjectCube
{
	
	class HierarchyNodeMonetDB : public HierarchyNodeDataAccess
	{
	public:
		virtual HierarchyNodeDataAccess* create();
		virtual void update();
		virtual void remove();		
	};
	
}

#endif