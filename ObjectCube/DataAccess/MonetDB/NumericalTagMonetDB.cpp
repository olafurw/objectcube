/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "NumericalTagMonetDB.h"

#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "TagMonetDB.h"
#include "../../LayerShared/Exception.h"

using namespace ObjectCube;
using namespace std;



NumericalTagDataAccess* NumericalTagMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, nt.number from tag t, numerical_tag nt where t.id = nt.id and t.id =  ?", id );
	
	processFetch_( resultsHandle, &db );
	
	return this;
}


NumericalTagDataAccess* NumericalTagMonetDB::fetch( int tagSetId, long number )
{
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagSetId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &number, CommonMonetDB::BoundParameter::INT ) );
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select t.id, t.tag_set_id, t.type_id, nt.number from tag t, numerical_tag nt where t.id = nt.id and t.tag_set_id = ? and nt.number = ?", parameters );
	
	processFetch_( resultsHandle, &db );	
	
	return this;
}


void NumericalTagMonetDB::processFetch_( MapiHdl resultsHandle, CommonMonetDB* db )
{	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id_ );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &tagSetId_ );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &typeId_ );
	mapi_bind_var( resultsHandle, 3, MAPI_INT, &number_ );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	db->cleanupBound( resultsHandle );
	if( !gotARow )
	{
		throw Exception("NumericalTagMonetDB::processFetch_", "No tag retrieved" );
	}
	
}
 

NumericalTagDataAccess* NumericalTagMonetDB::create()
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		setId( TagMonetDB::createTag( getTagSetId(), getTypeId() ) );
		
		stringstream stringStream;
		stringStream << "insert into numerical_tag (id, number, tag_set_id) values( " << getId() << ", '" << getNumber() << "', " << getTagSetId() << ")";
		string sql = stringStream.str();
		
		db.executeSQLNoResults( sql );
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
	return this;
}


void NumericalTagMonetDB::remove( int id )
{
	CommonMonetDB db;
	db.beginTransaction();
	
	try 
	{
		db.executeBoundSQLNoResults( "delete from numerical_tag where id = ?", id );
		TagMonetDB::removeTag( id );
		
		db.commitTransaction();
	}
	catch( ... ) 
	{
		db.rollbackTransaction();
		throw;
	}
	
}


int NumericalTagMonetDB::inUse( int id )
{
	return TagMonetDB::tagInUse( id );
}



#endif