/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_PLUGIN_TAG_SET_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_PLUGIN_TAG_SET_MONETDB_
#define ObjectCube_PLUGIN_TAG_SET_MONETDB_

#include "../Root/PluginTagSetDataAccess.h"

using namespace std;

namespace ObjectCube
{
	
	class PluginTagSetMonetDB : public PluginTagSetDataAccess
	{
	public:
		virtual void updateTagSetId( int id, int tagSetId );
		
	};
	
}

#endif