/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "DateRangeFilterMonetDB.h"

#include <sstream>

#include "../../LayerShared/SharedDefinitions.h"
#include "../../LayerShared/Exception.h"
#include "../../LayerShared/DateSupport.h"
#include "../../LayerShared/FilterCommon.h"
#include "CommonMonetDB.h"

using namespace ObjectCube;



string DateRangeFilterMonetDB::getSelectionPredicate_()
{
	stringstream stringStream;
	stringStream << "select o.id, o.qualified_name, ot.tag_id, " << getId() << " as filter_id, " << INVALID_VALUE << " as dimension_node_id, ot.upper_left_x, ot.upper_left_y, ot.lower_right_x, ot.lower_right_y " 
	<< "from object o, object_tag ot, date_tag dt "
	<< "where o.id = ot.object_id and dt.id = ot.tag_id and "
	<< getSelectionPostfix_();
	return stringStream.str();
}


string DateRangeFilterMonetDB::getFiltrationPredicate_()
{
	stringstream stringStream;
	stringStream << "select distinct( ot.object_id ) as id " 
	<< "from object_tag ot, date_tag dt "
	<< "where dt.id = ot.tag_id and "
	<< getSelectionPostfix_();
	return stringStream.str();
}


string DateRangeFilterMonetDB::getSelectionPostfix_()
{
	stringstream stringStream;
	
	stringStream << "dt.tag_date between " 
	<< CommonMonetDB::dateToSQLString( getYearFrom(), getMonthFrom(), getDayOfMonthFrom() )
	<< " and " 
	<< CommonMonetDB::dateToSQLString( getYearTo(), getMonthTo(), getDayOfMonthTo() )
	<< " and dt.tag_set_id = " << getTagSetId();
	return stringStream.str();	
}



#endif