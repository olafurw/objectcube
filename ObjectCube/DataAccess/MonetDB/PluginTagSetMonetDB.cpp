/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "PluginTagSetMonetDB.h"

#include <sstream>

#include "CommonMonetDB.h"
#include "../../LayerShared/Exception.h"

using namespace ObjectCube;



void PluginTagSetMonetDB::updateTagSetId( int id, int tagSetId )
{
	CommonMonetDB db;
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &tagSetId, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &id, CommonMonetDB::BoundParameter::INT ) );
	
	db.executeBoundSQLNoResults( "update plugin_tag_set set tag_set_id = ? where id = ?", parameters );
	return;
}



#endif