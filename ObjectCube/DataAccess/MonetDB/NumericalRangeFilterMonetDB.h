/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_NUMERICAL_RANGE_FILTER_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_NUMERICAL_RANGE_FILTER_MONETDB_
#define ObjectCube_NUMERICAL_RANGE_FILTER_MONETDB_

#include "../Root/NumericalRangeFilterDataAccess.h"

namespace ObjectCube 
{	
	class NumericalRangeFilterMonetDB : public NumericalRangeFilterDataAccess
	{
	public:
		//Constructors
		NumericalRangeFilterMonetDB() {;}
		virtual ~NumericalRangeFilterMonetDB() {;}
		
	protected:
		virtual string getSelectionPredicate_();
		virtual string getFiltrationPredicate_();
		
	private:
		string getSelectionPostfix_();
		
	};
	
}

#endif