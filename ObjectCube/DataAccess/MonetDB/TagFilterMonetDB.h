/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_TAG_FILTER_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_TAG_FILTER_MONETDB_
#define ObjectCube_TAG_FILTER_MONETDB_

#include "../Root/TagFilterDataAccess.h"

#include <string>

using namespace std;

namespace ObjectCube 
{
	
	class TagFilterMonetDB : public TagFilterDataAccess 
	{
	public:
		//Constructors
		TagFilterMonetDB() {;}
		virtual ~TagFilterMonetDB() {;}
		
	protected:
		virtual string getSelectionPredicate_();
		virtual string getFiltrationPredicate_();
		
	private:
		string getSelectionPostfix_();
		
	};
	
}

#endif