/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_TAG_SET_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_TAG_SET_MONETDB_
#define ObjectCube_TAG_SET_MONETDB_

#include "../Root/TagSetDataAccess.h"

namespace ObjectCube
{
	
	class TagSetMonetDB : public TagSetDataAccess
	{
	public:
		virtual TagSetDataAccess* create();
		virtual TagSetDataAccess* fetch( int id );
		virtual vector<int> fetchDimensionIds();
		virtual void erase();
		virtual vector<TagDataAccess*> getTags();
	};
	
}

#endif
