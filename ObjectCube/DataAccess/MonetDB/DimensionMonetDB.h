/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/


#if !defined(ObjectCube_DIMENSION_MONETDB_) && defined(OC_MONETDB)
//#ifndef ObjectCube_DIMENSION_MONETDB_
#define ObjectCube_DIMENSION_MONETDB_

#include "../Root/DimensionDataAccess.h"

namespace ObjectCube
{
	
	class DimensionMonetDB : public DimensionDataAccess
	{
	public:
		virtual int nextId();
		virtual vector<HierarchyNodeDataAccess*> fetch( int id );
		
	};
	
}

#endif