/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "HubMonetDB.h"

#include <string.h>
#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "../Root/ObjectDataAccess.h"
#include "../Root/TagSetDataAccess.h"
#include "TagSetMonetDB.h"
#include "../Root/TagDataAccess.h"
#include "../Root/FilterDataAccess.h"
#include "../../LayerShared/SharedDefinitions.h"
#include "../../LayerShared/DebugInfo.h"


using namespace ObjectCube;
using namespace std;



vector<StateObjectDataAccess*> HubMonetDB::getObjects( const string& select )
{
	vector<StateObjectDataAccess*> objectsDA;
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeSQL( select );
	
	char* name = 0;
	int objectId = 0, tagId = 0, filterId = 0, dimensionNodeId = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &objectId );
	mapi_bind( resultsHandle, 1, &name );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &tagId );
	mapi_bind_var( resultsHandle, 3, MAPI_INT, &filterId );
	mapi_bind_var( resultsHandle, 4, MAPI_INT, &dimensionNodeId );
	
	StateObjectDataAccess* stateObjectDA = 0; 
	StateTagDataAccess* stateTagDA = 0;
	
	int lastObjectId = 0, lastTagId = 0;
	
	while( mapi_fetch_row( resultsHandle ) )
	{
		if( objectId != lastObjectId ) //New object
		{
			if( stateObjectDA != 0 )
			{
				objectsDA.push_back( stateObjectDA);
			}
			lastObjectId = objectId;
			lastTagId = 0; //New iteration
			
			stateObjectDA = new StateObjectDataAccess();
			stateObjectDA->setId( objectId );
		}
		
		if( stateObjectDA != 0 && tagId != lastTagId )
		{
			lastTagId = tagId;
			stateTagDA = new StateTagDataAccess();
			stateTagDA->setTagId( tagId );
			stateObjectDA->addTag( stateTagDA );
			
			//Sækjum tögin í application lagi (úr tag-set, ekki DB).
		}
		
		if( stateTagDA != 0 && filterId != INVALID_VALUE )
		{
			stateTagDA->addFilterAndDimensionNodeId( filterId, dimensionNodeId );		
		}	
		
		if( stateObjectDA != 0 && name != 0 )
		{
			stateObjectDA->setName( name );
		}
		
	}
	
	if( stateObjectDA != 0 )
	{
		objectsDA.push_back( stateObjectDA);
	}
	
	db.cleanup( resultsHandle );
	
	return objectsDA;
}


vector<StateObjectDataAccess*> HubMonetDB::getObjects( const vector<FilterDataAccess*>& filters )
{
	string select = filtersToSelect_( filters );
	DebugInfo::getDebugInfo()->output( "HubMonetDB", "getObjects", "select:\n" + select );
	
	return getObjects( select );
}


string HubMonetDB::filtersToSelect_( const vector<FilterDataAccess*>& filters )
{
	string select = "SELECT c.ID, c.QUALIFIED_NAME, c.TAG_ID, c.FILTER_ID, c.DIMENSION_NODE_ID, c.UPPER_LEFT_X, c.UPPER_LEFT_Y, c.LOWER_RIGHT_X, c.LOWER_RIGHT_Y\nFROM\n(\n\tSELECT b.ID, b.QUALIFIED_NAME, b.TAG_ID, b.FILTER_ID, b.DIMENSION_NODE_ID, b.UPPER_LEFT_X, b.UPPER_LEFT_Y, b.LOWER_RIGHT_X, b.LOWER_RIGHT_Y\n\tFROM\n\t(\n";	
	
	string filtrationSubSelect;
	bool connectRestrictions = false;
	for( vector<FilterDataAccess*>::const_iterator itr = filters.begin(); itr != filters.end(); ++itr )
	{
		string restriction = (*itr)->getSelectionPredicate();
		string filtration = (*itr)->getFiltrationPredicate();
		if( connectRestrictions )
		{
			select += "\n\t\tUNION ALL\n"; 
			filtrationSubSelect += "\n\t\tINTERSECT\n";
		}
		connectRestrictions = true;
		select += restriction;
		filtrationSubSelect += filtration;
	}
	//Having gone through all filters
	
	if( filtrationSubSelect.length() == 0 ) //No filters in effect
	{
		stringstream stringStream;
		stringStream	<< "select o.id, o.qualified_name, ot.tag_id, " << INVALID_VALUE << " as filter_id, " << INVALID_VALUE << " as dimension_node_id, ot.upper_left_x, ot.upper_left_y, ot.lower_right_x, ot.lower_right_y "
		<< "from object o, object_tag ot "
		<< "where o.id = ot.object_id "
		<< "order by ID, TAG_ID, FILTER_ID desc";
		select = stringStream.str();
	}
	else 
	{
		select += "\n\t) as b,\n\t(\n";
		select += filtrationSubSelect;
		select += "\n\t) as a\n\twhere b.id = a.id\n\tUNION ALL\n";
		
		stringstream additionalTagsStream;
		additionalTagsStream << "select	ot.object_id as id, '' as qualified_name, ot.tag_id, " << INVALID_VALUE << " as filter_id, " << INVALID_VALUE << " as dimension_node_id, ot.upper_left_x, ot.upper_left_y, ot.lower_right_x, ot.lower_right_y "
		<< " from object_tag ot where ot.object_id in ( " <<  filtrationSubSelect << "\n\t)\n) as c\norder by c.ID, c.TAG_ID, c.FILTER_ID desc";
		
		select += additionalTagsStream.str();
	}
	
	setObjectQueryAsString( select );
	return select;
}


StateDataAccess HubMonetDB::getState( const string& select )
{
	vector<StateObjectDataAccess*> objectsDA;
	map<int, map<int, vector<int> > > filterIdNodeIdObjectIds;
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeSQL( select );
	
	char* name = 0;
	int objectId = 0, tagId = 0, filterId = 0, dimensionNodeId = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &objectId );
	mapi_bind( resultsHandle, 1, &name );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &tagId );
	mapi_bind_var( resultsHandle, 3, MAPI_INT, &filterId );
	mapi_bind_var( resultsHandle, 4, MAPI_INT, &dimensionNodeId );
	
	StateObjectDataAccess* stateObjectDA = 0; 
	StateTagDataAccess* stateTagDA = 0;
	
	int lastObjectId = 0, lastTagId = 0;
	
	while( mapi_fetch_row( resultsHandle ) )
	{
		if( objectId != lastObjectId ) //New object
		{
			if( stateObjectDA != 0 )
			{
				DebugInfo::getDebugInfo()->output( "HubMonetDB", "getState", "name:\n" + stateObjectDA->getName() );
				objectsDA.push_back( stateObjectDA);
			}
			lastObjectId = objectId;
			lastTagId = 0; //New iteration
			
			stateObjectDA = new StateObjectDataAccess();
			stateObjectDA->setId( objectId );
		}
		
		if( stateObjectDA != 0 && tagId != lastTagId )
		{
			lastTagId = tagId;
			stateTagDA = new StateTagDataAccess();
			stateTagDA->setTagId( tagId );
			stateObjectDA->addTag( stateTagDA );
			
			//Sækjum tögin í application lagi (úr tag-set, ekki DB).
		}
		
		if( stateTagDA != 0 && filterId != INVALID_VALUE )
		{
			stateTagDA->addFilterAndDimensionNodeId( filterId, dimensionNodeId );		
			
			filterIdNodeIdObjectIds[ filterId ][dimensionNodeId].push_back( objectId );
		}	
		
		if( stateObjectDA != 0 && strlen(name) )
		{
			stateObjectDA->setName( string(name) );
		}
		
	}
	
	if( stateObjectDA != 0 )
	{
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getState", "name:\n" + stateObjectDA->getName() );
		objectsDA.push_back( stateObjectDA);
	}
	
	db.cleanup( resultsHandle );
	
	StateDataAccess retState;
	retState.setObjects( objectsDA );
	retState.setFilterIdNodeIdObjectsIds( filterIdNodeIdObjectIds );
	
	return retState;	
}


StateDataAccess HubMonetDB::getState( const vector<FilterDataAccess*>& filters  )
{
	string select = filtersToSelect_( filters );
	DebugInfo::getDebugInfo()->output( "HubMonetDB", "getState", "select:\n" + select );

	return getState( select );		
}

vector<TagSetDataAccess*> HubMonetDB::getTagSets()
{
	DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "Start" );
	
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeSQL( "select t.id, t.name, t.description, t.type_id, t.access_id from tag_set t" );
	
	DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "1" );
	
	int id, typeId, accessId;
	char* name = 0;
	char* description = 0;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &id );
	mapi_bind( resultsHandle, 1, &name );
	mapi_bind( resultsHandle, 2, &description );
	mapi_bind_var( resultsHandle, 3, MAPI_INT, &typeId );
	mapi_bind_var( resultsHandle, 4, MAPI_INT, &accessId );
	
	DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "2" );

	vector<TagSetDataAccess*> tagSetsDA;
	while( mapi_fetch_row( resultsHandle ) )
	{
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "3" );
		TagSetDataAccess* tagSetDA = new TagSetMonetDB();
		
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "3 - before id" );
		tagSetDA->setId( id );
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "3 - before name" );
		if( name != 0 )
		{
			tagSetDA->setName( string(name) );
		}
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "3 - before description" );
		if( description != 0 )
		{
			tagSetDA->setDescription( string(description) );
		}
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "3 - before type id" );
		tagSetDA->setTypeId( typeId );
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "3 - before access id" );
		tagSetDA->setAccessId( accessId );
		DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "3 - before push back of item" );
		
		tagSetsDA.push_back( tagSetDA );
	}
	DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "Before cleanup" );
	db.cleanup( resultsHandle );
	DebugInfo::getDebugInfo()->output( "HubMonetDB", "getTagSets", "Done" );
	return tagSetsDA;
}


void HubMonetDB::disconnect()
{
	CommonMonetDB db;
	db.disconnect();
}


void HubMonetDB::refreshMaterializedViews()
{
	CommonMonetDB db;
	db.executeSQLNoResults( "delete from dimension_tag_object" );

	
	db.executeSQLNoResults( "insert into dimension_tag_object \
								  select d.id as dimension_id, d.node_id, ot.tag_id, ot.object_id, ot.upper_left_x, ot.upper_left_y, ot.lower_right_x, ot.lower_right_y, d.left_border, d.right_border \
								  from dimension d, object_tag ot \
								  where d.tag_id = ot.tag_id \
								  order by d.node_id" );
}


#endif
