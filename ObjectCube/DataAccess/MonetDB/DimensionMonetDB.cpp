/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "DimensionMonetDB.h"

#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "../Root/HierarchyNodeDataAccess.h"
#include "HierarchyNodeMonetDB.h"

using namespace ObjectCube;
using namespace std;



int DimensionMonetDB::nextId()
{
	CommonMonetDB db;
	return db.getMaxValue( "dimension", "id" ) + 1;
}


vector<HierarchyNodeDataAccess*> DimensionMonetDB::fetch( int id )
{
	CommonMonetDB db;
	MapiHdl resultsHandle = db.executeBoundSQL( "select d.id, d.node_id, d.tag_set_id, d.tag_id, d.child_category_title, d.left_border, d.right_border from dimension d where d.id = ? order by d.left_border asc", id );
	
	char* childCategoryTitle = 0;
	int dimensionId, nodeId, tagSetId, tagId, leftBorder, rightBorder;
	
	mapi_bind_var( resultsHandle, 0, MAPI_INT, &dimensionId );
	mapi_bind_var( resultsHandle, 1, MAPI_INT, &nodeId );
	mapi_bind_var( resultsHandle, 2, MAPI_INT, &tagSetId );
	mapi_bind_var( resultsHandle, 3, MAPI_INT, &tagId );
	mapi_bind( resultsHandle, 4, &childCategoryTitle );
	mapi_bind_var( resultsHandle, 5, MAPI_INT, &leftBorder );
	mapi_bind_var( resultsHandle, 6, MAPI_INT, &rightBorder );
		
	vector<HierarchyNodeDataAccess*> nodes;
	while( mapi_fetch_row( resultsHandle ) )
	{
		HierarchyNodeDataAccess* node = new HierarchyNodeMonetDB();
		node->setHierarchyId( dimensionId );
		node->setId( nodeId );
		node->setTagSetId( tagSetId );
		node->setTagId( tagId );
		if( childCategoryTitle != 0 )
		{
			node->setChildCategoryTitle( childCategoryTitle );
		}
		node->setLeftBorder( leftBorder );
		node->setRightBorder( rightBorder );

		nodes.push_back( node );
	}
	db.cleanupBound( resultsHandle );
	return nodes;	
}



#endif