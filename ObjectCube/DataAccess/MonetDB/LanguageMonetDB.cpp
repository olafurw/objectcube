/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#ifdef OC_MONETDB

#include "LanguageMonetDB.h"

#include <sstream>
#include <memory>

#include "CommonMonetDB.h"
#include "../../LayerShared/Exception.h"

using namespace ObjectCube;
using namespace std;




const string LanguageMonetDB::getTranslatedText( const string& entityName, int entityType, int languageId )
{
	CommonMonetDB db;
	vector<CommonMonetDB::BoundParameter> parameters;
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &entityName, CommonMonetDB::BoundParameter::STRING ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &entityType, CommonMonetDB::BoundParameter::INT ) );
	parameters.push_back( CommonMonetDB::BoundParameter( (void*) &languageId, CommonMonetDB::BoundParameter::INT ) );
	
	MapiHdl resultsHandle = db.executeBoundSQL( "select translated_text from translation where entity_name = '?' and entity_id = ? and language_id = ?", parameters );
	
	char* text = 0;

	mapi_bind( resultsHandle, 0, &text );
	
	int gotARow = mapi_fetch_row( resultsHandle );
	string translated;
	if( gotARow && text != 0)
	{
		translated = text;
	}
	db.cleanupBound( resultsHandle );
	if( !gotARow )
	{
		stringstream stringStream;
		stringStream << "No text was found for entityName = " << entityName << ", entityType = " << entityType << ", languageId = " << languageId << endl;
		throw Exception( "LanguageMonetDB::getTranslatedText", stringStream.str() );
	}
	
	return translated;
}



#endif