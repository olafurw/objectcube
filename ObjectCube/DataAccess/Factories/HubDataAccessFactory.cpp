/*
 *  HubDataAccessFactory.cpp
 *  Prada
 *
 *  Created by Grímur Tómasson on 8.2.2010.
 *  Copyright 2010 RU. All rights reserved.
 *
 */

#include "HubDataAccessFactory.h"

#include <sstream>

#include "../Root/HubDataAccess.h"
#include "../MonetDB/HubMonetDB.h"

#include "../../LayerShared/HubCommon.h"
#include "../../LayerShared/Exception.h"

using namespace ObjectCube;

std::unique_ptr<HubDataAccess> HubDataAccessFactory::create()
{
	switch ( HubCommon::getDataAccessType() ) 
	{
		case HubCommon::MONETDB: return std::unique_ptr<HubDataAccess>{new HubMonetDB()};
	}
	stringstream stringStream;
	stringStream << "Unknown data access type: " << HubCommon::getDataAccessType();
	throw Exception( "HubDataAccessFactory::create", stringStream.str() );
}
