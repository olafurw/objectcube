/*
 *  Framework.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 25.10.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */

#ifndef ObjectCube_HUB_
#define ObjectCube_HUB_

#include <vector>
#include <memory>
#include <map>

#include "LayerShared/HubCommon.h"
#include "Object.h"
#include "State/StateObject.h"
#include "State/State.h"
#include "LayerShared/LanguageCommon.h"
#include "Plugin/PluginTaggingProcessed.h"
#include "Utils/Log.h"

using namespace std;

namespace ObjectCube 
{
	class TagSet;
	class Tag;
	class Filter;
	class PluginObject;
	class PluginServer;
	class PluginCommon;
	class PluginTagSet;
	class PluginHub;
	class Dimension;
	class PersistentDimension;
	class VirtualDimension;

	/*
	 This class is not a part of the public interface
	 */
	class Hub : public HubCommon //Singleton
	{
	public:
		typedef vector<Filter*> Filters; // TODO: replace with filters encaps

	public:
		static Hub* getHub();
		static void destroyHub();

		TagSet* addTagSet( TagSet *tagSet ) __attribute__ ((deprecated));
		void removeTagSet( TagSet* /*const*/ tagSet ) __attribute__ ((deprecated));
		//Setja inn addSystemDimension fall, á ekki heima í TagSet tréinu
		
		void addObject( Object& object );
		void addObjects( std::vector<Object*>& objects );

		bool isProcessing();
		unsigned int itemsRemaining() const;
		unsigned int itemsTotal() const;

		int getParentTagSetsAccessType( const Tag* /*const*/ tag ) const __attribute__ ((deprecated));  //Helper function, Tags only have access by association with dimensions (inheritance)
		const Tag* /*const*/ getUncategorizedTag() const;  //The one and only uncategorized tag from the uncategorized dimension.

		vector<StateObject> getObjects(const Filters &filters);

		vector<TagSet*> getTagSets() const __attribute__ ((deprecated));
		TagSet* getTagSet( int id ) __attribute__ ((deprecated));
		TagSet* getTagSet( const string& name ) __attribute__ ((deprecated));
		
//		unsigned int storeState();
//		void removeState(unsigned int index);
//		void clearStates();
//		shared_ptr<State> getState(unsigned int index);
//		shared_ptr<State> getCurrentState();
//		map<unsigned int, shared_ptr<State>> getStates();
//		unsigned int getStateCount();

		State getState(const Filters &filters);

		const vector<Dimension*> getDimensions() const __attribute__ ((deprecated));
		const vector<PersistentDimension*> getPersistentDimensions() const __attribute__ ((deprecated));
		const vector<VirtualDimension*> getVirtualDimensions() const __attribute__ ((deprecated));
		
		const vector<Tag*> getTags( const vector<int>& tagIds ) const __attribute__ ((deprecated)); //Objects only store pointers to tags, the tag-sets own and manage tag memory.
		const vector<Tag*> getAllTags() const __attribute__ ((deprecated)); //All tags in the system
		const vector<Tag*> getTagsByTagSet( int id ) const __attribute__ ((deprecated));
		const Tag* /*const*/ getTag( int id ) const __attribute__ ((deprecated));
		
		static int getLanguageId() { return LanguageCommon::getLanguageId_(); }
		static void setLanguageId( int languageId ) { LanguageCommon::setLanguageId_( languageId ); }
		
		const string getLastObjectsQueryAsString() { return lastGetObjectsQueryAsString_; }  //For debugging and experiments

		void destroy();
		void refreshMaterializedViews();
		
		void confirmTagging( int objectId, const string& tagSetName, const BoundingBox& boundingBox, const string& tag );

		~Hub();
	protected:
		Hub();
		Hub( const Hub& hub ); //Not implemented on purpose
		Hub& operator=( const Hub& hub ); //Not implemented on purpose
		
	private:
		static void init_();
		void tagObject_(  const vector<PluginTaggingProcessed>& potentialTags, Object& object );
		void fillInAndValidateFilters_(Filters &filter );
		
		//Memory management
		void cleanup_();
		
	private:
		int dataAccessType_;
		string lastGetObjectsQueryAsString_;
		unique_ptr<PluginHub> pluginHub_;

		static unique_ptr<Hub> instance_;
		
		std::unique_ptr<Utils::Log> log_;

		friend class HubTest;
		friend class PluginHub;
	};
}

#endif
