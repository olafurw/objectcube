/*
 *  Photo.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 25.10.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */

//Declaration
#include "Object.h"

//Data access
#include "DataAccess/Factories/ObjectDataAccessFactory.h"
#include "DataAccess/Root/ObjectDataAccess.h"
#include "Converters/ObjectConverter.h"
#include "Converters/ObjectTagConverter.h"
//Other local
#include "Hub.h"
#include "TagSet/TagSet.h" //Due to enum, consider another placement for it 
#include "LayerShared/SharedDefinitions.h"
#include "LayerShared/Exception.h"
#include "LayerShared/MemoryManagement.h"
#include "LayerShared/DebugInfo.h"
#include "Tag/Tag.h"
#include "Tag/TagFactory.h"
#include "Tag/AlphanumericalTag.h"

using namespace ObjectCube;

Object::Object()
{
	log_ = std::unique_ptr<Utils::Log>( new Utils::Log("oc_object.log") );
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	init_();
}

Object::Object( const string& name )
{
	log_ = std::unique_ptr<Utils::Log>( new Utils::Log("oc_object.log") );
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	init_();
	name_ = name;
}

Object::Object( const Object& o )
{
	log_ = std::unique_ptr<Utils::Log>( new Utils::Log("oc_object.log") );
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	name_ = o.getName();
	id_ = o.getId();
	tags_ = o.getTags();
}

Object::Object( Object&& o )
{
	log_ = std::unique_ptr<Utils::Log>( new Utils::Log("oc_object.log") );
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	name_ = std::move(o.name_);
	id_ = o.id_;
	tags_ = std::move(o.tags_);

	o.id_ = 0;
}

Object::~Object()
{

}

Object& Object::operator=( const Object& o )
{
	if(this != &o)
	{
		name_ = o.getName();
		id_ = o.getId();
		tags_ = o.getTags();
	}

	return *this;
}

Object& Object::operator=( Object&& o )
{
	if(this != &o)
	{
		name_ = std::move(o.name_);
		id_ = o.id_;
		tags_ = std::move(o.tags_);

		o.id_ = 0;
	}

	return *this;
}

void Object::init_()
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	id_ = INVALID_VALUE;
}

void Object::validateCreate_()
{
	if( getName().length() < 1 )
	{
		throw Exception("Object::validateCreate_","No objects can be created without a name");
	}
}

/**
 * First adds an object to the database, then adds "Uncategorized" tag, and finally adds the object to the hub.
 * If the database returns an error (i.e. duplicate error), an exception is caught and
 * this function returns a -1.
 */
Object& Object::create()
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;

	DebugInfo::getDebugInfo()->pushTimer( "Object", "create" );
	
	DebugInfo::getDebugInfo()->pushTimer( "Object", "create", "add to DB" );
	unique_ptr<ObjectDataAccess> dataAccess( 	ObjectConverter::logicToDataAccess( this ) );

	try
	{
		*this = ObjectConverter::dataAccessToLogic( dataAccess->create() );
	}
	catch( exception& e)
	{
		cout << "Object::create: Error while inserting the object in the database:" << endl;
		cout << this->getName() << endl;
		cout << e.what() << endl;
		return *this;
	}
	DebugInfo::getDebugInfo()->popTimer();
	DebugInfo::getDebugInfo()->pushTimer( "Object", "create", "add uncategorized" );
	Hub* hub = Hub::getHub();

	if( !hasUserTags() ) //Uncategorized
	{
		const Tag* tag = hub->getUncategorizedTag();
		ObjectTag uncatTag( tag );
		uncatTag.setObjectId_( getId() );
		tags_.push_back( uncatTag );
	}
	DebugInfo::getDebugInfo()->popTimer();
	cout << "Uncategorized tag added" << endl;
	
	DebugInfo::getDebugInfo()->pushTimer( "Object", "create", "add tags to DB" );
	vector<ObjectTag>::iterator itr;
	for( itr = tags_.begin(); itr != tags_.end(); ++itr )
	{
		unique_ptr<ObjectTagDataAccess> objectTagDA( ObjectTagConverter::logicToDataAccess( *itr ) );
		dataAccess->addTag( objectTagDA.get() );
	}
	DebugInfo::getDebugInfo()->popTimer();
	
	DebugInfo::getDebugInfo()->popTimer();
	return *this;
}

Object& Object::create( const string& path )
{
	name_ = path;

	create();

	Hub* hub = Hub::getHub();
	hub->addObject( *this );

	return *this;
}

Object Object::fetch( int id )
{
	DebugInfo::getDebugInfo()->output("Object", "fetch(id)", "Start" );
	
	unique_ptr<ObjectDataAccess> dataAccess( ObjectDataAccessFactory::create() );
	Object object = ObjectConverter::dataAccessToLogic( dataAccess->fetch( id ) );

	DebugInfo::getDebugInfo()->output("Object", "fetch(id)", "Done" );

	return object;
}

Object Object::fetch( const string& qualifiedName )
{
	DebugInfo::getDebugInfo()->output("Object", "fetch(name)", "Start" );
	
	unique_ptr<ObjectDataAccess> dataAccess( ObjectDataAccessFactory::create() );
	Object object = ObjectConverter::dataAccessToLogic( dataAccess->fetch( qualifiedName ) );

	DebugInfo::getDebugInfo()->output("Object", "fetch(name)", "Done" );

	return object;
}

void Object::erase()
{
	unique_ptr<ObjectDataAccess> dataAccess( 	ObjectConverter::logicToDataAccess( this ) );
	dataAccess->erase();
}

void Object::addTag( ObjectTag& tag )
{
	if( tag.getTag()->getId() == INVALID_VALUE )
	{
		throw Exception( "Object::addTag", "Invalid value in tag id" );
	}
	if( hasTag( tag.getTag()->getId() ) )  //Trying to add the same tag twice is not an error
	{
		return;
	}
	
	tag.setObjectId_( getId() );
	
	unique_ptr<ObjectDataAccess> dataAccess( ObjectDataAccessFactory::create() );
	unique_ptr<ObjectTagDataAccess> objectTagDA( ObjectTagConverter::logicToDataAccess( tag ) );
	dataAccess->addTag( objectTagDA.get() );
	
	Hub* hub = Hub::getHub();
	if( hub->getParentTagSetsAccessType( tag.getTag() ) == TagSet::USER && hasTag( hub->getUncategorizedTag()->getId() ) )
	{
		//ObjectTag oTag = ObjectTag( const_cast<Tag*>( hub->getUncategorizedTag() ) ); 
        ObjectTag oTag = getTag( hub->getUncategorizedTag()->getId() );
		oTag.setObjectId_( getId() );
		removeTag( oTag );
	}
	
	tags_.push_back(  tag );
}

void Object::removeTag( ObjectTag& tag )
{
	unique_ptr<ObjectDataAccess> dataAccess( ObjectDataAccessFactory::create() );
	unique_ptr<ObjectTagDataAccess> objectTagDA( ObjectTagConverter::logicToDataAccess( tag ) );
	objectTagDA->setObjectId(id_);
	dataAccess->removeTag( objectTagDA.get() );

	vector<ObjectTag>::iterator itr = find_if( tags_.begin(), tags_.end(), tag );
	if( itr == tags_.end() )
	{
		throw Exception( "Object::removeTag", "The object was not tagged with the tag in question!" );
	}
	 
	tags_.erase( itr );
}

void Object::removeTags( BoundingBox& bb )
{
	for(auto& tag : tags_)
	{
		BoundingBox tagBB = tag.getBoundingBox();

		if(tagBB == bb)
		{
			const Tag* t = tag.getTag();
			unique_ptr<TagDataAccess> tda = TagConverter::logicToDataAccess(t);

			// Removes from the object_tag table
			removeTag(tag);

			// Removes from the tag and tag type table (like alphanumeric_tag)
			tda->remove(t->getId());
		}
	}
}

void Object::removeTagIfExists( ObjectTag& tag )
{
	vector<ObjectTag>::iterator itr = find_if( tags_.begin(), tags_.end(), tag );
	if( itr == tags_.end() )
	{
		return;
	}

	unique_ptr<ObjectDataAccess> dataAccess( ObjectDataAccessFactory::create() );
	unique_ptr<ObjectTagDataAccess> objectTagDA( ObjectTagConverter::logicToDataAccess( tag ) );
	objectTagDA->setObjectId( id_ );
	dataAccess->removeTag( objectTagDA.get() );

	tags_.erase( itr );
}

void Object::updateTagging( ObjectTag& tagging, const Tag* tag )
{
	removeTagIfExists( tagging );
    ObjectTag newTag( tag, tagging.getBoundingBox() );
    addTag( newTag );
    newTag.confirmTagging();
}


void Object::pluginProcess()
{
    Hub* hub = Hub::getHub();
    hub->addObject( *const_cast<Object*>(this) );
}

bool Object::hasUserTags()
{
	Hub* hub = Hub::getHub();
	
	vector<ObjectTag>::iterator itr;
	for( itr = tags_.begin(); itr != tags_.end(); ++itr )
	{
		if( hub->getParentTagSetsAccessType( (*itr).getTag() ) == TagSet::USER )
		{
			return true;
		}
	}
	return false;
}

const ObjectTag Object::getTag( int id ) const
{
	vector<ObjectTag>::const_iterator itr;
	for( itr = tags_.begin(); itr != tags_.end(); ++itr )
	{
		if( (*itr).getTag()->getId() == id )
		{
			return *itr;
		}
	}
	throw Exception("Object::getTag(id)", "Tag requested is not attached to this object.", id);
}

bool Object::hasTag( int id )
{
	try 
	{
		getTag( id );  //If it does not throw an error the tag was found
		return true;
	}
	catch (...) {}
	return false;
}
