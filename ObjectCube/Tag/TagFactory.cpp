/*
 *  TagFactory.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 20.3.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "TagFactory.h"

#include <sstream>

#include "Tag.h"
#include "AlphanumericalTag.h"
#include "NumericalTag.h"
#include "TimeTag.h"
#include "DateTag.h"

#include "../LayerShared/Exception.h"

using namespace ObjectCube;

std::unique_ptr<Tag> TagFactory::create( int tagType)
{
	switch ( tagType ) 
	{
		case Tag::ALPHANUMERICAL: return std::unique_ptr<Tag>{new AlphanumericalTag()};
		case Tag::NUMERICAL:      return std::unique_ptr<Tag>{new NumericalTag()};
		case Tag::TIME:           return std::unique_ptr<Tag>{new TimeTag()};
		case Tag::DATE:           return std::unique_ptr<Tag>{new DateTag()};
	}
	ostringstream stringstream;
	stringstream << "Unknown tag type: " << tagType;
	throw Exception("TagFactory::create", stringstream.str() );
}
