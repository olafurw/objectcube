#ifndef PATHSEARCH_H
#define PATHSEARCH_H

#include <string>
#include <vector>

// stopgap solution to stop relying on hardcoded install paths

namespace Utils
{

void addSearchPath(const std::string &prefixPath);
void removeSearchPath(const std::string &prefixPath);
std::vector<std::string> getSearchPaths();

// returns empty string if path does not point to readable file in any of the search pathes
std::string findFullPath(const std::string &filePath);

}

#endif // PATHSEARCH_H
