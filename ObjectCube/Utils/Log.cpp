#include "Log.h"

std::unique_ptr<std::string> current_time(const char* format)
{
	time_t rawtime;
	tm* timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, 80, format, timeinfo);
	std::unique_ptr<std::string> result{ new std::string(buffer) };

	return result;
}

namespace Utils
{

Log::Log(const std::string& filename)
{
	m_out = std::unique_ptr<std::ofstream>(new std::ofstream());
	m_out->open(filename, std::fstream::app);
}

std::ostream& Log::writeInfo()
{
	return write(Utils::Log::Type::Info);
}

std::ostream& Log::writeWarning()
{
	return write(Utils::Log::Type::Warning);
}

std::ostream& Log::writeError()
{
	return write(Utils::Log::Type::Error);
}

std::ostream& Log::writeDebug()
{
	return write(Utils::Log::Type::Debug);
}

std::ostream& Log::writeEnter()
{
	static std::mutex mtx;
	std::unique_lock<std::mutex> lock(mtx);

	std::unique_ptr<std::string> ctime = current_time("%Y-%m-%d %H:%M:%S");

	*m_out << "[F] " << *ctime << " Entered " << std::flush;

	return *m_out;
}

std::ostream& Log::writeExit()
{
	static std::mutex mtx;
	std::unique_lock<std::mutex> lock(mtx);

	std::unique_ptr<std::string> ctime = current_time("%Y-%m-%d %H:%M:%S");

	*m_out << "[F] " << *ctime << " Exited " << std::flush;

	return *m_out;
}

std::ostream& Log::write(Utils::Log::Type log_type)
{
	static std::mutex mtx;
	std::unique_lock<std::mutex> lock(mtx);

    std::string type_string = "";

    switch(log_type)
    {
        case Type::Info:
            type_string = "I";
        break;
        case Type::Warning:
            type_string = "W";
        break;
        case Type::Error:
            type_string = "E";
        break;
        case Type::Debug:
            type_string = "D";
        break;
        default:
            type_string = "?";
        break;
    }

    std::unique_ptr<std::string> ctime = current_time("%Y-%m-%d %H:%M:%S");
    *m_out << "[" << type_string << "] " << *ctime << " " << std::flush;

    return *m_out;
}

Log::~Log()
{
}

}
