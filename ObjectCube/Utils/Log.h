#ifndef OC_LOG_H_
#define OC_LOG_H_

#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <mutex>
#include <thread>

namespace Utils
{
class Log
{
public:
	enum class Type : unsigned int
	{
		Info = 1,
		Warning = 2,
		Error = 3,
		Debug = 4
	};

	Log(const std::string& filename);
	virtual ~Log();

	std::ostream& writeInfo();
	std::ostream& writeWarning();
	std::ostream& writeError();
	std::ostream& writeDebug();
	std::ostream& writeEnter();
	std::ostream& writeExit();
    std::ostream& write(Utils::Log::Type log_type);

private:
	std::unique_ptr<std::ofstream> m_out;
};
}
#endif /* LOG_H_ */
