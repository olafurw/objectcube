#include "PathSearch.h"

#include <vector>
#include <mutex>
#include <iostream> // cerr
#include <algorithm>

#include <unistd.h>

namespace
{

std::vector<std::string> g_searchPaths;
std::mutex g_searchPathsLock;

struct _InitSearchPaths
{
	_InitSearchPaths() {
		// FIXME: this is bit quirky due to how the plugins are built causing this to trigger multiple times
		// checking if Utils::getSearchPaths() is empty is a workaround, but not a proper solution
		if (Utils::getSearchPaths().empty()) {
			Utils::addSearchPath("./");
#ifdef BUILD_PATH
			Utils::addSearchPath(BUILD_PATH);
#endif
#ifdef INSTALL_PATH
			Utils::addSearchPath(INSTALL_PATH);
#endif
		}
	}
} _init;

bool fileExists(const std::string &filePath)
{
	return access(filePath.c_str(), R_OK) == 0;
}

} // anonymous namespace

namespace Utils
{

void addSearchPath(const std::string &prefixPath)
{
	//std::cerr << "ObjectCube::addSearchPath: " << prefixPath << std::endl;
	std::lock_guard<std::mutex> lock(g_searchPathsLock);
	g_searchPaths.push_back(prefixPath);
}

void removeSearchPath(const std::string &prefixPath)
{
	size_t sizeDelta = 0;
	{
		std::lock_guard<std::mutex> lock(g_searchPathsLock);
		sizeDelta = g_searchPaths.size();
		g_searchPaths.erase( std::remove( std::begin(g_searchPaths), std::end(g_searchPaths), prefixPath ), std::end(g_searchPaths) );
		sizeDelta -= g_searchPaths.size();
	}
	if (!sizeDelta)
	{
		std::cerr << "ObjectCube::removeSearchPath: Failed: Path not found: " << prefixPath << std::endl;
	}
}

std::vector<std::string> getSearchPaths()
{
	std::lock_guard<std::mutex> lock(g_searchPathsLock);
	return g_searchPaths;
}

std::string findFullPath(const std::string &filePath)
{
	const bool relativePath = filePath.length() && filePath[0] != '/';
	if (relativePath)
	{
		// if relative path try all the known search paths
		std::lock_guard<std::mutex> lock(g_searchPathsLock);
		for (const auto &path : g_searchPaths)
		{
			auto fullFilePath = path + "/" + filePath;
			if (fileExists(fullFilePath)) return fullFilePath;
		}
	}
	else if (fileExists(filePath)) // absolute
	{
		return filePath; // use absolute path as is
	}

	std::cerr << "ObjectCube::findFullPath: Could not find: " << filePath << std::endl;
	if (relativePath) {
		std::lock_guard<std::mutex> lock(g_searchPathsLock);
		std::cerr << "Searched:" << std::endl;
		for (const auto &path : g_searchPaths)
		{
			std::cerr << "   " << path << std::endl;
		}
		std::cerr << std::endl;
	}
	return {}; // not found
}

} // namespace Utils
