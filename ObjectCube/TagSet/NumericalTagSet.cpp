/*
 *  NumericalTagSet.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 27.3.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "NumericalTagSet.h"

#include "TagSet.h"
#include "../Tag/Tag.h"
#include "../Tag/NumericalTag.h"
#include "../Filters/TagFilter.h"
#include "../LayerShared/Exception.h"

#include <cstdlib>
#include <algorithm>

using namespace ObjectCube;



NumericalTagSet::NumericalTagSet()
: TagSet() 
{
	init_();
}


NumericalTagSet::NumericalTagSet( const string& name ) 
: TagSet( name ) 
{ 
	init_();
}


NumericalTagSet::NumericalTagSet( const string& name, const string& description ) 
: TagSet( name, description ) 
{ 
	init_();
}


void NumericalTagSet::init_()
{
	setTypeId_( TagSet::NUMERICAL );
	
	supportedTagTypes_.push_back( Tag::NUMERICAL );
	
	supportedFilterTypes_.push_back( Filter::NUMERICAL_RANGE_FILTER );
}


TagSet* NumericalTagSet::createImp_()
{
	return TagSet::createImp_();
}


NumericalTagSet* NumericalTagSet::create()
{
	return dynamic_cast<NumericalTagSet*>( createImp_() );
}



const NumericalTag* /*const*/ NumericalTagSet::getNumericalTag( long number ) const
{
	lazyLoadTags_(); // must always call before using tags_
	auto itr = find_if (tags_.begin(), tags_.end(), [number](const unique_ptr<Tag> &i) {
		auto tag = dynamic_cast<const NumericalTag *>(i.get());
		return tag && tag->getNumber() == number;
	});
	if( itr != tags_.end() ) //Not found
	{
		auto tag = dynamic_cast<const NumericalTag *>(itr->get());
		if (tag) return tag;
	}
	
	throw Exception( "NumericalTagSet::getTag", "TagSet did not contain requested tag (number).", number );	
}


void NumericalTagSet::fetch_( int id )
{
	TagSet::fetch_( id );
	
	if( getTypeId() != NUMERICAL )
	{
		throw Exception( "NumericalTagSet::fetch_", "Invalid tag set type in fetch.", getTypeId() );
	}
}


const Tag* /*const*/ NumericalTagSet::fetchOrAddTag( const string& value )
{
	long lValue = 0;
	try 
	{
		//Only integer support for now, consider making the number of a NumericalTag a real number.
		auto slashLocation = value.find( "/" );
		if( slashLocation != string::npos )
		{
			string numerator = value.substr( 0, slashLocation );
			string denominator = value.substr( slashLocation + 1, value.length() - (slashLocation + 1) );
			long lNumerator = atoi( numerator.data() );
			long lDenominator = atoi( denominator.data() );
			if( lNumerator == 0 || lDenominator == 0 )
			{
				throw Exception( "NumericalTagSet::fetchOrAddTag", "Illegal number, either denominator or numerator = 0" );
			}
			lValue = lNumerator / lDenominator;
		}
		else 
		{
			lValue = atoi( value.data() );
		}
	}
	catch ( ... ) 
	{
		throw Exception( "NumericalTagSet::fetchOrAddTag", "Cannot convert value to an integer.", value );
	}
	
	try
	{
		return getNumericalTag( lValue ); //NumericalTag::fetch( getId(), lValue );
	}
	catch( ... ) {}
	NumericalTag tag( lValue );
	return addTag( &tag );
}


