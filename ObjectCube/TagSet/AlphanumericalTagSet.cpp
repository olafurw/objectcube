/*
 *  AlphanumericalTagSet.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 25.10.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */

#include "AlphanumericalTagSet.h"

#include "../Hub.h"
#include "../Tag/Tag.h"
#include "../Tag/AlphanumericalTag.h"
#include "../Filters/TagFilter.h"
#include "../LayerShared/Exception.h"
#include "../LayerShared/DebugInfo.h"

#include <algorithm>

using namespace ObjectCube;



AlphanumericalTagSet::AlphanumericalTagSet()
: TagSet() 
{
	init_();
}


AlphanumericalTagSet::AlphanumericalTagSet( const string& name ) 
: TagSet( name ) 
{ 
	init_();
}


AlphanumericalTagSet::AlphanumericalTagSet( const string& name, const string& description ) 
: TagSet( name, description ) 
{ 
	init_();
}


void AlphanumericalTagSet::init_()
{
	setTypeId_( TagSet::ALPHANUMERICAL );
	
	supportedTagTypes_.push_back( Tag::ALPHANUMERICAL );
}


TagSet* AlphanumericalTagSet::createImp_()
{
	TagSet::createImp_();
	
	//Specific things here
	return Hub::getHub()->getTagSet( getId() );
}


AlphanumericalTagSet* AlphanumericalTagSet::create()
{
	createImp_();
	return dynamic_cast<AlphanumericalTagSet*>( Hub::getHub()->getTagSet( getId() ) );
}



const AlphanumericalTag* /*const*/ AlphanumericalTagSet::getAlphanumericalTag(const string &name ) const
{
	lazyLoadTags_(); // must always call before using tags_
	auto itr = find_if (tags_.begin(), tags_.end(), [&name](const unique_ptr<Tag> &i) {
		auto tag = dynamic_cast<const AlphanumericalTag *>(i.get());
		return tag && tag->getName() == name;
	});
	if( itr != tags_.end() ) //Not found
	{
		auto tag = dynamic_cast<const AlphanumericalTag *>(itr->get());
		if (tag) return tag;
	}
	
	throw Exception( "AlphanumericalTagSet::getTag", "TagSet did not contain requested tag (name).", name );	
}


void AlphanumericalTagSet::fetch_( int id )
{
	TagSet::fetch_( id );
	
	if( getTypeId() != ALPHANUMERICAL )
	{
		throw Exception( "AlphanumericalTagSet::fetch_", "Invalid tag set type in fetch.", getTypeId() );
	}
}


const Tag* /*const*/ AlphanumericalTagSet::fetchOrAddTag( const string& value )
{
	//DebugInfo::getDebugInfo()->output("AlphanumericalTagSet", "fetchOrAddTag", "Trying to fetch!");
	try 
	{
		return getAlphanumericalTag( value ); //AlphanumericalTag::fetch( getId(), value );
	}
	catch ( ... ) {}
	//DebugInfo::getDebugInfo()->output("AlphanumericalTagSet", "fetchOrAddTag", "Could not fetch, trying to create!");
	AlphanumericalTag tag( value );
	return addTag( &tag );
}













