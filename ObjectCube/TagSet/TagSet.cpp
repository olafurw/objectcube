/*
 *  dimension.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 25.10.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */

#include "TagSet.h"

#include <iostream>
#include <memory>
#include <sstream>

#include "../DataAccess/Factories/TagSetDataAccessFactory.h"
#include "../DataAccess/Root/TagSetDataAccess.h"
#include "../Converters/TagSetConverter.h"

#include "../Object.h"
#include "../Tag/Tag.h" 
#include "../Tag/AlphanumericalTag.h"
#include "../Hub.h"
#include "../Filters/Filter.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../LayerShared/Exception.h"
#include "../Tag/TagFactory.h"
#include "../Filters/FilterFactory.h"
#include "../Hierarchy/DefaultDimension.h"
#include "../Hierarchy/DimensionFactory.h"
#include "../LayerShared/MemoryManagement.h"
#include "../Language.h"
#include "../LayerShared/DebugInfo.h"


using namespace ObjectCube;
using namespace std;

TagSet::TagSet()
{
	init_();
}


TagSet::TagSet( const string& name ) 
{ 
	init_();
	setName( name ); 
}


TagSet::TagSet( const string& name, const string& description ) 
{ 
	init_();
	setName( name );
	setDescription( description );
}

//TagSet &TagSet::operator=(TagSet &&tagSet)
//{
//	id_          = std::move(tagSet.id_);
//	name_        = std::move(tagSet.name_);
//	description_ = std::move(tagSet.description_);
//	typeId_      = std::move(tagSet.typeId_);
//	accessId_    = std::move(tagSet.accessId_);

//	tagsLoaded_  = std::move(tagSet.tagsLoaded_);
//	tags_        = std::move(tagSet.tags_);
//	dimensions_  = std::move(tagSet.dimensions_);

//	supportedFilterTypes_  = std::move(tagSet.supportedFilterTypes_);
//	supportedTagTypes_     = std::move(tagSet.supportedTagTypes_);
//	return *this;
//}


//TagSet::TagSet( const TagSet& tagSet )
//{
//	copyValues_( tagSet );
//}


//TagSet& TagSet::operator=( const TagSet& tagSet )
//{
//	if( this == &tagSet )  //No overwriting of self
//	{
//		return *this;
//	}

//	copyValues_( tagSet );
	
//	return *this;
//}


//void TagSet::copyValues_( const TagSet& tagSet )  //A helper function for = operator and copy constructor
//{
//	cleanup_();
	
//	setId_( tagSet.getId() );
//	setTypeId_( tagSet.getTypeId() );
//	setAccessId_( tagSet.getAccessId() );
//	setName( tagSet.getName() );
//	setDescription( tagSet.getDescription() );
	
//	supportedFilterTypes_ = tagSet.supportedFilterTypes_;
//	supportedTagTypes_ = tagSet.supportedTagTypes_;
	
//	//Tags
//	/*
//	const vector<Tag*> tags = tagSet.getTags();
//	for( vector<Tag*>::const_iterator tagItr = tags.begin(); tagItr != tags.end(); ++tagItr )
//	{
//		shared_ptr<Tag> tag( TagFactory::create( (*tagItr)->getTypeId() ) );
//		*tag.get() = *(*tagItr);
//		tags_.push_back( tag );
//	}

//	//Filters
//	const vector<Filter*> filters = tagSet.getFilters();
//	for( vector<Filter*>::const_iterator filterItr = filters.begin(); filterItr != filters.end(); ++filterItr )
//	{
//		shared_ptr<Filter> filter( FilterFactory::create( (*filterItr)->getTypeId() ) );
//		*filter.get() = *(*filterItr);
//		filters_.push_back( filter );
//	}
	
//	//Dimensions
//	const vector<Dimension*> dimensions = tagSet.getDimensions();
//	for( vector<Dimension*>::const_iterator dimensionItr = dimensions.begin(); dimensionItr != dimensions.end(); ++dimensionItr )
//	{
//		shared_ptr<Dimension> dimension( DimensionFactory::create( (*dimensionItr)->getTypeId() ) );
//		*dimension.get() = *(*dimensionItr);
//		dimensions_.push_back( dimension );
//	}
//	 */
	
//	tags_ = tagSet.tags_;
//	dimensions_ = tagSet.dimensions_;
//}


void TagSet::cleanup_()
{
	tags_.clear();
	dimensions_.clear();
}


void TagSet::init_()
{
	id_ = INVALID_VALUE;
	typeId_ = INVALID_VALUE;
	accessId_ = INVALID_VALUE;
	
	setAccessId_( TagSet::USER );  //All tag-sets are by default user access level
	
	//Supported by all TagSet types
	supportedFilterTypes_.push_back( Filter::TAG_FILTER ); 
	supportedFilterTypes_.push_back( Filter::DIMENSION_FILTER );
}


TagSet::~TagSet()
{
	cleanup_();	
}


TagSet* TagSet::create()
{
	return createImp_();
}


PersistentDimension* TagSet::createPersistentDimension( const Tag* tag )
{
	testIfCreated_();
	
	//Validate that the tag has been created and belongs to the dimension
	getTag( tag->getId() );  //Throws exception if it does not belong to this dimension
	
	//Create the Dimension
	shared_ptr<PersistentDimension> persistentDimension( new PersistentDimension( getId(), tag ) );
	persistentDimension->create_();
	int dimensionId = persistentDimension->getId();
	dimensions_.push_back( persistentDimension );
	return getPersistentDimension( dimensionId );
}


void TagSet::deleteDimension( int id )
{
	for( vector<shared_ptr<Dimension> >::iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getId() == id )
		{
			(*itr)->delete_();
			//This delete causes invalid read errors in Valgrind, ToDo:  Find out why
			dimensions_.erase( itr );	
			return;
		}
	}
	
	throw Exception( "TagSet::deleteDimension", "Dimension does not belong to tag-set.", id );
}

Dimension* TagSet::getDimension( int dimensionId )
{
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getId() == dimensionId )
		{
			return (*itr).get();
		}
	}
	throw Exception( "TagSet::getDimension", "TagSet did not contain the requested dimension.", dimensionId );
}


Dimension* TagSet::getDimension( const string& name )
{
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getRoot()->getName() == name )
		{
			return (*itr).get();
		}
	}
	throw Exception( "TagSet::getDimension", "TagSet did not contain the requested dimension.", name );
}


const Dimension* TagSet::getDimension( int dimensionId ) const
{
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getId() == dimensionId )
		{
			return (*itr).get();
		}
	}
	throw Exception( "TagSet::getDimension", "TagSet did not contain the requested dimension.", dimensionId );
}


const Dimension* TagSet::getDimension( const string& name ) const 
{
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getRoot()->getName() == name )
		{
			return (*itr).get();
		}
	}
	throw Exception( "TagSet::getDimension", "TagSet did not contain the requested dimension.", name );
}



PersistentDimension* TagSet::getPersistentDimension( int dimensionId )
{
	return dynamic_cast<PersistentDimension*>( getDimension( dimensionId ) );
}


PersistentDimension* TagSet::getPersistentDimension( const string& name )  
{
	return dynamic_cast<PersistentDimension*>( getDimension( name ) );
}


const VirtualDimension* TagSet::getVirtualDimension( int dimensionId ) const
{
	return dynamic_cast<const VirtualDimension*>( getDimension( dimensionId ) );
}


const VirtualDimension* TagSet::getVirtualDimension( const string& name ) const
{
	return dynamic_cast<const VirtualDimension*>( getDimension( name ) );
}


const vector<Dimension*> TagSet::getDimensions() const
{
	vector<Dimension*> dimensions;
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		dimensions.push_back( (*itr).get() );
	}
	return dimensions;
}

const DefaultDimension* TagSet::getDefaultDimension() const
{
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getTypeId() == Dimension::DEFAULT )
		{
			return dynamic_cast<DefaultDimension*>( (*itr).get() );
		}
	}
	throw Exception( "TagSet::getDefaultDimension", "TagSet did not contain a default dimesion." );	
}


const vector<PersistentDimension*> TagSet::getPersistentDimensions() const
{
	vector<PersistentDimension*> dimensions;
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getTypeId() == Dimension::PERSISTENT )
		{
			dimensions.push_back( dynamic_cast<PersistentDimension*>( (*itr).get()  ) );
		}
	}
	return dimensions;
}


const vector<VirtualDimension*> TagSet::getVirtualDimensions() const
{
	vector<VirtualDimension*> dimensions;
	for( vector<shared_ptr<Dimension> >::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr)->getTypeId() == Dimension::VIRTUAL )
		{
			dimensions.push_back( dynamic_cast<VirtualDimension*>( (*itr).get()  ) );
		}
	}
	return dimensions;
}


const Tag* /*const*/ TagSet::getTag( int id ) const
{
	lazyLoadTags_(); // must always call before using tags_
	auto itr = find_if (tags_.begin(), tags_.end(), [id](const unique_ptr<Tag> &i) {
		return i->getId() == id;
	});
	if( itr != tags_.end() ) //Not found
	{
		return itr->get();
	}
	throw Exception( "TagSet::getTag", "TagSet did not contain requested tag (id).", id );
}


const vector<Tag*> TagSet::getTags( const vector<int>& tagIds ) const
{
	lazyLoadTags_(); // must always call before using tags_
	vector<Tag*> tags;
	for( const auto &i : tagIds )
	{
		tags.push_back( const_cast<Tag*>( getTag( i ) ) );
	}
	return tags;	
}


const vector<Tag*> TagSet::getTags() const
{
	lazyLoadTags_(); // must always call before using tags_
	vector<Tag*> tags;
	for( const auto &i : tags_ )
	{
		tags.push_back( i.get() );
	}
	return tags;
}


const Tag* /*const*/ TagSet::addTag( const Tag* /*const*/ tag)
{
	testIfCreated_();
	
	if( !supportedTagType_( tag ) )
	{
		stringstream stringStream;
		stringStream << "TagSet of type " << getTypeId() << " does not support tagType " << tag->getTypeId();
		throw Exception( "TagSet::addTag",  stringStream.str() );
	}
	
	//We copy the tag to limit the affects of user actions on the framework.  It also simplifies user memory management.
	auto tagCopy = TagFactory::create( tag->getTypeId() );
	//Tag* tagCopy = TagFactory::create( tag->getTypeId() );
	//*tagCopy = *tag;
	*tagCopy.get() = *tag;
	
	tagCopy->setTagSetId_( getId() );
	tagCopy->create_();
	tags_.push_back( std::move(tagCopy) );

	loadDefaultDimension_();  //Add the tag, brute force
	
	return tagCopy.get();
}


void TagSet::erase()
{
	//erase all tags of the tag-set from objects
	//erase all persistent dimensions of the tag-set
	//erase all tags of the tag-set
	//erase the tag-set itself & remove it from the hub
	
	//Do all the DB deletions in one transaction, this is all or nothing operation
	
	TagSetConverter::logicToDataAccess( this )->erase();

	Hub* hub = Hub::getHub();
	hub->removeTagSet( this );
}


void TagSet::deleteTag( const Tag* /*const*/ tag ) 
{
	getTag( tag->getId() ); //Throws an error if it is not found
	
	/*
	vector<Tag*>::iterator itr = find_if( tags_.begin(), tags_.end(), dereference( tag ) );
	if( itr == tags_.end() )  //Was not found
	{
		throw Exception( "TagSet::deleteTag", "Tag was not found!", tag->getId() );
	}
	 */
	if( tag->inUse() )
	{
		throw Exception( "TagSet::deleteTag", "The tag is in use, remove it from all objects before deleting it!", tag->getId() );
	}
	
	for( auto itr = tags_.begin(); itr != tags_.end(); ++itr )
	{
		if( (*itr)->getId() == tag->getId() )
		{
			(*itr)->delete_();
			
			tags_.erase( itr );	
			
			loadDefaultDimension_(); //Remove the tag, brute force
			
			return;
		}
	}

	throw Exception( "TagSet::deleteTag", "Tag was not found!", tag->getId() );
}


TagSet* TagSet::createImp_()
{
	auto tagSet = TagSetConverter::dataAccessToLogic( TagSetConverter::logicToDataAccess( this )->create() );
	*this = std::move(*tagSet);

	// hs: Added for reloading dimensions, when new tagset is added.
	loadDefaultDimension_();
	return this;
}


void TagSet::testIfCreated_()
{
	if( getId() == INVALID_VALUE ) //Has not been created
	{
		throw Exception( "TagSet::creationTest_", "Requested operation could not be completed since the tag-set has not been created!" );
	}
}


void TagSet::fetch_( int id )
{ 	
	DebugInfo::getDebugInfo()->output( "TagSet", "fetch_", "id: ", id );
	DebugInfo::getDebugInfo()->pushTimer( "TagSet", "fetch_" );
	
	DebugInfo::getDebugInfo()->pushTimer( "TagSet", "fetch_", "fetch tag-set" );
	setId_( id );
	unique_ptr<TagSetDataAccess> dataAccess( TagSetDataAccessFactory::create() );
	shared_ptr<TagSet> tagSet( TagSetConverter::dataAccessToLogic( dataAccess->fetch( id ) ) );
	*this = std::move(*tagSet.get());

	DebugInfo::getDebugInfo()->popTimer();
	
	loadDefaultDimension_();
		
	DebugInfo::getDebugInfo()->pushTimer( "TagSet", "fetch_", "persistent dimensions" );
	vector<int> hierarchyIds = dataAccess->fetchDimensionIds();
	for( vector<int>::iterator itr = hierarchyIds.begin(); itr != hierarchyIds.end(); ++itr )
	{
		dimensions_.push_back( shared_ptr<Dimension>( PersistentDimension::fetch_( *itr ) ) );
	}
	DebugInfo::getDebugInfo()->popTimer();
	
	DebugInfo::getDebugInfo()->popTimer();
}


void TagSet::loadDefaultDimension_()
{
	try 
	{
		const DefaultDimension* defaultDim = getDefaultDimension();
		deleteDimension( defaultDim->getId() );
	}
	catch ( ... ) {}
	
	DebugInfo::getDebugInfo()->pushTimer( "TagSet", "fetch_", "default dimension" );
	
	shared_ptr<DefaultDimension> defaultDimension( new DefaultDimension( getId(), getTags() ) );
	dimensions_.push_back( defaultDimension );
	
	DebugInfo::getDebugInfo()->popTimer();	
}

bool TagSet::supportedFilter_( const Filter* /*const*/ filter )
{
	return count( supportedFilterTypes_.begin(), supportedFilterTypes_.end(), filter->getTypeId() ) > 0;
}


bool TagSet::supportedTagType_( const Tag* /*const*/ tag )
{
	return count( supportedTagTypes_.begin(), supportedTagTypes_.end(), tag->getTypeId() ) > 0;
}


void TagSet::loadVirtualDimensions_()
{
	throw Exception( "TagSet::loadVirtualDimensions_", "Not implemented for base class!" );
}

void TagSet::lazyLoadTags_() const
{
	// TODO..
}

void TagSet::setTags_(vector<unique_ptr<Tag> > tags)
{
	tags_ = std::move(tags);
}

const string TagSet::typeAsString() const
{
	return Language::asString( "TagSetType", getTypeId() );
}


const string TagSet::typeAsString( int tagSetTypeId )
{
	return Language::asString( "TagSetType", tagSetTypeId );
}


const string TagSet::accessTypeAsString() const
{
	return Language::asString( "TagSetAccessType", getAccessId() );
}


const string TagSet::accessTypeAsString( int tagSetAccessTypeId )
{
	return Language::asString( "TagSetAccessType", tagSetAccessTypeId );
}



























