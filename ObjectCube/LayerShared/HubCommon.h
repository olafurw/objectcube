/*
 *  HubCommon.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 17.2.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#ifndef ObjectCube_HUB_COMMON_
#define ObjectCube_HUB_COMMON_

#include <string>
#include <iostream>

using namespace std;

namespace ObjectCube
{
	class HubCommon 
	{
	public:
		HubCommon() {;}
		virtual ~HubCommon() {;}
		
		static int getDataAccessType() { return dataAccessType_(); }
		static void setDataAccessType( int type ) { dataAccessType_() = type; }

		static string getUncategorizedDimensionName() { return uncategorizedDimName_(); }
		static string getUncategorizedTagName() { return uncategorizedTagName_(); }

		static void setDataAccessTypeParameterName( string value )
		{
		    dataAccessTypeParameterName_() = value;
		}

		static string getDataAccessTypeParameterName()
		{
		    return dataAccessTypeParameterName_();
		}
		
	private:

		static string& dataAccessTypeParameterName_()
		{
		    static string name = "dataAccessType";
		    return name;
		}
		
		static int& dataAccessType_()
        {
            static int name = HubCommon::MONETDB;
            return name;
        }

		static string& uncategorizedDimName_()
        {
            static string name = "UNCATEGORIZED";
            return name;
        }

		static string& uncategorizedTagName_()
        {
            static string name = "Uncategorized";
            return name;
        }

	public:
		enum DATA_ACCESS_TYPE 
		{
			CANNED = 1,
			SQLITE = 2,
			MONETDB = 3,
			ORACLE = 4
		};
		
	};	
	
}

#endif
