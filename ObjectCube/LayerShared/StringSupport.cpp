/*
 *  StringSupport.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 23.8.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "StringSupport.h"

using namespace ObjectCube;
using namespace std;



vector<string> StringSupport::controlChars_;
string StringSupport::escapeCharString_ = "\'";



StringSupport::StringSupport()
{
}


void StringSupport::init_()
{
	controlChars_.clear();
	//controlChars_.push_back( "\\" );	
	controlChars_.push_back( "\'" );
	//controlChars_.push_back( "\"" );
}


string StringSupport::neutralizeControlCharacters( const string& input )
{
	init_();
	
	string output = input;
	for( vector<string>::iterator itr = controlChars_.begin(); itr != controlChars_.end(); ++itr )
	{
		string controlSymbol = *itr;
		size_t startingLocation = 0;
		while( ( startingLocation = output.find( controlSymbol, startingLocation ) ) != string::npos )
		{
			output.insert( startingLocation, escapeCharString_ );
			startingLocation += 2;
		}
	}
	
	return output;	
}


string StringSupport::removeControlCharacterNeutralization( const string& input )
{
	init_();
	
	string output = input;
	for( vector<string>::iterator itr = controlChars_.begin(); itr != controlChars_.end(); ++itr )
	{
		string controlSymbol = *itr;
		string toFind = escapeCharString_ + controlSymbol;
		size_t startingLocation = 0;
		while( ( startingLocation = output.find( toFind, startingLocation ) ) != string::npos )
		{
			output.erase( startingLocation, escapeCharString_.length() );
			startingLocation += 2; //change
		}
	}
	
	return output;	
}


string StringSupport::trim( string& input, const string& remove )
{
	string trimmed = input.erase( input.find_last_not_of( remove ) + 1) ;
	trimmed = trimmed.erase( 0, trimmed.find_first_not_of( remove ) );
	input = trimmed;
	return trimmed;
}


string StringSupport::toSimpleAscii( const string& input )
{
	string simple;
	int length = input.length();
	for( int i = 0; i < length; ++i )
	{
		char c = input[i];
		if( c >= 32 && c <= 126 )
		{
			simple.append( 1, c );
		}
		else 
		{
			simple += "_"; //Character not in the simple ascii set
		}
	}
	return simple;
}



