/*
 *  PluginServer.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 15.3.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include <memory>
#include <iostream>

#include "PluginServer.h"

#include "../DataAccess/Root/PluginDataAccess.h"
#include "../DataAccess/Factories/PluginDataAccessFactory.h"
#include "../LayerShared/MemoryManagement.h"
#include "PluginCommon.h"
#include "../LayerShared/DebugInfo.h"

#include "../Utils/PathSearch.h"

using namespace ObjectCube;



PluginServer::PluginServer()
{
	init_();
}

void PluginServer::init_()
{
	DebugInfo::getDebugInfo()->output("PluginServer", "init_", "1" );
	
	unique_ptr<PluginDataAccess> dataAccess( PluginDataAccessFactory::create() );
	vector<PluginDataAccess*> plugins = dataAccess->fetchAll();  //Using the data access class here is dirty, but we have no implementation classes for abstract class ProcessObjectPlugin
	
	DebugInfo::getDebugInfo()->output("PluginServer", "init_", "2" );

	vector<PluginDataAccess*>::iterator itr;
	for( itr = plugins.begin(); itr != plugins.end(); itr++ )
	{
		DebugInfo::getDebugInfo()->output("PluginServer", "init_", "3" );
		
		auto pluginPath = Utils::findFullPath((*itr)->getName());
		if (!pluginPath.empty()) {
			loadPlugin_( (*itr)->getId(), pluginPath );
		}
	}
	clearAndDestroy( plugins );
	
	DebugInfo::getDebugInfo()->output("PluginServer", "init_", "4" );
	processObjectServer_.loadPluginInfo();
	DebugInfo::getDebugInfo()->output("PluginServer", "init_", "Done" );
}



void PluginServer::loadPlugin_( const int id, const string& libraryName ) 
{
	if( plugins_.find( libraryName ) == plugins_.end() )
	{
		PluginLibrary library( libraryName );
		library.registerPlugin( id, *this );
		plugins_.insert( map<string, PluginLibrary>::value_type( libraryName, library ) );
		//PluginCommon* processor = processObjectServer_.getProcessors()[0];
	}
}


