#include "PluginHub.h"

#include "../Hub.h"

namespace ObjectCube
{
PluginHub::PluginHub()
{
	std::cout << "PluginHub create" << std::endl;

	log_.reset( new Utils::Log("oc_pluginhub.log") );
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;

	pluginServer_.reset(new PluginServer());

	auto& processObjectPlugins = pluginServer_->getProcessObjectServer().getProcessors();
	for( auto& poPlugin : processObjectPlugins )
	{
		vector<PluginTagSet> pluginTagSets = poPlugin->getPluginTagSets();
		for( auto& pluginTagSet : pluginTagSets )
		{
			if( pluginTagSet.getTagSetId() == INVALID_VALUE )
			{
				pluginTagSet.updateTagSetId( createPluginTagSet_( pluginTagSet.getTagSetTypeId(), pluginTagSet.getTagSetName() ) );
			}
		}
		poPlugin->setPluginTagSets_( pluginTagSets );
	}

	processing_ = false;
	processExit_ = false;
	itemsTotal_ = 0;
	itemsRemaining_ = 0;
	processThread_.reset( new thread(&PluginHub::processLoop_, this) );
}

PluginHub::~PluginHub()
{
	processExit_ = true;
	processCond_.notify_all();
	processThread_->join();
}

/**
 * This is run in another thread and is a continuous loop
 * checking if there is something to be processed.
 */
void PluginHub::processLoop_()
{
	const std::chrono::milliseconds sleep_duration( 100 );

	while(!processExit_ || processQueue_.size())
	{
		unique_lock<mutex> clock(processCondMutex_);
		processCond_.wait_for( clock, sleep_duration );

		if (processExit_) {
			std::cout << "draining queue: " << processQueue_.size() << std::endl;
		}

		queue<Object> workQueue;

		// Lock scope
		// Move the elements of the process queue to a local queue to process
		// This prevents a lock while processing and allows new things to be added
		// if a process takes a long while.
		{
			lock_guard<mutex> slock(processQueueMutex_);
			workQueue = std::move(processQueue_);
		}

		itemsTotal_.store( workQueue.size() );
		itemsRemaining_.store( itemsTotal_ );

		while(workQueue.size() > 0)
		{
			Object obj = workQueue.front();
			workQueue.pop();

			Hub::getHub()->tagObject_(pluginServer_->getProcessObjectServer().process( getPluginObject_(obj) ), obj);

			itemsRemaining_.store( workQueue.size() );
		}
	}
}

int PluginHub::createPluginTagSet_( int tagSetType, const string& name )
{
	unique_ptr<TagSet> tagSet( TagSetFactory::create( tagSetType ) );
	tagSet->setName( name );
	tagSet->setDescription( "Automatically created tag-set, for a plugin." );
	tagSet->setAccessId_( TagSetCommon::SYSTEM );

	Hub::getHub()->addTagSet( tagSet.get() );

	return tagSet->getId();
}

PluginObject PluginHub::getPluginObject_( const Object& object ) const
{
    ifstream file( object.getName().data(), ios::in | ios::binary | ios::ate);
	if(!file.is_open() )
	{
		throw Exception("PluginHub::getPluginObject_", "Couldn't retrieve object from file", object.getName() );
	}

	long size = file.tellg();
	file.seekg(0, std::ios::beg);

	std::vector<char> data( size );
	file.read( data.data(), data.size() );
	file.close();

	return std::move(PluginObject( object.getId(), object.getName(), std::move(data) ));
}

void PluginHub::queueObject(Object& object)
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	lock_guard<mutex> lock(processQueueMutex_);

	processQueue_.push(object);
	processCond_.notify_all();
}

void PluginHub::queueObjects(std::vector<Object*>& objects)
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	lock_guard<mutex> lock(processQueueMutex_);

	for(Object* object : objects)
	{
		processQueue_.push(*object);
	}
	processCond_.notify_all();
}

/**
 * Is the plugin hub processing images
 */
bool PluginHub::isProcessing()
{
	return itemsRemaining_.load() > 0;
}

/**
 * How many images are remaining to be processed in the plugin hub
 */
unsigned int PluginHub::itemsRemaining() const
{
	return itemsRemaining_.load();
}

/**
 * How many items in total was the plugin hub supposed to process.
 */
unsigned int PluginHub::itemsTotal() const
{
	return itemsTotal_.load();
}

void PluginHub::confirmTagging( int objectId, const string& tagSetName, const BoundingBox& boundingBox, const string& tag )
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	pluginServer_->getProcessObjectServer().confirmTagging( objectId, tagSetName, boundingBox, tag );
}

}
