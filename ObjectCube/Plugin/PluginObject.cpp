/*
 *  PluginObject.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 11.8.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */


#include "PluginObject.h"

using namespace ObjectCube;

PluginObject::PluginObject(int id, const string& name, std::vector<char> data)
	: id_(id)
	, name_(name)
	, data_(std::move(data))
{
}



