/*
 *  PluginObject.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 14.3.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#ifndef ObjectCube_PLUGIN_OBJECT_
#define ObjectCube_PLUGIN_OBJECT_

#include <string>
#include <memory>
#include <vector>

using namespace std;

namespace ObjectCube
{
	class PluginObject
	{
	public:
		PluginObject( int id, const string& name, std::vector<char> data );
		PluginObject( PluginObject && ) = default;

		//Get/Set
		int getId() const { return id_; }
		string getName() const { return name_; }
		const char* getBinaryData() const { return data_.data();}
		long getDataSizeInBytes() const { return data_.size(); }
		
	private:
		int id_;
		string name_;
		std::vector<char> data_;

	};
}

#endif
