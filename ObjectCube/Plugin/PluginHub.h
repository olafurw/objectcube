#ifndef ObjectCube_PLUGIN_HUB_
#define ObjectCube_PLUGIN_HUB_

#include <vector>
#include <memory>
#include <fstream>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <queue>

#include "../Hub.h"
#include "../Object.h"

#include "../TagSet/TagSet.h"
#include "../TagSet/TagSetFactory.h"
#include "../Tag/Tag.h"

#include "PluginCommon.h"
#include "PluginObject.h"
#include "PluginTaggingProcessed.h"
#include "PluginServer.h"

#include "../Utils/Log.h"

namespace ObjectCube
{

class Hub;

class PluginHub
{
public:
	PluginHub();
	~PluginHub();

	void queueObject(Object& object);
	void queueObjects(std::vector<Object*>& objects);

	void confirmTagging( int objectId, const string& tagSetName, const BoundingBox& boundingBox, const string& tag );
	bool isProcessing();
	unsigned int itemsRemaining() const;
	unsigned int itemsTotal() const;

private:
	int createPluginTagSet_( int tagSetType, const string& name );
	PluginObject getPluginObject_( const Object& object ) const;
	void processLoop_();

	unique_ptr<PluginServer> pluginServer_;

	unique_ptr<Utils::Log> log_;

	bool processExit_ = false;
	unique_ptr<thread> processThread_;
	condition_variable processCond_;
	mutex processCondMutex_;

	mutex processQueueMutex_;
	queue<Object> processQueue_;

	mutex statusLock_;
	bool processing_;

	// there isn't really need for this to be atomic on x86, but might as well
	std::atomic_uint_fast32_t itemsRemaining_ = {0};
	std::atomic_uint_fast32_t itemsTotal_ = {0};
};
}
#endif
