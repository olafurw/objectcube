/*
 *  PhotoSet.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 25.10.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */

#include "State.h"

#include "../Hub.h"
#include "../TagSet/TagSet.h"

#include "../Filters/Filter.h"
#include "../Filters/DimensionFilter.h"

#include "../LayerShared/MemoryManagement.h"
#include "../LayerShared/Exception.h"
#include "../LayerShared/DebugInfo.h"

#include "../DataAccess/Root/HubDataAccess.h"
#include "../DataAccess/Root/FilterDataAccess.h"
#include "../DataAccess/Factories/HubDataAccessFactory.h"

#include "../Converters/StateObjectConverter.h"
#include "../Converters/FilterConverter.h"

using namespace ObjectCube;

State::State()
{
	Hub* hub = Hub::getHub();

//--- get state from hub

	// Filter setup
	vector<Filter*> filters;// = hub->getFilters();

	unique_ptr<HubDataAccess> dataAccess( HubDataAccessFactory::create() );
	vector<FilterDataAccess*> filtersDA = FilterConverter::logicToDataAccess( filters );

	StateDataAccess stateDA = dataAccess->getState( filtersDA );
	clearAndDestroy( filtersDA );

	vector<StateObjectDataAccess*>& stateObjectsDA = stateDA.getObjects();

	for( vector<StateObjectDataAccess*>::iterator itr = stateObjectsDA.begin(); itr != stateObjectsDA.end(); ++itr )
	{
		StateObject stateObject = StateObjectConverter::dataAccessToLogic( *itr );

//		vector<StateTag>::iterator tagItr;
//		int tagIndex = 0;
//		for( tagItr = stateObject.tags_.begin(); tagItr != stateObject.tags_.end(); ++tagItr )
//		{
//			(*tagItr).setTag_( TagSet::allTags_[ (*itr)->getTags()[ tagIndex ]->getTagId() ] );
//			++tagIndex;
//		}

		objects_.push_back( stateObject );
	}
	clearAndDestroy( stateObjectsDA );
	filterIdNodeIdObjectIds_ = stateDA.getHierarchyNodeObjectIds();

//--- get state function

	//dimensions
	const vector<Filter*> dimensionFilters;// = hub->getFilters( Filter::DIMENSION_FILTER );
	vector<StateDimension> dimensions;
	for( vector<Filter*>::const_iterator itr = dimensionFilters.begin(); itr != dimensionFilters.end(); ++itr )
	{
		DimensionFilter* dimensionFilter = dynamic_cast<DimensionFilter*>( *itr );
		int tagSetId = dimensionFilter->getTagSetId();
		int dimensionId = dimensionFilter->getDimensionId();
		TagSet* tagSet = hub->getTagSet( tagSetId );
		Dimension* origDimension = tagSet->getDimension( dimensionId );

		StateDimension stateDimension( origDimension, dimensionFilter->getId(), getFilterIdNodeIdObjectIds());

		dimensions.push_back( stateDimension );
	}
	setDimensions_( dimensions );
}

const vector<StateObject> State::getObjects( const vector<int>& objectIds ) const
{
	vector<StateObject> objects;
	for( vector<StateObject>::const_iterator objectItr = objects_.begin(); objectItr != objects_.end(); ++objectItr )
	{
		for( vector<int>::const_iterator idItr = objectIds.begin(); idItr != objectIds.end(); ++idItr )
		{
			if( (*idItr) == (*objectItr).getId() )
			{
				objects.push_back( *objectItr );
				continue;
			}
		}
	}
	
	return objects;
}


const StateDimension State::getDimension( int dimensionId ) const
{
	for( vector<StateDimension>::const_iterator itr = dimensions_.begin(); itr != dimensions_.end(); ++itr )
	{
		if( (*itr).getId() == dimensionId )
		{
			return *itr;
		}
	}
	throw Exception("State::getDimension", "Dimension requested does not exist, id: ", dimensionId );
}


const MultiDimensionalView State::getMultiDimensionalView( bool withEmptySlices ) const
{
	return MultiDimensionalView( objects_, withEmptySlices );
}

const MultiDimensionalView* State::getNewMultiDimensionalView( bool withEmptySlices ) const
{
	return new MultiDimensionalView( objects_, withEmptySlices );
}

void State::initializeMdv(MultiDimensionalView* mdv)
{
	mdv->construct(objects_);
}
