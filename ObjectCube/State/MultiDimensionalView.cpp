/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#include "MultiDimensionalView.h"

#include <map>
#include <sstream>
#include <iostream>

#include "../LayerShared/SharedDefinitions.h"
#include "../LayerShared/Exception.h"
#include "StateDimension.h"
#include "StateDimensionNode.h"
#include "../TagSet/TagSet.h"
#include "../Tag/Tag.h"
#include "../Hub.h"

#include "../Utils/stacktrace.h"

using namespace ObjectCube;

MultiDimensionalView::MultiDimensionalView( const vector<StateObject>& objects, bool withEmptySlices )
{
	setWithEmptySlices_( withEmptySlices );
		
	construct( objects );
}

void MultiDimensionalView::construct( const vector<StateObject>& objects )
{
	setUniqueObjectCount_( objects.size() );
	setObjectCount_( 0 );

	//Create tag* lists for all axes, populate those in effect - simplifies code
	for( auto& kv : axes_ )
	{
		getTagsForAxis_( kv.second, objects );
	}

	addObjectsToCells_( objects );	
}

int MultiDimensionalView::getAxisCellCount( unsigned int axis )
{
	size_t axisST = (size_t) axis;
	if( axisST > axes_.size() )
	{
		return 0;
	}
	return getAxis_( axis ).getCellCount();
}

MultiDimensionalView::Axis MultiDimensionalView::getAxis_( unsigned int axis )
{
	// Index check when fetching axis from vector.
	if( axes_.find(axis) == axes_.end() )
	{
		throw Exception( "MultiDimensionalView::getAxis", "Axis not set", axis );
	}
	
	return axes_[ axis ];
}

void MultiDimensionalView::setAxis( unsigned int axis, StateDimensionNode stateDimensionNode, int viewDepth )
{
	Axis axisValue( stateDimensionNode, viewDepth );
	axes_[ axis ] = axisValue;
}

void MultiDimensionalView::setAxis( unsigned int axis, TagSet* tagSet )
{
	Axis axisValue( tagSet );
	axes_[ axis ] = axisValue;
}

void MultiDimensionalView::clearAxis( unsigned int axis )
{
	size_t axisST = (size_t) axis;
	if( axes_.size() > 0 && axes_.size() >= axisST )
	{
		axes_.erase( axis );
		return;
	}
	throw Exception( "MultiDimensionalView::getAxisCellCount", "Axis has not been set!", axisST );
}

void MultiDimensionalView::clearAxes()
{
	axes_.clear();
}

bool MultiDimensionalView::generateKey_( string& key, const vector<unsigned int>& dimensionCellCounts, vector<unsigned int>& currCounts, unsigned int& currDimension )
{
	//Think of this as an arbitrary number of nested loops.
	//For anything but the outer most "loop" we reset counters and recurse.

	// for three dimensions, and cell counts [ 3, 2, 2 ] we should get:
	//	0:0:0
	//	0:0:1
	//	0:1:0
	//	0:1:1
	//	1:0:0

	//When ever we raise a counter we reset all counters that come after it in the vector

	if( ! dimensionCellCounts.size() )
	{
		return false;
	}

	if( !( currCounts[ currDimension ] < dimensionCellCounts[ currDimension ] ) ) //Reset or return false
	{
		if( currDimension == 0 ) //We have processed all keys
		{
			return false;
		}
		//We have not processed all and need to raise and reset counters

		//reset the current dimension, and all after it
		for( unsigned int resetIndex = currDimension; resetIndex < currCounts.size(); ++resetIndex )
		{
			currCounts[ resetIndex ] = 0;
		}
		currCounts[ --currDimension ]++; //move the current dimension one up and raise it by one.

		return generateKey_( key, dimensionCellCounts, currCounts, currDimension );
	}

	//Normal processing
	currDimension = currCounts.size() - 1;

	stringstream keyStream;
	for( unsigned int dimIndex = 0; dimIndex < currCounts.size(); ++dimIndex )
	{
		if( dimIndex > 0 )
		{
			keyStream << ":";
		}
		keyStream << currCounts[ dimIndex ];
	}
	currCounts[ currDimension ]++;

	key = keyStream.str();
	return true;
}

map<unsigned int, unsigned int> MultiDimensionalView::keyToMap_( const string& key )
{
	map<unsigned int, unsigned int> result;

	stringstream keyStream(key);
	unsigned int keyValue;
	auto axisIter = axes_.begin();

	while(keyStream >> keyValue)
	{
		result[ axisIter->first ] = keyValue;

		if(keyStream.peek() == ':')
		{
			keyStream.ignore();
		}

		axisIter++;
	}

	return result;
}

void MultiDimensionalView::applyKeyToCell_( Cell& cell, const string& key )
{
	map<unsigned int, unsigned int> keyMap = keyToMap_( key );
	for( auto& kv : keyMap )
	{
		cell.setPosition(kv.first, kv.second);
	}
}

void MultiDimensionalView::addObjectsToCells_( const vector<StateObject>& objects )
{
	dimensionalCells_.clear();

	if( axes_.size() == 0 )
	{
		return;
	}

	std::cout << "[MDV] Object size: " << objects.size() << std::endl;
	std::cout << "[MDV] Axis Count: " << axes_.size() << std::endl;

	vector<unsigned int> axisTagCounts;
	for( auto& kv : axes_ )
	{
		axisTagCounts.push_back( axes_[ kv.first ].tags_.size() );
	}

	vector<unsigned int> currAxisCounts( axisTagCounts.size(), 0 );
	unsigned int currAxis = axisTagCounts.size() - 1;
	string key;

	while( generateKey_( key, axisTagCounts, currAxisCounts, currAxis ) )
	{
		map<unsigned int, vector<Tag*>> dimensionTags;
		map<unsigned int, string> displayLabels;

		for( auto& kv : axes_ )
		{
			if( kv.second.getType() != INVALID )
			{
				try
				{
					map<unsigned int, unsigned int> keyMap = keyToMap_( key );
					dimensionTags[ kv.first ] = axes_[ kv.first ].tags_.at( keyMap[ kv.first ] );
					displayLabels[ kv.first ] = axes_[ kv.first ].displayLabels_.at( keyMap[ kv.first ] );
				}
				catch(...)
				{
					continue;
				}
			}
		}

		Cell currentCell( axes_.size(), dimensionTags, displayLabels );
		for( auto so : objects )
		{
			if( currentCell.belongs( so ) )
			{
				++noObjects_;
			}
		}

		applyKeyToCell_( currentCell, key );

		dimensionalCells_.push_back( currentCell );
	}
}

bool MultiDimensionalView::inUse_( int tagId, const vector<StateObject>& objects )
{
	for( vector<StateObject>::const_iterator itr = objects.begin(); itr != objects.end(); ++itr )
	{
		if( (*itr).hasTag( tagId ) )
		{
			return true;
		}
	}
	return false;
}

bool MultiDimensionalView::inUse_( map<int, int>& tagIdNodeId, const vector<StateObject>& objects )
{
	map<int, int> replacementMap;
	bool foundTagInUse = false;
	for( map<int, int>::iterator mapItr = tagIdNodeId.begin(); mapItr != tagIdNodeId.end(); ++mapItr )
	{
		if( inUse_( mapItr->first, objects ) )
		{
			foundTagInUse = true;
			replacementMap.insert( *mapItr );
		}
	}
	tagIdNodeId = replacementMap;
	return foundTagInUse;
}

void MultiDimensionalView::getTagsForAxis_( Axis& axis, const vector<StateObject>& objects )
{
	if( axis.getType() == INVALID ) //Not in use, not an error
	{
		return;
	}
	
	axis.displayLabels_.clear();
	axis.tags_.clear();
	
	if( axis.getType() == TAG_SET )
	{
		vector<Tag*> tags = axis.getTagSet()->getTags();
		vector<vector<Tag*> > dimTags;
		for( vector<Tag*>::iterator itr = tags.begin(); itr != tags.end(); ++itr )
		{
			if( !getWithEmptySlices() && !inUse_( (*itr)->getId() , objects ) )
			{
				continue;
			}
			vector<Tag*> tempTagVector;
			tempTagVector.push_back( *itr );
			dimTags.push_back( tempTagVector );
			axis.displayLabels_.push_back( (*itr)->valueAsString() );
		}
		axis.tags_ = dimTags;
		return;
	}
	else if( axis.getType() == HIERARCHY_NODE )
	{
		//Get the tags for each of the branches of the current node!!!
		//const vector<StateDimensionNode> branches = axis.node_.getBranches();
		vector<StateDimensionNode> branches = axis.node_.getSubHierarchyLevel( axis.getViewDepth() );
		
		for( vector<StateDimensionNode>::const_iterator itr = branches.begin(); itr != branches.end(); ++itr )
		{
			map<int, int> tagIdNodeId = (*itr).getNode()->getSubtreeTagIds();
			if( !getWithEmptySlices() && !inUse_( tagIdNodeId, objects ) )
			{
				continue;
			}
			
			axis.displayLabels_.push_back( (*itr).getNode()->getName() );
			
			TagSet* tagSet = Hub::getHub()->getTagSet( (*itr).getNode()->getTagSetId() );
			vector<Tag*> tempTagVector;
			for( map<int, int>::iterator mapItr = tagIdNodeId.begin(); mapItr != tagIdNodeId.end(); ++mapItr )
			{
				Tag* tempTag = const_cast<Tag*>( tagSet->getTag( mapItr->first ) );
				tempTagVector.push_back( tempTag );
			}
			axis.tags_.push_back( tempTagVector );
		}
		return;
	}
	throw Exception( "MultiDimensionalView::getTagsForAxis_", "Unknown axis type", axis.getType() );
}

vector<Cell> MultiDimensionalView::getCells()
{
	return dimensionalCells_;
}

//
// Axis
//

MultiDimensionalView::Axis::Axis( TagSet* tagSet )
{
	tagSet_ = tagSet;
	type_ = TAG_SET;
	viewDepth_ = 0;
}


MultiDimensionalView::Axis::Axis( StateDimensionNode node, int viewDepth )
{
	node_ = node;
	type_ = HIERARCHY_NODE;
	viewDepth_ = viewDepth;
	tagSet_ = nullptr;
}


MultiDimensionalView::Axis::Axis()
{
	tagSet_ = 0;
	type_ = INVALID;
	viewDepth_ = 0;
}


int MultiDimensionalView::Axis::getCellCount() const
{
	return tags_.size();
}


TagSet* MultiDimensionalView::Axis::getTagSet()
{
	if( getType() != TAG_SET )
	{
		throw Exception( "MultiDimensionalView::Axis::getTagSet", "TagSet requested from an axis with no tag-set" );
	}
	return tagSet_;
}


StateDimensionNode MultiDimensionalView::Axis::getNode()
{
	if( getType() != HIERARCHY_NODE )
	{
		throw Exception( "MultiDimensionalView::Axis::getNode", "StateDimensionNode requested from an axis with no node" );
	}

	return node_;
}


bool MultiDimensionalView::Axis::operator==( Axis const& x ) const
{
	return	tagSet_ == x.tagSet_ && 
			node_ == x.node_ && 
			type_ == x.type_ && 
			viewDepth_ == x.viewDepth_ &&
			tags_ == x.tags_ &&
			displayLabels_ == x.displayLabels_;
}

