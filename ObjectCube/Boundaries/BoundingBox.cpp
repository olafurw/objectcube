/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#include "BoundingBox.h"

#include <utility>

using namespace ObjectCube;



BoundingBox::BoundingBox()
{
	upperLeftCorner_.setX( 0.0 );
	upperLeftCorner_.setY( 100.0 );
	lowerRightCorner_.setX( 100.0 );
	lowerRightCorner_.setY( 0.0 );
}

BoundingBox::BoundingBox( Coordinate2D upperLeftCorner, Coordinate2D lowerRightCorner )
{
	upperLeftCorner_ = upperLeftCorner;
	lowerRightCorner_ = lowerRightCorner;
}

BoundingBox::BoundingBox( const BoundingBox& b )
{
	upperLeftCorner_ = b.upperLeftCorner_;
	lowerRightCorner_ = b.lowerRightCorner_;
}

BoundingBox::BoundingBox( BoundingBox&& b )
{
	upperLeftCorner_ = std::move(b.upperLeftCorner_);
	lowerRightCorner_ = std::move(b.lowerRightCorner_);
}

BoundingBox& BoundingBox::operator=( const BoundingBox& b )
{
	if( this != &b )
	{
		upperLeftCorner_ = b.upperLeftCorner_;
		lowerRightCorner_ = b.lowerRightCorner_;
	}

	return *this;
}

BoundingBox& BoundingBox::operator=( BoundingBox&& b )
{
	if( this != &b )
	{
		upperLeftCorner_ = std::move(b.upperLeftCorner_);
		lowerRightCorner_ = std::move(b.lowerRightCorner_);
	}

	return *this;
}



bool BoundingBox::operator==( const BoundingBox& x ) const
{
	return upperLeftCorner_ == x.getUpperLeftCorner() && lowerRightCorner_ == x.getLowerRightCorner();
}

