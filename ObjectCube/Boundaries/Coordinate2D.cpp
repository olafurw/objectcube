/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#include "Coordinate2D.h"

using namespace ObjectCube;



Coordinate2D::Coordinate2D()
:	x_( 0.0 ),
	y_( 0.0 )
{
	
}


Coordinate2D::Coordinate2D( double x, double y )
:	x_( 0.0 ),
	y_( 0.0 )
{
	x_ = x;
	y_ = y;
}

Coordinate2D::Coordinate2D( const Coordinate2D& c )
{
	x_ = c.x_;
	y_ = c.y_;
}

Coordinate2D::Coordinate2D( Coordinate2D&& c )
{
	x_ = c.x_;
	y_ = c.y_;

	c.x_ = 0.0;
	c.y_ = 0.0;
}

Coordinate2D& Coordinate2D::operator=( const Coordinate2D& c )
{
	if( this != &c )
	{
		x_ = c.x_;
		y_ = c.y_;
	}

	return *this;
}

Coordinate2D& Coordinate2D::operator=( Coordinate2D&& c )
{
	if( this != &c )
	{
		x_ = c.x_;
		y_ = c.y_;

		c.x_ = 0.0;
		c.y_ = 0.0;
	}

	return *this;
}



bool Coordinate2D::operator==( const Coordinate2D& x ) const
{
	return x_ == x.getX() && y_ == x.getY();
}

