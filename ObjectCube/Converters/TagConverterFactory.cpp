/*
 *  TagConverterFactory.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 29.3.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "TagConverterFactory.h"

#include <sstream>

#include "../LayerShared/TagCommon.h"
#include "TagConverter.h"
#include "AlphanumericalTagConverter.h"
#include "NumericalTagConverter.h"
#include "TimeTagConverter.h"
#include "DateTagConverter.h"
#include "../LayerShared/Exception.h"

using namespace ObjectCube;



unique_ptr<TagConverter> TagConverterFactory::create( int tagTypeId )
{
	switch ( tagTypeId  )
	{
	case TagCommon::ALPHANUMERICAL: return unique_ptr<TagConverter>{new AlphanumericalTagConverter()};
	case TagCommon::NUMERICAL:      return unique_ptr<TagConverter>{new NumericalTagConverter()};
	case TagCommon::TIME:           return unique_ptr<TagConverter>{new TimeTagConverter()};
	case TagCommon::DATE:           return unique_ptr<TagConverter>{new DateTagConverter()};
	}
	stringstream stringStream;
	stringStream << "Unknown tag type: " << tagTypeId;
	throw Exception( "TagConverterFactory::create", stringStream.str() );
}
