/*
 *  AlphanumericalTagConverter.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 23.3.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#ifndef ObjectCube_ALPHANUMERICAL_TAG_CONVERTER_
#define ObjectCube_ALPHANUMERICAL_TAG_CONVERTER_

#include <memory>

#include "TagConverter.h"

using namespace std;

namespace ObjectCube 
{	
	class AlphanumericalTagDataAccess;
	
	class AlphanumericalTagConverter : public TagConverter
	{
	protected:
		virtual unique_ptr<Tag> dataAccessToLogic_( const TagDataAccess* tagDA );
		virtual unique_ptr<TagDataAccess> logicToDataAccess_( const Tag* tag );
		
	};
}

#endif
