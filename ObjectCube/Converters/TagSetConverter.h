/*
 *  TagSetConverter.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 9.2.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#ifndef ObjectCube_TAG_SET_CONVERTER_
#define ObjectCube_TAG_SET_CONVERTER_

#include <memory>
#include <vector>
#include "../TagSet/TagSet.h"

namespace ObjectCube 
{
	class TagSetDataAccess;
	
	class TagSetConverter
	{
	public:
		static shared_ptr<TagSet> dataAccessToLogic( TagSetDataAccess* tagSetDA );
		static TagSetDataAccess* logicToDataAccess( const TagSet* dimension );
		
		static vector<shared_ptr<TagSet> > dataAccessToLogic( vector<TagSetDataAccess*> dimensionsDA );
		static vector<TagSetDataAccess*> logicToDataAccess( const vector<TagSet*>& dimensions );
				
	};
}

#endif