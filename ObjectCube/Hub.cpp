/*
 *  Framework.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 25.10.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */ 

#include <memory>
#include <sstream>
#include <fstream>
#include <iostream>

#include "Hub.h"

#include "Tag/Tag.h"
#include "TagSet/TagSet.h"
#include "TagSet/TagSetFactory.h"
#include "Tag/TagFactory.h"
#include "Object.h"
#include "ObjectTag.h"
#include "Filters/Filter.h"
#include "Tag/AlphanumericalTag.h"
#include "Hierarchy/Dimension.h"
#include "Hierarchy/PersistentDimension.h"
#include "Hierarchy/VirtualDimension.h"

#include "DataAccess/Root/HubDataAccess.h"
#include "DataAccess/Root/FilterDataAccess.h"
#include "DataAccess/Root/ObjectDataAccess.h"
#include "DataAccess/Root/StateObjectDataAccess.h"

#include "DataAccess/Factories/HubDataAccessFactory.h"
#include "DataAccess/Factories/ObjectDataAccessFactory.h"

#include "Converters/ObjectConverter.h"
#include "Converters/TagSetConverter.h"
#include "LayerShared/SharedDefinitions.h"
#include "Converters/StateObjectConverter.h"
#include "Converters/FilterConverter.h"


#include "LayerShared/Exception.h"

#include "Plugin/PluginCommon.h"
#include "Plugin/PluginTagSet.h"
#include "Plugin/PluginObject.h"
#include "Plugin/PluginServer.h"
#include "Plugin/PluginHub.h"

#include "LayerShared/MemoryManagement.h"
#include "LayerShared/Parameters.h"
#include "LayerShared/LanguageCommon.h"
#include "LayerShared/DebugInfo.h"

using namespace ObjectCube;
using namespace std;

unique_ptr<Hub> Hub::instance_;

Hub::Hub()
{
	log_ = std::unique_ptr<Utils::Log>( new Utils::Log("oc_hub.log") );
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;

	dataAccessType_ = Hub::getDataAccessType();
}

Hub::~Hub()
{
	//while(pluginHub_->isProcessing());

	cleanup_();
}

Hub* Hub::getHub()
{
	if( instance_ == 0 )
	{
		instance_.reset( new Hub() );
		//Override default values is supplied
		if( Parameters::getParameters()->contains( getDataAccessTypeParameterName() ) )
		{
			HubCommon::setDataAccessType( Parameters::getParameters()->getValueAsInt( getDataAccessTypeParameterName() ) );
		}
		if( Parameters::getParameters()->contains( LanguageCommon::getLanguageIdParameterName() ) )
		{
			setLanguageId( Parameters::getParameters()->getValueAsInt( LanguageCommon::getLanguageIdParameterName() ) );
		}
		init_();
	}
	if( Hub::getDataAccessType() != instance_->dataAccessType_ )
	{
		init_();
	}

	return instance_.get();
}

void Hub::destroyHub()
{
	if (instance_) {
		// ugly, need to takedown plugin hub first so threads don't create another hub by calling getHub while stopping..
		instance_->pluginHub_ = nullptr;
		instance_ = nullptr;
	}
}

void Hub::init_()
{
	instance_->dataAccessType_ = Hub::getDataAccessType();

	DebugInfo::getDebugInfo()->pushTimer( "Hub", "init_" );
	
	instance_->pluginHub_.reset(new PluginHub);
	
	DebugInfo::getDebugInfo()->popTimer();
}


void Hub::destroy()
{
	if (instance_) {
		instance_->destroyHub();
	}
}


void Hub::cleanup_()
{	
	HubDataAccessFactory::create()->disconnect();
}

 

TagSet* Hub::addTagSet(TagSet *tagSet )
{
	if( tagSet->getId() == INVALID_VALUE )
	{
		return tagSet->createImp_();
	}
	
	//We copy the TagSet to limit user action on the framework (and to simplify user memory management)
	shared_ptr<TagSet> tagSetCopy( TagSetFactory::create( tagSet->getTypeId() ) );
//	*(tagSetCopy.get()) = *tagSet;
//	tagSets_.push_back( tagSetCopy );
	return tagSetCopy.get();
}


void Hub::removeTagSet( TagSet* /*const*/ tagSet )
{
//	for( vector<shared_ptr<TagSet> >::iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		if( (*itr)->getId() == tagSet->getId() )
//		{
//			tagSets_.erase( itr );
//			return;
//		}
//	}
	stringstream stringStream;
	stringStream << "Framework did not contain tag-set with id: " << tagSet->getId();
	throw Exception( "Hub::removeTagSet", stringStream.str() );		
	
	/*
	vector<TagSet*>::iterator itr = find_if( tagSets_.begin(), tagSets_.end(), dereference( tagSet ) );
	if( itr == tagSets_.end() )
	{
		stringstream stringStream;
		stringStream << "Framework did not contain tag-set with id: " << tagSet->getId();
		throw Exception( "Hub::removeTagSet", stringStream.str() );		
	}
	delete *itr; //All user pointers are now invalid !
	*itr = 0;
	tagSets_.erase( itr );
	 */
}


void Hub::addObject( Object& object )
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;
	/*
	 An uncategorized photo is a photo that has no user added tags.
	 
	 Using dimensions and tags for everything includes Uncategorized photos.  Uncategorized is a dimension that has one tag, uncategorized.  If a photo is added without a tag, uncategorized is added to it.
	 It is removed when a tag is added to the photo.
		Another way of doing this would be to use an outerjoin for photos that have no tags.  That would require special handling and it would not help in filtering only on uncatgorized photos.
	 */
	
	if( object.getId() == INVALID_VALUE )
	{
		object.create();
	}

	pluginHub_->queueObject( object );
}

void Hub::addObjects( std::vector<Object*>& objects )
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;

	for( Object* object : objects )
	{
		if( object->getId() == INVALID_VALUE )
		{
			object->create();
		}
	}

	pluginHub_->queueObjects( objects );
}

/**
 * Is the plugin hub processing images
 */
bool Hub::isProcessing()
{
	return pluginHub_->isProcessing();
}

/**
 * How many images are remaining to be processed in the plugin hub
 */
unsigned int Hub::itemsRemaining() const
{
	return pluginHub_->itemsRemaining();
}

/**
 * How many items in total was the plugin hub supposed to process.
 */
unsigned int Hub::itemsTotal() const
{
	return pluginHub_->itemsTotal();
}

void Hub::tagObject_( const vector<PluginTaggingProcessed>& potentialTags, Object& object )
{
	log_->writeEnter() << __PRETTY_FUNCTION__ << std::endl;

	DebugInfo::getDebugInfo()->pushTimer( "Hub", "tagObject_", " (add tags from plugins)" );

	for( auto& potentialTag : potentialTags )
	{
		try 
		{
			TagSet* tagSet = getTagSet( potentialTag.getTagSetId() );
			const Tag* tag = tagSet->fetchOrAddTag( potentialTag.getTag() );
			ObjectTag objectTag( const_cast<Tag*>( tag ), potentialTag.getBoundingBox() );
            objectTag.setConfirmed( potentialTag.getConfirmed() ); //Put in constructor
			object.addTag( objectTag );
		}
		catch ( Exception& e )
		{
			DebugInfo::getDebugInfo()->output( "Hub", "tagObject_", &e );
		}
	}

	DebugInfo::getDebugInfo()->popTimer();
}

void Hub::fillInAndValidateFilters_(Filters &filters)
{
	for (auto &filter : filters)
	{
		filter->fillInAndValidateValues();
	}
}

vector<StateObject> Hub::getObjects(const Hub::Filters &filters)
{
	/*
	 Change the name of this function to getState
	 Change the DataAccess function to getState and have it return a StateDateAccess (not a vector of StateObject)
		Inside StateDataAccess is to be a map<int (filter_id), map<hierarchy_node_id, vector<object_id> > >
			This should fix the hierarchy slowdown
	 
	 
	 */

	DebugInfo::getDebugInfo()->pushTimer( "Hub", "getObjects" );

	unique_ptr<HubDataAccess> dataAccess( HubDataAccessFactory::create() );
	
	DebugInfo::getDebugInfo()->pushTimer( "Hub", "getObjects", "data access" );

	Filters verfiedFilters = filters;
	fillInAndValidateFilters_(verfiedFilters);
	vector<FilterDataAccess*> filtersDA = FilterConverter::logicToDataAccess( verfiedFilters );
	vector<StateObjectDataAccess*> stateObjectsDA = dataAccess->getObjects( filtersDA );
	clearAndDestroy( filtersDA );
	DebugInfo::getDebugInfo()->popTimer();
	
	lastGetObjectsQueryAsString_ = dataAccess->getObjectsQueryAsString();
	
	DebugInfo::getDebugInfo()->output( "Hub", "getObjects", "no. objects", stateObjectsDA.size() );
	
	vector<StateObject> stateObjects;
	DebugInfo::getDebugInfo()->pushTimer( "Hub", "getObjects", "adding tags" );

	//The looped code must perform well!
	vector<StateTag>::iterator tagItr;
	for( vector<StateObjectDataAccess*>::iterator itr = stateObjectsDA.begin(); itr != stateObjectsDA.end(); ++itr )
	{
		StateObject stateObject = StateObjectConverter::dataAccessToLogic( *itr );
		
		int tagIndex = 0;
		for( tagItr = stateObject.tags_.begin(); tagItr != stateObject.tags_.end(); ++tagItr )
		{
			//(*tagItr).setTag_( TagSet::allTags_[ (*itr)->getTags()[ tagIndex ]->getTagId() ].get() );
			//(*tagItr).setTag_( TagSet::allTags_[ (*itr)->getTags()[ tagIndex ]->getTagId() ] );
			++tagIndex;
		}
	
		/*
		for( unsigned int tagIndex = 0; tagIndex < stateObject.getTags().size(); ++tagIndex )
		{
			stateObject.tags_[ tagIndex ].setTag_( 
												  const_cast<Tag*>( getTag( (*itr)->getTags()[ tagIndex ]->getTagId() ) ) 
												  );
		}*/
		
		stateObjects.push_back( stateObject );
	}
	DebugInfo::getDebugInfo()->popTimer();
	clearAndDestroy( stateObjectsDA );	
	
	DebugInfo::getDebugInfo()->popTimer();
	
	return stateObjects;
}


int Hub::getParentTagSetsAccessType( const Tag* /*const*/ tag ) const
{
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		if( (*itr)->getId() == tag->getTagSetId() )
//		{
//			return (*itr)->getAccessId();
//		}
//	}
	return TagSet::ACCESS_TYPE_UNKNOWN;
}


const Tag * /*const*/ Hub::getUncategorizedTag() const
{
//	TagSet* tagSet = 0;
//	//Find the dimension
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		tagSet = (*itr).get();
//		if( tagSet->getName() == Hub::getUncategorizedDimensionName() )
//		{
//			break;
//		}
//	}
//	if( tagSet == 0) //Something very wrong
//	{
//		throw Exception( "Hub::getUncategorizedTag", "Could not find Uncategorized tag-set!" );
//	}
//	//Find the tag
//	vector<Tag*> tags = tagSet->getTags();  //Debug
//	for( vector<Tag*>::iterator itrT = tags.begin(); itrT != tags.end(); ++itrT )
//	{
//		Tag* tag = *itrT;
//		if( tag->getTypeId() == TagCommon::ALPHANUMERICAL )
//		{
//			AlphanumericalTag* anTag = dynamic_cast<AlphanumericalTag*>( *itrT );
//			if( anTag->getName() == Hub::getUncategorizedTagName() )
//			{
//				return *itrT;
//			}
//		}
//	}
	//Did not find it :@
	throw Exception( "Hub::getUncategorizedTag", "Could not find Uncategorized tag in Uncategorized tag-set!" );
}


vector<TagSet*> Hub::getTagSets() const
{
	vector<TagSet*> tagSets;
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		tagSets.push_back( (*itr).get() );
//	}
	return tagSets;
}


TagSet* Hub::getTagSet( int id )
{
//	for( vector<shared_ptr<TagSet> >::iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		if( (*itr)->getId() == id )
//		{
//			return (*itr).get();
//		}
//	}
	//TagSet not found
	stringstream stringStream;
	stringStream << "Framework did not contain tag-set with id: " << id;
	throw Exception( "Hub::getTagSet(id)", stringStream.str() );
}


TagSet* Hub::getTagSet( const string& name )
{
//	for( vector<shared_ptr<TagSet> >::iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		if( (*itr)->getName() == name )
//		{
//			return (*itr).get();
//		}
//	}
	//TagSet not found
	stringstream stringStream;
	stringStream << "Framework did not contain tag-set with name: " << name;
	throw Exception( "Hub::getTagSet(name)", stringStream.str() );
}

State Hub::getState(const vector<Filter *> &filters)
{
	(void)filters;
	return {};
}

const vector<Tag*> Hub::getTags( const vector<int>& tagIds ) const
{
	vector<Tag*> tags;
	for( vector<int>::const_iterator idItr = tagIds.begin(); idItr != tagIds.end(); ++idItr )
	{
		tags.push_back( const_cast<Tag*>( getTag( *idItr ) ) );
	}
	return tags;
}


const vector<Tag*> Hub::getAllTags() const
{
	vector<Tag*> tags;
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		vector<Tag*> tempTags = (*itr)->getTags();
//		tags.insert( tags.end(), tempTags.begin(), tempTags.end() );
//	}
	return tags;
}

const vector<Tag*> Hub::getTagsByTagSet( int id ) const
{
	vector<Tag*> tags;
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		if( (*itr)->getId() == id )
//		{
//			vector<Tag*> tempTags = (*itr)->getTags();
//			tags.insert( tags.end(), tempTags.begin(), tempTags.end() );

//			break;
//		}
//	}

	return tags;
}

const Tag* /*const*/ Hub::getTag( int id ) const
{
//	map<int, Tag* >::iterator itr = TagSet::allTags_.find( id );
//	//map<int, shared_ptr<Tag> >::iterator itr = TagSet::allTags_.find( id );
//	if( itr == TagSet::allTags_.end() ) //Not found
//	{
		throw Exception( "Hub::getTag", "Tag does not exist (id).", id );
//	}
//	//return (*itr).second.get();
//	return (*itr).second;
}


const vector<Dimension*> Hub::getDimensions() const
{
	vector<Dimension*> dimensions;
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		vector<Dimension*> tempDimensions = (*itr)->getDimensions();
//		dimensions.insert( dimensions.end(), tempDimensions.begin(), tempDimensions.end() );
//	}
	return dimensions;
}


const vector<PersistentDimension*> Hub::getPersistentDimensions() const
{
	vector<PersistentDimension*> dimensions;
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		vector<PersistentDimension*> tempDimensions = (*itr)->getPersistentDimensions();
//		dimensions.insert( dimensions.end(), tempDimensions.begin(), tempDimensions.end() );
//	}
	return dimensions;
}


const vector<VirtualDimension*> Hub::getVirtualDimensions() const
{
	vector<VirtualDimension*> dimensions;
//	for( vector<shared_ptr<TagSet> >::const_iterator itr = tagSets_.begin(); itr != tagSets_.end(); ++itr)
//	{
//		vector<VirtualDimension*> tempDimensions = (*itr)->getVirtualDimensions();
//		dimensions.insert( dimensions.end(), tempDimensions.begin(), tempDimensions.end() );
//	}
	return dimensions;
}

void Hub::refreshMaterializedViews()
{
	unique_ptr<HubDataAccess> dataAccess( HubDataAccessFactory::create() );
	dataAccess->refreshMaterializedViews();
}

void Hub::confirmTagging( int objectId, const string& tagSetName, const BoundingBox& boundingBox, const string& tag )
{
	pluginHub_->confirmTagging( objectId, tagSetName, boundingBox, tag );
}
