/*
 *  DateTagSetTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 12.4.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "DateTagSetTest.h"

#include <cppunit/extensions/HelperMacros.h>

#include "../Hub.h"
#include "TestIds.h"
#include "../TagSet/TagSet.h"
#include "../TagSet/DateTagSet.h"
#include "TagSetAssertionTraits.h"
#include "../LayerShared/HubCommon.h"
#include "../Tag/Tag.h"
#include "../Tag/DateTag.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../LayerShared/Exception.h"

using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( DateTagSetTest );



void DateTagSetTest::setUp()
{
	
}


void DateTagSetTest::tearDown()
{
	
}


//  Canned tests --------------------------------------------------------------------------------------------------------------------


void DateTagSetTest::testCreateCanned()
{
	cerr << "\nDateTagSetTest::testCreateCanned = ";
	Hub::setDataAccessType( Hub::CANNED );
	
	DateTagSet userDim("Yet another date user tag set");
	userDim.create();
	
	CPPUNIT_ASSERT( userDim.getId() != INVALID_VALUE );
	CPPUNIT_ASSERT_EQUAL( (int)TagSetCommon::USER, userDim.getAccessId() );
	CPPUNIT_ASSERT_EQUAL( (int)TagSetCommon::DATE, userDim.getTypeId() );
}


//  SQLite tests --------------------------------------------------------------------------------------------------------------------

void DateTagSetTest::testFetchSQLite()
{
	cerr << "\nDateTagSetTest::testFetchSQLite = ";
	Hub::setDataAccessType( Hub::SQLITE );
	
	testFetch_( TEST_SQLITE_CLEANUP_PREFIX + " Date unit test tag-set" );
}


void DateTagSetTest::testCreateSQLite()
{
	cerr << "\nDateTagSetTest::testCreateSQLite = ";
	Hub::setDataAccessType( Hub::SQLITE );
	
	testCreate_( TEST_SQLITE_CLEANUP_PREFIX + " DateTagSet creation test" );
}


//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void DateTagSetTest::testFetchMonetDB()
{
	cerr << "\nDateTagSetTest::testFetchMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testFetch_( TEST_MONETDB_CLEANUP_PREFIX + " Date unit test tag-set" );
}


void DateTagSetTest::testCreateMonetDB()
{
	cerr << "\nDateTagSetTest::testCreateMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testCreate_( TEST_MONETDB_CLEANUP_PREFIX + " DateTagSet creation test" );
}


//  Oracle tests --------------------------------------------------------------------------------------------------------------------

void DateTagSetTest::testFetchOracle()
{
	cerr << "\nDateTagSetTest::testFetchOracle = ";
	Hub::setDataAccessType( Hub::ORACLE );
	
	testFetch_( TEST_ORACLE_CLEANUP_PREFIX + " Date unit test tag-set" );	
}


void DateTagSetTest::testCreateOracle()
{
	cerr << "\nDateTagSetTest::testCreateOracle = ";
	Hub::setDataAccessType( Hub::ORACLE );
	
	testCreate_( TEST_ORACLE_CLEANUP_PREFIX + " DateTagSet creation test" );	
}


//  Common --------------------------------------------------------------------------------------------------------------------

void DateTagSetTest::testFetch_( string tagSetName )
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_DATE_TAG_SET_ID );
	
	CPPUNIT_ASSERT_EQUAL( (int)TEST_DATE_TAG_SET_ID, tagSet->getId() );
	CPPUNIT_ASSERT_EQUAL( (int)TagSet::USER, tagSet->getAccessId() );
	CPPUNIT_ASSERT_EQUAL( (int)TagSet::DATE, tagSet->getTypeId() );
	CPPUNIT_ASSERT_EQUAL( tagSetName, tagSet->getName() );
	CPPUNIT_ASSERT( tagSet->getTags().size() > 0 );
	//CPPUNIT_ASSERT( tagSet->getDimensions().size() > 0 );
}


void DateTagSetTest::testCreate_( string tagSetName )
{
	DateTagSet userDim( tagSetName );
	userDim.create();
	
	CPPUNIT_ASSERT( userDim.getId() != INVALID_VALUE );
	CPPUNIT_ASSERT_EQUAL( (int)TagSetCommon::USER, userDim.getAccessId() );
	CPPUNIT_ASSERT_EQUAL( (int)TagSetCommon::DATE, userDim.getTypeId() );
	CPPUNIT_ASSERT_EQUAL( 0, (int)userDim.getTags().size() );
	CPPUNIT_ASSERT_EQUAL( 1, (int)userDim.getDimensions().size() );
}



