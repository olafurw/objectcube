
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "SetupDataStorage.h"

#include "../LayerShared/DebugInfo.h"

using namespace ObjectCube;

int main(int, char **)
{
	DebugInfo::getDebugInfo()->output( "main", "unitTests", "Starting cleanup!" );
	SetupDataStorage::getSetupDataStorage()->cleanup(); //Deletes test data, here for safety in case of crashes, post unit test cleanup should do the job in normal circumstances.
	DebugInfo::getDebugInfo()->output( "main", "unitTests", "Starting setup!" );
	SetupDataStorage::getSetupDataStorage()->setup(); //Inserts fresh data to test against

	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest( registry.makeTest() );
	return runner.run();
}
