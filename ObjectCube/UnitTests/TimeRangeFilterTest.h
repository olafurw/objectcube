/*
 *  TimeRangeFilterTest.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 3.5.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;

#ifndef ObjectCube_TIME_RANGE_FILTER_TEST_
#define ObjectCube_TIME_RANGE_FILTER_TEST_

namespace ObjectCube
{
	
	class TimeRangeFilterTest : public TestFixture
	{
		CPPUNIT_TEST_SUITE( TimeRangeFilterTest );

		//MonetDB
		#ifdef OC_MONETDB
		CPPUNIT_TEST( testFilterMonetDB );
		#endif
		
		
		CPPUNIT_TEST_SUITE_END();
		
	public:
		//MonetDB
		void testFilterMonetDB();
		
	private:
		void testFilter_();
		
	};
	
}

#endif
