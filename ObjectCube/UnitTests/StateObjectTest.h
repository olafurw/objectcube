/*
 *  StateObjectTest.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 26.8.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#ifndef ObjectCube_STATE_OBJECT_TEST_
#define ObjectCube_STATE_OBJECT_TEST_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;

namespace ObjectCube
{	
	class StateObjectTest : public TestFixture
	{
		CPPUNIT_TEST_SUITE( StateObjectTest );

		//MonetDB
		#ifdef OC_MONETDB
		CPPUNIT_TEST( testGetTagsByTagSetIdMonetDB );
		#endif
		
		CPPUNIT_TEST_SUITE_END();
		
	public:
		//MonetDB
		void testGetTagsByTagSetIdMonetDB();

		
	private:
		void testGetTagsByTagSetId_( int tagId );
	};
	
}

#endif
