/*
 *  TagSetTest.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 17.2.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */


#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <string>

using namespace CppUnit;
using namespace std;

#ifndef ObjectCube_TAG_SET_TEST_
#define ObjectCube_TAG_SET_TEST_

namespace ObjectCube
{
	class TagSet;
	
	class TagSetTest : public TestFixture
	{
		CPPUNIT_TEST_SUITE( TagSetTest );

		//MonetDB
		#ifdef OC_MONETDB
		CPPUNIT_TEST( testFetchMonetDB );
		CPPUNIT_TEST( testFetchNameMonetDB );
		CPPUNIT_TEST( testAddTagMonetDB );
		CPPUNIT_TEST( testDeleteTagMonetDB );
		CPPUNIT_TEST( testGetTagIdMonetDB );
		CPPUNIT_TEST( testGetTagNameMonetDB );
		CPPUNIT_TEST( testCreateHierarchyMonetDB );
		CPPUNIT_TEST( testDeleteHierarchyMonetDB );
		CPPUNIT_TEST( testGetHierarchyMonetDB );
		CPPUNIT_TEST( testEraseMonetDB );
		CPPUNIT_TEST( testTypeAsStringMonetDB );
		#endif
		
		
		CPPUNIT_TEST_SUITE_END();
		
	public:
		//MonetDB
		void testFetchMonetDB();
		void testFetchNameMonetDB();
		void testAddTagMonetDB();
		void testDeleteTagMonetDB();
		void testGetTagIdMonetDB();
		void testGetTagNameMonetDB();
		void testCreateHierarchyMonetDB();
		void testDeleteHierarchyMonetDB();
		void testGetHierarchyMonetDB();
		void testEraseMonetDB();
		void testTypeAsStringMonetDB();
				
				
		void setUp();
		void tearDown();

	private:
		void testFetch_( string name );
		void testFetchName_();
		void testAddTag_();
		void testDeleteTag_();
		void testGetTagId_( int tagId );
		void testGetTagName_( int tagId, string name );
		void testCreateHierarchy_();
		void testDeleteHierarchy_();
		void testGetHierarchy_( int tagId );
		void testErase_( string tagSetName );
		void testTypeAsString_();
		
	};
	
}

#endif
