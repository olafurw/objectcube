/*
 *  DimensionFilterTest.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 24.4.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;

#ifndef ObjectCube_DIMENSION_FILTER_TEST_
#define ObjectCube_DIMENSION_FILTER_TEST_

namespace ObjectCube
{
	
	class DimensionFilterTest : public TestFixture
	{
		CPPUNIT_TEST_SUITE( DimensionFilterTest );
				
		//MonetDB
		#ifdef OC_MONETDB
		CPPUNIT_TEST( testFilterMonetDB );
		#endif
		
		CPPUNIT_TEST_SUITE_END();
		
	public:
		//MonetDB
		void testFilterMonetDB();
		
	private:
		void testFilter_();
		
	};
	
}

#endif
