/*
 *  NumericalRangeFilterTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 1.5.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "NumericalRangeFilterTest.h"

#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <vector>

#include "../Hub.h"
#include "../TagSet/TagSet.h"
#include "../Filters/NumericalRangeFilter.h"
#include "TestIds.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../LayerShared/Exception.h"


using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( NumericalRangeFilterTest );

//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void NumericalRangeFilterTest::testFilterMonetDB()
{
	cerr << "\nNumericalRangeFilterTest::testFilterMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testFilter_();
}


//  Common --------------------------------------------------------------------------------------------------------------------

void NumericalRangeFilterTest::testFilter_()
{
	Hub* hub = Hub::getHub(); 
	TagSet* tagSet = hub->getTagSet( TEST_NUMERICAL_TAG_SET_ID );
	
	NumericalRangeFilter filter( 0, 20, tagSet->getId() );
	
	vector<StateObject> objects = hub->getObjects({&filter});
	
	CPPUNIT_ASSERT( objects.size() );
	
	//Add a second filter to the same tag-set
	NumericalRangeFilter filter2( 20, 30, tagSet->getId() ); 
	objects = hub->getObjects({&filter,&filter2});
	
	CPPUNIT_ASSERT( objects.size() == 0 ); 	
}


