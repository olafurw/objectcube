/*
 *  DateRangeFilterTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 3.5.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "DateRangeFilterTest.h"

#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <vector>

#include "../Hub.h"
#include "../TagSet/TagSet.h"
#include "../Filters/DateRangeFilter.h"
#include "TestIds.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../LayerShared/Exception.h"


using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( DateRangeFilterTest );

//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void DateRangeFilterTest::testFilterMonetDB()
{
	cerr << "\nDateRangeFilterTest::testFilterMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testFilter_();	
}

//  Common --------------------------------------------------------------------------------------------------------------------

void DateRangeFilterTest::testFilter_()
{
	Hub* hub = Hub::getHub(); 
	TagSet* tagSet = hub->getTagSet( TEST_DATE_TAG_SET_ID );
	
	DateRangeFilter filter( 2009, tagSet->getId() );
	
	vector<StateObject> objects = hub->getObjects({&filter});
	
	CPPUNIT_ASSERT( objects.size() );
	
	//Add a second filter to the same tag-set
	DateRangeFilter filter2( 2050, tagSet->getId() ); 
	objects = hub->getObjects({&filter, &filter2});
	
	CPPUNIT_ASSERT( objects.size() == 0 );
		
	DateRangeFilter filter3( 2010, 3, 25, 2010, 2, 30, tagSet->getId() ); // Illegal date
	CPPUNIT_ASSERT_THROW( filter3.fillInAndValidateValues(), ObjectCube::Exception );
		
	DateRangeFilter filter4( tagSet->getId() ); //No values
	CPPUNIT_ASSERT_THROW( filter4.fillInAndValidateValues(), ObjectCube::Exception );  //Empty filter, error
}


