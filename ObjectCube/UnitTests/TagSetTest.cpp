/*
 *  TagSetTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 17.2.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "TagSetTest.h"

#include <cppunit/extensions/HelperMacros.h>

#include "../Hub.h"
#include "TestIds.h"
#include "../TagSet/TagSet.h"
#include "../TagSet/AlphanumericalTagSet.h"
#include "TagSetAssertionTraits.h"
#include "TagAssertionTraits.h"
#include "../Tag/AlphanumericalTag.h"
#include "../LayerShared/HubCommon.h"
#include "../LayerShared/Exception.h"
#include "../LayerShared/SharedDefinitions.h"

using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( TagSetTest );



void TagSetTest::setUp()
{

}


void TagSetTest::tearDown()
{
	
}


//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void TagSetTest::testFetchMonetDB()
{
	cerr << "\nTagSetTest::testFetchMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testFetch_( string( TEST_MONETDB_CLEANUP_PREFIX + " Alphanumerical unit test tag-set" ) );
}


void TagSetTest::testFetchNameMonetDB()
{
	cerr << "\nTagSetTest::testFetchNameMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testFetchName_();
}


void TagSetTest::testAddTagMonetDB()
{
	cerr << "\nTagSetTest::testAddTagMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testAddTag_();
}


void TagSetTest::testDeleteTagMonetDB()
{
	cerr << "\nTagSetTest::testDeleteTagMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testDeleteTag_();
}


void TagSetTest::testGetTagIdMonetDB()
{
	cerr << "\nTagSetTest::testGetTagIdMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testGetTagId_( MonetDBTestIds::TEST_ALPHANUMERICAL_TAG_ID );
}


void TagSetTest::testGetTagNameMonetDB()
{
	cerr << "\nTagSetTest::testGetTagNameMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testGetTagName_( MonetDBTestIds::TEST_ALPHANUMERICAL_TAG_ID, TEST_MONETDB_CLEANUP_PREFIX + TEST_ALPHANUMERICAL_TAG_NAME );
}


void TagSetTest::testCreateHierarchyMonetDB()
{
	cerr << "\nTagSetTest::testCreateHierarchyMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testCreateHierarchy_();
}


void TagSetTest::testDeleteHierarchyMonetDB()
{
	cerr << "\nTagSetTest::testDeleteHierarchyMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testDeleteHierarchy_();
}


void TagSetTest::testGetHierarchyMonetDB()
{
	cerr << "\nTagSetTest::testGetHierarchyMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testGetHierarchy_( MonetDBTestIds::TEST_ALPHANUMERICAL_TAG_ID );
}


void TagSetTest::testEraseMonetDB()
{
	cerr << "\nTagSetTest::testEraseMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testErase_( TEST_MONETDB_CLEANUP_PREFIX + " erase tag-set test" );
}


void TagSetTest::testTypeAsStringMonetDB()
{
	cerr << "\nTagSetTest::testTypeAsStringMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testTypeAsString_();
}


//  Common --------------------------------------------------------------------------------------------------------------------

void TagSetTest::testFetch_( string name )
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	CPPUNIT_ASSERT_EQUAL( (int)TEST_ALPHANUMERICAL_TAG_SET_ID, tagSet->getId() );
	CPPUNIT_ASSERT_EQUAL( (int)TagSet::USER, tagSet->getAccessId() );
	CPPUNIT_ASSERT_EQUAL( (int)TagSet::ALPHANUMERICAL, tagSet->getTypeId() );
	CPPUNIT_ASSERT_EQUAL( name, tagSet->getName() );
}


void TagSetTest::testFetchName_()
{
	Hub* hub = Hub::getHub();
	TagSet* tagSetById = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	TagSet* tagSetByName = hub->getTagSet( tagSetById->getName() );
	CPPUNIT_ASSERT_EQUAL( tagSetById, tagSetByName );	
}


void TagSetTest::testAddTag_()
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	AlphanumericalTag tagData( "TagSet add test tag" );
	const Tag* tag = tagSet->addTag( &tagData );
	
	const Tag* tagExpected = tagSet->getTag( tag->getId() );  //Testing getTag(id)
	CPPUNIT_ASSERT_EQUAL( *const_cast<Tag*>( tagExpected ), *const_cast<Tag*>( tag ) );
	
	if( tagSet->getTypeId() == TagSetCommon::ALPHANUMERICAL )
	{
		const AlphanumericalTag* tagExpected = dynamic_cast<AlphanumericalTagSet*>( tagSet)->getAlphanumericalTag( dynamic_cast<const AlphanumericalTag*>( tag )->getName() ); //Testing getTag(name)
		CPPUNIT_ASSERT_EQUAL( *dynamic_cast<Tag*>( const_cast<AlphanumericalTag*>( tagExpected ) ), *const_cast<Tag*>( tag ) ); 
	}
	unique_ptr<Tag> persistentTag( Tag::fetch_( tag->getId() ) ); //Persistence testing
	CPPUNIT_ASSERT_EQUAL( *persistentTag.get(), *tag );	
}


void TagSetTest::testDeleteTag_()
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	AlphanumericalTag tagData( "TagSet delete test tag" );
	const Tag* tag = tagSet->addTag( &tagData );
	
	const Tag* tagExpected = tagSet->getTag( tag->getId() );  
	CPPUNIT_ASSERT_EQUAL( const_cast<Tag*>( tagExpected ), const_cast<Tag*>( tag ) ); 
	
	const AlphanumericalTag tagCopy = *dynamic_cast<const AlphanumericalTag*>( tag );  //To use after deletion of tag
	tagSet->deleteTag( tag ); //The tag pointer gets deleted, no usage after deletion !!!
	
	CPPUNIT_ASSERT_THROW( Tag::fetch_( tagCopy.getId() ), ObjectCube::Exception );  //Should not exist
	CPPUNIT_ASSERT_THROW( tagSet->deleteTag( &tagCopy ), ObjectCube::Exception ); //Should not exists	
}


void TagSetTest::testGetTagId_( int tagId )
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	const Tag* tag = tagSet->getTag( tagId );
	unique_ptr<Tag> expected( Tag::fetch_( tagId ) );
	
	CPPUNIT_ASSERT_EQUAL( *expected, *const_cast<Tag*>( tag ) );	
}


void TagSetTest::testGetTagName_( int tagId, string name )
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	const AlphanumericalTag* tag = dynamic_cast<AlphanumericalTagSet*>( tagSet )->getAlphanumericalTag( name );
	unique_ptr<Tag> expected( Tag::fetch_( tagId ) );
	
	CPPUNIT_ASSERT_EQUAL( *expected, *dynamic_cast<Tag*>( const_cast<AlphanumericalTag*>( tag ) ) );
}


void TagSetTest::testCreateHierarchy_()
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	string tagName = "Hierarchy root test tag";
	AlphanumericalTag tagData( tagName );
	const Tag* tag = tagSet->addTag( &tagData );
	
	PersistentDimension* hierarchy = tagSet->createPersistentDimension( tag );
	
	CPPUNIT_ASSERT( hierarchy->getId() != INVALID_VALUE );
	CPPUNIT_ASSERT( hierarchy->getTagSetId() != INVALID_VALUE );
	CPPUNIT_ASSERT( hierarchy->getRoot()->getId() != INVALID_VALUE );
	CPPUNIT_ASSERT( hierarchy->getRoot()->getTagSetId() == hierarchy->getTagSetId() );
	CPPUNIT_ASSERT( hierarchy->getPersistentRoot()->getDimensionId() == hierarchy->getId() );
	CPPUNIT_ASSERT_EQUAL( 1, hierarchy->getRoot()->getLeftBorder() );
	CPPUNIT_ASSERT_EQUAL( 2, hierarchy->getRoot()->getRightBorder() );
	CPPUNIT_ASSERT( hierarchy->getPersistentRoot()->getTagId() != INVALID_VALUE );
	
	PersistentDimension* expected = tagSet->getPersistentDimension( hierarchy->getId() );
	
	CPPUNIT_ASSERT_EQUAL( expected , hierarchy );	
}


void TagSetTest::testDeleteHierarchy_()
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	PersistentDimension* hierarchy =  tagSet->getPersistentDimension("Hierarchy root test tag");
	int hierarchyId = hierarchy->getId();
	tagSet->deleteDimension( hierarchy->getId() );
	
	CPPUNIT_ASSERT_THROW( tagSet->getDimension( hierarchyId ), ObjectCube::Exception ); //Should not exist
	CPPUNIT_ASSERT_THROW( tagSet->deleteDimension( hierarchyId ), ObjectCube::Exception ); //Should not exist	
	 
}


void TagSetTest::testGetHierarchy_( int tagId )
{
	Hub* hub = Hub::getHub();
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	PersistentDimension* hierarchy = tagSet->getPersistentDimension( TEST_HIERARCHY_ID );
	
	CPPUNIT_ASSERT_EQUAL( (int)TEST_HIERARCHY_ID, hierarchy->getId() );
	CPPUNIT_ASSERT_EQUAL( (int)TEST_ALPHANUMERICAL_TAG_SET_ID, hierarchy->getTagSetId() );
	CPPUNIT_ASSERT_EQUAL( (int)TEST_HIERARCHY_NODE_ID, hierarchy->getRoot()->getId() );
	CPPUNIT_ASSERT_EQUAL( (int)TEST_HIERARCHY_ID, hierarchy->getPersistentRoot()->getDimensionId() );
	CPPUNIT_ASSERT_EQUAL( (int)TEST_ALPHANUMERICAL_TAG_SET_ID, hierarchy->getRoot()->getTagSetId() );
	CPPUNIT_ASSERT_EQUAL( 1, hierarchy->getRoot()->getLeftBorder() );
	CPPUNIT_ASSERT_EQUAL( 2, hierarchy->getRoot()->getRightBorder() );
	CPPUNIT_ASSERT_EQUAL( tagId, hierarchy->getPersistentRoot()->getTagId() );	
}


void TagSetTest::testErase_( string tagSetName )
{
	Hub* hub = Hub::getHub();
	
	AlphanumericalTagSet tagSetToCreate( tagSetName );
	TagSet* tagSet = tagSetToCreate.create();
	
	//Add tags
	AlphanumericalTag tagToCreate( "Tag to erase" );
	const Tag* tag = tagSet->addTag( &tagToCreate );
	
	//Add dimensions
	PersistentDimension* hierarchy = tagSet->createPersistentDimension( tag );
	CPPUNIT_ASSERT( 0 < hierarchy->getId() );
	
	//Add tags to objects
	Object object = Object::fetch( TEST_OBJECT_ID );
	ObjectTag objectTag( tag );
	object.addTag( objectTag );
	
	//Erase all of the above
	int tagSetId = tagSet->getId();
	tagSet->erase();
	
	
	//Try to fetch from Hub
	CPPUNIT_ASSERT_THROW( hub->getTagSet( tagSetId ), ObjectCube::Exception );
}


void TagSetTest::testTypeAsString_()
{
	TagSet* tagSet = Hub::getHub()->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	Hub::setLanguageId( LanguageCommon::ENGLISH );
	CPPUNIT_ASSERT_EQUAL( tagSet->typeAsString() , TEST_ALPHANUMERICAL_TAG_SET_TYPE_NAME_ENGLISH );
	
	Hub::setLanguageId( LanguageCommon::ICELANDIC );
	CPPUNIT_ASSERT_EQUAL( tagSet->typeAsString() , TEST_ALPHANUMERICAL_TAG_SET_TYPE_NAME_ICELANDIC );
	
	Hub::setLanguageId( LanguageCommon::ENGLISH );	
}



