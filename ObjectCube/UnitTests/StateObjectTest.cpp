/*
 *  StateObjectTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 26.8.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "StateObjectTest.h"

#include <cppunit/extensions/HelperMacros.h>

#include "../Hub.h"
#include "TestIds.h"
#include "../TagSet/TagSet.h"
#include "../LayerShared/Exception.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../Filters/DimensionFilter.h"
#include "../State/State.h"
#include "../Filters/TagFilter.h" 
#include "../Filters/NumericalRangeFilter.h"

using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( StateObjectTest );

//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void StateObjectTest::testGetTagsByTagSetIdMonetDB()
{
	cerr << "\nStateObjectTest::testGetTagsByTagSetIdMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testGetTagsByTagSetId_( MonetDBTestIds::TEST_ALPHANUMERICAL_TAG_ID );
}

//  Common --------------------------------------------------------------------------------------------------------------------

void StateObjectTest::testGetTagsByTagSetId_( int tagId )
{
	Hub* hub = Hub::getHub();
	TagSet* anTagSet = hub->getTagSet(	TEST_ALPHANUMERICAL_TAG_SET_ID );
	TagFilter tFilter( anTagSet->getTag( tagId ), anTagSet->getId() );

	vector<StateObject> objects = hub->getObjects({&tFilter});
	CPPUNIT_ASSERT(!objects.empty());
	for( vector<StateObject>::iterator itr = objects.begin(); itr != objects.end(); ++itr )
	{
		CPPUNIT_ASSERT( (*itr).getTags( TEST_ALPHANUMERICAL_TAG_SET_ID ).size() > 0 ); //We filtered by a tag from this tag-set, all objects in the results should have it		
	}
}



