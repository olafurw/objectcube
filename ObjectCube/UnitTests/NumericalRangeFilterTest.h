/*
 *  NumericalRangeFilterTest.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 1.5.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;

#ifndef ObjectCube_NUMERICAL_RANGE_FILTER_TEST_
#define ObjectCube_NUMERICAL_RANGE_FILTER_TEST_

namespace ObjectCube
{
	
	class NumericalRangeFilterTest : public TestFixture
	{
		CPPUNIT_TEST_SUITE( NumericalRangeFilterTest );
		
		//MonetDB
		#ifdef OC_MONETDB
		CPPUNIT_TEST( testFilterMonetDB );
		#endif

		CPPUNIT_TEST_SUITE_END();
		
	public:
		//MonetDB
		void testFilterMonetDB();
		
	private:
		void testFilter_();
		
	};
	
}

#endif
