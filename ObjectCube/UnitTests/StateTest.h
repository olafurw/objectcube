/*
 *  StateTest.h
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 20.5.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */
#ifndef ObjectCube_STATE_TEST_
#define ObjectCube_STATE_TEST_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <vector>
#include <string>

using namespace CppUnit;
using namespace std;

namespace ObjectCube
{	
	class MultiDimensionalView;
	
	class StateTest : public TestFixture
	{
		CPPUNIT_TEST_SUITE( StateTest );
		
		//MonetDB
		#ifdef OC_MONETDB
		CPPUNIT_TEST( testGetObjectsMonetDB );
		CPPUNIT_TEST( testGetDimensionMonetDB );
		CPPUNIT_TEST( testGetObjectIdsMonetDB );
		CPPUNIT_TEST( testGetParentNodeMonetDB );
		CPPUNIT_TEST( testGetMultiDimensionalViewMonetDB );
		#endif

		CPPUNIT_TEST_SUITE_END();
		
	public:
		//MonetDB
		void testGetObjectsMonetDB();
		void testGetDimensionMonetDB();
		void testGetObjectIdsMonetDB();
		void testGetParentNodeMonetDB();
		void testGetMultiDimensionalViewMonetDB();
		
	private:
		void testGetObjects_( int tagId );
		void testGetDimension_();
		void testGetObjectIds_();
		void testGetParentNode_();
		void testGetMultiDimensionalView_();
		
		//Helpers
		void outputMultiDimensionalView_( MultiDimensionalView* mdv );
		bool generateKey_( string& key, const vector<unsigned int>& dimensionCellCounts, vector<unsigned int>& currCounts, unsigned int& currDimension );
		
	};
	
}

#endif
