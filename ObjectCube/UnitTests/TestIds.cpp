/*
 *  TestIds.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 23.3.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */


#include "TestIds.h"

#include "../LayerShared/Parameters.h"

using namespace ObjectCube;


// TODO: rename to TestData pathish, todo use relative path and look up dynamic
string TestIds::TEST_OBJECT_PREFIX_DEFAULT  = INSTALL_PATH "/ObjectCube/UnitTest/TestData/";
string TestIds::TEST_OBJECT_PREFIX = TEST_OBJECT_PREFIX_DEFAULT;

int SQLiteTestIds::TEST_ALPHANUMERICAL_TAG_ID = -1;
int SQLiteTestIds::TEST_NUMERICAL_TAG_ID = -1;
int SQLiteTestIds::TEST_TIME_TAG_ID = -1;
int SQLiteTestIds::TEST_DATE_TAG_ID = -1;

int MonetDBTestIds::TEST_ALPHANUMERICAL_TAG_ID = -1;
int MonetDBTestIds::TEST_NUMERICAL_TAG_ID = -1;
int MonetDBTestIds::TEST_TIME_TAG_ID = -1;
int MonetDBTestIds::TEST_DATE_TAG_ID = -1;

int OracleTestIds::TEST_ALPHANUMERICAL_TAG_ID = -1;
int OracleTestIds::TEST_NUMERICAL_TAG_ID = -1;
int OracleTestIds::TEST_TIME_TAG_ID = -1;
int OracleTestIds::TEST_DATE_TAG_ID = -1;

namespace ObjectCube
{

int const TEST_NUMERICAL_TAG_VALUE = 12;

//Time
int const TEST_TIME_TAG_VALUE_HOURS = 14;
int const TEST_TIME_TAG_VALUE_MINUTES = 37;
int const TEST_TIME_TAG_VALUE_SECONDS = 48;
int const TEST_TIME_TAG_VALUE_MILLISECONDS = 942;

//Date
int const TEST_DATE_TAG_VALUE_YEAR = 2010;
int const TEST_DATE_TAG_VALUE_MONTH = 04;
int const TEST_DATE_TAG_VALUE_DAY = 29;

}


void TestIds::updateObjectPrefix()
{
	if( Parameters::getParameters()->contains( UNIT_TEST_OBJECT_PATH_PARAMETER ) )
	{
		TEST_OBJECT_PREFIX = Parameters::getParameters()->getValue( UNIT_TEST_OBJECT_PATH_PARAMETER );
	}
}




