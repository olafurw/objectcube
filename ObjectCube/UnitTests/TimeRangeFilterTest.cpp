/*
 *  TimeRangeFilterTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 3.5.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "TimeRangeFilterTest.h"

#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <vector>

#include "../Hub.h"
#include "../TagSet/TagSet.h"
#include "../Filters/TimeRangeFilter.h"
#include "TestIds.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../LayerShared/Exception.h"


using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( TimeRangeFilterTest );


//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void TimeRangeFilterTest::testFilterMonetDB()
{
	cerr << "\nTimeRangeFilterTest::testFilterMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );
	
	testFilter_();	
}

//  Common --------------------------------------------------------------------------------------------------------------------

void TimeRangeFilterTest::testFilter_()
{
	Hub* hub = Hub::getHub(); 
	TagSet* tagSet = hub->getTagSet( TEST_TIME_TAG_SET_ID );
	
	TimeRangeFilter filter( 11, 14, tagSet->getId() );
	
	vector<StateObject> objects = hub->getObjects({&filter});
	
	CPPUNIT_ASSERT( objects.size() );
	
	//Add a second filter to the same tag-set
	TimeRangeFilter filter2( 15, 18, tagSet->getId() ); 
	objects = hub->getObjects({&filter,&filter2});
	
	CPPUNIT_ASSERT( objects.size() == 0 ); 
		
	TimeRangeFilter filter3( 12, 24, tagSet->getId() ); // Illegal time
	CPPUNIT_ASSERT_THROW( filter3.fillInAndValidateValues() , ObjectCube::Exception );

	TimeRangeFilter filter4( tagSet->getId() ); //No values
	CPPUNIT_ASSERT_THROW( filter4.fillInAndValidateValues(), ObjectCube::Exception );  //Empty filter, error
}



