/*
 *  DimensionFilterTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 24.4.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "DimensionFilterTest.h"

#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <vector>

#include "../Hub.h"
#include "../TagSet/AlphanumericalTagSet.h"
#include "../Tag/AlphanumericalTag.h"
#include "../Filters/DimensionFilter.h"
#include "../Hierarchy/PersistentDimension.h"
#include "../Hierarchy/PersistentHierarchyNode.h"
#include "TestIds.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../LayerShared/Exception.h"


using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( DimensionFilterTest );





//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void DimensionFilterTest::testFilterMonetDB()
{
	cerr << "\nDimensionFilterTest::testFilterMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testFilter_();
}


//  Common --------------------------------------------------------------------------------------------------------------------

void DimensionFilterTest::testFilter_()
{
	Hub* hub = Hub::getHub(); 
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
 	PersistentDimension* dimension = tagSet->getPersistentDimension( TEST_HIERARCHY_ID );
	PersistentHierarchyNode* node = dimension->getPersistentRoot();
	DimensionFilter filter( *node, tagSet->getId() );
	vector<StateObject> objects = hub->getObjects({&filter});
	
	CPPUNIT_ASSERT( objects.size() );

	//Time

	TagSet* tTagSet = hub->getTagSet( TEST_TIME_TAG_SET_ID );
	
	const VirtualDimension* vDim = tTagSet->getVirtualDimension( "Hour" );
	const VirtualHierarchyNode* vNode = vDim->getVirtualRoot();
	DimensionFilter vFilter( *vNode, tTagSet->getId() );

	objects = hub->getObjects({&vFilter});
	CPPUNIT_ASSERT( objects.size() );
	
	//Date
	
	TagSet* dTagSet = hub->getTagSet( TEST_DATE_TAG_SET_ID );
	
	const VirtualDimension* vdDim = dTagSet->getVirtualDimension( "Month" );
	const VirtualHierarchyNode* vdNode = vdDim->getVirtualRoot();
	DimensionFilter vdFilter( *vdNode, dTagSet->getId() );
	
	objects = hub->getObjects({&vdFilter});
	CPPUNIT_ASSERT( objects.size() );
	
	//Day of week (recurring)
	
	const VirtualDimension* vdDim2 = dTagSet->getVirtualDimension( "Day of Week" );
	const VirtualHierarchyNode* vdNode2 = vdDim2->getVirtualRoot();
	DimensionFilter vdFilter2( *vdNode2, dTagSet->getId() );
	
	objects = hub->getObjects({&vdFilter2});
	CPPUNIT_ASSERT( objects.size() );	
}


