/*
 *  StateTest.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 20.5.2010.
 *  Copyright 2010 Reykjavik University. All rights reserved.
 *
 */

#include "StateTest.h"

#include <cppunit/extensions/HelperMacros.h>

#include "../Hub.h"
#include "TestIds.h"
#include "../TagSet/TagSet.h"
#include "../Tag/Tag.h"
#include "../LayerShared/Exception.h"
#include "../LayerShared/SharedDefinitions.h"
#include "../Filters/DimensionFilter.h"
#include "../State/State.h"
#include "../Filters/TagFilter.h" 
#include "../Filters/NumericalRangeFilter.h"

using namespace ObjectCube;

CPPUNIT_TEST_SUITE_REGISTRATION( StateTest );


//  MonetDB tests --------------------------------------------------------------------------------------------------------------------

void StateTest::testGetObjectsMonetDB()
{
	cerr << "\nStateTest::testGetObjectsMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testGetObjects_( MonetDBTestIds::TEST_ALPHANUMERICAL_TAG_ID );
}

void StateTest::testGetDimensionMonetDB()
{
	cerr << "\nStateTest::testGetDimensionMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testGetDimension_();
}


void StateTest::testGetObjectIdsMonetDB()
{
	cerr << "\nStateTest::testGetObjectIdsMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testGetObjectIds_();
}


void StateTest::testGetParentNodeMonetDB()
{
	cerr << "\nStateTest::testGetParentNodeMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testGetParentNode_();
}

void StateTest::testGetMultiDimensionalViewMonetDB()
{
	cerr << "\nStateTest::testGetMultiDimensionalViewMonetDB = ";
	Hub::setDataAccessType( Hub::MONETDB );

	testGetMultiDimensionalView_();
}

//  Common --------------------------------------------------------------------------------------------------------------------

void StateTest::testGetObjects_( int tagId )
{
	Hub* hub = Hub::getHub();

	State state = hub->getState({});
	vector<StateObject> objects = state.getObjects();
	CPPUNIT_ASSERT( objects.size() > 0 );
	for( vector<StateObject>::iterator itr = objects.begin(); itr != objects.end(); ++itr )
	{
		//cout << "1. id: " << (*itr).getId() << "  Name. " << (*itr).getName() << endl;
		CPPUNIT_ASSERT( (*itr).getName().length() > 0 );
	}

	TagSet* anTagSet = hub->getTagSet(	TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	TagFilter tagFilter( anTagSet->getTag( tagId ), anTagSet->getId() );
	
	state = hub->getState({&tagFilter});
	objects = state.getObjects();
	for( vector<StateObject>::iterator itr = objects.begin(); itr != objects.end(); ++itr )
	{
		//cout << "2. id: " << (*itr).getId() << "  Name. " << (*itr).getName() << endl;
		CPPUNIT_ASSERT( (*itr).getName().length() > 0 );
	}	
}

void StateTest::testGetDimension_()
{
	Hub* hub = Hub::getHub();

	TagSet* dTagSet = hub->getTagSet( TEST_DATE_TAG_SET_ID );
	const VirtualDimension* vdDim = dTagSet->getVirtualDimension( "Month" );
	const HierarchyNode* vdNode = vdDim->getVirtualRoot()->getBranch( 4 /*April*/ );
	DimensionFilter vdFilter( *vdNode, dTagSet->getId() );
	
	State state = hub->getState({&vdFilter});
	const StateDimension stateDim = state.getDimension( vdDim->getId() );
	
	CPPUNIT_ASSERT_EQUAL( stateDim.getTagSetId(), dTagSet->getId() );
	CPPUNIT_ASSERT_EQUAL( stateDim.getTypeId(), vdDim->getTypeId() );
	
}


void StateTest::testGetObjectIds_()
{
	Hub* hub = Hub::getHub();
	
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	PersistentDimension* dimension = tagSet->getPersistentDimension( TEST_HIERARCHY_ID + 1 );
	HierarchyNode* root = dimension->getRoot();
	DimensionFilter dFilter( *root, tagSet->getId() );
	
	State state = hub->getState({&dFilter});
	const StateDimension stateDim = state.getDimension( dimension->getId() );
	
	const StateDimensionNode stateDimRoot = stateDim.getRoot();
	CPPUNIT_ASSERT_EQUAL( 3, (int) stateDimRoot.getObjectIds().size() );

}


void StateTest::testGetParentNode_()
{
	Hub* hub = Hub::getHub();
		
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	PersistentDimension* dimension = tagSet->getPersistentDimension( TEST_HIERARCHY_ID + 1 );
	HierarchyNode* root = dimension->getRoot();
	DimensionFilter dFilter( *root, tagSet->getId() );
	
	State state = hub->getState({&dFilter});
	const StateDimension stateDim = state.getDimension( dimension->getId() );
	
	const StateDimensionNode& stateDimRoot = stateDim.getRoot();
	CPPUNIT_ASSERT( stateDimRoot.getBranches().size() > 0 );
	
	StateDimensionNode stateDimChild = stateDimRoot.getBranches()[0];
	
	const StateDimensionNode stateDimParent = stateDim.getParentNode( stateDimChild.getNode()->getId() );
	
	CPPUNIT_ASSERT_EQUAL( stateDimRoot.getNode()->getId(), stateDimParent.getNode()->getId()  );
}


void StateTest::testGetMultiDimensionalView_()
{
	Hub* hub = Hub::getHub();
		
	TagSet* tagSet = hub->getTagSet( TEST_ALPHANUMERICAL_TAG_SET_ID );
	
	State state = hub->getState({});
	
	//Test 1-dimensional with tag-set
	/*
	MultiDimensionalView::setAxis( MultiDimensionalView::AXIS_1, tagSet );
	const MultiDimensionalView mdv = state.getMultiDimensionalView();
	outputMultiDimensionalView_( const_cast<MultiDimensionalView*>( &mdv ) );
	*/
	//Assert 
	
	/*
	//Test 2-dimensional with tag-sets
	MultiDimensionalView::setAxis( MultiDimensionalView::AXIS_2, tagSet ); //Diagonal, same tag-set
	const MultiDimensionalView mdv2 = state.getMultiDimensionalView();
	outputMultiDimensionalView_( const_cast<MultiDimensionalView*>( &mdv2 ) );
	*/
	//Test 1-dimensional with hierarchy
	PersistentDimension* dimension = tagSet->getPersistentDimension( TEST_HIERARCHY_ID + 1 );
	HierarchyNode* root = dimension->getRoot();
	DimensionFilter dFilter( *root, tagSet->getId() );
	
	state = hub->getState({&dFilter});
//	MultiDimensionalView::clearAxes();
	const StateDimension stateDim = state.getDimension( dimension->getId() );
//	MultiDimensionalView::setAxis( MultiDimensionalView::AXIS_1, stateDim.getRoot(), 0 );
	
	const MultiDimensionalView mdv3 = state.getMultiDimensionalView( false );
	outputMultiDimensionalView_( const_cast<MultiDimensionalView*>( &mdv3 ) );
	
	/*
	PersistentDimension* dimension = tagSet->getPersistentDimension( TEST_HIERARCHY_ID + 1 );
	HierarchyNode* root = dimension->getRoot();
	DimensionFilter dFilter( *root, tagSet->getId() );
	State::addFilter( &dFilter );
	CPPUNIT_ASSERT( State::getFilters().size() == 1 );
	
	State state = hub->getState();
	const StateDimension stateDim = state.getDimension( dimension->getId() );
	
	CPPUNIT_ASSERT_THROW( state.getMultiDimensionalView(), ObjectCube::Exception);
	
	MultiDimensionalView::setXAxis( &stateDim.getRoot() );
	
	const MultiDimensionalView mdv = state.getMultiDimensionalView();
	//Check contents of cells ?
	
	CPPUNIT_ASSERT_EQUAL( 3, mdv.getUniqueObjectCount() );
	CPPUNIT_ASSERT_EQUAL( 4, mdv.getObjectCount() );
	CPPUNIT_ASSERT_EQUAL( 4, mdv.getXAxisCellCount() );
	CPPUNIT_ASSERT_EQUAL( 1, mdv.getYAxisCellCount() );
	CPPUNIT_ASSERT_EQUAL( 1, mdv.getZAxisCellCount() );
	
	outputMultiDimensionalView_( &mdv );
	
	//Add Y axis, getMDV, check contents
	MultiDimensionalView::setYAxis( &stateDim.getRoot() ); //Diagonal
	const MultiDimensionalView mdv2 = state.getMultiDimensionalView();
	CPPUNIT_ASSERT_EQUAL( 3, mdv2.getUniqueObjectCount() );
	CPPUNIT_ASSERT_EQUAL( 6, mdv2.getObjectCount() );
	CPPUNIT_ASSERT_EQUAL( 4, mdv2.getXAxisCellCount() );
	CPPUNIT_ASSERT_EQUAL( 4, mdv2.getYAxisCellCount() );
	CPPUNIT_ASSERT_EQUAL( 1, mdv2.getZAxisCellCount() );
	
	
	outputMultiDimensionalView_( &mdv2 );
	//Add Z axis, getMDV, check contents
	 */
}


bool StateTest::generateKey_( string& key, const vector<unsigned int>& dimensionCellCounts, vector<unsigned int>& currCounts, unsigned int& currDimension )
{
	//Think of this as an arbitrary number of nested loops.
	//For anything but the outer most "loop" we reset counters and recurse.
	
	// for three dimensions, and cell counts [ 3, 2, 2 ] we should get:
	//	0:0:0
	//	0:0:1
	//	0:1:0
	//	0:1:1
	//	1:0:0
	
	//When ever we raise a counter we reset all counters that come after it in the vector
	
	if( ! dimensionCellCounts.size() )
	{
		return false;
	}
	
	if( !( currCounts[ currDimension ] < dimensionCellCounts[ currDimension ] ) ) //Reset or return false
	{
		if( currDimension == 0 ) //We have processed all keys
		{
			return false;
		}
		//We have not processed all and need to raise and reset counters

		//reset the current dimension, and all after it
		for( size_t resetIndex = currDimension; resetIndex < currCounts.size(); ++resetIndex )
		{
			currCounts[ resetIndex ] = 0;  
		}
		currCounts[ --currDimension ]++; //move the current dimension one up and raise it by one.
				
		return generateKey_( key, dimensionCellCounts, currCounts, currDimension );
	}
	
	//Normal processing
	currDimension = currCounts.size() - 1;
	
	stringstream keyStream;
	for( size_t dimIndex = 0; dimIndex < currCounts.size(); ++dimIndex )
	{
		if( dimIndex > 0 )
		{
			keyStream << ":";
		}
		keyStream << currCounts[ dimIndex ];
	}
	currCounts[ currDimension ]++;
	
	key = keyStream.str();
	return true;
}


void StateTest::outputMultiDimensionalView_( MultiDimensionalView* mdv )
{
	cout << "Broken pending fix" << endl << endl;
//	cout << "Dimension count: " << mdv->numberOfDimensions() << endl << endl;
	
//	vector<unsigned int> dimensionCellCounts;
//	for( size_t cellCount = 0; cellCount < mdv->numberOfDimensions(); ++cellCount )
//	{
//		unsigned int count = mdv->getAxisCellCount( cellCount );
//		cout << "Cell count for dimension: " << cellCount << " count: " << count << endl;
//		dimensionCellCounts.push_back( count );
//	}
	
//	vector<unsigned int> currDimensionCounts( dimensionCellCounts.size(), 0 );
//	unsigned int currDimension = dimensionCellCounts.size() - 1;
//	string key;

//	auto cells = mdv->getCells();
//	while( generateKey_( key, dimensionCellCounts, currDimensionCounts, currDimension ) )
//	{
//		//Always the same !
//		if( !cells.count( key ) )
//		{
//			cout << "No cell for key: " << key << " was found !!!!!" << endl << endl;
//		}
//		Cell tempCell = cells[ key ];
//		cout << "\nkey: " << key << endl;
//		cout << "Objects in cell: " << tempCell.getObjects().size() << endl;
//		for( int d = 0; d < mdv->numberOfDimensions(); ++d )
//		{
//			string label = tempCell.getAxisLabel( d );
//			cout << "\tAxis: " << d << " dimension tag name: " << label << endl;
//		}
//		const vector<StateObject> objects = tempCell.getObjects();
//		for( vector<StateObject>::const_iterator oItr = objects.begin(); oItr != objects.end(); ++oItr )
//		{
//			cout << "\n\tObject name: " << (*oItr).getName() << endl;
//			const vector<StateTag> objectTags = (*oItr).getTags();
//			for( vector<StateTag>::const_iterator tItr = objectTags.begin(); tItr != objectTags.end(); ++tItr )
//			{
//				cout << "\t\tObject tag name: " << (*tItr).getTag()->valueAsString() << endl;
//			}
//		}
//	}
	
//	cout << endl << "Unique objects: " << mdv->getUniqueObjectCount() << endl;
//	cout << "Objects total: " << mdv->getObjectCount() << endl;
}




















