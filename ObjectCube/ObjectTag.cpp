/***********************************************************************
 ** Reykjavik University
 ** Grímur Tómasson
 ** Copyright (c) 2010. All rights reserved.
 **********************************************************************/

#include "ObjectTag.h"

#include "Hub.h"

#include "Tag/Tag.h"
#include "TagSet/TagSet.h"
#include "LayerShared/Exception.h"
#include "LayerShared/SharedDefinitions.h"

#include "DataAccess/Factories/ObjectTagDataAccessFactory.h"
#include "DataAccess/Root/ObjectTagDataAccess.h"
#include "Converters/ObjectTagConverter.h"

#include "Plugin/PluginServer.h"

using namespace ObjectCube;



ObjectTag::ObjectTag( Tag* tag )
:	tag_( 0 ),
	objectId_( INVALID_VALUE ),
	confirmed_( true ),
	boundingBox_()
{
	tag_ = tag;
}


ObjectTag::ObjectTag( const Tag* tag )
:	tag_( 0 ),
	objectId_( INVALID_VALUE ),
	confirmed_( true ),
	boundingBox_()
{
	tag_ = const_cast<Tag*>( tag );
}


ObjectTag::ObjectTag( Tag* tag, BoundingBox boundingBox )
:	tag_( 0 ),
	objectId_( INVALID_VALUE ),
	confirmed_( true ),
	boundingBox_( boundingBox )
{
	tag_ = tag;
	//boundingBox_ = boundingBox;
}


ObjectTag::ObjectTag( const Tag* tag, BoundingBox boundingBox )
:	tag_( 0 ),
	objectId_( INVALID_VALUE ),
	confirmed_( true ),
	boundingBox_( boundingBox)
{
	tag_ = const_cast<Tag*>( tag );
	//boundingBox_ = boundingBox_;
}


ObjectTag::ObjectTag( const ObjectTag& objectTag )
:	tag_( 0 ),
	objectId_( INVALID_VALUE ),
	confirmed_(true),
	boundingBox_()
{
	tag_ = objectTag.tag_;
	objectId_ = objectTag.objectId_;
	confirmed_ = objectTag.confirmed_;
	boundingBox_ = objectTag.boundingBox_;
}

ObjectTag::ObjectTag( ObjectTag&& objectTag )
{
	tag_ = objectTag.tag_;
	objectId_ = objectTag.objectId_;
	confirmed_ = objectTag.confirmed_;
	boundingBox_ = std::move(objectTag.boundingBox_);

	objectTag.tag_ = nullptr;
	objectTag.objectId_ = 0;
	objectTag.confirmed_ = false;
}



ObjectTag& ObjectTag::operator=( const ObjectTag& objectTag )
{
	if( this != &objectTag )
	{
		tag_ = objectTag.tag_;
		objectId_ = objectTag.objectId_;
		confirmed_ = objectTag.confirmed_;
		boundingBox_ = objectTag.boundingBox_;
	}

	return *this;
}

ObjectTag& ObjectTag::operator=( ObjectTag&& objectTag )
{
	if( this != &objectTag )
	{
		tag_ = objectTag.tag_;
		objectId_ = objectTag.objectId_;
		confirmed_ = objectTag.confirmed_;
		boundingBox_ = std::move(objectTag.boundingBox_);

		objectTag.tag_ = nullptr;
		objectTag.objectId_ = 0;
		objectTag.confirmed_ = false;
	}

	return *this;
}


ObjectTag::~ObjectTag()
{
	
}



BoundingBox ObjectTag::getBoundingBox() const 
{
	/*
	if( !boundingBox_ )
	{
		throw Exception( "ObjectTag::getBoundingBox", "Attempt to get a non-existing bounding box!" );
	}
	*/
	return boundingBox_;
}

/*
bool ObjectTag::hasBoundingBox() const
{
	return boundingBox_ == 0 ? false : true;
}
*/


bool ObjectTag::operator==( const ObjectTag& x ) const
{
	
	return	getTag()->getId() == x.getTag()->getId() && boundingBox_ == x.getBoundingBox() && confirmed_ == x.getConfirmed();
}


bool ObjectTag::operator!=( ObjectTag const& x ) const
{
	return !( getTag()->getId() == x.getTag()->getId() );
}


bool ObjectTag::operator < ( const ObjectTag& x ) const
{ 
	return getTag()->getId() < x.getTag()->getId(); 
}


bool ObjectTag::operator()( const ObjectTag& x ) const
{
	return operator==( x );
}


void ObjectTag::confirmTagging()
{
	setConfirmed( true );
	unique_ptr<ObjectTagDataAccess> dataAccess( ObjectTagConverter::logicToDataAccess( *this ) );
	dataAccess->modify();
	
	//Let plug-ins know
	string tagSetName = Hub::getHub()->getTagSet( getTag()->getTagSetId() )->getName();
	string tagValue = getTag()->valueAsString();

	Hub* hub = Hub::getHub();
	hub->confirmTagging( getObjectId_(), tagSetName, getBoundingBox(), tagValue );
}

