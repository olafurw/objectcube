/*
 *  Filter.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 6.11.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */

#include "Filter.h"

#include "../LayerShared/Exception.h"
#include "../Language.h"

using namespace ObjectCube;



int Filter::idCounter_ = 1;



Filter::Filter()
{
	init_();
}


Filter::Filter( int tagSetId )
{
	init_();
	setTagSetId_( tagSetId );
}


Filter::~Filter()
{

}


void Filter::init_()
{
	setId_( idCounter_++ );
	setTypeId_( NOT_SET );
	setTagSetId_( NOT_SET );
	isNull_ = true;
}


void Filter::assignValues_( const Filter& filter )
{
	setId_( filter.getId() );
	setTypeId_( filter.getTypeId() );
	setTagSetId_( filter.getTagSetId() );
}


Filter& Filter::operator=( const Filter& filter )
{
	*this = filter;
	return *this;
}


const string Filter::typeAsString() const
{
	return Language::asString( "FilterType", getTypeId() );
}


const string Filter::typeAsString( int filterTypeId )
{
	return Language::asString( "FilterType", filterTypeId );
}


bool Filter::operator==( const Filter& filter )
{
	return getId() == filter.getId();
}


bool Filter::operator==( const Filter* /*const*/ filter )
{
	return getId() == filter->getId();
}


void Filter::fillInAndValidateValues()
{

}

