/*
 *  TagFilter.cpp
 *  ObjectCube
 *
 *  Created by Grímur Tómasson on 6.11.2009.
 *  Copyright 2009 Reykjavik University. All rights reserved.
 *
 */

#include "TagFilter.h"

#include <sstream>

#include "../Tag/Tag.h"
#include "../LayerShared/SharedDefinitions.h"


using namespace ObjectCube;



TagFilter::TagFilter()
{
	init_();
}


TagFilter::TagFilter( const Tag* /*const*/ tag, int tagSetId )
: Filter( tagSetId )
{
	init_();
	setTagId_( tag->getId() );
}


TagFilter::~TagFilter()
{

}


void TagFilter::init_()
{
	setTypeId_( Filter::TAG_FILTER );
	tagId_ = 0;  //Initialization, setTagId_ sets isNull to false
}


void TagFilter::setTagId_( int tagId )
{ 
	tagId_ = tagId;
	setNullStatus_( false );
}


Filter& TagFilter::operator=( const Filter& filter )
{
	assignValues_( filter );
	setTagId_( dynamic_cast<const TagFilter*>( &filter )->getTagId() );
	return *this;
}


